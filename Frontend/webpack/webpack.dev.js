const path = require('path');
const merge = require('webpack-merge');
const common = require('./webpack.common.js');


module.exports = (env, args) => {

    return merge(
        common(args.mode),
        {
            mode: "development",
            devServer: {
                // This is where webpack-dev-server serves your bundle
                // which is created in memory.
                // To use the in-memory bundle,
                // your <script> 'src' should point to the bundle
                // prefixed with the 'publicPath', e.g.:
                //   <script src='http://localhost:9001/assets/bundle.js'>
                //   </script>
                publicPath: '/static/build/',
                contentBase: [
                    path.resolve(__dirname, '../src/'),
                ],
                watchContentBase: true,
                compress: true,
                host: '127.0.0.1',
                port: 9001
            },
        }
    )
}
