const path = require('path');
const MinifyPlugin = require('babel-minify-webpack-plugin');
const MiniCSSExtract = require('mini-css-extract-plugin');
const OptimizeCSSAssetsPlugin = require('optimize-css-assets-webpack-plugin');
const webpack = require('webpack')

// const CleanWebpackPlugin = require('clean-webpack-plugin');

const basePath = __dirname;
const distPath = '../src/static/build';
const distImages = '../../../src/static/images/';

// the path(s) that should be cleaned
let pathsToClean = ['build'];

// the clean options to use
// let cleanOptions = {
//     root: path.join(basePath, '../src/static/')
// };

module.exports = mode => {
    var plugins = [
        new MinifyPlugin(),
        new MiniCSSExtract({
            filename: '[name].min.css'
        }),
        new webpack.DefinePlugin({
            PRODUCTION: (mode == 'production') ? true : false
        })
    ];

    // if (mode == 'production') {
    //     plugins.push(new CleanWebpackPlugin(pathsToClean, cleanOptions));
    // }

    return {
        resolve: {
            extensions: ['.js']
        },
        entry: {

            core_client_list: './src/templates/core/client/list/client_list.js',
            core_client_new: './src/templates/core/client/new/client_new.js',
            core_client_edit: './src/templates/core/client/edit/client_edit.js',
            core_client_import: './src/templates/core/client/import/client_import.js',

            core_contactclient_list: './src/templates/core/contactclient/list/contactclient_list.js',
            core_contactclient_new: './src/templates/core/contactclient/new/contactclient_new.js',
            core_contactclient_edit: './src/templates/core/contactclient/edit/contactclient_edit.js',
            core_contactclient_import: './src/templates/core/contactclient/import/contactclient_import.js',

            core_contactsupplier_list: './src/templates/core/contactsupplier/list/contactsupplier_list.js',
            core_contactsupplier_new: './src/templates/core/contactsupplier/new/contactsupplier_new.js',
            core_contactsupplier_edit: './src/templates/core/contactsupplier/edit/contactsupplier_edit.js',
            core_contactsupplier_import: './src/templates/core/contactsupplier/import/contactsupplier_import.js',

            core_udm_list: './src/templates/core/udm/list/udm_list.js',
            core_udm_new: './src/templates/core/udm/new/udm_new.js',
            core_udm_edit: './src/templates/core/udm/edit/udm_edit.js',

            core_satproduct_list: './src/templates/core/satproduct/list/satproduct_list.js',
            core_satproduct_new: './src/templates/core/satproduct/new/satproduct_new.js',
            core_satproduct_edit: './src/templates/core/satproduct/edit/satproduct_edit.js',

            core_item_list: './src/templates/core/item/list/item_list.js',
            core_item_new: './src/templates/core/item/new/item_new.js',
            core_item_edit: './src/templates/core/item/edit/item_edit.js',
            core_item_import: './src/templates/core/item/import/item_import.js',

            core_stockitem_list: './src/templates/core/stockitem/list/stockitem_list.js',
            core_stockitem_retrieve: './src/templates/core/stockitem/retrieve/stockitem_retrieve.js',

            core_supplier_list: './src/templates/core/supplier/list/supplier_list.js',
            core_supplier_new: './src/templates/core/supplier/new/supplier_new.js',
            core_supplier_edit: './src/templates/core/supplier/edit/supplier_edit.js',
            core_supplier_import: './src/templates/core/supplier/import/supplier_import.js',

            core_warehouse_list: './src/templates/core/warehouse/list/warehouse_list.js',
            core_warehouse_new: './src/templates/core/warehouse/new/warehouse_new.js',
            core_warehouse_edit: './src/templates/core/warehouse/edit/warehouse_edit.js',
            core_warehouse_import: './src/templates/core/warehouse/import/warehouse_import.js',

            // // ecommerce_terms: './src/templates/ecommerce/terms/ecommerce_terms.js',

            // home_activation_confirm: './src/templates/home/activation/confirm/activation_confirm.js',
            // home_activation_done: './src/templates/home/activation/done/activation_done.js',

            home_dashboard: './src/templates/home/dashboard/home_dashboard.js',
            home_errors_404: './src/templates/home/errors/404/404.js',
            home_login: './src/templates/home/login/home_login.js',
            home_password_done: './src/templates/home/password/done/password_done.js',
            home_password_forgot: './src/templates/home/password/forgot/password_forgot.js',
            home_password_request: './src/templates/home/password/request/password_request.js',
            home_password_reset: './src/templates/home/password/reset/password_reset.js',
            home_password_start: './src/templates/home/password/start/password_start.js',
            home_profile_detail: './src/templates/home/profile/detail/profile_detail.js',
            home_profile_password: './src/templates/home/profile/password/profile_password.js',
            home_settings: './src/templates/home/settings/home_settings.js',
            terms: './src/templates/home/terms/terms.js',
            questions: './src/templates/home/questions/questions.js',
            privacity: './src/templates/home/privacity/privacity.js',

            inventory_docinitialentry_list: './src/templates/inventory/docinitialentry/list/docinitialentry_list.js',
            inventory_docinitialentry_new: './src/templates/inventory/docinitialentry/new/docinitialentry_new.js',
            inventory_docinitialentry_edit: './src/templates/inventory/docinitialentry/edit/docinitialentry_edit.js',
            inventory_docinitialentry_retrieve: './src/templates/inventory/docinitialentry/retrieve/docinitialentry_retrieve.js',


            inventory_docadjustment_new: './src/templates/inventory/docadjustment/new/docadjustment_new.js',
            inventory_docadjustment_edit: './src/templates/inventory/docadjustment/edit/docadjustment_edit.js',
            inventory_docadjustment_list: './src/templates/inventory/docadjustment/list/docadjustment_list.js',
            inventory_docadjustment_retrieve: './src/templates/inventory/docadjustment/retrieve/docadjustment_retrieve.js',

            inventory_shrinkage_list: './src/templates/inventory/shrinkage/list/shrinkage_list.js',
            inventory_shrinkage_new: './src/templates/inventory/shrinkage/new/shrinkage_new.js',
            inventory_shrinkage_edit: './src/templates/inventory/shrinkage/edit/shrinkage_edit.js',
            inventory_shrinkage_retrieve: './src/templates/inventory/shrinkage/retrieve/shrinkage_retrieve.js',

            inventory_docreception_list: './src/templates/inventory/docreception/list/docreception_list.js',
            inventory_docreception_new: './src/templates/inventory/docreception/new/docreception_new.js',
            inventory_docreception_edit: './src/templates/inventory/docreception/edit/docreception_edit.js',
            inventory_docreception_retrieve: './src/templates/inventory/docreception/retrieve/docreception_retrieve.js',

            inventory_docdispatch_new: './src/templates/inventory/docdispatch/new/docdispatch_new.js',
            inventory_docdispatch_edit: './src/templates/inventory/docdispatch/edit/docdispatch_edit.js',
            inventory_docdispatch_list: './src/templates/inventory/docdispatch/list/docdispatch_list.js',
            inventory_docdispatch_retrieve: './src/templates/inventory/docdispatch/retrieve/docdispatch_retrieve.js',

            procurement_requisition_list: './src/templates/procurement/requisition/list/requisition_list.js',
            procurement_requisition_new: './src/templates/procurement/requisition/new/requisition_new.js',
            procurement_requisition_edit: './src/templates/procurement/requisition/edit/requisition_edit.js',

            procurement_purchaseorder_list: './src/templates/procurement/purchaseorder/list/purchaseorder_list.js',
            procurement_purchaseorder_new: './src/templates/procurement/purchaseorder/new/purchaseorder_new.js',
            procurement_purchaseorder_edit: './src/templates/procurement/purchaseorder/edit/purchaseorder_edit.js',
            procurement_purchaseorder_retrieve: './src/templates/procurement/purchaseorder/retrieve/purchaseorder_retrieve.js',

            procurement_expense_list: './src/templates/procurement/expense/list/expense_list.js',
            procurement_expense_new: './src/templates/procurement/expense/new/expense_new.js',
            procurement_expense_edit: './src/templates/procurement/expense/edit/expense_edit.js',
            procurement_expense_retrieve: './src/templates/procurement/expense/retrieve/expense_retrieve.js',
            procurement_expense_import: './src/templates/procurement/expense/import/expense_import.js',


            sales_quotation_list: './src/templates/sales/quotation/list/quotation_list.js',
            sales_quotation_new: './src/templates/sales/quotation/new/quotation_new.js',
            sales_quotation_edit: './src/templates/sales/quotation/edit/quotation_edit.js',

            sales_saleorder_new: './src/templates/sales/saleorder/new/saleorder_new.js',
            sales_saleorder_edit: './src/templates/sales/saleorder/edit/saleorder_edit.js',
            sales_saleorder_list: './src/templates/sales/saleorder/list/saleorder_list.js',
            sales_saleorder_retrieve: './src/templates/sales/saleorder/retrieve/saleorder_retrieve.js',
            sales_saleorder_invoice: './src/templates/sales/saleorder/invoice/saleorder_invoice.js',

            sales_payment_list: './src/templates/sales/payment/list/payment_list.js',
            sales_payment_new: './src/templates/sales/payment/new/payment_new.js',
            sales_payment_edit: './src/templates/sales/payment/edit/payment_edit.js',
            sales_payment_retrieve: './src/templates/sales/payment/retrieve/payment_retrieve.js',
            sales_payment_import:'./src/templates/sales/payment/import/payment_import.js',


            security_dashboard: './src/templates/security/dashboard/security_dashboard.js',

            // security_user_list: './src/templates/security/user/list/user_list.js',
            // security_user_new: './src/templates/security/user/new/user_new.js',
            // security_user_edit: './src/templates/security/user/edit/user_edit.js',
            // security_user_password: './src/templates/security/user/password/user_password.js',
            // security_user_permissions: './src/templates/security/user/permissions/user_permissions.js',

            // security_companyclass_list: './src/templates/security/companyclass/list/companyclass_list.js',
            // security_companyclass_new: './src/templates/security/companyclass/new/companyclass_new.js',
            // security_companyclass_edit: './src/templates/security/companyclass/edit/companyclass_edit.js',

            security_company_list: './src/templates/security/company/list/company_list.js',
            security_company_new: './src/templates/security/company/new/company_new.js',
            security_company_edit: './src/templates/security/company/edit/company_edit.js',
            security_companyemail_setting: './src/templates/security/company/email/companyemail_setting.js',
            security_company_billing: './src/templates/security/company/billing/company_billing.js',
            security_company_import: './src/templates/security/company/import/company_import.js',

            security_branchoffice_list: './src/templates/security/branchoffice/list/branchoffice_list.js',
            security_branchoffice_new: './src/templates/security/branchoffice/new/branchoffice_new.js',
            security_branchoffice_edit: './src/templates/security/branchoffice/edit/branchoffice_edit.js',
            security_branchoffice_import: './src/templates/security/branchoffice/import/branchoffice_import.js',


            security_employee_list: './src/templates/security/employee/list/employee_list.js',
            security_employee_new: './src/templates/security/employee/new/employee_new.js',
            security_employee_edit: './src/templates/security/employee/edit/employee_edit.js',
            security_employee_import: './src/templates/security/employee/import/employee_import.js',


            security_assignment: './src/templates/security/employee/assignment/assignment.js',

            security_position_new: './src/templates/security/position/new/position_new.js',
            security_position_edit: './src/templates/security/position/edit/position_edit.js',
            security_position_list: './src/templates/security/position/list/position_list.js',
            security_position_import: './src/templates/security/position/import/position_import.js',


            // security_permission_new: './src/templates/security/permission/new/permission_new.js',
            // security_permission_edit: './src/templates/security/permission/edit/permission_edit.js',
            // security_permission_list: './src/templates/security/permission/list/permission_list.js',

            support_alert_settings: './src/templates/support/alert/settings/alert_settings.js',
            support_backup_list: './src/templates/support/backup/list/backup_list.js',
            support_email_settings: './src/templates/support/email/settings/email_settings.js',

            // support_post_list: './src/templates/support/post/list/post_list.js',
            // support_post_new: './src/templates/support/post/new/post_new.js',
            // support_post_edit: './src/templates/support/post/edit/post_edit.js',

            // support_ticket_list: './src/templates/support/ticket/list/ticket_list.js',
            // support_ticket_new: './src/templates/support/ticket/new/ticket_new.js',
            // support_ticket_edit: './src/templates/support/ticket/edit/ticket_edit.js',
        },
        output: {
            path: path.join(basePath, distPath),
            filename: '[name].min.js'
        },
        module: {
            rules: [{
                test: /\.(js|jsx)$/,
                exclude: /(node_modules|build)/,
                use: ['babel-loader']
            },
            {
                test: /\.(css|sass|scss)$/,
                use: [
                    MiniCSSExtract.loader,
                    'css-loader',
                    'sass-loader'
                ]
            },
            {
                test: /\.(png|jpg|jpeg|svg|gif)$/,
                use: [{
                    loader: 'file-loader',
                    options: {
                        outputPath: distImages
                        // publicPath: distImages,
                    }
                }]
            }
        ]
    },
    plugins: plugins,
    optimization: {
        minimizer: [
            new OptimizeCSSAssetsPlugin({})
        ]
    }
};
};
