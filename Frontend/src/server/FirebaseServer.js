import * as firebase from 'firebase'

function get_Config() {
    return {
        apiKey: "AIzaSyAvR7CoeKGlhpF7tfn09ll_0iKTNt_pjWw",
        authDomain: "crm-2d15c.firebaseapp.com",
        databaseURL: "https://crm-2d15c.firebaseio.com",
        projectId: "crm-2d15c",
        storageBucket: "",
        messagingSenderId: "635394808087",
        appId: "1:635394808087:web:547e526a85c7dffc07bf52",
        measurementId: "G-LXTHZFTY3C"
    }
}


var google_app = firebase.initializeApp(get_Config())
// let provider = new firebase.auth.Face

const FirebaseServer = {

    async register_Simple(email, password) {
        return new Promise( async (resolve, reject) => {
            try {
                let result = await google_app.auth().createUserWithEmailAndPassword(
                    email, password
                )
                resolve(result)

            } catch (error) {
                console.log(error.message)
                let format_error = error.code
                reject(format_error)
            }
        })
    },

    async login_Simple(email, password) {
        return new Promise( async (resolve, reject) => {
            try {
                let result = await google_app.auth().signInWithEmailAndPassword(
                    email, password
                )
                resolve(result)

            } catch (error) {
                console.log(error.message)
                let format_error = error.code
                reject(format_error)
            }
        })
    }
}

export default FirebaseServer

