import Connector from '../utils/Connector'
import App from '../AppSettings'

const GoodsServer = {

    storeitems: {
        async search(value) {
            const url = `${App.api_domain}goods/api/storeitems/search/?value=${value}`
            return Connector.get(url)
        }
    }
}

export default GoodsServer
