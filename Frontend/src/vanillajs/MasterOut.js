import 'normalize.css'

import Spinner from './Spinner.js'


export default class MasterOut {

    constructor () {
        this.container = document.getElementById("main")
        this.spinner = new Spinner(this.container)
    }
}
