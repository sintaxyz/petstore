import './styles/ImportMessages.scss'

export default class ImportMessage {

    constructor (_obj) {
        this.container = _obj
        this.button = document.getElementById("import_msg-btn-close")

        this.set_Events()
    }

    set_Events () {
        this.button.addEventListener(
            "click",
            this.click_Button.bind(this)
        )        
    }

    click_Button (e) {
        this.container.classList.add('import_msg--close')
    }
}