import Modal from './Modal.js'

class Search {

    constructor () {
        this.btn_show = document.getElementById("search-bar__btn-filters")

        if (this.btn_show) {
            this.modal_filters = new Modal(
                "modal-filter-id",
                "modal-filter-title",
                "modal-filter-close"
            )
            this.set_Events()
        }

        this.init()
    }

    init() {
        this.modal_filters
    }

    set_Events() {
        this.btn_show.addEventListener(
            "click",
            this.click_ShowButton.bind(this)
        )
    }

    click_ShowButton(event) {
        event.preventDefault()
        this.modal_filters.open()
    }
}


export default Search