
class ButtonsManager {

    constructor(_spinner) {
        this.buttons = document.querySelector("[data-loading='true']")
        this.spinner = _spinner

        if (this.buttons != null) {
            console.log("control")
            this.init()
        }
    }

    init() {
        this.buttons.addEventListener(
            "click",
            this.click_Btn.bind(this)
        )
    }

    click_Btn(event) {
        this.spinner.start()
    }

}

export default ButtonsManager
