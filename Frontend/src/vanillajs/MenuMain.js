
class MenuMain {

    constructor () {
        this.container = document.getElementById("menu-main")
        this.btn_open = document.getElementById("bar-main-in__menu-icon")
        this.menu_open = true
        this.items = document.getElementsByClassName("menu-main__item")
        this.set_Events()
    }

    set_Events () {
        this.btn_open.addEventListener(
            "click",
            this.toggle_OpenMenu.bind(this)
        )
        for (var item of this.items) {
            if (item.hasAttribute("data-type")) {
                if (item.getAttribute("data-type") == "submenu") {
                    new SubmenuMain(item)
                }
            }
        }
    }

    toggle_OpenMenu (event) {
        event.preventDefault()
        if (this.menu_open === false){
            this.close()
            this.menu_open = !this.menu_open
        } else {
            this.open()
            this.menu_open = !this.menu_open
        }
    }

    click_BtnClose (event) {
        event.preventDefault()
        this.close()
    }

    open () {
        this.container.classList.add('menu-main--open')
        document.body.classList.add('menu-main--hide-body')
    }

    close () {
        this.container.classList.remove('menu-main--open')
        document.body.classList.remove('menu-main--hide-body')
    }
}


class SubmenuMain {

    constructor (_item_obj) {
        this.container = _item_obj.getElementsByClassName("item-menu")[0]
        this.button = _item_obj.getElementsByClassName("item-link")[0]

        this.set_Events()
    }

    set_Events() {
        this.button.addEventListener(
            "click",
            this.click_Button.bind(this)
        )
    }

    click_Button (event) {
        event.preventDefault()
        this.open()
    }

    open() {
        if (this.check_IsOpen() == true) {
            this.close()
        }
        else {
            this.container.classList.remove('item-menu--close')
            this.button.classList.add('item_link--active')

            let iconito = this.button.getElementsByClassName("fa-angle-down")[0]
            iconito.classList.remove("fa-angle-down")
            iconito.classList.add("fa-angle-up")
        }
    }

    close() {
        this.container.classList.add('item-menu--close')
        this.button.classList.remove('item_link--active')

        let iconito = this.button.getElementsByClassName("fa-angle-up")[0]
        iconito.classList.remove("fa-angle-up")
        iconito.classList.add("fa-angle-down")
    }

    check_IsOpen () {
        let value = this.container.classList.contains("item-menu--close")
        if (value == true) {
            return false
        }
        else {
            return true
        }
    }
}

export default MenuMain;