
const DomainEndpoins = `${window.location.origin}/`

console.log(PRODUCTION)

const DomainStatics = () => {
    if (PRODUCTION) {
        return `${window.location.origin}/`
    } else {
        return 'http://127.0.0.1:9001/'
    }
}

const App = {
    name: 'PetStore',
    year_release: '2019',
    powered_by: 'Sintaxyz.com',
    powered_by_url: 'http://sintacyz.com/'
}

export { DomainStatics, DomainEndpoins, App }