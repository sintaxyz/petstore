const Urls = {
    items: "/core/items/",
    dashboard: "/",
    terms: "/terms/",
    privacity: "/privacity/",
    questions: "/questions/",
    initial_entries: "/inventory/docinitialentries/",
    purchase_orders: "/procurement/purchaseorders/",
    expense: "/procurement/expenses/",
    sale_orders: "/sales/saleorders/",
    sale_orders_new: "/sales/saleorders/new/",
    purchase_order_new: "/procurement/purchaseorders/new/",
    payments: "/sales/payments/",
    payments_new: "/sales/payments/new/",
    doc_receptions_new: "/inventory/docreceptions/new/",
    doc_receptions: "/inventory/docreceptions/",
    doc_dispatches: "/inventory/docdispatches/",
    doc_adjustments: "/inventory/docadjustments/",
    shrinkages: "/inventory/docshrinkages/",
    assignments: "/security/assignments/",
    employees: "/security/employees/",
}

export default Urls

