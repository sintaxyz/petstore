import Connector from '../../utils/Connector'
import { DomainEndpoins } from '../settings'

const Endpoints = {
    list: `${DomainEndpoins}security/api/assignments/`,
    get_ByUser(id){
        return `${DomainEndpoins}security/api/assignments/employee/${id}/`
    },
    // lines(id){
    //     return `${DomainEndpoins}security/api/assignments/${id}/`
    // },
    // update_Description(id){
    //     return `${DomainEndpoins}security/api/assignments/${id}/desc/`
    // },
    // finish(id){
    //     return `${DomainEndpoins}security/api/assignments/${id}/finish/`
    // }
}

const Assignment = {
    async all() {
        return new Promise( async (resolve, reject) => {
            try {
                let url = Endpoints.list
                let data = await Connector.post(url)
                resolve(data)
            } catch (error) {
                reject(error)
            }
        })
    },
    async create(info) {
        return new Promise( async (resolve, reject) => {
            try {
                let url = Endpoints.list
                let data = await Connector.post(url, info)
                resolve(data)
            } catch (error) {
                reject(error)
            }
        })
    },
    // async create_Line(id, info) {
    //     return new Promise( async (resolve, reject) => {
    //         try {
    //             let url = Endpoints.lines(id)
    //             let data = await Connector.post(url, info)
    //             resolve(data)
    //         } catch (error) {
    //             reject(error)
    //         }
    //     })
    // },
    async get_ByUser(pk) {
        return new Promise( async (resolve, reject) => {
            try {
                let url = Endpoints.get_ByUser(pk)
                let data = await Connector.get(url)
                resolve(data)
            } catch (error) {
                reject(error)
            }
        })
    },
    // async finish(id) {
    //     return new Promise( async (resolve, reject) => {
    //         try {
    //             let url = Endpoints.finish(id)
    //             let data = await Connector.get(url)
    //             resolve(data)
    //         } catch (error) {
    //             reject(error)
    //         }
    //     })
    // },    
}

export default Assignment