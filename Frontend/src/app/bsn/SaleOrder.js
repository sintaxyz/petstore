import Connector from '../../utils/Connector'
import { DomainEndpoins } from '../settings'

const Endpoints = {
    list: `${DomainEndpoins}sales/api/saleorders/`,
    lines(id){
        return `${DomainEndpoins}sales/api/saleorders/${id}/`
    },
    update_Description(id){
        return `${DomainEndpoins}sales/api/saleorders/${id}/desc/`
    },
    finish(id){
        return `${DomainEndpoins}sales/api/saleorders/${id}/finish/`
    },
    cancel(id){
        return `${DomainEndpoins}sales/api/saleorders/${id}/cancel/`
    },
    pendingpay_list: `${DomainEndpoins}sales/api/saleorders/pendingpay/`,
    pendingpay_listByClient(id){
        return  `${DomainEndpoins}sales/api/saleorders/${id}/pendingclient/`
    },
    accounts_Receivable(filters){
        return  `${DomainEndpoins}sales/api/saleorders/accountsreceivable/?${filters}`
    }
}

const SaleOrder = {
    async all() {
        return new Promise( async (resolve, reject) => {
            try {
                let url = Endpoints.list
                let data = await Connector.get(url)
                resolve(data)
            } catch (error) {
                reject(error)
            }
        })
    },
    async create(info) {
        return new Promise( async (resolve, reject) => {
            try {
                let url = Endpoints.list
                let data = await Connector.post(url, info)
                resolve(data)
            } catch (error) {
                reject(error)
            }
        })
    },
    async create_Line(id, info) {
        return new Promise( async (resolve, reject) => {
            try {
                let url = Endpoints.lines(id)
                let data = await Connector.post(url, info)
                resolve(data)
            } catch (error) {
                reject(error)
            }
        })
    },
    async update_Description(id, info) {
        return new Promise( async (resolve, reject) => {
            try {
                let url = Endpoints.update_Description(id)
                let data = await Connector.post(url, info)
                resolve(data)
            } catch (error) {
                reject(error)
            }
        })
    },
    async finish(id) {
        return new Promise( async (resolve, reject) => {
            try {
                let url = Endpoints.finish(id)
                let data = await Connector.get(url)
                resolve(data)
            } catch (error) {
                reject(error)
            }
        })
    },
    async cancel(id) {
        return new Promise( async (resolve, reject) => {
            try {
                let url = Endpoints.cancel(id)
                let data = await Connector.get(url)
                resolve(data)
            } catch (error) {
                reject(error)
            }
        })
    },
    async get(id) {
        return new Promise( async (resolve, reject) => {
            try {
                let url = Endpoints.lines(id)
                let data = await Connector.get(url)
                resolve(data)
            } catch (error) {
                reject(error)
            }
        })
    },
    async pendingpay_list() {
        return new Promise( async (resolve, reject) => {
            try {
                let url = Endpoints.pendingpay_list
                let data = await Connector.get(url)
                resolve(data)
            } catch (error) {
                reject(error)
            }
        })
    },
    async pendingpay_listByClient(id) {
        return new Promise( async (resolve, reject) => {
            try {
                let url = Endpoints.pendingpay_listByClient(id)
                let data = await Connector.get(url)
                resolve(data)
            } catch (error) {
                reject(error)
            }
        })
    },    
    async accounts_Receivable(filters) {
        return new Promise( async (resolve, reject) => {
            try {
                let filters_array = []
                if (filters.length > 0) {
                    filters.map(filter => filters_array.push(`${filter.label}=${filter.value}`))
                }
                let filters_string = filters_array.join('&')
                let url = Endpoints.accounts_Receivable(filters_string)
                let data = await Connector.get(url)
                resolve(data)
            } catch (error) {
                reject(error)
            }
        })
    },
}

export default SaleOrder