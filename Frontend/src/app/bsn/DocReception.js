import Connector from '../../utils/Connector'
import { DomainEndpoins } from '../settings'

const Endpoints = {
    list: `${DomainEndpoins}inventory/api/docreceptions/`,
    get(id){
        return `${DomainEndpoins}inventory/api/docreceptions/${id}/`},
    update_Description(id){
        return `${DomainEndpoins}inventory/api/docreceptions/${id}/desc/`},
    lines(id) {
        return `${DomainEndpoins}inventory/api/docreceptions/${id}/lines/`},
    update_Line(id, line_id) {
        return `${DomainEndpoins}inventory/api/docreceptions/${id}/lines/${line_id}/`},
    finish(id) {
        return `${DomainEndpoins}inventory/api/docreceptions/${id}/finish/`},
    cancel(id) {
        return `${DomainEndpoins}inventory/api/docreceptions/${id}/cancel/`},
}

const DocReception = {
    async all() {
        return new Promise( async (resolve, reject) => {
            try {
                let url = Endpoints.list
                let data = await Connector.post(url)
                resolve(data)
            } catch (error) {
                reject(error)
            }
        })
    },
    async get(id) {
        return new Promise( async (resolve, reject) => {
            try {
                let url = Endpoints.get(id)
                let data = await Connector.get(url)
                resolve(data)
            } catch (error) {
                reject(error)
            }
        })
    },
    async create(info) {
        return new Promise( async (resolve, reject) => {
            try {
                let url = Endpoints.list
                let data = await Connector.post(url, info)
                resolve(data)
            } catch (error) {
                reject(error)
            }
        })
    },
    async update_Description(id, info) {
        return new Promise( async (resolve, reject) => {
            try {
                let url = Endpoints.update_Description(id)
                let data = await Connector.post(url, info)
                resolve(data)
            } catch (error) {
                reject(error)
            }
        })
    },
    async lines(id) {
        return new Promise( async (resolve, reject) => {
            try {
                let url = Endpoints.lines(id)
                let data = await Connector.get(url,)
                resolve(data)
            } catch (error) {
                reject(error)
            }
        })
    },
    async update_Line(id, line_id, info) {
        return new Promise( async (resolve, reject) => {
            try {
                let url = Endpoints.update_Line(id, line_id)
                let data = await Connector.put(url, info)
                resolve(data)
            } catch (error) {
                reject(error)
            }
        })
    },
    async finish(id) {
        return new Promise( async (resolve, reject) => {
            try {
                let url = Endpoints.finish(id)
                let data = await Connector.get(url)
                resolve(data)
            } catch (error) {
                reject(error)
            }
        })
    },
    async cancel(id) {
        return new Promise( async (resolve, reject) => {
            try {
                let url = Endpoints.cancel(id)
                let data = await Connector.get(url)
                resolve(data)
            } catch (error) {
                reject(error)
            }
        })
    }
}

export default DocReception