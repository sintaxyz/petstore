import Connector from '../../utils/Connector'
import { DomainEndpoins } from '../settings'

const Endpoints = {
    list: `${DomainEndpoins}security/api/employees/`,
    retrieve(id){
        return `${DomainEndpoins}security/api/employees/${id}/`
    },
    active: `${DomainEndpoins}security/api/employees/active/`,
}

const Employee = {
    async all() {
        return new Promise( async (resolve, reject) => {
            try {
                let url = Endpoints.list
                let data = await Connector.get(url)
                resolve(data)
            } catch (error) {
                reject(error)
            }
        })
    },
    async active() {
        return new Promise( async (resolve, reject) => {
            try {
                let url = Endpoints.active
                let data = await Connector.get(url)
                resolve(data)
            } catch (error) {
                reject(error)
            }
        })
    },
    async get(id) {
        return new Promise( async (resolve, reject) => {
            try {
                let url = Endpoints.retrieve(id)
                let data = await Connector.get(url)
                resolve(data)
            } catch (error) {
                reject(error)
            }
        })
    },
}

export default Employee