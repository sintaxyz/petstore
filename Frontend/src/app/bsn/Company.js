import Connector from '../../utils/Connector'
import { DomainEndpoins } from '../settings'

const Endpoints = {
    list: `${DomainEndpoins}security/api/companies/`,
    active: `${DomainEndpoins}security/api/companies/active`,
    get(pk){
        return `${DomainEndpoins}security/api/companies/${pk}/`
    },
    positions(pk){
        return `${DomainEndpoins}security/api/companies/${pk}/companypositions/`
    },
    branches(pk){
        return `${DomainEndpoins}security/api/companies/${pk}/branchoffices/`
    },
}


const Company = {
    async all() {
        return new Promise( async (resolve, reject) => {
            try {
                let url = Endpoints.list
                let data = await Connector.get(url)
                resolve(data)
            } catch (error) {
                reject(error)
            }
        })
    },
    async active() {
        return new Promise( async (resolve, reject) => {
            try {
                let url = Endpoints.active
                let data = await Connector.get(url)
                resolve(data)
            } catch (error) {
                reject(error)
            }
        })
    },
    async get(pk) {
        return new Promise( async (resolve, reject) => {
            try {
                let url = Endpoints.get(pk)
                let data = await Connector.get(url)
                resolve(data)
            } catch (error) {
                reject(error)
            }
        })
    },
    async positions(pk) {
        return new Promise( async (resolve, reject) => {
            try {
                let url = Endpoints.positions(pk)
                let data = await Connector.get(url)
                resolve(data)
            } catch (error) {
                reject(error)
            }
        })
    },
    async branches(pk) {
        return new Promise( async (resolve, reject) => {
            try {
                let url = Endpoints.branches(pk)
                let data = await Connector.get(url)
                resolve(data)
            } catch (error) {
                reject(error)
            }
        })
    },
}

export default Company