import Connector from '../../utils/Connector'
import { DomainEndpoins } from '../settings'

const Endpoints = {
    list: `${DomainEndpoins}core/api/suppliers/`,
    active: `${DomainEndpoins}core/api/suppliers/active/`,
}

const Supplier = {
    async all() {
        return new Promise( async (resolve, reject) => {
            try {
                let url = Endpoints.list
                let data = await Connector.get(url)
                resolve(data)
            } catch (error) {
                reject(error)
            }
        })
    },
    async active() {
        return new Promise( async (resolve, reject) => {
            try {
                let url = Endpoints.active
                let data = await Connector.get(url)
                resolve(data)
            } catch (error) {
                reject(error)
            }
        })
    }
}

export default Supplier