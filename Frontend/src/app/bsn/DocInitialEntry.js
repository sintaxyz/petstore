import Connector from '../../utils/Connector'
import { DomainEndpoins } from '../settings'

const Endpoints = {
    list: `${DomainEndpoins}inventory/api/docinitialentries/`,
    get(id){
        return `${DomainEndpoins}inventory/api/docinitialentries/${id}/`},
    lines(id){
        return `${DomainEndpoins}inventory/api/docinitialentries/${id}/lines/`
    },
    update_Description(id){
        return `${DomainEndpoins}inventory/api/docinitialentries/${id}/desc/`
    },
    update_Line(id, line_id) {
        return `${DomainEndpoins}inventory/api/docinitialentries/${id}/lines/${line_id}/`
    },
    finish(id){
        return `${DomainEndpoins}inventory/api/docinitialentries/${id}/finish/`
    },
    cancel(id){
        return `${DomainEndpoins}inventory/api/docinitialentries/${id}/cancel/`
    }
}

const DocInitialEntry = {
    async all() {
        return new Promise( async (resolve, reject) => {
            try {
                let url = Endpoints.list
                let data = await Connector.get(url)
                resolve(data)
            } catch (error) {
                reject(error)
            }
        })
    },
    async create(info) {
        return new Promise( async (resolve, reject) => {
            try {
                let url = Endpoints.list
                let data = await Connector.post(url, info)
                resolve(data)
            } catch (error) {
                reject(error)
            }
        })
    },
    async create_Line(id, info) {
        return new Promise( async (resolve, reject) => {
            try {
                let url = Endpoints.get(id)
                let data = await Connector.post(url, info)
                resolve(data)
            } catch (error) {
                reject(error)
            }
        })
    },
    async update_Description(id, info) {
        return new Promise( async (resolve, reject) => {
            try {
                let url = Endpoints.update_Description(id)
                let data = await Connector.post(url, info)
                resolve(data)
            } catch (error) {
                reject(error)
            }
        })
    },
    async finish(id) {
        return new Promise( async (resolve, reject) => {
            try {
                let url = Endpoints.finish(id)
                let data = await Connector.get(url)
                resolve(data)
            } catch (error) {
                reject(error)
            }
        })
    },
    async cancel(id) {
        return new Promise( async (resolve, reject) => {
            try {
                let url = Endpoints.cancel(id)
                let data = await Connector.get(url)
                resolve(data)
            } catch (error) {
                reject(error)
            }
        })
    },
    async get(id) {
        return new Promise( async (resolve, reject) => {
            try {
                let url = Endpoints.get(id)
                let data = await Connector.get(url)
                resolve(data)
            } catch (error) {
                reject(error)
            }
        })
    },
    async lines(id) {
        return new Promise( async (resolve, reject) => {
            try {
                let url = Endpoints.lines(id)
                let data = await Connector.get(url)
                resolve(data)
            } catch (error) {
                reject(error)
            }
        })
    },
    async update_Line(id, line_id, info) {
        return new Promise( async (resolve, reject) => {
            try {
                let url = Endpoints.update_Line(id, line_id)
                let data = await Connector.put(url, info)
                resolve(data)
            } catch (error) {
                reject(error)
            }
        })
    }
}

export default DocInitialEntry