import Connector from '../../utils/Connector'
import { DomainEndpoins } from '../settings'

const Endpoints = {
    list: `${DomainEndpoins}core/api/items/`,
    most_Selled(filters){
        return `${DomainEndpoins}core/api/items/mostselled/?${filters}`
    },
    most_InStock(filters){
        return `${DomainEndpoins}core/api/items/mostinstock/?${filters}`
    },
}

const Item = {
    async all() {
        return new Promise( async (resolve, reject) => {
            try {
                let url = Endpoints.list
                let data = await Connector.get(url)
                resolve(data)
            } catch (error) {
                reject(error)
            }
        })
    },
    async most_Selled(filters=[]) {
        return new Promise( async (resolve, reject) => {
            try {
                let filters_array = []
                if (filters.length > 0) {
                    filters.map(filter => filters_array.push(`${filter.label}=${filter.value}`))
                }
                let filters_string = filters_array.join('&')
                let url = Endpoints.most_Selled(filters_string)
                let data = await Connector.get(url)
                resolve(data)
            } catch (error) {
                reject(error)
            }
        })
    },
    async most_InStock(filters=[]) {
        return new Promise( async (resolve, reject) => {
            try {
                let filters_array = []
                if (filters.length > 0) {
                    filters.map(filter => filters_array.push(`${filter.label}=${filter.value}`))
                }
                let filters_string = filters_array.join('&')
                let url = Endpoints.most_InStock(filters_string)
                let data = await Connector.get(url)
                resolve(data)
            } catch (error) {
                reject(error)
            }
        })
    },
}

export default Item