import Connector from '../../utils/Connector'
import { DomainEndpoins } from '../settings'

const Endpoints = {
    list: `${DomainEndpoins}sales/api/payments/`,
    // retrieve(id){return `${DomainEndpoins}sales/api/payments/${id}/`},
    payments_InDays(filters){
        return  `${DomainEndpoins}sales/api/payments/indays/?${filters}`
    }
}

const Payment = {
    async all() {
        return new Promise( async (resolve, reject) => {
            try {
                let url = Endpoints.list
                let data = await Connector.get(url)
                resolve(data)
            } catch (error) {
                reject(error)
            }
        })
    },
    async create(info) {
        return new Promise( async (resolve, reject) => {
            try {
                let url = Endpoints.list
                let data = await Connector.post(url, info)
                resolve(data)
            } catch (error) {
                reject(error)
            }
        })
    },
    async payments_InDays(filters) {
        return new Promise( async (resolve, reject) => {
            try {
                let filters_array = []
                if (filters.length > 0) {
                    filters.map(filter => filters_array.push(`${filter.label}=${filter.value}`))
                }
                let filters_string = filters_array.join('&')
                let url = Endpoints.payments_InDays(filters_string)
                let data = await Connector.get(url)
                resolve(data)
            } catch (error) {
                reject(error)
            }
        })
    },
    // async get(id) {
    //     return new Promise( async (resolve, reject) => {
    //         try {
    //             let url = Endpoints.get(id)
    //             let data = await Connector.get(url)
    //             resolve(data)
    //         } catch (error) {
    //             reject(error)
    //         }
    //     })
    // },
}

export default Payment