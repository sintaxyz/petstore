import Connector from '../../utils/Connector'
import { DomainEndpoins } from '../settings'

const Endpoints = {
    list: `${DomainEndpoins}core/api/warehouses/`,
    for_Chart(filters){
        return `${DomainEndpoins}core/api/warehouses/charts/?${filters}`
    },
    items(pk){
        return `${DomainEndpoins}core/api/warehouses/${pk}/items/stock/`
    },
    items_OutStock(pk){
        return `${DomainEndpoins}core/api/warehouses/${pk}/items/outstock/`
    },
    items_OutStockActive(pk){
        return `${DomainEndpoins}core/api/warehouses/${pk}/items/outstock/active/`
    },
    items_InStock(pk){
        return `${DomainEndpoins}core/api/warehouses/${pk}/items/stock/`
    },
    items_InStockExist(pk){
        return `${DomainEndpoins}core/api/warehouses/${pk}/items/stockexist/`
    },
    with_Stock: `${DomainEndpoins}core/api/warehouses/withstock/`,
    active: `${DomainEndpoins}core/api/warehouses/active/`,
}

const Warehouse = {
    async all() {
        return new Promise( async (resolve, reject) => {
            try {
                let url = Endpoints.list
                let data = await Connector.get(url)
                resolve(data)
            } catch (error) {
                reject(error)
            }
        })
    },
    async with_Stock() {
        return new Promise( async (resolve, reject) => {
            try {
                let url = Endpoints.with_Stock
                let data = await Connector.get(url)
                resolve(data)
            } catch (error) {
                reject(error)
            }
        })
    },
    async active() {
        return new Promise( async (resolve, reject) => {
            try {
                let url = Endpoints.active
                let data = await Connector.get(url)
                resolve(data)
            } catch (error) {
                reject(error)
            }
        })
    },
    async for_Chart(filters) {
        return new Promise( async (resolve, reject) => {
            try {
                let filters_array = []
                if (filters.length > 0) {
                    filters.map(filter => filters_array.push(`${filter.label}=${filter.value}`))
                }
                let filters_string = filters_array.join('&')
                let url = Endpoints.for_Chart(filters_string)
                let data = await Connector.get(url)
                resolve(data)
            } catch (error) {
                reject(error)
            }
        })
    },
    async items(pk) {
        return new Promise( async (resolve, reject) => {
            try {
                let url = Endpoints.items(pk)
                let data = await Connector.get(url)
                resolve(data)
            } catch (error) {
                reject(error)
            }
        })
    },
    async items_OutStock(pk) {
        return new Promise( async (resolve, reject) => {
            try {
                let url = Endpoints.items_OutStock(pk)
                let data = await Connector.get(url)
                resolve(data)
            } catch (error) {
                reject(error)
            }
        })
    },
    async items_OutStockActive(pk) {
        return new Promise( async (resolve, reject) => {
            try {
                let url = Endpoints.items_OutStockActive(pk)
                let data = await Connector.get(url)
                resolve(data)
            } catch (error) {
                reject(error)
            }
        })
    },
    async items_InStock(pk) {
        return new Promise( async (resolve, reject) => {
            try {
                let url = Endpoints.items_InStock(pk)
                let data = await Connector.get(url)
                resolve(data)
            } catch (error) {
                reject(error)
            }
        })
    },
    async items_InStockExist(pk) {
        return new Promise( async (resolve, reject) => {
            try {
                let url = Endpoints.items_InStockExist(pk)
                let data = await Connector.get(url)
                resolve(data)
            } catch (error) {
                reject(error)
            }
        })
    }
}

export default Warehouse