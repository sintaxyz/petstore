import Connector from '../../utils/Connector'
import { DomainEndpoins } from '../settings'

const Endpoints = {
    list: `${DomainEndpoins}procurement/api/purchaseorders/`,    
    lines(id){
        return `${DomainEndpoins}procurement/api/purchaseorders/${id}/`
    },
    update_Description(id){
        return `${DomainEndpoins}procurement/api/purchaseorders/${id}/desc/`
    },
    finish(id){
        return `${DomainEndpoins}procurement/api/purchaseorders/${id}/approve/`
    },
    cancel(id){
        return `${DomainEndpoins}procurement/api/purchaseorders/${id}/cancel/`
    },
    list_Approved(){
        return `${DomainEndpoins}procurement/api/purchaseorders/approved/`
    },
}

const PurcharseOrder = {
    async all() {
        return new Promise( async (resolve, reject) => {
            try {
                let url = Endpoints.list
                let data = await Connector.get(url)
                resolve(data)
            } catch (error) {
                reject(error)
            }
        })
    },
    async create(info) {
        return new Promise( async (resolve, reject) => {
            try {
                let url = Endpoints.list
                let data = await Connector.post(url, info)
                resolve(data)
            } catch (error) {
                reject(error)
            }
        })
    },
    async create_Line(id, info) {
        return new Promise( async (resolve, reject) => {
            try {
                let url = Endpoints.lines(id)
                let data = await Connector.post(url, info)
                resolve(data)
            } catch (error) {
                reject(error)
            }
        })
    },
    async update_Description(id, info) {
        return new Promise( async (resolve, reject) => {
            try {
                let url = Endpoints.update_Description(id)
                let data = await Connector.put(url, info)
                resolve(data)
            } catch (error) {
                reject(error)
            }
        })
    },
    async finish(id) {
        return new Promise( async (resolve, reject) => {
            try {
                let url = Endpoints.finish(id)
                let data = await Connector.get(url)
                resolve(data)
            } catch (error) {
                reject(error)
            }
        })
    },
    async cancel(id) {
        return new Promise( async (resolve, reject) => {
            try {
                let url = Endpoints.cancel(id)
                let data = await Connector.get(url)
                resolve(data)
            } catch (error) {
                reject(error)
            }
        })
    },
    async list_Approved() {
        return new Promise( async (resolve, reject) => {
            try {
                let url = Endpoints.list_Approved()
                let data = await Connector.get(url)
                resolve(data)
            } catch (error) {
                reject(error)
            }
        })
    },
    async get(id) {
        return new Promise( async (resolve, reject) => {
            try {
                let url = Endpoints.lines(id)
                let data = await Connector.get(url)
                resolve(data)
            } catch (error) {
                reject(error)
            }
        })
    },
}

export default PurcharseOrder