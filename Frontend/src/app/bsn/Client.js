import Connector from '../../utils/Connector'
import { DomainEndpoins } from '../settings'

const Endpoints = {
    list: `${DomainEndpoins}core/api/clients/`,
    retrieve(id){return `${DomainEndpoins}core/api/clients/${id}/`},
    for_Chart(filters){
        return `${DomainEndpoins}core/api/clients/charts/?${filters}`
    },
    active: `${DomainEndpoins}core/api/clients/active/`,
    client_debtor: `${DomainEndpoins}core/api/clients/debtor`,
}

const Client = {
    async all() {
        return new Promise( async (resolve, reject) => {
            try {
                let url = Endpoints.list
                let data = await Connector.get(url)
                resolve(data)
            } catch (error) {
                reject(error)
            }
        })
    },
    async client_debtor() {
        return new Promise( async (resolve, reject) => {
            try {
                let url = Endpoints.client_debtor
                let data = await Connector.get(url)
                resolve(data)
            } catch (error) {
                reject(error)
            }
        })
    },
    async active() {
        return new Promise( async (resolve, reject) => {
            try {
                let url = Endpoints.active
                let data = await Connector.get(url)
                resolve(data)
            } catch (error) {
                reject(error)
            }
        })
    },
    async retrieve(id) {
        return new Promise( async (resolve, reject) => {
            try {
                let url = Endpoints.retrieve(id)
                let data = await Connector.get(url)
                resolve(data)
            } catch (error) {
                reject(error)
            }
        })
    },

    async for_Chart(filters) {
        return new Promise( async (resolve, reject) => {
            try {
                let filters_array = []
                if (filters.length > 0) {
                    filters.map(filter => filters_array.push(`${filter.label}=${filter.value}`))
                }
                let filters_string = filters_array.join('&')
                let url = Endpoints.for_Chart(filters_string)
                let data = await Connector.get(url)
                resolve(data)
            } catch (error) {
                reject(error)
            }
        })
    }
}

export default Client