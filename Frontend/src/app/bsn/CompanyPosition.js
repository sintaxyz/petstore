import Connector from '../../utils/Connector'
import { DomainEndpoins } from '../settings'

const Endpoints = {
    list: `${DomainEndpoins}security/api/companypositions/`,
}

const CompanyPosition = {
    async all() {
        return new Promise( async (resolve, reject) => {
            try {
                let url = Endpoints.list
                let data = await Connector.get(url)
                resolve(data)
            } catch (error) {
                reject(error)
            }
        })
    }
}

export default CompanyPosition