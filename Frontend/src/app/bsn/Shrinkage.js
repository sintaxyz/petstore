import Connector from '../../utils/Connector'
import { DomainEndpoins } from '../settings'

const Endpoints = {
    list: `${DomainEndpoins}inventory/api/docshrinkages/`,
    lines(id){
        return `${DomainEndpoins}inventory/api/docshrinkages/${id}/`
    },
    update_Description(id){
        return `${DomainEndpoins}inventory/api/docshrinkages/${id}/desc/`
    },
    finish(id){
        return `${DomainEndpoins}inventory/api/docshrinkages/${id}/finish/`
    }
}

const Shrinkage = {
    async all() {
        return new Promise( async (resolve, reject) => {
            try {
                let url = Endpoints.list
                let data = await Connector.post(url)
                resolve(data)
            } catch (error) {
                reject(error)
            }
        })
    },
    async create(info) {
        return new Promise( async (resolve, reject) => {
            try {
                let url = Endpoints.list
                let data = await Connector.post(url, info)
                resolve(data)
            } catch (error) {
                reject(error)
            }
        })
    },
    async create_Line(id, info) {
        return new Promise( async (resolve, reject) => {
            try {
                let url = Endpoints.lines(id)
                let data = await Connector.post(url, info)
                resolve(data)
            } catch (error) {
                reject(error)
            }
        })
    },
    async update_Description(id, info) {
        return new Promise( async (resolve, reject) => {
            try {
                let url = Endpoints.update_Description(id)
                let data = await Connector.post(url, info)
                resolve(data)
            } catch (error) {
                reject(error)
            }
        })
    },    
    async finish(id) {
        return new Promise( async (resolve, reject) => {
            try {
                let url = Endpoints.finish(id)
                let data = await Connector.get(url)
                resolve(data)
            } catch (error) {
                reject(error)
            }
        })
    },
    async get(id) {
        return new Promise( async (resolve, reject) => {
            try {
                let url = Endpoints.lines(id)
                let data = await Connector.get(url)
                resolve(data)
            } catch (error) {
                reject(error)
            }
        })
    },    
}

export default Shrinkage