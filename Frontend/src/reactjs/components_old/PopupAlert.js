import React, { useEffect } from 'react'

import { library } from '@fortawesome/fontawesome-svg-core'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faTimesCircle } from '@fortawesome/free-solid-svg-icons'
import { faExclamationTriangle } from '@fortawesome/free-solid-svg-icons'
import { faSadTear } from '@fortawesome/free-solid-svg-icons'
import { faCheckCircle } from '@fortawesome/free-solid-svg-icons'

library.add(faTimesCircle)
library.add(faExclamationTriangle)
library.add(faSadTear)
library.add(faCheckCircle)


function PopupAlert(props) {

    const {
        close_handle,
        msg,
        title,
        type
    } = props


    useEffect(() => {
        document.addEventListener(
            "keydown",
            press_KeyScape
        )
        document.body.classList.add("popup-alert--hide-body")

        return () => {
            document.removeEventListener(
                "keydown",
                press_KeyScape
            )
            document.body.classList.remove("popup-alert--hide-body")
        }
    }, [])

    function press_KeyScape(event) {
        event.preventDefault()

        if (e.keyCode == 27) {
            close_handle()
        }
    }

    let icon_class = "popup-alert-body-icon"
    let icon_object = <FontAwesomeIcon icon="exclamation-triangle"/>
    switch(type) {
        case "WARNING":
        icon_class = "popup-alert-body-icon warning"
        icon_object = <FontAwesomeIcon icon="exclamation-triangle"/>
        break;

        case "ERROR":
        icon_class = "popup-alert-body-icon error"
        icon_object = <FontAwesomeIcon icon="sad-tear"/>
        break;

        case "SUCCESS":
        icon_class = "popup-alert-body-icon success"
        icon_object = <FontAwesomeIcon icon="check-circle"/>
        break;
    }

    return (
        <div className="popup-alert popup-alert--show">
            <div className="popup-alert-wrap">
                <div className="popup-alert-header">
                    <a
                    href="#"
                    className="popup-alert-header-icon"
                    onClick={close_handle}>
                        <FontAwesomeIcon icon="times-circle"/>
                    </a>
                </div>
                <div className="popup-alert-body">
                    <span className={icon_class}>
                        {icon_object}
                    </span>
                    <div className="popup-alert-body-text">
                        {msg}
                    </div>
                    <div className="popup-alert-body-controls">
                        <button
                        className="btn btn-default"
                        onClick={close_handle}>
                            {
                                !title ? "Entendido" : title
                            }
                        </button>
                    </div>
                </div>
            </div>
        </div>
    )
}


export default PopupAlert