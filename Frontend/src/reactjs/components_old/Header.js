import React from 'react'
import Menu from './Menu'

class Header extends React.Component {

    constructor(props) {
        super(props)
    }

    render() {

        const { option, logo_url } = this.props

        return (
            <header className="header">
                <a href="/" className="logo">
                    <img src={logo_url} alt="logo" />
                </a>
                <Menu option={option}></Menu>
            </header>
        )
    }
}

export default Header
