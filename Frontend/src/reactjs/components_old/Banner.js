import React from 'react'

class Banner extends React.Component {

    constructor(props) {
        super(props)
    }

    render() {
        const {
            img
        } = this.props

        return (
            <div className="banner">
                <img src={img} alt="logo" />
            </div>
        )
    }
}

export default Banner