import React from 'react'

import { library } from '@fortawesome/fontawesome-svg-core'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'

import { SearchContext } from '../context/SearchContext'

import './styles/ListSearch.scss'

class ListSearch extends React.Component {

    constructor(props) {
        super(props)
        this.get_Componentes = this.get_Componentes.bind(this)
    }

    get_Componentes () {
        const {
            empty,
            results
        } = this.context

        let component = ''
        if (empty) {
            component = <div className="result-search__empty">No se encontraron resultados</div>
        } else {
            component = <div className="result-search__records">
                { results.map((item, index) => {
                    return (
                        <div className="record" key={index}>
                            <div className="record_img">
                                <img src={item.image}></img>
                            </div>
                            <div className="record_inf">
                                <div className="record_inf_title">
                                    {item.name}
                                </div>
                                <div className="record_inf_text">
                                    {item.description}
                                </div>
                                <div className="record_inf_others">

                                    <div className="generics">
                                        <div className="generics_title">
                                            Genericos:
                                        </div>
                                        <div className="generics_body">
                                        { item.generic_associated.map((generic, index) => {
                                            return (
                                                <div className="generics_body_record" key={index}>
                                                    {generic.name}
                                                </div>
                                            )
                                        })}</div>
                                    </div>
                                    <div className="adjacencies">
                                        <div className="adjacencies_title">
                                            Adyacentes:
                                        </div>
                                        <div className="adjacencies_body">
                                        { item.adjacencies.map((adjacency, index) => {
                                            return (
                                                <div className="adjacencies_body_record" key={index}>
                                                    {adjacency.name}
                                                </div>
                                            )
                                        })}
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    )
                })}
            </div>
        }
        return component
    }

    render() {

        let component = this.get_Componentes()

        return (
            <div className="result-search">
                {component}
            </div>
        )
    }
}

ListSearch.contextType = SearchContext

export default ListSearch
