import React from 'react'

const TitleMain = (props) => {

    const {
        text
    } = props

    return (
        <div className="title-main">
            {text}
        </div>
    )
}


export default TitleMain