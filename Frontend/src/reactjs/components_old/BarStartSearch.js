import React from 'react'

import { library } from '@fortawesome/fontawesome-svg-core'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faSearch } from '@fortawesome/free-solid-svg-icons'
import { faTimesCircle } from '@fortawesome/free-solid-svg-icons'
library.add(faSearch)
library.add(faTimesCircle)


class BarStartSearch extends React.Component {

    constructor(props) {
        super(props)

        this.state = {
            keywords: '',
            show_btn_clear: false
        }

        this.search_input = React.createRef()
        this.change_Input = this.change_Input.bind(this)
        this.click_BtnBuscar = this.click_BtnBuscar.bind(this)
        this.goto_Search = this.goto_Search.bind(this)
        this.press_Enter = this.press_Enter.bind(this)
    }

    componentDidMount() {
        document.addEventListener(
            "keydown",
            this.press_Enter
        )
    }

    change_Input(event) {
        event.preventDefault()
        this.setState({
            keywords: event.target.value,
            show_btn_clear: event.target.value != "" ? true : false
        })
    }

    press_Enter(e) {
        if (e.keyCode == 13) {
            this.goto_Search()
        }
    }

    goto_Search() {
        const {
            keywords
        } = this.state

        if (keywords.length > 0) {
            location.href = `/search/?value=${keywords}`
        }
    }

    click_BtnBuscar(event) {
        event.preventDefault()
        this.goto_Search()
    }

    click_BtnClear(event) {
        event.preventDefault()
        this.setState({
            keyword: "",
            show_btn_clear: false
        })
        this.search_input.current.focus()
    }

    render() {
        return (
            <div className="start-search">
                <div className="start-search__title">
                    COMENZAR ES MUY <span>FACIL</span>
                </div>
                <div className="start-search__body">
                    <div className="body__input">
                        <input
                            value={this.state.keyword}
                            placeholder="Buscar artículo, sustancia activa, etc..."
                            onChange={this.change_Input}
                            ref={this.search_input}
                        />

                        { this.state.show_btn_clear &&
                            <span onClick={this.click_BtnClear}>
                                <FontAwesomeIcon icon="times-circle" />
                            </span>
                        }
                    </div>
                    <button
                    className="body__icon"
                    type="button"
                    onClick={this.click_BtnBuscar}>
                        <FontAwesomeIcon icon="search" />
                    </button>
                </div>
            </div>
        )
    }
}

export default BarStartSearch
