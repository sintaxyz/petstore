import React, { useState } from 'react'

import { library } from '@fortawesome/fontawesome-svg-core'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faBars } from '@fortawesome/free-solid-svg-icons'
import { faTimes } from '@fortawesome/free-solid-svg-icons'
library.add(faBars)
library.add(faTimes)


const Menu = (props) => {

    const {
        option
    } = props

    function click_BtnOpen(e) {
        e.preventDefault()
        document.body.classList.add("menu-body--show-body")
        setIsOpen(true)
    }

    function click_BtnClose(e) {
        e.preventDefault()
        document.body.classList.remove("menu-body--show-body")
        setIsOpen(false)
    }

    const [ is_open, setIsOpen ] = useState(false)

    const menu_class = is_open ? "menu-body menu-body--show" : "menu-body"

    return (
        <div key="2" className="menu">
            <nav className={menu_class}>
                <a href="#" className="menu-body-close" onClick={click_BtnClose}>
                    <FontAwesomeIcon icon="times" />
                </a>
                <a className={ option === 'search_items' ? 'selected' : '' } href="/search/"><span>BÚSQUEDA DE PRODUCTOS</span></a>
                <a className={ option === 'history' ? 'selected' : '' } href="/"><span>HISTORIAL</span></a>
                <a className={ option === 'my_orders' ? 'selected' : '' } href="/"><span>MIS PEDIDOS</span></a>
                <a className={ option === 'my_address' ? 'selected' : '' } href="/"><span>MIS DIRECCIONES</span></a>
            </nav>
            <a href="#" className="menu-open" onClick={click_BtnOpen} >
                <FontAwesomeIcon icon="bars" />
            </a>
        </div>
    )
}

export default Menu