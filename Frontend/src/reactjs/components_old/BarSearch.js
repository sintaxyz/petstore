import React from 'react'

import { library } from '@fortawesome/fontawesome-svg-core'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faSearch } from '@fortawesome/free-solid-svg-icons'
import { faTimesCircle } from '@fortawesome/free-solid-svg-icons'
import { SearchContext } from '../context/SearchContext'
library.add(faSearch)
library.add(faTimesCircle)


class BarSearch extends React.Component {

    constructor(props) {
        super(props)

        this.state = {
            keyword: this.props.keyword == "None" ? "" : this.props.keyword,
            show_btn_clear: this.props.keyword == "None" ? false : true,
        }

        this.search_input = React.createRef()
        this.change_Input = this.change_Input.bind(this)
        this.click_BtnBuscar = this.click_BtnBuscar.bind(this)
        this.goto_Search = this.goto_Search.bind(this)
        this.press_Enter = this.press_Enter.bind(this)
        this.click_BtnClear = this.click_BtnClear.bind(this)
    }

    componentDidMount() {
        document.addEventListener(
            "keydown",
            this.press_Enter
        )
    }

    change_Input(event) {
        event.preventDefault()
        this.setState({
            keyword: event.target.value,
            show_btn_clear: event.target.value != "" ? true : false
        })
    }

    press_Enter(e) {
        if (e.keyCode == 13) {
            this.goto_Search()
        }
    }

    goto_Search() {
        const {
            keyword
        } = this.state

        if (keyword.length > 0) {
            this.context.search_Item(keyword)
        }
    }

    click_BtnBuscar(event) {
        event.preventDefault()
        this.goto_Search()
    }

    click_BtnClear(event) {
        event.preventDefault()
        this.setState({
            keyword: "",
            show_btn_clear: false
        })
        this.search_input.current.focus()
    }

    render() {
        return (
            <div className="bar-search">
                <div className="bar-search__title">
                    ¿Que artículo necesitas?
                </div>
                <div className="bar-search__body">
                    <div className="body__input">
                        <input
                            value={this.state.keyword}
                            placeholder="Buscar artículo, sustancia activa, etc..."
                            onChange={this.change_Input}
                            ref={this.search_input}
                        />

                        { this.state.show_btn_clear &&
                            <span onClick={this.click_BtnClear}>
                                <FontAwesomeIcon icon="times-circle" />
                            </span>
                        }
                    </div>
                    <button
                    className="body__icon"
                    type="button"
                    onClick={this.click_BtnBuscar}>
                        <FontAwesomeIcon icon="search" />
                    </button>
                </div>
            </div>
        )
    }
}


BarSearch.contextType = SearchContext

export default BarSearch
