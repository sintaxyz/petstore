import React from 'react'

function FieldText({
    field, // { name, value, onChange, onBlur }
    form: { touched, errors }, // also values, setXXXX, handleXXXX, dirty, isValid, status, etc.
    ...props
}) {

    let field_class = "text-input"
    let label_class = "text-label"
    let container_class = "text"

    if (props.size) {
        container_class = `text text--${props.size}`
    }

    let show_errrors = false
    if (touched[field.name] && errors[field.name]) {
        show_errrors = true
        field_class = "text-input text-input--error"
        label_class = "text-label text-label--error"
    }

    let type = "text"
    if (props.type) {
        type = props.type
    }

    let autocomplete = ""
    if (props.autocomplete) {
        autocomplete = props.autocomplete
    }

    return (
        <div className={container_class}>
            <label className={label_class}>
                {props.title}
                { props.optional == true &&
                    <span className="text-label-optional">
                        (opcional)
                    </span>
                }
            </label>
            <input
                type={type} {...field}
                className={field_class}
                autoComplete={autocomplete}
            />
            { show_errrors &&
                <div className="text-errors">
                    <ul className="text-errors-list">
                        {errors[field.name]}
                    </ul>
                </div>
            }
            { props.helptext &&
                <div className="text-help">
                    {props.helptext}
                </div>
            }
        </div>
    )
}

export default FieldText