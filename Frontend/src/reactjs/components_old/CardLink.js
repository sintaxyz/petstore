import React from 'react'

import { library } from '@fortawesome/fontawesome-svg-core'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faSearch } from '@fortawesome/free-solid-svg-icons'
import { faTable } from '@fortawesome/free-solid-svg-icons'
import { faTruck } from '@fortawesome/free-solid-svg-icons'

library.add(faSearch)
library.add(faTable)
library.add(faTruck)


class CardLink extends React.Component {

    constructor(props) {
        super(props)
    }

    render() {

        const {
            icon,
            title,
            text,
            link,
            link_title
        } = this.props

        let icon_component = ''
        switch (icon) {

            case "prescription":
                icon_component = <FontAwesomeIcon icon="search" />
                break;

            case "orders":
                icon_component = <FontAwesomeIcon icon="table" />
                break;

            case "requisitions":
                icon_component = <FontAwesomeIcon icon="truck" />
                break;
        }

        return (
            <div className="card-link">
                <div className="card-link__header">
                    <div className="header__icon">
                        {icon_component}
                    </div>
                </div>
                <div className="card-link__body">
                    <div className="body__title">
                        {title}
                    </div>
                    <div className="body__text">
                        {text}
                    </div>
                    <div className="body__link">
                        <a href={link}>
                            {link_title}
                        </a>
                    </div>
                </div>
            </div>
        )
    }
}

export default CardLink