import React, { useState, useEffect, useContext } from 'react'
import { RegisterContext } from '../context/RegisterContext'

import { Formik, Form, Field } from 'formik'
import FieldText from './FieldText'
import * as Yup from 'yup'



const ModalUpdateSchema = Yup.object().shape({
    email: Yup.string()
    .email('El correo es invalido')
    .required('No puede dejar este campo vacio')
    ,name: Yup.string()
    .required('No puede dejar este campo vacio')
    ,password1: Yup.string()
    .min(6, 'La contraseña debe tener al menos 6 caracteres')
    .required('No puede dejar este campo vacio')
    ,password2: Yup.string()
    .oneOf([Yup.ref('password1'), null], 'Las contraseñas debe coincidir')
    .required('No puede dejar este campo vacio')
})


function FormRegister(props) {

    const {
        click_BtnRegister
    } = useContext(RegisterContext)

    return(
        <div className="form-register">
            <Formik
                initialValues={{
                    email: "",
                    name: "",
                    password1: "",
                    password2: "",
                }}
                validationSchema={ModalUpdateSchema}
                onSubmit={click_BtnRegister}
                render = {
                ({
                    status,
                    dirty,
                    resetForm,
                    handleChange,
                    handleBlur,
                    handleReset,
                    setFieldValue,
                    setTouched,
                    errors,
                    touched,
                    values,
                    handleSubmit,
                    isSubmitting,
                    isValid,
                }) => (
                    <form className="body"
                    onSubmit={handleSubmit}>
                        <div className="fields">
                            <div className="field">
                                <Field
                                    name="email"
                                    component={FieldText}
                                    title="Correo electronico"
                                    optional={false}
                                    size={4}
                                />
                            </div>
                            <div className="field">
                                <Field
                                    name="name"
                                    component={FieldText}
                                    title="Nombre completo"
                                    optional={false}
                                    size={4}
                                />
                            </div>
                            <div className="field">
                                <Field
                                    name="password1"
                                    component={FieldText}
                                    title="Contraseña"
                                    optional={false}
                                    size={4}
                                    type="password"
                                    autocomplete="new-password"
                                />
                            </div>
                            <div className="field">
                                <Field
                                    name="password2"
                                    component={FieldText}
                                    title="Confirmar Contraseña"
                                    optional={false}
                                    size={4}
                                    type="password"
                                    autocomplete="new-password"
                                />
                            </div>
                        </div>
                        <div className="controls">
                            <button
                            type="submit"
                            disabled={isSubmitting || !isValid}
                            className="btn btn-main">
                                Crear cuenta
                            </button>
                        </div>
                    </form>
                )}
            />
        </div>
    )

}

export default FormRegister
