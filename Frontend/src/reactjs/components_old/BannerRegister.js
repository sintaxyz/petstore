import React from 'react'
import { library } from '@fortawesome/fontawesome-svg-core'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'

import { faClipboardCheck } from '@fortawesome/free-solid-svg-icons'
library.add(faClipboardCheck)

function BannerRegister(props) {

    return (
        <div className="banner-register">
            <div className="wrapper">
                <div className="icon">
                    <FontAwesomeIcon icon="clipboard-check" />
                </div>
                <div className="body">
                    <div className="body-title">
                        ¡Tu cuenta ha sido creada!
                    </div>
                    <div className="body-subtitle">
                        Ahora puedes ingresar con tu email y comprar de forma segura.
                    </div>
                </div>
                <div className="footer">
                    <a href="/login" className="btn btn-main">
                        Ir a login
                    </a>
                </div>
            </div>
        </div>
    )
}

export default BannerRegister