import React from 'react'
import CardLink from './CardLink'

class BarLinks extends React.Component {

    constructor(props) {
        super(props)
    }

    render() {
        return (
            <div className="links-bar">
                <CardLink
                    icon={"prescription"}
                    title={"TU RECETA"}
                    text={"Busca tus articulos de manera sencilla y comparativa contra los articulos de marca"}
                    link={""}
                    link_title={"+Iniciar"}
                ></CardLink>

                <CardLink
                    icon={"orders"}
                    title={"PEDIDOS"}
                    text={"Revisa tus pedido, estatus, factura, etc."}
                    link={""}
                    link_title={"+Ver detalles"}
                ></CardLink>

                <CardLink
                    icon={"requisitions"}
                    title={"SOLICITAR"}
                    text={"Configura tus direcciones de entrega y recibe tus articulos en 60 minutos"}
                    link={""}
                    link_title={"+Ver detalles"}
                ></CardLink>
            </div>
        )
    }
}

export default BarLinks