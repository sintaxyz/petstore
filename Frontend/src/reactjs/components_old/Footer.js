import React from 'react'

const Footer = (props) => {

    const {
        logo_url
    } = props

    return (
        <div className="footer">
            <div className="footer__body">
                <div className="body__brand">
                    <a href="/" className="brand__logo">
                        <img src={logo_url} alt="logo" />
                    </a>
                    <span className="brand__email">
                        <a href="mailto:contacto@crm.com.mx">contacto@crm.com.mx</a>
                    </span>
                    <div className="brand__social">

                    </div>
                </div>
                <div className="body__legal">
                    <a  href="/terms/" className="legal_terms">CONDICIONES DE SERVICIO</a>
                    <a  href="/privacity/" className="legal_privacity">POLÍTICAS DE PRIVACIDAD</a>
                </div>
            </div>
            <div className="footer__bottom">
                <a href="http://sintaxyx.com/">
                    Powered by Sintaxyz.com
                </a>
            </div>
        </div>
    )
}


export default Footer