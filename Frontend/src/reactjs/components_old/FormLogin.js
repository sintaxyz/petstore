import React from 'react';

import { library } from '@fortawesome/fontawesome-svg-core'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faUser } from '@fortawesome/free-solid-svg-icons'
import { faLock } from '@fortawesome/free-solid-svg-icons'
import { faFacebookF } from '@fortawesome/free-brands-svg-icons'

library.add(faUser)
library.add(faLock)
library.add(faFacebookF);


class FormLogin extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            email: '',
            pass: ''
        };

        this.click_BtnLogin = this.click_BtnLogin.bind(this)
        this.click_BtnSignup = this.click_BtnSignup.bind(this)
        this.change_InputEmail = this.change_InputEmail.bind(this)
        this.change_InputPassword = this.change_InputPassword.bind(this)
    }

    click_BtnLogin(e) {
        e.preventDefault()
        console.log("Login")
    }

    click_BtnSignup(e) {
        e.preventDefault()
        console.log("Logout")
    }

    click_BtnFacebookSignup(e) {
        e.preventDefault()
        alert("Aun no disponible")
    }

    change_InputEmail(e) {
        this.setState({ email: e })
    }

    change_InputPassword(e) {
        this.setState({ email: e })
    }

    render() {
        return (
            <div className="login">
                <div className="login-body">
                    <div className="fields">
                        <div className="field-group">
                            <span>
                                <FontAwesomeIcon icon="search" />
                            </span>
                            <input type="email"
                            onChange={this.change_InputEmail}
                            placeholder="Correo electrónico"/>
                        </div>

                        <div className="field-group">
                            <span>
                                <FontAwesomeIcon icon="lock" />
                            </span>
                            <input
                            type="password" placeholder="Contraseña"
                            onChange={this.change_InputPassword}/>
                        </div>
                    </div>
                    <div className="controls">
                        <button
                        className="controls-login"
                        type="button" onClick={this.click_BtnLogin}>
                            Iniciar sesión
                        </button>

                        <button
                        className="controls-facebook"
                        type="button" onClick={this.click_BtnFacebookSignup}>
                            <FontAwesomeIcon icon={['fab', 'facebook-f']} />
                            <span>
                                Ingresa con Facebook
                            </span>
                        </button>
                    </div>
                    <div className="forgot">
                        <a href={"/reset/"}>
                            ¿Olvidó su contraseña?
                        </a>
                    </div>
                    <div className="register">
                        <a href={"/register/"}>
                            <span>
                                ¿No tienes una cuenta?
                            </span>
                            Crea una cuenta.
                        </a>
                    </div>
                </div>
            </div>
        )
    }
}

export default FormLogin
