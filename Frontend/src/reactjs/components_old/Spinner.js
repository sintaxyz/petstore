import React from 'react'


function Spinner() {

    return (
        <div className="spinner-wrap">
            <div className="spinner"></div>
        </div>
    )
}

export default Spinner
