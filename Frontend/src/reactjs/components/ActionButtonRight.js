import React from 'react'


class ItemAction extends React.Component {

    render() {

        const {
            tag,
            handle_event
        } = this.props

        return(
            <div className="content-form-controls__right">
                <button
                onClick={handle_event}
                className="btn btn-main">
                        {tag}
                </button>
            </div>
        )
    }
}

export default ItemAction