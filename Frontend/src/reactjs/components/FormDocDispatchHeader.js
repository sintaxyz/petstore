import React from 'react'
import { Formik } from 'formik'
import FieldTextFormik from './FieldTextFormik'
import FieldSelectFormik from './FieldSelectFormik'
import FieldTextareaFormik from './FieldTextareaFormik'
import * as Yup from 'yup'


const ModalUpdateSchema = Yup.object().shape({
    date: Yup.string().required('No puedes dejar este campo vacio'),
    warehouse: Yup.string().required('Favor de proporcionar el almacen'),
    delivery_by: Yup.string().required('Favor de proporcionar el usuario que entrega'),
    received_by: Yup.string().required('Favor de proporcionar el usuario que recibe'),
    description: Yup.string().required('No puedes dejar este campo vacio'),
    comments: Yup.string(),
})

const build_Options = (data, display_value, property_value) => {
    if (display_value) {
       var property_display = display_value
    } else {
        var property_display = 'name'
    }
    if (property_value) {
       var property_value = property_value
    } else {
        var property_value = 'id'
    }
    let options = data.map((item, index, array) => {
        let new_item = {}
        new_item.value = item[property_value]
        new_item.label = item[property_display]
        return new_item
    })

    return options
}

const build_UserOptions = (data, display_value, property_value) => {
    let options = data.map((item, index, array) => {
        let new_item = {}
        new_item.value = item["user"]
        new_item.label = item["user_name"]
        return new_item
    })

    return options
}


const FormDocDispatchHeader = (props) => {

    const {
        submit_handle,
        warehouse_data,
        delivery_by_data,
        received_by_data,
    } = props
    console.log(props);
    
    const warehouse_options = build_Options(warehouse_data)
    const delivery_by_options = build_UserOptions(delivery_by_data)
    const received_by_options = build_UserOptions(received_by_data)

    return (
        <Formik
        validationSchema={ModalUpdateSchema}
        initialValues={{
            warehouse: "",
            delivery_by: "",
            received_by: "",
            date: "",
            description: "",
            comments: "",
         }}
        onSubmit={submit_handle}>
        {props => (
            <form onSubmit={props.handleSubmit}>
                <div className="card-form-body">
                    <FieldSelectFormik
                        name="warehouse"
                        title={"almacen"}
                        options={warehouse_options}
                        change_handle={props.setFieldValue}
                        blur_handle={props.setFieldTouched}
                    />
                    <FieldSelectFormik
                        name="delivery_by"
                        title={"Entregado por:"}
                        options={delivery_by_options}
                        change_handle={props.setFieldValue}
                        blur_handle={props.setFieldTouched}
                    />
                    <FieldSelectFormik
                        name="received_by"
                        title={"Recibido por:"}
                        options={received_by_options}
                        change_handle={props.setFieldValue}
                        blur_handle={props.setFieldTouched}
                    />
                    <FieldTextFormik
                        name="date"
                        title={"Fecha de Entrega"}
                        placeholder={"AAAA-MM-DD HH:MM"}
                        optional={false}
                    />
                    <FieldTextFormik
                        name="description"
                        title={"Descripción"}
                        optional={false}
                    />
                    <FieldTextareaFormik
                        class_name="field field--x3"
                        name="comments"
                        title={"Observaciones"}
                        optional={true}
                    />
                </div>
                <div className="form-header-controls">
                    <button
                    type="submit"
                    disabled={props.isSubmitting || !props.isValid}
                    className="btn btn-main">
                            Continuar
                    </button>
                </div>
            </form>
        )}
        </Formik>
    )
}

export default FormDocDispatchHeader