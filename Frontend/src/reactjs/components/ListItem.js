import React from 'react'

const ListItem = (props) => {
    return (
        <div className="list-item">
            {props.children}
        </div>
    )
}

export default ListItem