import React from 'react'

const FilterDate = (props) => {


    const {
        size,
        title,
        // value,
        change_handle,
        placeholder,
        helptext,
        class_name,
    } = props

    let cls_name = "field"
    if (class_name) {
        cls_name = class_name
    }

    let container_class = "text"
    if (size) {
        container_class = `text text--${size}`
    }

    let field_class = "text-input"
    let label_class = "text-label"       

    const set_Value = (event) => {        
        change_handle(event.target.value)        
    }

    return (
        <div className={cls_name} data-type="field">
            <div className={container_class}>
                <label className={label_class}>
                    {title}                    
                </label>
                <input
                    type={"date"}
                    className={field_class}
                    // placeholder={placeholder}
                    // value={value}
                    onChange={set_Value}
                />                
                { helptext &&
                    <div className="text-help">
                        {helptext}
                    </div>
                }
            </div>
        </div>
    )
}

export default FilterDate