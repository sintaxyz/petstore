import React from 'react'

const ListFormLines = (props) => {
    return (
        <div className="list-form-item">
            {props.children}
        </div>
    )
}

export default ListFormLines