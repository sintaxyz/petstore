import React from 'react'

class ItemText extends React.Component {

    constructor(props) {
        super(props)
    }

    render() {

        const {
            value,
            label,
        } = this.props

        return(
            <div className="card-body-item">
                <div className="item-label">
                    {label}
                </div>
                <div className="item-data">
                    <div className="data-value">
                        <div className="value">
                            {value}
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

export default ItemText