import React from 'react'
import { App } from '../../app/settings'
import Urls from '../../app/server/urls'

const Footer = (props) => {
    return (
        <footer className="footer">
            <div className="footer-text-left">
                <div>{App.name} © {App.year_release}. All rights reserved</div>
                <div>Powered by <a href={App.powered_by_url}>{App.powered_by}</a></div>
            </div>
            <div className="footer-text-right">
                <div><a href={Urls.terms}>Terminos y Condiciones</a></div>
                <div><a href={Urls.privacity}>Aviso de Privacidad</a></div>
                <div><a href={Urls.questions}>Preguntas Frecuentes</a></div>
            </div>
        </footer>)
}

export default Footer