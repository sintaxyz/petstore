import React from 'react'
import { Formik } from 'formik'
import FieldTextFormik from './FieldTextFormik'
import FieldSelectFormik from './FieldSelectFormik'
import FieldTextareaFormik from './FieldTextareaFormik'
import * as Yup from 'yup'


const ModalUpdateSchema = Yup.object().shape({
    date: Yup.string(),
    warehouse: Yup.string().required('Favor de proporcionar el almacen'),
    register_by: Yup.string().required('Favor de proporcionar el usuario que registra'),
    description: Yup.string(),
    comments: Yup.string(),
    type: Yup.string().required('No puedes dejar este campo vacio'),
})

var type_options = [
    {value: 'ADM', label: 'Administrativa'},
    {value: 'OPE', label: 'Operativa'},
    {value: 'NAT', label: 'Natural'},
    {value: 'PRO', label: 'Producción'},
]

const build_Options = (data, display_value, property_value) => {
    if (display_value) {
       var property_display = display_value
    } else {
        var property_display = 'name'
    }
    if (property_value) {
       var property_value = property_value
    } else {
        var property_value = 'id'
    }
    let options = data.map((item, index, array) => {
        let new_item = {}
        new_item.value = item[property_value]
        new_item.label = item[property_display]
        return new_item
    })

    return options
}

const build_UserOptions = (data, display_value, property_value) => {
    let options = data.map((item, index, array) => {
        let new_item = {}
        new_item.value = item["user"]
        new_item.label = item["user_name"]
        return new_item
    })

    return options
}


const FormShrinkageHeader = (props) => {

    const {
        submit_handle,
        warehouse_data,
        register_by_data,
    } = props
    console.log(props);
    
    const warehouse_options = build_Options(warehouse_data, 'name')
    const register_by_options = build_UserOptions(register_by_data)

    return (
        <Formik
        validationSchema={ModalUpdateSchema}
        initialValues={{
            date: "",
            warehouse: "",
            register_by: "",
            description: "",
            comments: "",
            type: "ADM",
         }}
        onSubmit={submit_handle}>
        {props => (
            <form onSubmit={props.handleSubmit}>
                <div className="card-form-body">
                    <FieldSelectFormik
                        name="warehouse"
                        title={"almacen"}
                        options={warehouse_options}
                        change_handle={props.setFieldValue}
                        blur_handle={props.setFieldTouched}
                    />
                    <FieldSelectFormik
                        name="register_by"
                        title={"almacen"}
                        options={register_by_options}
                        change_handle={props.setFieldValue}
                        blur_handle={props.setFieldTouched}
                    />
                    <FieldSelectFormik
                        name="type"
                        title={"Tipo"}
                        options={type_options}
                        change_handle={props.setFieldValue}
                        blur_handle={props.setFieldTouched}   
                    />                    
                    <FieldTextFormik
                        name="date"
                        title={"Fecha de Entrega"}
                        placeholder={"AAAA-MM-DD HH:MM"}
                        optional={false}
                    />
                    <FieldTextFormik
                        name="description"
                        title={"Descripción"}
                        optional={false}
                    />
                    <FieldTextareaFormik
                        class_name="field field--x3"
                        name="comments"
                        title={"Observaciones"}
                        optional={true}
                    />
                </div>
                <div className="form-header-controls">
                    <button
                    type="submit"
                    disabled={props.isSubmitting || !props.isValid}
                    className="btn btn-main">
                            Continuar
                    </button>
                </div>
            </form>
        )}
        </Formik>
    )
}

export default FormShrinkageHeader