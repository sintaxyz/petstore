import React from 'react'

const ActionControl = (props) => {
    return (
        <div className="list-control">
            {props.children}
        </div>
    )
}

export default ActionControl