import React from 'react'
import { Formik } from 'formik'
import FieldTextFormik from './FieldTextFormik'
import FieldSelectFormik from './FieldSelectFormik'
import FieldTextareaFormik from './FieldTextareaFormik'
import * as Yup from 'yup'


const ModalUpdateSchema = Yup.object().shape({
    order: Yup.string().required('No puedes dejar este campo vacio'),
    date: Yup.string().required('No puedes dejar este campo vacio'),
    description: Yup.string(),
})

const build_Options = (data, display_value, property_value) => {
    if (display_value) {
       var property_display = display_value
    } else {
        var property_display = 'name'
    }
    if (property_value) {
       var property_value = property_value
    } else {
        var property_value = 'id'
    }
    let options = data.map((item, index, array) => {
        let new_item = {}
        new_item.value = item[property_value]
        new_item.label = item[property_display]
        return new_item
    })

    return options
}


const FormOrderHeader = (props) => {

    const {
        submit_handle,
        purchaseorder_data,
    } = props
    console.log(purchaseorder_data);
    
    const order_options = build_Options(purchaseorder_data, 'display_value')

    return (
        <Formik
        validationSchema={ModalUpdateSchema}
        initialValues={{
            order: "",
            date: "",
            description: "",
         }}
        onSubmit={submit_handle}>
        {props => (
            <form onSubmit={props.handleSubmit}>
                <div className="card-form-body">
                    <FieldSelectFormik
                        name="order"
                        title={"Orden de Compra"}
                        options={order_options}
                        change_handle={props.setFieldValue}
                        blur_handle={props.setFieldTouched}
                    />
                    <FieldTextFormik
                        name="date"
                        title={"Fecha de Entrega"}
                        placeholder={"AAAA-MM-DD HH:MM"}
                        optional={false}
                    />
                    <FieldTextFormik
                        name="description"
                        title={"Descripción"}
                        optional={false}
                    />
                    <FieldTextareaFormik
                        class_name="field field--x3"
                        name="comments"
                        title={"Observaciones"}
                        optional={true}
                    />
                </div>
                <div className="form-header-controls">
                    <button
                    type="submit"
                    disabled={props.isSubmitting || !props.isValid}
                    className="btn btn-main">
                            Continuar
                    </button>
                </div>
            </form>
        )}
        </Formik>
    )
}

export default FormOrderHeader