import React from 'react'

import RetrieveText from './RetrieveText'

class DetailDocAdjustmentHeader extends React.Component {

    constructor(props) {
        super(props)
    }

    render() {

        const {
            id,
            instance,
        } = this.props
        
        return (
            <div className="card-info-body">
                <div className="card-info-body__items">
                    <RetrieveText
                        label="ID"
                        value={instance.id}
                    ></RetrieveText>
                    <RetrieveText
                        label="Almacen"
                        value={instance.warehouse_name}
                    ></RetrieveText>
                    <RetrieveText
                        label="Ajustado por:"
                        value={instance.adjusted_by_name}
                    ></RetrieveText>
                    <RetrieveText
                        label="Fecha"
                        value={instance.date_desc}
                    ></RetrieveText>
                    <RetrieveText
                        label="type"
                        value={instance.type}
                    ></RetrieveText>
                    <RetrieveText
                        label="Descripcion"
                        value={instance.description}
                    ></RetrieveText>   
                </div>
            </div>
        )
    }
}


export default DetailDocAdjustmentHeader