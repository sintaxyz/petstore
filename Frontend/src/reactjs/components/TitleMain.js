import React from 'react'
import BarNavigation from '../../reactjs/components/BarNavigation'

const TitleMain = (props) => {

    const {
        icon,
        url,
        url_label,
        text
    } = props

    return (
        <div className="title-main">            
            <div className="title-main-icon">
                {icon}
            </div>
            <div className="title-main-text">
                <BarNavigation
                    url={url}
                    label={url_label}
                ></BarNavigation>
                {text}
            </div>
        </div>
    )
}


export default TitleMain