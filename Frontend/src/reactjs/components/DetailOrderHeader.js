import React from 'react'

import RetrieveText from './RetrieveText'

class PurchaseOrderLine extends React.Component {

    constructor(props) {
        super(props)
    }

    render() {

        const {
            id,
            instance,
        } = this.props
        
        return (
            <div className="card-info-body">
                <div className="card-info-body__items">
                    <RetrieveText
                        label="ID"
                        value={instance.id}
                    ></RetrieveText>
                    <RetrieveText
                        label="Almacen"
                        value={instance.warehouse_name}
                    ></RetrieveText>
                    <RetrieveText
                        label="Proveedor"
                        value={instance.supplier_name}
                    ></RetrieveText>
                    <RetrieveText
                        label="Cantidad de Productos"
                        value={instance.qty_items}
                    ></RetrieveText>
                    <RetrieveText
                        label="Subtotal de Productos"
                        value={instance.subtotal_price}
                    ></RetrieveText>
                    <RetrieveText
                        label="Total de impuestos"
                        value={instance.total_tax}
                    ></RetrieveText>
                    <RetrieveText
                        label="Precio Total"
                        value={instance.total_amount}
                    ></RetrieveText>
                </div>
            </div>
        )
    }
}


export default PurchaseOrderLine