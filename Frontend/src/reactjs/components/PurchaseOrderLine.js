import React from 'react'

import ItemHeader from './ItemHeader'
import CardBodyItem from './CardBodyItem'

class PurchaseOrderLine extends React.Component {

    constructor(props) {
        super(props)

        this.click_OnEdit = this.click_OnEdit.bind(this)
    }

    click_OnEdit(event) {
        event.preventDefault()
        this.props.handle_event(this.props.id)
    }

    render() {

        const {
            instance
        } = this.props

        return (
            <div className="card">
                <div className="card-header">
                    <ItemHeader
                        label="ID"
                        value={instance.id}
                    ></ItemHeader>
                </div>
                <div className="card-body">
                    <div className="card-body-items">
                        {/* <CardBodyItem
                            label="Linea"
                            value={instance.line}
                        ></CardBodyItem> */}
                        <CardBodyItem
                            label="Producto"
                            value={instance.item_name}
                        ></CardBodyItem>
                        <CardBodyItem
                            label="Cantidad"
                            value={instance.qty}
                        ></CardBodyItem>
                        <CardBodyItem
                            label="Costo unitario"
                            value={instance.price}
                        ></CardBodyItem>
                        <CardBodyItem
                            label="Subtotal"
                            value={instance.subtotal_price}
                        ></CardBodyItem>
                        <CardBodyItem
                            label="Impuesto"
                            value={instance.total_tax}
                        ></CardBodyItem>
                        <CardBodyItem
                            label="Total"
                            value={instance.total_amount}
                        ></CardBodyItem>
                    </div>
                </div>
            </div>
        )
    }
}


export default PurchaseOrderLine