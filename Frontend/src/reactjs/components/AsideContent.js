import React from 'react'

const ActionControl = (props) => {
    return (
        <div className="content-body__aside">
            {props.children}
        </div>
    )
}

export default ActionControl