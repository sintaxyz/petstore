import React, { useState, useEffect } from 'react'
import { Formik } from 'formik'
import FieldTextFormik from './FieldTextFormik'
import FieldSelectFormik from './FieldSelectFormik'
import FieldTextareaFormik from './FieldTextareaFormik'
import * as Yup from 'yup'


const ModalUpdateSchema = Yup.object().shape({
    client: Yup.string().required('No puedes dejar este campo vacio'),
    saleorder: Yup.string().required('No puedes dejar este campo vacio'),
    amount: Yup.string().required('No puedes dejar este campo vacio'),
    method: Yup.string().required('Favor de proporcionar un metodo de pago'),
})

var method_options = [
    {value: 'CA', label: 'Efectivo'},
    {value: 'TA', label: 'Tarjeta'},
    // {value: 'CH', label: 'Cheque'},
]

const build_Options = (data, display_value, property_value) => {
    
    if (display_value) {
       var property_display = display_value
    } else {
        var property_display = 'name'
    }
    if (property_value) {
       var property_value = property_value
    } else {
        var property_value = 'id'
    }
    let options = data.map((item, index, array) => {
        let new_item = {}
        new_item.value = item[property_value]
        new_item.label = item[property_display]
        return new_item
    })

    return options
}

const FormPayment = (props) => {

    const {
        submit_handle,
        clients_data,
        saleorders_data,
        branchoffices_data,
    } = props

    const clients_options = build_Options(clients_data, 'commercial_name')
    const saleorders_options = build_Options(saleorders_data, 'id')
    const branchoffices_options = build_Options(branchoffices_data)
    console.log(props);
    
    return (
        <Formik
        validationSchema={ModalUpdateSchema}
        initialValues={{
            client: "",
            saleorder: "",
            amount: "",
            method: "CA",
            branchoffice: "",
         }}
        onSubmit={submit_handle}>
        {props => (
            <form onSubmit={props.handleSubmit}>
                <div className="card-form-body">
                    <FieldSelectFormik
                        name="client"
                        title={"Cliente"}
                        options={clients_options}
                        change_handle={props.setFieldValue}
                        blur_handle={props.setFieldTouched}
                    />
                    <FieldSelectFormik
                        name="saleorder"
                        title={"Order de Venta"}
                        options={saleorders_options}
                        change_handle={props.setFieldValue}
                        blur_handle={props.setFieldTouched}
                        optional={false}
                    />
                    <FieldTextFormik
                        name="amount"
                        title={"Monto de Pago"}
                        optional={false}
                        type={"number"}
                    />
                    <FieldSelectFormik
                        name="method"
                        title={"Metodo de Pago"}
                        options={method_options}
                        change_handle={props.setFieldValue}
                        blur_handle={props.setFieldTouched}
                        optional={false}
                    />
                    <FieldSelectFormik
                        name="branchoffice"
                        title={"Sucursal"}
                        options={branchoffices_options}
                        change_handle={props.setFieldValue}
                        blur_handle={props.setFieldTouched}
                        optional={false}
                    />
                </div>
                <div className="form-header-controls">
                    <button
                        type="submit"
                        disabled={props.isSubmitting || !props.isValid}
                        className="btn btn-main">
                            Registrar Pago
                    </button>
                </div>
            </form>
        )}
        </Formik>
    )
}

export default FormPayment