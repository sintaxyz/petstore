import React from 'react'

import RetrieveText from './RetrieveText'

class DetailShrinkageHeader extends React.Component {

    constructor(props) {
        super(props)
    }

    render() {

        const {
            id,
            instance,
        } = this.props
        
        return (
            <div className="card-info-body">
                <div className="card-info-body__items">
                    <RetrieveText
                        label="ID"
                        value={instance.id}
                    ></RetrieveText>
                    <RetrieveText
                        label="Almacen"
                        value={instance.warehouse_name}
                    ></RetrieveText>
                    <RetrieveText
                        label="Registrado por:"
                        value={instance.register_by_name}
                    ></RetrieveText>
                    <RetrieveText
                        label="Tipo"
                        value={instance.type_desc}
                    ></RetrieveText>
                    <RetrieveText
                        label="Fecha"
                        value={instance.date_desc}
                    ></RetrieveText>
                    <RetrieveText
                        label="Descripcion"
                        value={instance.description}
                    ></RetrieveText>              
                </div>
            </div>
        )
    }
}


export default DetailShrinkageHeader