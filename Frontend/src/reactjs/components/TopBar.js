import React from 'react'

const TopBar = (props) => {

    return (
        <header className="header">
            <div className="bar-main-in">
                <span className="bar-main-in__title">
                    <a href={props.url}>
                        {props.title}
                    </a>
                </span>
            </div>
        </header>
    )
}

export default TopBar