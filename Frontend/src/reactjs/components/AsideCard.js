import React from 'react'

const ActionControl = (props) => {
    console.log(props);
    
    return (
        <div className="card-form">
            <div className="card-form-header">
                {props.title}
            </div>
            {props.children}
        </div>        
        
    )
}

export default ActionControl