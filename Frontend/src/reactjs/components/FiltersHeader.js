import React, { useState, useEffect } from 'react'

const FiltersHeader = (props) => {

    const {
        class_name
    } = props

    var cls_name = 'chart-filter-body'

    if (class_name){
        var cls_name = class_name
    }

    return (
        <div className={cls_name}>
            {props.children}
        </div>
    )
}

export default FiltersHeader