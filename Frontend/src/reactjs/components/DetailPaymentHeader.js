import React from 'react'

import RetrieveText from './RetrieveText'

class DetailAssignmentHeader extends React.Component {

    constructor(props) {
        super(props)
    }

    render() {

        const {
            id,
            instance,
        } = this.props
        console.log(instance);
        
        
        return (
            <div className="card-info-body">
                <div className="card-info-body__items">
                    <RetrieveText
                        label="ID"
                        value={instance.id}
                    ></RetrieveText>
                    <RetrieveText
                        label="Cliente"
                        value={instance.legal_entity}
                    ></RetrieveText>
                    <RetrieveText
                        label="Adeudo"
                        value={instance.debt}
                    ></RetrieveText>                
                </div>
            </div>
        )
    }
}


export default DetailAssignmentHeader