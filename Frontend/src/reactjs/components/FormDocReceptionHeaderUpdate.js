import React from 'react'
import { Formik } from 'formik'
import FieldTextFormik from './FieldTextFormik'
import FieldSelectFormik from './FieldSelectFormik'
import FieldTextareaFormik from './FieldTextareaFormik'
import DetailDocReceptionHeader from './DetailDocReceptionHeader'
import * as Yup from 'yup'


const ModalUpdateSchema = Yup.object().shape({
    description: Yup.string().required('No puedes dejar este campo vacio'),
    comments: Yup.string(),
})


const FormOrderHeaderUpdate = (props) => {

    const {
        submit_handle,
        instance,
    } = props
    

    return (
        <Formik
        validationSchema={ModalUpdateSchema}
        initialValues={{
            description: instance.description ? instance.description : "",
            comments: instance.comments ? instance.comments : "",
        }}
        onSubmit={submit_handle}>
        {props => (
            <form onSubmit={props.handleSubmit}>
                <DetailDocReceptionHeader
                    instance={instance}
                ></DetailDocReceptionHeader>
                <div className="card-form-body">
                    <FieldTextFormik
                        name="description"
                        title={"Descripción"}
                        optional={false}
                    />
                    <FieldTextareaFormik
                        class_name="field field--x3"
                        name="comments"
                        title={"Observaciones"}
                        optional={true}
                    />
                </div>
                <div className="form-header-controls">
                    <button
                    type="submit"
                    disabled={props.isSubmitting || !props.isValid}
                    className="btn btn-main">
                            Actualizar
                    </button>
                </div>
            </form>
        )}
        </Formik>
    )
}

export default FormOrderHeaderUpdate