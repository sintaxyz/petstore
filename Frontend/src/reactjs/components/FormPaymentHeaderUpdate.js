import React from 'react'
import { Formik } from 'formik'
import FieldTextFormik from './FieldTextFormik'
import FieldSelectFormik from './FieldSelectFormik'
import FieldMultiSelectFormik from './FieldMultiSelectFormik'
import FieldTextareaFormik from './FieldTextareaFormik'
import DetailPaymentHeader from './DetailPaymentHeader'
import * as Yup from 'yup'


const ModalUpdateSchema = Yup.object().shape({
    saleorder: Yup.string().required('No puedes dejar este campo vacio'),
    amount: Yup.string().required('No puedes dejar este campo vacio'),
    method: Yup.string().required('Favor de proporcionar un metodo de pago'),
})

var method_options = [
    {value: 'CA', label: 'Efectivo'},
    {value: 'TA', label: 'Tarjeta'},
    // {value: 'CH', label: 'Cheque'},
]

const build_OrderOptions = (data, display_value, property_value) => {
    let options = data.map((item, index, array) => {
        let new_item = {}
        new_item.value = item["id"]
        new_item.label = "$".concat(item["total_amount"], ": ", item["branchoffice_name"])
        return new_item
    })

    return options
}

const FormPaymentHeaderUpdate = (props) => {

    const {
        submit_handle,
        saleorders_data,
       
        instance,        
    } = props

    console.log(props);

    const saleorders_options = build_OrderOptions(saleorders_data)

    return (
        <Formik
        validationSchema={ModalUpdateSchema}
        initialValues={{
            saleorder: "",
            client: instance,
            amount: "",
            method: "CA",
            // end_date: "",
        }}
        onSubmit={submit_handle}>
        {props => (
            <form onSubmit={props.handleSubmit}>
                <DetailPaymentHeader
                instance={instance}
                ></DetailPaymentHeader>
                <div className="form-header-fields">                     
                    <FieldSelectFormik
                        name="saleorder"
                        title={"Order de Venta"}
                        options={saleorders_options}
                        change_handle={props.setFieldValue}
                        blur_handle={props.setFieldTouched}
                        optional={false}
                    />
                    <FieldTextFormik
                        name="amount"
                        title={"Monto de Pago"}
                        optional={false}
                        type={"number"}
                    />
                    <FieldSelectFormik
                        name="method"
                        title={"Metodo de Pago"}
                        options={method_options}
                        change_handle={props.setFieldValue}
                        blur_handle={props.setFieldTouched}
                        optional={false}
                    />                    
                </div>
                <div className="form-header-controls">
                    <button
                    type="submit"
                    disabled={props.isSubmitting || !props.isValid}
                    className="btn btn-main">
                            Guardar
                    </button>
                </div>
            </form>
        )}
        </Formik>
    )
}

export default FormPaymentHeaderUpdate