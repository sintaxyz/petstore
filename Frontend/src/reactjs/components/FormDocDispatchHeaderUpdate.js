import React from 'react'
import { Formik } from 'formik'
import FieldTextFormik from './FieldTextFormik'
import FieldSelectFormik from './FieldSelectFormik'
import FieldTextareaFormik from './FieldTextareaFormik'
import DetailDocDispatchHeader from './DetailDocDispatchHeader'
import * as Yup from 'yup'


const ModalUpdateSchema = Yup.object().shape({
    description: Yup.string().required('No puedes dejar este campo vacio'),
    comments: Yup.string(),
})

const FormDocDispatchHeaderUpdate = (props) => {

    const {
        submit_handle,
        instance,
    } = props

    console.log(instance);

    return (
        <Formik
        validationSchema={ModalUpdateSchema}
        initialValues={{
            date: instance.date,
            warehouse: instance.warehouse,
            delivery_by: instance.delivery_by,
            received_by: instance.received_by,
            description: instance.description,
            comments: instance.comments,            
        }}
        onSubmit={submit_handle}>
        {props => (
            <form onSubmit={props.handleSubmit}>
                <DetailDocDispatchHeader
                instance={instance}
                ></DetailDocDispatchHeader>
                <div className="form-header-fields">                   
                    <FieldTextFormik
                        name="description"
                        title={"Descripción"}
                        optional={false}
                    />
                    <FieldTextareaFormik
                        class_name="field field--x3"
                        name="comments"
                        title={"Observaciones"}
                        optional={true}
                    />
                </div>
                <div className="form-header-controls">
                    <button
                    type="submit"
                    disabled={props.isSubmitting || !props.isValid}
                    className="btn btn-main">
                            Actualizar
                    </button>
                </div>
            </form>
        )}
        </Formik>
    )
}

export default FormDocDispatchHeaderUpdate