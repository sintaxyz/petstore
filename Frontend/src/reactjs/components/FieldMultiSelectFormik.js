import React from 'react'
import { useField } from 'formik'
import Select from 'react-select'


const FieldSelectFormik = (props) => {

    const [field, meta] = useField(props.name)

    const {
        size,
        title,
        optional,
        helptext,
        options,
        change_handle,
        blur_handle,
        class_name,
    } = props

    let cls_name = "field"
    if (class_name) {
        cls_name = class_name
    }

    let container_class = "select"
    if (size) {
        container_class = `select select--${size}`
    }

    let field_class = "select-input"
    let label_class = "select-label"
    let show_errors = false

    if (meta.touched && meta.error) {
        show_errors = true
        label_class = "select-label select-label--error"
        // field_class = "select-input select-input--error"
    }

    const change_Option = (param) => {
        change_handle(field.name, param.value)
    }

    const blur_Handle = () => {
        blur_handle(field.name, true)
    };

    return (
        <div className={cls_name}>
            <div className={container_class} data-type="field">
                <label className={label_class}>
                    {title}
                    { optional == true &&
                        <span className="select-label-optional">
                            (opcional)
                        </span>
                    }
                </label>
                <Select
                    options={options}
                    name={field.name}
                    className={field_class}
                    classNamePrefix={field_class}
                    value={options.filter(option => option.value === field.value)}
                    onChange={change_Option}
                    onBlur={blur_Handle}
                    // isMulti
                />
                { show_errors &&
                    <div className="select-errors">
                        <ul className="select-errors-list">
                            {meta.error}
                        </ul>
                    </div>
                }
                { helptext &&
                    <div className="select-help">
                        {helptext}
                    </div>
                }
            </div>
        </div>
    )
}

export default FieldSelectFormik