import React from 'react'
import { Formik } from 'formik'
import FieldTextFormik from './FieldTextFormik'
import FieldSelectFormik from './FieldSelectFormik'
import FieldTextareaFormik from './FieldTextareaFormik'
import DetailDocAdjustmentHeader from './DetailDocAdjustmentHeader'
import * as Yup from 'yup'


const ModalUpdateSchema = Yup.object().shape({
    description: Yup.string().required('No puedes dejar este campo vacio'),
    type: Yup.string().required('No puedes dejar este campo vacio'),
    comments: Yup.string(),
})


const FormDocAdjustmentHeaderUpdate = (props) => {

    const {
        submit_handle,
        instance,
    } = props
    
    console.log(instance);

    return (
        <Formik
        validationSchema={ModalUpdateSchema}
        initialValues={{
            warehouse: instance.warehouse,
            adjusted_by: instance.adjusted_by,
            date: instance.date,
            description: instance.description,
            type: instance.type,
            comments: instance.comments,
        }}
        onSubmit={submit_handle}>
        {props => (
            <form onSubmit={props.handleSubmit}>
                <DetailDocAdjustmentHeader
                    instance={instance}
                ></DetailDocAdjustmentHeader>
                <div className="card-form-body">                    
                    <FieldTextFormik
                        name="description"
                        title={"Descripción"}
                        optional={false}
                    />
                    <FieldTextFormik
                        name="Tipo"
                        title={"type"}
                        optional={false}
                    />
                    <FieldTextareaFormik
                        class_name="field field--x3"
                        name="comments"
                        title={"Observaciones"}
                        optional={true}
                    />
                </div>
                <div className="form-header-controls">
                    <button
                    type="submit"
                    disabled={props.isSubmitting || !props.isValid}
                    className="btn btn-main">
                            Actualizar
                    </button>
                </div>
            </form>
        )}
        </Formik>
    )
}

export default FormDocAdjustmentHeaderUpdate