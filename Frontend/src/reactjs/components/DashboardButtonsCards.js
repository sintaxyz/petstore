import React, { useState, useEffect } from 'react'
import Urls from '../../app/server/urls'

const DashboardButtonsCards = (props) => {

    var buttons = [
        {label: 'Vender', url: Urls.sale_orders_new},
        {label: 'Cobrar', url: Urls.payments_new},
        {label: 'Comprar', url: Urls.purchase_order_new},
        {label: 'Articulos', url: Urls.items},
        {label: 'Recibir Pedido', url: Urls.doc_receptions_new},
    ]

    return (
        <div className='buttons-cards-body'>
            {buttons.map((item, index) => 
                <div className="button-cards-item"
                    key={index}
                    onClick={event => window.location.href=item.url}
                    >
                    <span className="button-cards-item-text">
                        {item.label}
                    </span>
                </div>
            )}
        </div>
    )
}

export default DashboardButtonsCards