import React from 'react'
import { Chart } from 'react-charts'


const MyChart = (props) => {

    const {
      title,
      empty_text,
      chart_data,
    } = props
    
    var base_data = []
    chart_data. forEach(line => {
      base_data.push({
        label: line.title,
        data: line.data
      })
    });
    
    const series = React.useMemo(
        () => ({
          showPoints: true
        }),
        []
      )

    const data = React.useMemo(
        () => base_data
      )
    
    
    const axes = React.useMemo(
      () => [
        {
            primary: true,
            type: 'ordinal',
            position: 'bottom',
            stacked:true,
        },
        { 
            type: 'linear',
            position: 'left',
            stacked: false,
            hardMin: 0,
        }
      ],
      []
    )

    function chart(){
      if(chart_data.length > 0){
        return <Chart data={data} axes={axes} series={series} tooltip />
      } else {
        return empty_text
      }
    }

    var chart_element = chart()
    
    return (
      <div className="chart-card-section">
          <div className="chart-card-title">
              {title}
          </div>
          {props.children}
          <div className="chart-card">
            {chart_element}
          </div>
      </div>
    )
  
}

  export default MyChart