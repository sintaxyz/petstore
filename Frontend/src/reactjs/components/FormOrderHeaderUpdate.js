import React from 'react'
import { Formik } from 'formik'
import FieldTextFormik from './FieldTextFormik'
import FieldSelectFormik from './FieldSelectFormik'
import FieldTextareaFormik from './FieldTextareaFormik'
import DetailOrderHeader from './DetailOrderHeader'
import * as Yup from 'yup'


const ModalUpdateSchema = Yup.object().shape({
    paymethod: Yup.string().required('Favor de proporcionar impuesto'),
    credit_days: Yup.number().when('paymethod', {
        is: 'CR',
        then: Yup.number()
          .required('Requerido con metodo de pago a Credito.'),
        otherwise: Yup.number(),
    }),
    description: Yup.string(),
    comments: Yup.string(),
})

var paymethod_options = [
    {value: 'CA', label: 'Efectivo'},
    {value: 'TA', label: 'Tarjeta'},
    {value: 'CR', label: 'Credito'},
]

const on_ChangePaymenthod = (field, value, setFieldValue) => {
        setFieldValue(field, value)
        if (value != 'CR') {
        setFieldValue('credit_days', 0)
    }
    console.log('field', field, 'value', value);
}

const FormOrderHeaderUpdate = (props) => {

    const {
        submit_handle,
        instance,
    } = props
    console.log(instance);
    

    return (
        <Formik
        validationSchema={ModalUpdateSchema}
        initialValues={{
            description: instance.description ? instance.description : "",
            comments: instance.comments,
            paymethod: instance.paymethod,
            credit_days: instance.credit_days ? instance.credit_days : 0,
        }}
        onSubmit={submit_handle}>
        {props => (
            <form onSubmit={props.handleSubmit}>
                <DetailOrderHeader
                instance={instance}
                ></DetailOrderHeader>
                <div className="card-form-body">
                    <FieldSelectFormik
                        name="paymethod"
                        title={"Metodo de Pago"}
                        options={paymethod_options}
                        change_handle={
                            (field, value) => on_ChangePaymenthod(field, value, props.setFieldValue)
                        }
                        blur_handle={props.setFieldTouched}   
                    />
                    { props.values.paymethod == 'CR' &&
                        <FieldTextFormik
                            name="credit_days"
                            type={"number"}
                            title={"Días de Credito"}
                            optional={false}
                        />
                    }
                    <FieldTextFormik
                        name="description"
                        title={"Descripción"}
                        optional={false}
                    />
                    <FieldTextareaFormik
                        class_name="field field--x3"
                        name="comments"
                        title={"Observaciones"}
                        optiona={true}
                    />
                    
                </div>
                <div className="content-form-controls">
                    <div className="content-form-controls__right">
                        <button
                        type="submit"
                        disabled={props.isSubmitting || !props.isValid}
                        className="btn btn-main">
                                Actualizar
                        </button>
                    </div>
                </div>
            </form>
        )}
        </Formik>
    )
}

export default FormOrderHeaderUpdate