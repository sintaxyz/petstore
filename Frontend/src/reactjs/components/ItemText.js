import React from 'react'

class ItemText extends React.Component {

    constructor(props) {
        super(props)
    }

    render() {

        const {
            value,
            label,
        } = this.props

        return(
            <div className="item-text">
                <div className="item-text-label">
                    {label}
                </div>
                <div className="item-text-data">
                    <div className="data-value">
                        <div className="value">
                            {value}
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

export default ItemText