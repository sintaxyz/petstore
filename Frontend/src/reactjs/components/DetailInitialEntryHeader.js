import React from 'react'

import RetrieveText from './RetrieveText'

class DetailInitialEntryHeader extends React.Component {

    constructor(props) {
        super(props)
    }

    render() {

        const {
            id,
            instance,
        } = this.props
        
        return (
            <div className="card-info-body">
                <div className="card-info-body__items">
                    <RetrieveText
                        label="ID"
                        value={instance.id}
                    ></RetrieveText>
                    <RetrieveText
                        label="Almacen"
                        value={instance.warehouse_name}
                    ></RetrieveText>
                    <RetrieveText
                        label="Ingresado por:"
                        value={instance.entry_by_name}
                    ></RetrieveText>
                    <RetrieveText
                        label="Fecha"
                        value={instance.date_desc}
                    ></RetrieveText>
                    <RetrieveText
                        label="Cantidad de Productos"
                        value={instance.qty_items}
                    ></RetrieveText>
                    <RetrieveText
                        label="Costo Total"
                        value={instance.total_amount}
                    ></RetrieveText>
                </div>
            </div>
        )
    }
}


export default DetailInitialEntryHeader