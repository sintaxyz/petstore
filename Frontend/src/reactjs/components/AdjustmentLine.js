import React from 'react'

import ItemHeader from './ItemHeader'
import ItemText from './ItemText'
import CardBodyItem from './CardBodyItem'

class AdjustmentLine extends React.Component {

    constructor(props) {
        super(props)

        this.click_OnEdit = this.click_OnEdit.bind(this)
    }

    click_OnEdit(event) {
        event.preventDefault()
        this.props.handle_event(this.props.id)
    }

    render() {

        const {
            instance
        } = this.props

        return (
            <div className="card">
                <div className="card-header">
                    <ItemHeader
                        label="ID"
                        value={instance.id}
                    ></ItemHeader>
                </div>
                <div className="card-body">
                    <div className="card-body-items">
                        <CardBodyItem
                            label="Producto"
                            value={instance.item_name}
                        ></CardBodyItem>
                        <CardBodyItem
                            label="Linea"
                            value={instance.line}
                        ></CardBodyItem>
                        <CardBodyItem
                            label="Cantidad Ajustada"
                            value={instance.qty_new}
                        ></CardBodyItem>
                        <CardBodyItem
                            label="Cantidad Anterior"
                            value={instance.qty_old}
                        ></CardBodyItem>                                        
                    </div>
                </div>
            </div>
        )
    }
}


export default AdjustmentLine