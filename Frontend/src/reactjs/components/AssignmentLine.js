import React from 'react'

import ItemHeader from './ItemHeader'
import ItemText from './ItemText'
import CardBodyItem from './CardBodyItem'

class AssignmentLine extends React.Component {

    constructor(props) {
        super(props)

        this.click_OnEdit = this.click_OnEdit.bind(this)
    }

    click_OnEdit(event) {
        event.preventDefault()
        this.props.handle_event(this.props.id)
    }

    render() {

        const {
            instance
        } = this.props

        return (
            <div className="card">
                <div className="card-header">
                    <ItemHeader
                        label="ID"
                        value={instance.id}
                    ></ItemHeader>
                </div>
                <div className="card-body">
                    <div className="card-body-items">
                        <CardBodyItem
                            label="Posición"
                            value={instance.position_name}
                        ></CardBodyItem>
                        <CardBodyItem
                            label="Sucursales"
                            value={instance.branches_desc}
                        ></CardBodyItem>
                        <CardBodyItem
                            label="Empleado"
                            value={instance.user_name}
                        ></CardBodyItem>
                        <CardBodyItem
                            label="Fecha de inicio"
                            value={instance.start_date_desc}
                        ></CardBodyItem>
                        <CardBodyItem
                            label="Fecha de Finalización"
                            value={instance.end_date_desc}
                        ></CardBodyItem>
                        <CardBodyItem
                            label="Activo"
                            value={instance.status_text}
                        ></CardBodyItem>
                    </div>
                </div>
            </div>
        )
    }
}


export default AssignmentLine