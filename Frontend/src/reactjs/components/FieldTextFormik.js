import React from 'react'
import { useField } from 'formik'

const FieldTextFormik = (props) => {

    const [field, meta] = useField(props)

    const {
        size,
        title,
        placeholder,
        optional,
        helptext,
        class_name,
        type,
    } = props

    let cls_name = "field"
    if (class_name) {
        cls_name = class_name
    }

    let container_class = "text"
    if (size) {
        container_class = `text text--${size}`
    }

    let field_class = "text-input"
    let label_class = "text-label"
    let show_errors = false

    if (meta.touched && meta.error) {
        show_errors = true
        field_class = "text-input text-input--error"
        label_class = "text-label text-label--error"
    }

    let type_field = "text"
    if (type) {
        type_field = type
    }

    return (
        <div className={cls_name} data-type="field">
            <div className={container_class}>
                <label className={label_class}>
                    {title}
                    { optional == true &&
                        <span className="text-label-optional">
                            (opcional)
                        </span>
                    }
                </label>
                <input
                    type={type_field}
                    className={field_class}
                    {...field}
                    placeholder={placeholder}
                />
                { show_errors &&
                    <div className="text-errors">
                        <ul className="text-errors-list">
                            {meta.error}
                        </ul>
                    </div>
                }
                { helptext &&
                    <div className="text-help">
                        {helptext}
                    </div>
                }
            </div>
        </div>
    )
}

export default FieldTextFormik