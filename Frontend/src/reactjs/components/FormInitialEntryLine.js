import React from 'react'
import { Formik } from 'formik'
import FieldTextFormik from './FieldTextFormik'
import FieldSelectFormik from './FieldSelectFormik'
import FieldTextareaFormik from './FieldTextareaFormik'
import * as Yup from 'yup'


const ModalUpdateSchema = Yup.object().shape({
    item: Yup.string().required('No puedes dejar este campo vacio'),
    qty: Yup.string().required('No puedes dejar este campo vacio'),
    minimun_qty: Yup.string().required('No puedes dejar este campo vacio'),
    price: Yup.string().required('No puedes dejar este campo vacio'),
    sell_price: Yup.string().required('No puedes dejar este campo vacio'),
    comments: Yup.string(),
})

const build_Options = (data, display_value, property_value) => {
    if (display_value) {
       var property_display = display_value
    } else {
        var property_display = 'name'
    }
    if (property_value) {
       var property_value = property_value
    } else {
        var property_value = 'id'
    }
    let options = data.map((item, index, array) => {
        let new_item = {}
        new_item.value = item[property_value]
        new_item.label = item[property_display]
        return new_item
    })

    return options
}


const FormInitialEntryHeader = (props) => {

    const {
        submit_handle,
        item_data,
    } = props

    const item_options = build_Options(item_data)

    return (
        <Formik
        validationSchema={ModalUpdateSchema}
        initialValues={{
            item: "",
            qty: "",
            minimun_qty: "10",
            price: "",
            sell_price: "",
            comments: "",
         }}
        onSubmit={submit_handle}>
        {props => (
            <form onSubmit={props.handleSubmit}>
                <div className="card-form-body">                    
                    <FieldSelectFormik
                        name="item"
                        title={"Producto"}
                        options={item_options}
                        change_handle={props.setFieldValue}
                        blur_handle={props.setFieldTouched}
                    />
                    <FieldTextFormik
                        type="number"
                        name="qty"
                        title={"Cantidad"}
                        optional={false}
                    />
                    <FieldTextFormik
                        type="number"
                        name="minimun_qty"
                        title={"Minimo en almacen"}
                        optional={false}
                    />
                    <FieldTextFormik
                        name="price"
                        title={"Precio Unitario"}
                        optional={false}
                    />
                    <FieldTextFormik
                        name="sell_price"
                        title={"Precio de Venta"}
                        optional={false}
                    />
                </div>
                <div className="card-form-body">
                    <FieldTextareaFormik
                        name="comments"
                        title={"Comentarios"}
                        optional={true}
                    />
                </div>
                <div className="form-header-controls">
                    <div className="form-header-controls__right">
                        <button
                        type="submit"
                        disabled={props.isSubmitting || !props.isValid}
                        className="btn btn-main">
                                Agregar
                        </button>
                    </div>
                </div>
            </form>
        )}
        </Formik>
    )
}

export default FormInitialEntryHeader