import React from 'react'

class RetrieveText extends React.Component {

    constructor(props) {
        super(props)
    }

    render() {

        const {
            value,
            label,
        } = this.props

        return(
            <div className="item item--x1">
            <div className="item-label">
                {label}
            </div>
            <div className="item-data">
                <div className="data-value">
                    {value}
                </div>
            </div>
        </div>
        )
    }
}

export default RetrieveText