import React from 'react'
import { Chart } from 'react-charts'


const CardDashboard = (props) => {

    const {
        title,
        data,
    } = props
    
    return (
      <div className="chart-card-section">
          <div className="chart-card-title">
          </div>
          <div className="chart-card">
            <div className="chart-card-empty">
                <div className="chart-card-result">
					<div className="chart-result-text">
						Total Ingresos: 
					</div>
					<div className="chart-result-number">
						$ {data.payments}</div>
					</div>
                <div className="chart-card-result">
					<div className="chart-result-text">
						Total Egresos: 
					</div>
					<div className="chart-result-number">
						$ {data.expenses}</div>
					</div>
                <div className="chart-card-result separator">
					<div className="chart-result-text">
						Utilidad: 
					</div>
					<div className="chart-result-number">
						$ {data.utility}</div>
					</div>
            </div>
          </div>
      </div>
    )
  
}

  export default CardDashboard