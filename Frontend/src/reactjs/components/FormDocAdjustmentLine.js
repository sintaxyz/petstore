import React from 'react'
import { Formik } from 'formik'
import FieldTextFormik from './FieldTextFormik'
import FieldSelectFormik from './FieldSelectFormik'
import FieldTextareaFormik from './FieldTextareaFormik'
import RetrieveText from './RetrieveText'

import * as Yup from 'yup'


const ModalUpdateSchema = Yup.object().shape({
    line: Yup.string(),
    item: Yup.string().required('No puedes dejar este campo vacio'),
    qty_new: Yup.string().required('No puedes dejar este campo vacio'),
    comments: Yup.string()
})

const build_ItemOptions = (data, display_value, property_value) => {
    let options = data.map((item, index, array) => {
        let new_item = {}
        new_item.value = item["id"]
        new_item.label = "$".concat(item["sell_price"], ": ", item["item_name"])
        return new_item
    })

    return options
}

const FormDocAdjustmentHeader = (props) => {

    const {
        submit_handle,
        item_data,
    } = props

    const item_options = build_ItemOptions(item_data)    

    return (
        <Formik
        validationSchema={ModalUpdateSchema}
        initialValues={{
            line: "",
            item: "",
            qty_new: "",
            comments: "",
         }}
        onSubmit={submit_handle}>
        {props => (
            <form onSubmit={props.handleSubmit}>
                <div className="card-form-body">                    
                    <FieldSelectFormik
                        name="item"
                        title={"Producto"}
                        options={item_options}
                        change_handle={props.setFieldValue}
                        blur_handle={props.setFieldTouched}
                    />
                    <FieldTextFormik
                        name="qty_new"
                        title={"Cantidad"}
                        optional={false}
                    />                    
                </div>
                <div className="card-form-body">
                    <FieldTextareaFormik
                        name="comments"
                        title={"Comentarios"}
                        optional={true}
                    />
                </div>
                <div className="form-header-controls">
                    <button
                    type="submit"
                    disabled={props.isSubmitting || !props.isValid}
                    className="btn btn-main">
                            Agregar
                    </button>
                </div>
            </form>
        )}
        </Formik>
    )
}

export default FormDocAdjustmentHeader