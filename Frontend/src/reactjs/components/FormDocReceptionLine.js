import React from 'react'
import { Formik } from 'formik'
import FieldTextFormik from './FieldTextFormik'
import FieldSelectFormik from './FieldSelectFormik'
import FieldTextareaFormik from './FieldTextareaFormik'
import ItemHeader from './ItemHeader'
import RetrieveText from './RetrieveText'

import * as Yup from 'yup'


const ModalUpdateSchema = Yup.object().shape({
    qty_received: Yup.string().required('No puedes dejar este campo vacio'),
    comments: Yup.string()
})

const FormOrderHeader = (props) => {

    const {
        submit_handle,
        instance,
    } = props

    console.log(instance);
    

    return (
        <Formik
        validationSchema={ModalUpdateSchema}
        initialValues={{
            qty_received: instance.qty_received,
            comments: "",
         }}
        onSubmit={(Values) => {submit_handle(instance.id, Values)}}>
        {props => (
            <form onSubmit={props.handleSubmit} className="card-form">
                <div className="card-info-body">
                    <div className="card-info-body__items">
                        <RetrieveText
                            label="Artículo"
                            value={instance.item_name}
                        ></RetrieveText>
                        <RetrieveText
                            label="Precio en Orden"
                            value={instance.order_price}
                        ></RetrieveText>
                        <RetrieveText
                            label="Requerido"
                            value={instance.required}
                        ></RetrieveText>
                        <RetrieveText
                            label="Recibido"
                            value={instance.received}
                        ></RetrieveText>
                        <RetrieveText
                            label="Pendiente"
                            value={instance.pending}
                        ></RetrieveText>
                    </div>
                </div>
                <div className="card-form-body">
                    <FieldTextFormik
                        name="qty_received"
                        title={"Cantidad Recibida"}
                        optional={false}
                    />
                    <FieldTextFormik
                        name="comments"
                        title={"Comentarios"}
                        optional={true}
                    />
                </div>
                <div className="form-header-controls">
                    <button
                    type="submit"
                    disabled={props.isSubmitting || !props.isValid}
                    className="btn btn-main">
                            Guardar Cambio
                    </button>
                </div>
            </form>
        )}
        </Formik>
    )
}

export default FormOrderHeader