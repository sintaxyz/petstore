import React from 'react'


class ItemAction extends React.Component {

    render() {

        const {
            icon,
            tag,
            handle_event
        } = this.props

        return(           
        
            <button
            onClick={handle_event}
            className="btn-cancel">
                {icon}                
                {tag}
            </button>
        
        )
    }
}

export default ItemAction
