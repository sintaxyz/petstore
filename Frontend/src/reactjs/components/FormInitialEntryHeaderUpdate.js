import React from 'react'
import { Formik } from 'formik'
import FieldTextFormik from './FieldTextFormik'
import FieldSelectFormik from './FieldSelectFormik'
import FieldTextareaFormik from './FieldTextareaFormik'
import DetailInitialEntryHeader from './DetailInitialEntryHeader'
import * as Yup from 'yup'


const ModalUpdateSchema = Yup.object().shape({
    description: Yup.string().required('No puedes dejar este campo vacio'),
    comments: Yup.string(),
})

const FormInitialEntryHeaderUpdate = (props) => {

    const {
        submit_handle,
        instance,
    } = props

    return (
        <Formik
        validationSchema={ModalUpdateSchema}
        initialValues={{
            date: instance.date,
            warehouse: instance.warehouse,
            entry_by: instance.entry_by,
            description: instance.description,
            comments: instance.comments,
         }}
        onSubmit={submit_handle}>
        {props => (
            <form onSubmit={props.handleSubmit}>
                <DetailInitialEntryHeader
                instance={instance}
                ></DetailInitialEntryHeader>
                <div className="form-header-fields">
                    <FieldTextFormik
                        name="description"
                        title={"Descripción"}
                        optional={false}
                    />
                    <FieldTextareaFormik
                        class_name="field field--x3"
                        name="comments"
                        title={"Observaciones"}
                        optional={true}
                    />
                </div>
                <div className="form-header-controls">
                    <button
                    type="submit"
                    disabled={props.isSubmitting || !props.isValid}
                    className="btn btn-main">
                            Continuar
                    </button>
                </div>
            </form>
        )}
        </Formik>
    )
}

export default FormInitialEntryHeaderUpdate