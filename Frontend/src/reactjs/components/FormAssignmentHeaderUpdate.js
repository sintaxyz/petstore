import React from 'react'
import { Formik } from 'formik'
import FieldTextFormik from './FieldTextFormik'
import FieldSelectFormik from './FieldSelectFormik'
import FieldMultiSelectFormik from './FieldMultiSelectFormik'
import FieldTextareaFormik from './FieldTextareaFormik'
import DetailAssignmentHeader from './DetailAssignmentHeader'
import * as Yup from 'yup'


const ModalUpdateSchema = Yup.object().shape({
    position: Yup.string().required('Por favor ingrese una posición'),
    // branches: Yup.string(),
    // .required('Por favor intrese sucursal o sucursales'),
    // is_active: Yup.string().required('No puedes dejar esta campo vacio'),
    start_date: Yup.string().required('Por favor ingrese fecha de inicio'),
    // start_date: Yup.string(),
    end_date: Yup.string(),
})

const build_Options = (instance, display_value, property_value) => {
    if (display_value) {
       var property_display = display_value
    } else {
        var property_display = 'name'
    }
    if (property_value) {
       var property_value = property_value
    } else {
        var property_value = 'id'
    }
    let options = instance.map((item, index, array) => {
        let new_item = {}
        new_item.value = item[property_value]
        new_item.label = item[property_display]
        return new_item
    })

    return options
}

const FormAssignmentHeaderUpdate = (props) => {

    const {
        submit_handle,
        position_data,
        branches_data,
        user_data,
        instance,        
    } = props

    console.log(props);
    console.log(user_data.user);
    

    const position_options = build_Options(position_data)
    const branches_options = build_Options(branches_data)

    return (
        <Formik
        validationSchema={ModalUpdateSchema}
        initialValues={{
            position: "",
            // branches: "",
            user: user_data.user,
            company: instance.id,
            // is_active: instance.is_active,
            start_date: "",
            end_date: "",
        }}
        onSubmit={submit_handle}>
        {props => (
            <form onSubmit={props.handleSubmit}>
                <DetailAssignmentHeader
                company={instance}
                user={user_data}
                ></DetailAssignmentHeader>
                <div className="form-header-fields"> 
                    <FieldSelectFormik
                        name="position"
                        title={"Puestos"}
                        options={position_options}
                        change_handle={props.setFieldValue}
                        blur_handle={props.setFieldTouched}
                    />
                    {/* <FieldMultiSelectFormik
                        name="branches"
                        title={"sucursales"}
                        options={branches_options}
                        change_handle={props.setFieldValue}
                        blur_handle={props.setFieldTouched}
                    />                   */}
                    <FieldTextFormik
                        name="start_date"
                        title={"Fecha de inicio"}
                        placeholder={"AAAA-MM-DD HH:MM"}
                        optional={false}
                    />
                    <FieldTextFormik
                        name="end_date"
                        title={"Fecha de finalizacion"}
                        placeholder={"AAAA-MM-DD HH:MM"}
                        optional={true}
                    />
                </div>
                <div className="form-header-controls">
                    <button
                    type="submit"
                    disabled={props.isSubmitting || !props.isValid}
                    className="btn btn-main">
                            Guardar
                    </button>
                </div>
            </form>
        )}
        </Formik>
    )
}

export default FormAssignmentHeaderUpdate