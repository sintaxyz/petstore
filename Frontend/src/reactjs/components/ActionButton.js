import React from 'react'


class ItemAction extends React.Component {

    render() {

        const {
            tag,
            handle_event
        } = this.props

        return(
            <div className="actions-item">
                <a className="btn-action" href="#" onClick={handle_event}>
                    <span className="btn-a-text">
                        {tag}
                    </span>
                </a>
            </div>
        )
    }
}

export default ItemAction