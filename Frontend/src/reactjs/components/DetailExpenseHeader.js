import React from 'react'

import RetrieveText from './RetrieveText'

class DetailExpenseHeader extends React.Component {

    constructor(props) {
        super(props)
    }

    render() {

        const {
            id,
            instance,
        } = this.props
        console.log(instance);
        
        
        return (
            <div className="card-info-body">
                <div className="card-info-body__items">
                    <RetrieveText
                        label="ID"
                        value={instance.id}
                    ></RetrieveText>
                    <RetrieveText
                        label="Compañia"
                        value={instance.legal_entity}
                    ></RetrieveText>                                    
                </div>
            </div>
        )
    }
}


export default DetailExpenseHeader