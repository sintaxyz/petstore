import React from 'react'
import { Chart } from 'react-charts'


const MyChart = (props) => {

    const {
      title,
      item_type,
      empty_text,
      chart_data,
    } = props
    
    const series = React.useMemo(
        () => ({
          type: 'bar'
        }),
        []
      )

    const data = React.useMemo(
        () => [
          {
            label: item_type,
            data: chart_data
          }
        ]
      )
    
  
    const axes = React.useMemo(
      () => [
        {
            primary: true,
            type: 'ordinal',
            position: 'bottom',
            stacked:true,
        },
        { 
            type: 'linear',
            position: 'left',
            stacked: false,
            hardMin: 0,
        }
      ],
      []
    )

    function chart(){
      if(chart_data.length > 0){
        return <Chart data={data} axes={axes} series={series} tooltip />
      } else {
        return empty_text
      }
    }

    var chart_element = chart()
    
    return (
      <div className="chart-card-section">
          <div className="chart-card-title">
              {title}
          </div>
          {props.children}
          <div className="chart-card">
            {chart_element}
          </div>
      </div>
    )
  }

  export default MyChart