import React from 'react'
import { Formik } from 'formik'
import FieldTextFormik from './FieldTextFormik'
import FieldSelectFormik from './FieldSelectFormik'
import FieldTextareaFormik from './FieldTextareaFormik'
import * as Yup from 'yup'


const ModalUpdateSchema = Yup.object().shape({
    item: Yup.string().required('No puedes dejar este campo vacio'),
    qty: Yup.string().required('No puedes dejar este campo vacio'),
    price: Yup.string().required('No puedes dejar este campo vacio'),
    tax: Yup.string().required('No puedes dejar este campo vacio'),
    comments: Yup.string(),
})

const build_Options = (data, display_value, property_value) => {
    if (display_value) {
       var property_display = display_value
    } else {
        var property_display = 'name'
    }
    if (property_value) {
       var property_value = property_value
    } else {
        var property_value = 'id'
    }
    let options = data.map((item, index, array) => {
        let new_item = {}
        new_item.value = item[property_value]
        new_item.label = item[property_display]
        return new_item
    })

    return options
}


const FormOrderLine = (props) => {

    const {
        submit_handle,
        item_data,
        tax_data,
        header,
    } = props
    console.log(header);
    
    const item_options = build_Options(item_data, 'item_name')
    const tax_options = build_Options(tax_data, 'description')

    if (header) {
        if (header.tax_abbreviation) {
            var tax_abbr = header.tax_abbreviation
        } else {
            var tax_abbr = ""
        }
    } else {
        var tax_abbr = ""
    }
    console.log(tax_abbr);
    
    return (
        <Formik
        validationSchema={ModalUpdateSchema}
        initialValues={{
            item: "",
            qty: "",
            price: "",
            tax: {tax_abbr},
            comments: "",
         }}
        onSubmit={submit_handle}>
        {props => (
            <form onSubmit={props.handleSubmit}>
                <div className="card-form-body">
                    <FieldSelectFormik
                        name="item"
                        title={"Producto"}
                        options={item_options}
                        change_handle={props.setFieldValue}
                        blur_handle={props.setFieldTouched}
                    />
                    <FieldTextFormik
                        name="qty"
                        title={"Cantidad"}
                        optional={false}
                    />
                    <FieldTextFormik
                        name="price"
                        title={"Costo unitario"}
                        optional={false}
                    />
                    <FieldSelectFormik
                        name="tax"
                        title={"Impuesto"}
                        options={tax_options}
                        change_handle={props.setFieldValue}
                        blur_handle={props.setFieldTouched}
                    />
                    <FieldTextareaFormik
                        name="comments"
                        title={"Comentarios"}
                        optional={true}
                    />
                </div>
                <div className="form-header-controls">
                    <button
                    type="submit"
                    disabled={props.isSubmitting || !props.isValid}
                    className="btn btn-main">
                            Continuar
                    </button>
                </div>
            </form>
        )}
        </Formik>
    )
}

export default FormOrderLine