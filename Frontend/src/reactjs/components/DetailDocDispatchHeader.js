import React from 'react'

import RetrieveText from './RetrieveText'

class DetailDocDispatchHeader extends React.Component {

    constructor(props) {
        super(props)
    }

    render() {

        const {
            id,
            instance,
        } = this.props
        
        return (
            <div className="card-info-body">
                <div className="card-info-body__items">
                    <RetrieveText
                        label="ID"
                        value={instance.id}
                    ></RetrieveText>
                    <RetrieveText
                        label="Almacen"
                        value={instance.warehouse_name}
                    ></RetrieveText>
                    <RetrieveText
                        label="Entregado por:"
                        value={instance.delivery_by_name}
                    ></RetrieveText>
                    <RetrieveText
                        label="Recibido por:"
                        value={instance.received_by_name}
                    ></RetrieveText>
                    <RetrieveText
                        label="Fecha"
                        value={instance.date_desc}
                    ></RetrieveText>  
                    <RetrieveText
                        label="Descripcion"
                        value={instance.description}
                    ></RetrieveText>                    
                </div>
            </div>
        )
    }
}


export default DetailDocDispatchHeader