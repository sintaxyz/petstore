import React from 'react'

import RetrieveText from './RetrieveText'

class PurchaseOrderLine extends React.Component {

    constructor(props) {
        super(props)
    }

    render() {

        const {
            id,
            instance,
        } = this.props
        console.log(instance);
        
        return (
            <div className="card-info-body">
                <div className="card-info-body__items">
                    <RetrieveText
                        label="ID"
                        value={instance.id}
                    ></RetrieveText>
                    <RetrieveText
                        label="Almacen"
                        value={instance.warehouse_name}
                    ></RetrieveText>
                    <RetrieveText
                        label="Proveedor"
                        value={instance.supplier_name}
                    ></RetrieveText>
                    <RetrieveText
                        label="Fecha"
                        value={instance.date_desc}
                    ></RetrieveText>
                </div>
            </div>
        )
    }
}


export default PurchaseOrderLine