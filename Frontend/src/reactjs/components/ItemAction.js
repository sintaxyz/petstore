import React from 'react'


class ItemAction extends React.Component {

    render() {

        const {
            tag,
            handle_event
        } = this.props

        return(
            <div className="card-body-actions">
                <div className="card-body-actions-item">
                    <a className="btn-action" href="#" onClick={handle_event}>
                        {this.props.children}
                        <span className="btn-a-text">
                            {tag}
                        </span>
                    </a>
                </div>
            </div>
        )
    }
}

export default ItemAction