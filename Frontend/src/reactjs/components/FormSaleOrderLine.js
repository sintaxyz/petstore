import React from 'react'
import { Formik } from 'formik'
import FieldTextFormik from './FieldTextFormik'
import FieldSelectFormik from './FieldSelectFormik'
import FieldTextareaFormik from './FieldTextareaFormik'
import * as Yup from 'yup'


const ModalUpdateSchema = Yup.object().shape({
    item: Yup.string().required('No puedes dejar este campo vacio'),
    qty: Yup.string().required('No puedes dejar este campo vacio'),
    tax: Yup.string().required('No puedes dejar este campo vacio'),
    comments: Yup.string(),
})

const build_Options = (data, display_value, property_value) => {
    if (display_value) {
       var property_display = display_value
    } else {
        var property_display = 'name'
    }
    if (property_value) {
       var property_value = property_value
    } else {
        var property_value = 'id'
    }
    let options = data.map((item, index, array) => {
        let new_item = {}
        new_item.value = item[property_value]
        new_item.label = item[property_display]
        return new_item
    })

    return options
}

const build_ItemOptions = (data, display_value, property_value) => {
    let options = data.map((item, index, array) => {
        let new_item = {}
        new_item.value = item["id"]
        new_item.label = "$".concat(item["sell_price"], ": ", item["item_name"])
        return new_item
    })

    return options
}


const FormSaleOrderHeader = (props) => {

    const {
        submit_handle,
        item_data,
        tax_data,
    } = props

    const item_options = build_ItemOptions(item_data)
    const tax_options = build_Options(tax_data, 'description')

    return (
        <Formik
        validationSchema={ModalUpdateSchema}
        initialValues={{
            item: "",
            qty: "",
            tax: "",
            comments: "",
         }}
        onSubmit={submit_handle}>
        {props => (
            <form onSubmit={props.handleSubmit}>
                <div className="card-form-body">
                    <FieldSelectFormik
                        name="item"
                        title={"Producto"}
                        options={item_options}
                        change_handle={props.setFieldValue}
                        blur_handle={props.setFieldTouched}
                    />
                    <FieldSelectFormik
                        name="tax"
                        title={"Impuesto"}
                        options={tax_options}
                        change_handle={props.setFieldValue}
                        blur_handle={props.setFieldTouched}
                    />
                    <FieldTextFormik
                        name="qty"
                        title={"Cantidad"}
                        optional={false}
                    />
                    <FieldTextareaFormik
                        name="comments"
                        title={"Comentarios"}
                        optional={true}
                    />
                </div>
                <div className="form-header-controls">
                    <button
                    type="submit"
                    disabled={props.isSubmitting || !props.isValid}
                    className="btn btn-main">
                            Agregar
                    </button>
                </div>
            </form>
        )}
        </Formik>
    )
}

export default FormSaleOrderHeader