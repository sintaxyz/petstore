import React from 'react'
import RetrieveText from './RetrieveText'

class DetailSaleOrderHeader extends React.Component {

    constructor(props) {
        super(props)
    }

    render() {

        const {
            id,
            instance,
        } = this.props
        
        return (
            <div className="card-info-body">
                <div className="card-info-body__items">
                    <RetrieveText
                        label="ID"
                        value={instance.id}
                    ></RetrieveText>
                    <RetrieveText
                        label="Almacen"
                        value={instance.warehouse_name}
                    ></RetrieveText>
                    <RetrieveText
                        label="Sucursal"
                        value={instance.branchoffice_name}
                    ></RetrieveText>
                    <RetrieveText
                        label="Cantidad de Productos"
                        value={instance.qty_items}
                    ></RetrieveText>
                    <RetrieveText
                        label="Impuesto"
                        value={instance.tax_desc}
                    ></RetrieveText>
                    <RetrieveText
                        label="Subtotal de articulos"
                        value={instance.subtotal_price}
                    ></RetrieveText>
                    <RetrieveText
                        label="Total de impuesto en articulos"
                        value={instance.total_tax}
                    ></RetrieveText>
                    <RetrieveText
                        label="Precio Total"
                        value={instance.total_amount}
                    ></RetrieveText>
                    <RetrieveText
                        label="Cliente"
                        value={instance.client_name}
                    ></RetrieveText>                    
                </div>
            </div>
        )
    }
}


export default DetailSaleOrderHeader