import React from 'react'
import { Formik } from 'formik'
import FieldTextFormik from './FieldTextFormik'
import FieldSelectFormik from './FieldSelectFormik'
import FieldTextareaFormik from './FieldTextareaFormik'
import DetailShrinkageHeader from './DetailShringkageHeader'
import * as Yup from 'yup'


const ModalUpdateSchema = Yup.object().shape({
    description: Yup.string(),
    comments: Yup.string(),
    type: Yup.string().required('No puedes dejar este campo vacio'),
})

var type_options = [
    {value: 'ADM', label: 'Administrativa'},
    {value: 'OPE', label: 'Operativa'},
    {value: 'NAT', label: 'Natural'},
    {value: 'PRO', label: 'Producción'},
]

const FormShrinkageHeaderUpdate = (props) => {

    const {
        submit_handle,
        instance,
    } = props
    

    return (
        <Formik
        validationSchema={ModalUpdateSchema}
        initialValues={{
            date: instance.date,
            warehouse: instance.warehouse,
            register_by: instance.register_by,
            description: instance.description,
            comments: instance.comments,
            type: instance.type,           
        }}
        onSubmit={submit_handle}>
        {props => (
            <form onSubmit={props.handleSubmit}>
                <DetailShrinkageHeader
                    instance={instance}
                ></DetailShrinkageHeader>
                <div className="card-form-body">
                    <FieldSelectFormik
                        name="type"
                        title={"Tipo"}
                        options={type_options}
                        change_handle={props.setFieldValue}
                        blur_handle={props.setFieldTouched}
                    />                    
                    <FieldTextFormik
                        name="description"
                        title={"Descripción"}
                        optional={false}
                    />
                    <FieldTextareaFormik
                        class_name="field field--x3"
                        name="comments"
                        title={"Observaciones"}
                        optional={true}
                    />
                </div>
                <div className="form-header-controls">
                    <button
                    type="submit"
                    disabled={props.isSubmitting || !props.isValid}
                    className="btn btn-main">
                            Actualizar
                    </button>
                </div>
            </form>
        )}
        </Formik>
    )
}

export default FormShrinkageHeaderUpdate