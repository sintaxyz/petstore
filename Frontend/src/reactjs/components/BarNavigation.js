import React from 'react';
import { library } from '@fortawesome/fontawesome-svg-core'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faChevronCircleLeft } from '@fortawesome/free-solid-svg-icons';
library.add(faChevronCircleLeft)


class BarNavigation extends React.Component {

    constructor(props) {
        super(props)
    }

    render() {

        const {
            url,
            label
        }= this.props

        return (
            <div className="bar-navigation">
                <div className="bar-navigation-left">
                    <a className="bar-navigation-left__item" href={url}>
                        <span className="item-icon">
                            <FontAwesomeIcon icon="chevron-left" />
                        </span>
                        <span className="item-text">
                            {label}
                        </span>
                    </a>
                </div>
            </div>
        )
    }

}

export default BarNavigation