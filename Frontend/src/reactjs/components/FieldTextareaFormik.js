import React from 'react'
import { useField } from 'formik';

const FieldTexareatFormik = (props) => {

    const [field, meta] = useField(props)

    const {
        size,
        title,
        optional,
        helptext,
        class_name,
    } = props

    let cls_name = "field"
    if (class_name) {
        cls_name = class_name
    }

    let container_class = "textarea"
    if (size) {
        container_class = `textarea textarea--${size}`
    }

    let field_class = "textarea-input"
    let label_class = "textarea-label"
    let show_errrors = false

    if (meta.touched && meta.error) {
        show_errrors = true
        field_class = "textarea-input textarea-input--error"
        label_class = "textarea-label textarea-label--error"
    }

    return (
        <div className={cls_name}>
            <div className={container_class} data-type="field">
                <label className={label_class}>
                    {title}
                    { optional == true &&
                        <span className="textarea-label-optional">
                            (opcional)
                        </span>
                    }
                </label>
                <textarea
                    className={field_class}
                    {...field}
                />
                { show_errrors &&
                    <div className="textarea-errors">
                        <ul className="textarea-errors-list">
                            {meta.error}
                        </ul>
                    </div>
                }
                { helptext &&
                    <div className="textarea-help">
                        {helptext}
                    </div>
                }
            </div>
        </div>
    )
}

export default FieldTexareatFormik