import React from 'react'

class ItemHeader extends React.Component {

    constructor(props) {
        super(props)
    }

    render() {

        const {
            value,
            label,
        } = this.props

        return(
            <div className="card-header-item">
                <div className="item-label">
                    {label}
                </div>
                <div className="item-data ">
                    <div className="data-value">
                        <div className="value">
                            {value}
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

export default ItemHeader