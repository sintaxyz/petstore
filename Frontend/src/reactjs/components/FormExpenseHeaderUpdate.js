import React from 'react'
import { Formik } from 'formik'
import FieldTextFormik from './FieldTextFormik'
import FieldSelectFormik from './FieldSelectFormik'
import FieldMultiSelectFormik from './FieldMultiSelectFormik'
import FieldTextareaFormik from './FieldTextareaFormik'
import DetailExpenseHeader from './DetailExpenseHeader'
import * as Yup from 'yup'


const ModalUpdateSchema = Yup.object().shape({
    concept: Yup.string().required('Favor de proporcionar un metodo de pago'),
    concept: Yup.string(),
    amount: Yup.string().required('No puedes dejar este campo vacio'),
    comments: Yup.string(),
})

var concept_options = [
    {value: 'PUO', label: 'Orden de Compra'},
    {value: 'PAR', label: 'Nómina'},
    {value: 'LIS', label: 'Servicio de Luz'},
    {value: 'WAS', label: 'Servicio de Agua'},
    {value: 'OTH', label: 'Otros'},
]

const FormExpenseHeaderUpdate = (props) => {

    const {
        submit_handle,       
        instance,        
    } = props

    console.log(props);

    return (
        <Formik
        validationSchema={ModalUpdateSchema}
        initialValues={{
            company: instance.id,
            concept: "PUO",
            status: 'CO',
            key: "",
            amount: "",
            comments: "",
        }}
        onSubmit={submit_handle}>
        {props => (
            <form onSubmit={props.handleSubmit}>
                <DetailExpenseHeader
                instance={instance}
                ></DetailExpenseHeader>
                <div className="form-header-fields">
                    <FieldSelectFormik
                        name="concept"
                        title={"Concepto"}
                        options={concept_options}
                        change_handle={props.setFieldValue}
                        blur_handle={props.setFieldTouched}
                        optional={false}
                    />
                    <FieldTextFormik
                        name="key"
                        title={"Clave"}
                        optional={false}
                    />                     
                    <FieldTextFormik
                        name="amount"
                        title={"Cantidad"}
                        optional={false}
                        type={"number"}
                    />
                    <FieldTextFormik
                        name="date"
                        title={"Fecha de Aplicación"}
                        placeholder={"AAAA-MM-DD HH:MM"}
                        optional={false}
                    />
                    <FieldTextareaFormik
                        class_name="field field--x3"
                        name="comments"
                        title={"Observaciones"}
                        optional={true}
                    />
                </div>
                <div className="form-header-controls">
                    <button
                    type="submit"
                    disabled={props.isSubmitting || !props.isValid}
                    className="btn btn-main">
                            Guardar
                    </button>
                </div>
            </form>
        )}
        </Formik>
    )
}

export default FormExpenseHeaderUpdate