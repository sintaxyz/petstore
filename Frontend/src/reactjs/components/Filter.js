import React from 'react'
import Select from 'react-select'

const Filter = (props) => {

    const {
        size,
        title,
        name,
        value,
        options,
        change_handle,
        blur_handle,
        class_name,
    } = props

    let cls_name = "field"
    if (class_name) {
        cls_name = class_name
    }

    let container_class = "select"
    if (size) {
        container_class = `select select--${size}`
    }

    let field_class = "select-input"
    let label_class = "select-label"

    const change_Option = (param) => {
        if (param){
            change_handle(param.value)
        } else {
            change_handle(null)
        }
    }

    const blur_Handle = () => {
        blur_handle(true)
    };

    return (
        <div className={cls_name}>
            <div className={container_class} data-type="field">
                <label className={label_class}>
                    {title}
                </label>
                <Select
                    options={options}
                    name={name}
                    className={field_class}
                    classNamePrefix={field_class}
                    value={options.filter(option => option.value === value)}
                    onChange={change_Option}
                    onBlur={blur_Handle}
                    isClearable={true}
                />
            </div>
        </div>
    )
}

export default Filter