import React from 'react'

import RetrieveText from './RetrieveText'

class DetailAssignmentHeader extends React.Component {

    constructor(props) {
        super(props)
    }

    render() {

        const {
            // id,
            company,
            user,
        } = this.props
        console.log(user);
        
        
        return (
            <div className="card-info-body">
                <div className="card-info-body__items">
                    {/* <RetrieveText
                        label="ID"
                        value={instance.id}
                    ></RetrieveText> */}
                    <RetrieveText
                        label="Compañia"
                        value={company.legal_entity}
                    ></RetrieveText>
                    <RetrieveText
                        label="Usuario"
                        value={user.user_name}
                    ></RetrieveText>                
                </div>
            </div>
        )
    }
}


export default DetailAssignmentHeader