import React from 'react'
import { Formik } from 'formik'
import FieldTextFormik from './FieldTextFormik'
import FieldSelectFormik from './FieldSelectFormik'
import FieldTextareaFormik from './FieldTextareaFormik'
import * as Yup from 'yup'


const ModalUpdateSchema = Yup.object().shape({
    description: Yup.string(),
    comments: Yup.string(),
    work_id: Yup.string(),
    warehouse: Yup.string().required('Favor de proporcionar el almacen'),
    branchoffice: Yup.string().required('Favor de proporcionar la sucursal'),
    client: Yup.string(),
    tax: Yup.string().required('No puedes dejar este campo vacio'),
})


const build_Options = (data, display_value, property_value) => {
    if (display_value) {
       var property_display = display_value
    } else {
        var property_display = 'name'
    }
    if (property_value) {
       var property_value = property_value
    } else {
        var property_value = 'id'
    }
    let options = data.map((item, index, array) => {
        let new_item = {}
        new_item.value = item[property_value]
        new_item.label = item[property_display]
        return new_item
    })

    return options
}


const FormOrderHeader = (props) => {

    const {
        submit_handle,
        warehouse_data,
        tax_data,
        branchoffice_data,
        client_data,
    } = props

    const warehouse_options = build_Options(warehouse_data)
    const tax_options = build_Options(tax_data, 'description')
    const client_options = build_Options(client_data, 'commercial_name')
    const branchoffice_options = build_Options(branchoffice_data)

    return (
        <Formik
        validationSchema={ModalUpdateSchema}
        initialValues={{
            description: "",
            comments: "",
            warehouse: "",
            client: "",
            tax: "",
         }}
        onSubmit={submit_handle}>
        {props => (
            <form onSubmit={props.handleSubmit}>
                <div className="card-form-body">
                    <FieldSelectFormik
                        name="warehouse"
                        title={"Almacén"}
                        options={warehouse_options}
                        change_handle={props.setFieldValue}
                        blur_handle={props.setFieldTouched}
                    />
                    <FieldSelectFormik
                        name="branchoffice"
                        title={"Sucursal"}
                        options={branchoffice_options}
                        change_handle={props.setFieldValue}
                        blur_handle={props.setFieldTouched}
                    />
                    <FieldSelectFormik
                        name="client"
                        title={"Cliente"}
                        options={client_options}
                        change_handle={props.setFieldValue}
                        blur_handle={props.setFieldTouched}
                        optional={true}
                    />
                    <FieldSelectFormik
                        name="tax"
                        title={"Impuesto"}
                        options={tax_options}
                        change_handle={props.setFieldValue}
                        blur_handle={props.setFieldTouched}
                    />
                    <FieldTextFormik
                        name="description"
                        title={"Descripción"}
                        optional={true}
                    />
                    <FieldTextareaFormik
                        class_name="field field--x3"
                        name="comments"
                        title={"Observaciones"}
                        optional={true}
                    />
                </div>
                <div className="form-header-controls">
                    <button
                    type="submit"
                    disabled={props.isSubmitting || !props.isValid}
                    className="btn btn-main">
                            Continuar
                    </button>
                </div>
            </form>
        )}
        </Formik>
    )
}

export default FormOrderHeader