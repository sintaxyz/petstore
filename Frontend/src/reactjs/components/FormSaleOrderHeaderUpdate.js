import React from 'react'
import { Formik } from 'formik'
import FieldTextFormik from './FieldTextFormik'
import FieldSelectFormik from './FieldSelectFormik'
import FieldTextareaFormik from './FieldTextareaFormik'
import DetailSaleOrderHeader from './DetailSaleOrderHeader'

import * as Yup from 'yup'


const ModalUpdateSchema = Yup.object().shape({
    paymethod: Yup.string().required('No puedes dejar este campo vacio'),
    credit_days: Yup.number().when('paymethod', {
        is: 'CR',
        then: Yup.number()
          .required('Requerido con metodo de pago a Credito.'),
        otherwise: Yup.number(),
    }),
    description: Yup.string(),
    comments: Yup.string(),
});

var base_paymethod_options = [
    {value: 'CA', label: 'Efectivo'},
    {value: 'TA', label: 'Tarjeta'},
    {value: 'CR', label: 'Credito'},
]

var base_payment_days = [
    {value: 30, label: '30 Días'},
    {value: 60, label: '60 Días'},
    {value: 90, label: '90 Días'},
]

const on_ChangePaymenthod = (field, value, setFieldValue) => {
        setFieldValue(field, value)
        if (value != 'CR') {
        setFieldValue('credit_days', "")
    }
    console.log('field', field, 'value', value);
}

const FormOrderHeaderUpdate = (props) => {

    const {
        submit_handle,
        instance,
    } = props

    if (instance.client_credit_days) {
        var payment_days_credit = instance.client_credit_days ? base_payment_days.filter(
            days => days.value <= instance.client_credit_days || days.value === undefined
        ) : null
        var paymethod_options = base_paymethod_options
    } else {
        var paymethod_options = base_paymethod_options.filter(
            item => item.value != 'CR'
        )
    }
    console.log(instance)
    return (
        <Formik
        validationSchema={ModalUpdateSchema}
        initialValues={{
            paymethod: instance.paymethod,
            description: instance.description,
            comments: instance.comments,
            credit_days: instance.credit_days ? instance.credit_days : "",
            work_id: "",
        }}
        onSubmit={submit_handle}>
        {props => (
            <form onSubmit={props.handleSubmit}>
                <DetailSaleOrderHeader
                    instance={instance}
                ></DetailSaleOrderHeader>
                <div className="card-form-body">
                    <FieldSelectFormik
                        name="paymethod"
                        title={"Metodo de Pago"}
                        options={paymethod_options}
                        change_handle={
                            (field, value) => on_ChangePaymenthod(field, value, props.setFieldValue)
                        }
                        blur_handle={props.setFieldTouched}   
                    />
                    { instance.client && payment_days_credit && props.values.paymethod == 'CR' &&
                        <FieldSelectFormik
                            name="credit_days"
                            title={"Dias de Credito"}
                            options={payment_days_credit}
                            change_handle={props.setFieldValue}
                            blur_handle={props.setFieldTouched}   
                        />
                    }
                    { instance.client &&
                        <FieldTextFormik
                            name="work_id"
                            title={"Obra"}
                            optional={false}
                        />
                    }
                    <FieldTextFormik
                        name="description"
                        title={"Descripción"}
                        optional={false}
                    />
                    <FieldTextareaFormik
                        class_name="field field--x3"
                        name="comments"
                        title={"Observaciones"}
                        optional={true}
                    />
                </div>
                <div className="form-header-controls">
                    <button
                    type="submit"
                    disabled={props.isSubmitting || !props.isValid}
                    className="btn btn-main">
                            Actualizar
                    </button>
                </div>
            </form>
        )}
        </Formik>
    )
}

export default FormOrderHeaderUpdate