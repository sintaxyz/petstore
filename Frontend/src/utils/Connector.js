var cookies = require('browser-cookies')

const format_Fail = (data_fail) => {

    if (data_fail.non_field_errors) {
        return String(data_fail.non_field_errors[0])
    }

    if (data_fail.detail) {
        return String(data_fail.detail)
    }

    if (data_fail.__all__) {
        return String(data_fail.__all__)
    }

    let msg = ""
    let keys = Object.keys(data_fail)
    keys.forEach((i) => {
        msg += `${i}: ${data_fail[i]} \n`
    })
    return msg
}

const format_Error = (error) => {
    if (error.message == "Failed to fetch" || error.name == "FetchError") {
        return "NO_CONNECTION"
    }
    else {
        return error.message
    }
}

const format_Error500 = () => {
    return "No se encontro recurso en Servidor"
}

const get_Cookie = () => {
    let token = cookies.get('csrftoken')
    return token
}

const get_Headers = () => {
    let headers = {
        'Content-Type': 'application/json',
        "X-CSRFToken": get_Cookie()
    }

    return headers
}

const get_GetData = (token) => {
    let data_obj = {}
    data_obj.method = 'GET'

    if (token) {
        data_obj.headers = {
            'Content-Type': 'application/json',
            'Authorization': token
        }
    } else {
        data_obj.headers = get_Headers()
    }

    return data_obj
}

const get_PostData = (data, token) => {
    let data_obj = {}
    data_obj.method = 'POST'
    data_obj.body = JSON.stringify(data)

    if (token) {
        data_obj.headers = {
            'Content-Type': 'application/json',
            'Authorization': token
        }
    } else {
        data_obj.headers = get_Headers()
    }

    return data_obj
}

const get_PutData = (data, token) => {
    let data_obj = {}
    data_obj.method = 'PUT'
    data_obj.body = JSON.stringify(data)

    if (token) {
        data_obj.headers = {
            'Content-Type': 'application/json',
            'Authorization': token
        }
    } else {
        data_obj.headers = get_Headers()
    }

    return data_obj
}

const Connector = {

    async get(url, token=null) {
        return new Promise( async (resolve, reject) => {
            try {
                let response = await fetch(url, get_GetData(token))
                if (response.status >= 400 && response.status <= 451) {
                    let data_fail = await response.json()
                    reject(format_Fail(data_fail))

                } else if (response.status == 500) {
                    reject(format_Error500())

                } else {
                    let data = await response.json()
                    resolve(data)
                }
            } catch (error) {
                reject(format_Error(error))
            }
        })
    },
    async post(url, data, token=null) {
        return new Promise( async (resolve, reject) => {
            try {
                let response = await fetch(
                    url,
                    get_PostData(data, token)
                )
                if (response.status >= 400 && response.status <= 451) {
                    let data_fail = await response.json()
                    reject(format_Fail(data_fail))

                } else if (response.status == 500) {
                    reject(format_Error500())

                } else {
                    let data = await response.json()
                    resolve(data)
                }
            } catch (error) {
                reject(format_Error(error))
            }
        })
    },
    async put(url, data, token=null) {
        return new Promise( async (resolve, reject) => {
            try {
                let response = await fetch(
                    url,
                    get_PutData(data, token)
                )
                if (response.status >= 400 && response.status <= 451) {
                    let data_fail = await response.json()
                    reject(format_Fail(data_fail))

                } else if (response.status == 500) {
                    reject(format_Error500())

                } else {
                    let data = await response.json()
                    resolve(data)
                }
            } catch (error) {
                reject(format_Error(error))
            }
        })
    },
}

export default Connector