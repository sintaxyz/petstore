import React from 'react'
import ReactDOM from 'react-dom'
import 'normalize.css'
import './docinitialentry_edit.scss'

import MasterIn from '../../../../vanillajs/MasterIn'
import Urls from '../../../../app/server/urls'
import BarNavigation from '../../../../reactjs/components/BarNavigation'
import TitleMain from '../../../../reactjs/components/TitleMain'

import FormInitialEntryHeader from '../../../../reactjs/components/FormInitialEntryHeader'
import FormInitialEntryHeaderUpdate from '../../../../reactjs/components/FormInitialEntryHeaderUpdate'
import FormInitialEntryLine from '../../../../reactjs/components/FormInitialEntryLine'
import DocInitialEntry from '../../../../app/bsn/DocInitialEntry'
import InitialEntryLine from '../../../../reactjs/components/InitialEntryLine'
import ListFormLines from '../../../../reactjs/components/ListFormLines'
import Warehouse from '../../../../app/bsn/Warehouse'
import Employee from '../../../../app/bsn/Employee'

import PopupAlert from '../../../../reactjs/components/PopupAlert'
import ListItem from '../../../../reactjs/components/ListItem'
import ActionControl from '../../../../reactjs/components/ActionControl'
import ActionButtonRight from '../../../../reactjs/components/ActionButtonRight'
import { set } from 'browser-cookies'
import AsideContent from '../../../../reactjs/components/AsideContent'
import AsideCard from '../../../../reactjs/components/AsideCard'
import AsideCancel from '../../../../reactjs/components/AsideCancel'


/* ---------------- MainComponent ---------------- */

class DocInitialEntryNew extends React.Component {

    constructor(props) {
        super(props)

        this.state = {
            // Usability
            error_message: null,
            success_message: null,
            show_lines: false,

            // Header Data
            header: null,
            warehouse_data: [],
            entry_by_data: [],

            // Lines Data
            lines_data: [],
            item_data: [],
        }

        // Handle Events
        this.save_Line = this.save_Line.bind(this);
        this.save_HeaderDescription = this.save_HeaderDescription.bind(this);
        this.finish_Document = this.finish_Document.bind(this);
        this.close_PopupError = this.close_PopupError.bind(this);
        this.close_PopupSuccess = this.close_PopupSuccess.bind(this);
        this.cancel_Entry = this.cancel_Entry.bind(this);
    }

    async componentDidMount() {
        try {
            var array_url = [];
            array_url = document.URL.split("/");

            // Header Data
            let header = await DocInitialEntry.get(array_url[5])
            let lines = header.has_entrylines
            let items = await Warehouse.items_OutStockActive(header.warehouse)
            console.log(items);

            this.setState({
                // Header Data
                header: header,
                show_lines: true,
                item_data: items,
                lines_data: lines,
            })
        } catch (error) {
            this.setState({
                error_message: error,
            })
        }
    }

    async save_HeaderDescription(data) {
        try {
            // Header Data
            let header = await DocInitialEntry.update_Description(
                this.state.header.id, data)
            this.setState({
                header: header,
                show_lines: true,
                error_message: null,
                success_message: 'Registro actualizado.',
            })
        } catch (error) {
            this.setState({
                error_message: error,
                success_message: null,
            })
        }
    }

    async save_Line(data, {resetForm}) {
        try {
            // Lines Data
            let line = await DocInitialEntry.create_Line(
                this.state.header.id, data)
            let lines = this.state.lines_data

            // Header Data
            let header = await DocInitialEntry.get(this.state.header.id)
            lines.push(line)
            // Form Options
            resetForm({})
            this.setState({
                header: header,
                error_message: null,
                success_message: 'Linea registrada.',
                lines_data: lines,
            })
        } catch (error) {
            this.setState({
                error_message: error,
                success_message: null,
            })
        }
    }

    async finish_Document() {
        try {
            // Header Data
            let header = await DocInitialEntry.finish(
                this.state.header.id)
            window.location.replace(Urls.initial_entries)
            this.setState({
                header: header,
                success_message: 'Registro finalizado.',
                error_message: null,
            })
        } catch (error) {
            this.setState({
                error_message: error,
                success_message: null,
            })
        }
    }

    async cancel_Entry() {
        try {
            // Header Data
            let header = await DocInitialEntry.cancel(
                this.state.header.id)
            window.location.replace(Urls.initial_entries)
            this.setState({
                header: header,
                success_message: 'Carga Inicial Cancelada.',
                error_message: null,
            })
        } catch (error) {
            this.setState({
                error_message: error,
                success_message: null,
            })
        }
    }

    async close_PopupError() {
        this.setState({
            error_message: null,
        })
    }

    async close_PopupSuccess() {
        this.setState({
            success_message: null,
        })
    }

    render() {

        const {
            // Usability
            error_message,
            success_message,
            show_lines,

            // Header Data
            header,
            warehouse_data,
            entry_by_data,

            // Lines Data
            item_data,
            lines_data
        } = this.state

        return (
            <React.Fragment>
            <div className="content-header">
                <TitleMain
                    icon={<i className="fas fa-level-down-alt"></i>}
                    url={Urls.initial_entries}
                    url_label={"Cargas Iniciales"}
                    text={"Editar Carga Inicial"}
                ></TitleMain>
            </div>
            <div className="content-body">
                <div className="content-body__section">
                    <div  className="card-form">
                        <div className="card-form-header">
                            Cabecera del Documento:
                        </div>
                        {header &&
                            <FormInitialEntryHeaderUpdate
                                instance={header}
                                submit_handle={this.save_HeaderDescription}
                            ></FormInitialEntryHeaderUpdate>
                        }
                    </div>
                    { show_lines && item_data.length > 0 &&
                    <div  className="card-form">
                        <div className="card-form-header">
                            Agregar Linea
                        </div>
                        <FormInitialEntryLine
                        submit_handle={this.save_Line}
                        item_data={item_data}
                        ></FormInitialEntryLine>
                    </div>
                    }
                    { show_lines && lines_data.length > 0 &&
                    <div className="card-list">
                        <div className="card-list__body">
                            <ListItem>
                                { lines_data.map((item, index)=> {
                                    return (
                                        <InitialEntryLine
                                        key={index}
                                        instance={item}
                                        ></InitialEntryLine>
                                        )
                                    })}
                            </ListItem>
                        </div>
                    </div>
                    }
                    { show_lines && lines_data.length > 0 &&
                    <ActionControl>
                        <ActionButtonRight
                            handle_event={this.finish_Document}
                            tag={'Finalizar'}
                            ></ActionButtonRight>
                    </ActionControl>
                    }
                </div>
                <AsideContent>
                    <AsideCard title={'Otras Acciones'}>
                        <AsideCancel
                            icon={<i className="fas fa-window-close"></i>}
                            handle_event={this.cancel_Entry}
                            tag={'Cancelar'}
                        ></AsideCancel>
                    </AsideCard>
                </AsideContent>
            </div>
            { error_message &&
            <PopupAlert
                close_handle={this.close_PopupError}
                msg={error_message}
                type={"ERROR"}
            />
            }
            { success_message &&
            <PopupAlert
                close_handle={this.close_PopupSuccess}
                msg={success_message}
                type={"SUCCESS"}
            />
            }
            </React.Fragment>
        )
    }
}


/* ---------------- GLOBAL  ---------------- */
var page = null


/* ---------------- LOAD  ---------------- */
window.onload = function () {
    page = new Page()
}

/* ---------------- Page Object  ---------------- */
class Page {
    constructor () {
        this.master = new MasterIn()
        this.master.spinner.stop()
        this.wrapper = document.getElementById("wrapper")
        ReactDOM.render(
            <DocInitialEntryNew />,
            this.wrapper
        )
    }
}
