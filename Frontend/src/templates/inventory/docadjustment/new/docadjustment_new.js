import React from 'react'
import ReactDOM from 'react-dom'
import 'normalize.css'
import './docadjustment_new.scss'

import MasterIn from '../../../../vanillajs/MasterIn'
import Urls from '../../../../app/server/urls'
import BarNavigation from '../../../../reactjs/components/BarNavigation'
import TitleMain from '../../../../reactjs/components/TitleMain'

import FormDocAdjustmentHeader from '../../../../reactjs/components/FormDocAdjustmentHeader'
import FormDocAdjustmentHeaderUpdate from '../../../../reactjs/components/FormDocAdjustmentHeaderUpdate'
import DetailDocAdjustmentHeader from '../../../../reactjs/components/DetailDocAdjustmentHeader'
import FormDocAdjustmentLine from '../../../../reactjs/components/FormDocAdjustmentLine'
import DocAdjustment from '../../../../app/bsn/DocAdjustment'
import AdjustmentLine from '../../../../reactjs/components/AdjustmentLine'
import ListFormLines from '../../../../reactjs/components/ListFormLines'
import Warehouse from '../../../../app/bsn/Warehouse'
import Employee from '../../../../app/bsn/Employee'
import User from '../../../../app/bsn/User'
import Item from '../../../../app/bsn/Item'

import ListItem from '../../../../reactjs/components/ListItem'
import ActionControl from '../../../../reactjs/components/ActionControl'
import ActionButtonRight from '../../../../reactjs/components/ActionButtonRight'
import { set } from 'browser-cookies'


/* ---------------- MainComponent ---------------- */

class DocAdjustmentNew extends React.Component {

    constructor(props) {
        super(props)

        this.state = {
            // Usability
            error_message: null,
            success_message: null,
            show_lines: false,

            // Header Data
            header: null,
            warehouse_data: [],
            adjusted_by_data: [],
            
            // Lines Data
            lines_data: [],
            item_data: [],
        }

        // Handle Events
        this.save_Header = this.save_Header.bind(this);
        this.save_Line = this.save_Line.bind(this);
        this.save_HeaderDescription = this.save_HeaderDescription.bind(this);
        this.finish_Document = this.finish_Document.bind(this);
    }

    async componentDidMount() {
        try {
            // Header Data
            let warehouses = await Warehouse.all()
            let adjusteds_by = await Employee.all()
            this.setState({
                // Header Data
                warehouse_data: warehouses,
                adjusted_by_data: adjusteds_by,
            })
        } catch (error) {
            this.setState({
                error_message: error,
            })
        }
    }

    async save_Header(data) {
        try {            
            // Header Data
            let header = await DocAdjustment.create(data)
            // Lines Data
            console.log(header);

            let items = await Warehouse.items_InStock(header.warehouse)
            this.setState({
                header: header,
                item_data: items,
                // lines_data: lines,
                show_lines: true,
                error_message: null,
                success_message: 'Registro creado.',
            })
        } catch (error) {
            this.setState({
                error_message: error,
                success_message: null,
            })
        }
    }

    async save_HeaderDescription(data) {
        try {
            // Header Data
            let header = await DocAdjustment.update_Description(
                this.state.header.id, data)
            this.setState({
                header: header,
                show_lines: true,
                error_message: null,
                success_message: 'Registro actualizado.',
            })
        } catch (error) {
            this.setState({
                error_message: error,
                success_message: null,
            })
        }
    }

    async save_Line(data, {resetForm}) {
        try {
            // Lines Data
            let line = await DocAdjustment.create_Line(
                this.state.header.id, data)
            let lines = this.state.lines_data

            // Header Data
            let header = await DocAdjustment.get(this.state.header.id)
            lines.push(line)
            // Form Options
            resetForm({})
            this.setState({
                header: header,
                error_message: null,
                success_message: 'Linea registrada.',
                lines_data: lines,
            })
        } catch (error) {
            this.setState({
                error_message: error,
                success_message: null,
            })
        }
    }

    async finish_Document() {
        try {
            // Header Data
            let header = await DocAdjustment.finish(
                this.state.header.id)
            window.location.replace(Urls.doc_adjustments)
            this.setState({
                header: header,
                success_message: 'Registro finalizado.',
                error_message: null,
            })
        } catch (error) {
            this.setState({
                error_message: error,
                success_message: null,
            })
        }
    }

    render() {

        const {
            // Usability
            error_message,
            success_message,
            show_lines,

            // Header Data
            header,
            warehouse_data,
            adjusted_by_data,

            // Lines Data
            item_data,
            lines_data
        } = this.state

        return (
            <React.Fragment>
            <div className="content-header">
                <BarNavigation
                    url={Urls.doc_adjustments}
                    label={"Documentos de Ajuste"}
                ></BarNavigation>
                <TitleMain
                    text={"Crear Documento de Ajuste"}
                ></TitleMain>
            </div>
            <div className="content-body">
                <div className="content-body__section">
                    <div className="card-info-header">
                        <div className="header-item">
                            Cabecera del Documento:
                        </div>
                    </div>
                <div  className="card-form">
                    {header
                        ?<FormDocAdjustmentHeaderUpdate  
                            instance={header}
                            submit_handle={this.save_HeaderDescription}
                        ></FormDocAdjustmentHeaderUpdate>
                        :<FormDocAdjustmentHeader
                            submit_handle={this.save_Header}
                            warehouse_data={warehouse_data}
                            adjusted_by_data={adjusted_by_data}
                        ></FormDocAdjustmentHeader>
                    }
                </div>
                { show_lines && item_data.length > 0 &&
                <div  className="card-form">
                    <div className="card-form-header">
                        Agregar Linea
                    </div>
                    <FormDocAdjustmentLine
                    submit_handle={this.save_Line}
                    item_data={item_data}
                    ></FormDocAdjustmentLine>
                </div>
                }
                { show_lines && lines_data.length > 0 && 
                <div className="card-list">
                    <div className="card-list__body">
                        <ListItem>
                            { lines_data.map((item, index)=> {
                                return (
                                    <AdjustmentLine
                                    key={index}
                                    instance={item}
                                    ></AdjustmentLine>
                                )
                            })}
                        </ListItem>
                    </div>
                </div>
                }
                { show_lines && lines_data.length > 0 &&
                <ActionControl>
                    <ActionButtonRight
                        handle_event={this.finish_Document}
                        tag={'Finalizar'}
                        ></ActionButtonRight>
                </ActionControl>
                }
            </div>
            </div>
            </React.Fragment>
        )
    }
}


/* ---------------- GLOBAL  ---------------- */
var page = null


/* ---------------- LOAD  ---------------- */
window.onload = function () {
    page = new Page()
}

/* ---------------- Page Object  ---------------- */
class Page {
    constructor () {
        this.master = new MasterIn()
        this.master.spinner.stop()
        this.wrapper = document.getElementById("wrapper")
        ReactDOM.render(
            <DocAdjustmentNew />,
            this.wrapper
        )
    }
}
