import React from 'react'
import ReactDOM from 'react-dom'
import 'normalize.css'
import './docreception_new.scss'

import MasterIn from '../../../../vanillajs/MasterIn'
import Urls from '../../../../app/server/urls'
import BarNavigation from '../../../../reactjs/components/BarNavigation'
import TitleMain from '../../../../reactjs/components/TitleMain'

import FormDocReceptionHeader from '../../../../reactjs/components/FormDocReceptionHeader'
import FormDocReceptionHeaderUpdate from '../../../../reactjs/components/FormDocReceptionHeaderUpdate'
import DetailDocReceptionHeader from '../../../../reactjs/components/DetailDocReceptionHeader'
import FormDocReceptionLine from '../../../../reactjs/components/FormDocReceptionLine'
import DocReception from '../../../../app/bsn/DocReception'
import ListFormLines from '../../../../reactjs/components/ListFormLines'

import PopupAlert from '../../../../reactjs/components/PopupAlert'
import ListItem from '../../../../reactjs/components/ListItem'
import ActionControl from '../../../../reactjs/components/ActionControl'
import ActionButtonRight from '../../../../reactjs/components/ActionButtonRight'
import { set } from 'browser-cookies'
import PurcharseOrder from '../../../../app/bsn/PurchaseOrder'


/* ---------------- MainComponent ---------------- */

class DocReceptionNew extends React.Component {

    constructor(props) {
        super(props)

        this.state = {
            // Usability
            error_message: null,
            success_message: null,
            show_lines: false,

            // Header Data
            header: null,
            purchaseorder_data: [],

            // Lines Data
            lines_data: [],
        }

        // Handle Events
        this.save_Header = this.save_Header.bind(this);
        this.save_Line = this.save_Line.bind(this);
        this.save_HeaderDescription = this.save_HeaderDescription.bind(this);
        this.finish_Document = this.finish_Document.bind(this);
        this.close_PopupError = this.close_PopupError.bind(this);
        this.close_PopupSuccess = this.close_PopupSuccess.bind(this);
    }

    async componentDidMount() {
        try {
            // Header Data
            let purchaseorders = await PurcharseOrder.list_Approved()

            this.setState({
                // Header Data
                purchaseorder_data: purchaseorders,
            })
        } catch (error) {
            this.setState({
                error_message: error,
            })
        }
    }

    async save_Header(data) {
        try {
            // Header Data
            let header = await DocReception.create(data)

            // Lines Data
            let lines = await DocReception.lines(header.id)
            this.setState({
                header: header,
                lines_data: lines,
                show_lines: true,
                error_message: null,
                success_message: 'Registro creado.',
            })
        } catch (error) {
            this.setState({
                error_message: error,
                success_message: null,
            })
        }
    }

    async save_HeaderDescription(data) {
        try {
            // Header Data
            let header = await DocReception.update_Description(
                this.state.header.id, data)
            this.setState({
                header: header,
                error_message: null,
                success_message: 'Registro actualizado.',
            })
        } catch (error) {
            this.setState({
                error_message: error,
                success_message: null,
            })
        }
    }

    async save_Line(id, data) {
        try {
            // Lines Data
            let line = await DocReception.update_Line(
                this.state.header.id, id, data)
            this.setState({
                error_message: null,
                success_message: 'Linea actualizada: ' + line.id,
                // lines_data: lines,
            })
        } catch (error) {
            this.setState({
                error_message: error,
                success_message: null,
            })
        }
    }

    async finish_Document() {
        try {
            // Header Data
            let header = await DocReception.finish(
                this.state.header.id)
            window.location.replace(Urls.doc_receptions)
            this.setState({
                header: header,
                success_message: 'Registro finalizado.',
                error_message: null,
            })
        } catch (error) {
            this.setState({
                error_message: error,
                success_message: null,
            })
        }
    }

    async close_PopupError() {
        this.setState({
            error_message: null,
        })
    }

    async close_PopupSuccess() {
        this.setState({
            success_message: null,
        })
    }

    render() {

        const {
            // Usability
            error_message,
            success_message,
            show_lines,

            // Header Data
            header,
            purchaseorder_data,

            // Lines Data
            lines_data
        } = this.state

        return (
            <React.Fragment>
            <div className="content-header">
                <TitleMain
                    icon={<i className="fas fa-hands"></i>}
                    url={Urls.doc_receptions}
                    url_label={"Documentos de Recepción"}
                    text={"Crear Documento de Recepción"}
                ></TitleMain>
            </div>
            <div className="content-body">
                <div className="content-body__section">
                <div  className="card-form">
                    <div className="card-form-header">
                        Cabecera del Documento:
                    </div>
                    {header
                        ?<FormDocReceptionHeaderUpdate
                            instance={header}
                            submit_handle={this.save_HeaderDescription}
                        ></FormDocReceptionHeaderUpdate>
                        :<FormDocReceptionHeader
                            submit_handle={this.save_Header}
                            purchaseorder_data={purchaseorder_data}
                        ></FormDocReceptionHeader>
                    }
                </div>
                { lines_data.length > 0 &&
                        <ListFormLines>
                            { lines_data.map((instance, index)=> {
                                return (
                                    <FormDocReceptionLine
                                    key={index}
                                    submit_handle={this.save_Line}
                                    instance={instance}
                                    ></FormDocReceptionLine>
                                )
                            })}
                            { show_lines && lines_data.length > 0 &&
                            <ActionControl>
                                <ActionButtonRight
                                    handle_event={this.finish_Document}
                                    tag={'Finalizar'}
                                ></ActionButtonRight>
                            </ActionControl>
                            }
                        </ListFormLines>
                }
            </div>
            </div>
            { error_message &&
            <PopupAlert
                close_handle={this.close_PopupError}
                msg={error_message}
                type={"ERROR"}
            />
            }
            { success_message &&
            <PopupAlert
                close_handle={this.close_PopupSuccess}
                msg={success_message}
                type={"SUCCESS"}
            />
            }
            </React.Fragment>
        )
    }
}


/* ---------------- GLOBAL  ---------------- */
var page = null


/* ---------------- LOAD  ---------------- */
window.onload = function () {
    page = new Page()
}

/* ---------------- Page Object  ---------------- */
class Page {
    constructor () {
        this.master = new MasterIn()
        this.master.spinner.stop()
        this.wrapper = document.getElementById("wrapper")
        ReactDOM.render(
            <DocReceptionNew />,
            this.wrapper
        )
    }
}
