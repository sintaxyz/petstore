import React from 'react'
import ReactDOM from 'react-dom'
import 'normalize.css'
import './docdispatch_new.scss'

import MasterIn from '../../../../vanillajs/MasterIn'
import Urls from '../../../../app/server/urls'
import BarNavigation from '../../../../reactjs/components/BarNavigation'
import TitleMain from '../../../../reactjs/components/TitleMain'

import FormDocDispatchHeader from '../../../../reactjs/components/FormDocDispatchHeader'
import FormDocDispatchHeaderUpdate from '../../../../reactjs/components/FormDocDispatchHeaderUpdate'
import DetailDocDispatchHeader from '../../../../reactjs/components/DetailDocDispatchHeader'
import FormDocDispatchLine from '../../../../reactjs/components/FormDocDispatchLine'
import DocDispatch from '../../../../app/bsn/DocDispatch'
import DispatchLine from '../../../../reactjs/components/DispatchLine'
import ListFormLines from '../../../../reactjs/components/ListFormLines'
import Warehouse from '../../../../app/bsn/Warehouse'
import Employee from '../../../../app/bsn/Employee'
import User from '../../../../app/bsn/User'
import Item from '../../../../app/bsn/Item'

import ListItem from '../../../../reactjs/components/ListItem'
import ActionControl from '../../../../reactjs/components/ActionControl'
import ActionButtonRight from '../../../../reactjs/components/ActionButtonRight'
import { set } from 'browser-cookies'


/* ---------------- MainComponent ---------------- */

class DocDispatchNew extends React.Component {

    constructor(props) {
        super(props)

        this.state = {
            // Usability
            error_message: null,
            success_message: null,
            show_lines: false,

            // Header Data
            header: null,
            warehouse_data: [],
            delivery_by_data: [],
            received_by_data: [],
            
            // Lines Data
            lines_data: [],
            item_data: [],
        }

        // Handle Events
        this.save_Header = this.save_Header.bind(this);
        this.save_Line = this.save_Line.bind(this);
        this.save_HeaderDescription = this.save_HeaderDescription.bind(this);
        this.finish_Document = this.finish_Document.bind(this);
    }

    async componentDidMount() {
        try {
            // Header Data
            let warehouses = await Warehouse.all()
            let deliveries_by = await Employee.all()
            let receiveds_by = await Employee.all()            
            this.setState({
                // Header Data
                warehouse_data: warehouses,
                delivery_by_data: deliveries_by,
                received_by_data: receiveds_by,
            })
        } catch (error) {
            this.setState({
                error_message: error,
            })
        }
    }

    async save_Header(data) {
        try {            
            // Header Data
            let header = await DocDispatch.create(data)
            // Lines Data
            console.log(header);

            let items = await Warehouse.items_InStock(header.warehouse)
            this.setState({
                header: header,
                item_data: items,
                // lines_data: lines,
                show_lines: true,
                error_message: null,
                success_message: 'Registro creado.',
            })
        } catch (error) {
            this.setState({
                error_message: error,
                success_message: null,
            })
        }
    }

    async save_HeaderDescription(data) {
        try {
            // Header Data
            let header = await DocDispatch.update_Description(
                this.state.header.id, data)
            this.setState({
                header: header,
                show_lines: true,
                error_message: null,
                success_message: 'Registro actualizado.',
            })
        } catch (error) {
            this.setState({
                error_message: error,
                success_message: null,
            })
        }
    }

    async save_Line(data, {resetForm}) {
        try {
            // Lines Data
            let line = await DocDispatch.create_Line(
                this.state.header.id, data)
            let lines = this.state.lines_data

            // Header Data
            let header = await DocDispatch.get(this.state.header.id)
            lines.push(line)
            // Form Options
            resetForm({})
            this.setState({
                header: header,
                error_message: null,
                success_message: 'Linea registrada.',
                lines_data: lines,
            })
        } catch (error) {
            this.setState({
                error_message: error,
                success_message: null,
            })
        }
    }

    async finish_Document() {
        try {
            // Header Data
            let header = await DocDispatch.finish(
                this.state.header.id)
            window.location.replace(Urls.doc_dispatches)
            this.setState({
                header: header,
                success_message: 'Registro finalizado.',
                error_message: null,
            })
        } catch (error) {
            this.setState({
                error_message: error,
                success_message: null,
            })
        }
    }    

    render() {

        const {
            // Usability
            error_message,
            success_message,
            show_lines,

            // Header Data
            header,
            warehouse_data,
            delivery_by_data,
            received_by_data,

            // Lines Data
            item_data,
            lines_data
        } = this.state

        return (
            <React.Fragment>
            <div className="content-header">
                <BarNavigation
                    url={Urls.doc_dispatches}
                    label={"Documentos de Despacho"}
                ></BarNavigation>
                <TitleMain
                    text={"Crear Documento de Despacho"}
                ></TitleMain>
            </div>
            <div className="content-body">
                <div className="content-body__section">
                    <div className="card-info-header">
                        <div className="header-item">
                            Cabecera del Documento:
                        </div>
                    </div>
                <div  className="card-form">
                    {header ?
                        <FormDocDispatchHeaderUpdate  
                        instance={header}
                        submit_handle={this.save_HeaderDescription}
                        ></FormDocDispatchHeaderUpdate>:<FormDocDispatchHeader
                        submit_handle={this.save_Header}
                        warehouse_data={warehouse_data}
                        delivery_by_data={delivery_by_data}
                        received_by_data={received_by_data}
                        ></FormDocDispatchHeader>
                    }
                </div>
                { show_lines && item_data.length > 0 &&
                <div  className="card-form">
                    <div className="card-form-header">
                        Agregar Linea
                    </div>
                    <FormDocDispatchLine
                    submit_handle={this.save_Line}
                    item_data={item_data}
                    ></FormDocDispatchLine>
                </div>
                }
                { show_lines && lines_data.length > 0 && 
                <div className="card-list">
                    <div className="card-list__body">
                        <ListItem>
                            { lines_data.map((item, index)=> {
                                return (
                                    <DispatchLine
                                    key={index}
                                    instance={item}
                                    ></DispatchLine>
                                )
                            })}
                        </ListItem>
                    </div>
                </div>
                }
                { show_lines && lines_data.length > 0 &&
                <ActionControl>
                    <ActionButtonRight
                        handle_event={this.finish_Document}
                        tag={'Finalizar'}
                        ></ActionButtonRight>
                </ActionControl>
                }
            </div>
            </div>
            </React.Fragment>
        )
    }
}


/* ---------------- GLOBAL  ---------------- */
var page = null


/* ---------------- LOAD  ---------------- */
window.onload = function () {
    page = new Page()
}

/* ---------------- Page Object  ---------------- */
class Page {
    constructor () {
        this.master = new MasterIn()
        this.master.spinner.stop()
        this.wrapper = document.getElementById("wrapper")
        ReactDOM.render(
            <DocDispatchNew />,
            this.wrapper
        )
    }
}
