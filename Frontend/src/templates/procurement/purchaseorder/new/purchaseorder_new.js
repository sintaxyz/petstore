import React from 'react'
import ReactDOM from 'react-dom'
import 'normalize.css'
import './purchaseorder_new.scss'

import MasterIn from '../../../../vanillajs/MasterIn'
import Urls from '../../../../app/server/urls'
import BarNavigation from '../../../../reactjs/components/BarNavigation'
import TitleMain from '../../../../reactjs/components/TitleMain'

import FormOrderHeader from '../../../../reactjs/components/FormOrderHeader'
import FormOrderHeaderUpdate from '../../../../reactjs/components/FormOrderHeaderUpdate'
import FormOrderLine from '../../../../reactjs/components/FormOrderLine'
import PurchaseOrderLine from '../../../../reactjs/components/PurchaseOrderLine'
import Supplier from '../../../../app/bsn/Supplier'
import Tax from '../../../../app/bsn/Tax'
import Warehouse from '../../../../app/bsn/Warehouse'
import ActionButtonRight from '../../../../reactjs/components/ActionButtonRight'
import PurcharseOrder from '../../../../app/bsn/PurchaseOrder'

import ListItem from '../../../../reactjs/components/ListItem'
import ActionControl from '../../../../reactjs/components/ActionControl'
import PopupAlert from '../../../../reactjs/components/PopupAlert'
import { set } from 'browser-cookies'


/* ---------------- MainComponent ---------------- */

class PurchaseOrderNew extends React.Component {

    constructor(props) {
        super(props)

        this.state = {
            // Usability
            error_message: null,
            success_message: null,
            show_lines: false,

            // Header Data
            header: null,
            warehouse_data: [],
            supplier_data: [],

            // Lines Data
            lines_data: [],
            item_data: [],
            tax_data: [],
        }

        // Handle Events
        this.save_Header = this.save_Header.bind(this);
        this.save_Line = this.save_Line.bind(this);
        this.save_HeaderDescription = this.save_HeaderDescription.bind(this);
        this.finish_Document = this.finish_Document.bind(this);
        this.close_PopupError = this.close_PopupError.bind(this);
        this.close_PopupSuccess = this.close_PopupSuccess.bind(this);
    }

    async componentDidMount() {
        try {
            // Header Data
            let warehouses = await Warehouse.with_Stock()
            let taxes = await Tax.all()
            let suppliers = await Supplier.active()
            this.setState({
                // Header Data
                warehouse_data: warehouses,
                tax_data: taxes,
                supplier_data: suppliers
            })
        } catch (error) {
            this.setState({
                error_message: error,
            })
        }
    }

    async save_Header(data) {
        try {
            // Header Data
            let header = await PurcharseOrder.create(data)
            // Lines Data
            let items = await Warehouse.items(header.warehouse)
            this.setState({
                header: header,
                item_data: items,
                show_lines: true,
                error_message: null,
                success_message: 'Registro creado.',
            })
        } catch (error) {
            this.setState({
                error_message: error,
                success_message: null,
            })
        }
    }

    async save_HeaderDescription(data) {
        try {
            console.log(data);

            // Header Data
            let header = await PurcharseOrder.update_Description(
                this.state.header.id, data)
            this.setState({
                header: header,
                error_message: null,
                success_message: 'Registro actualizado.',
            })
        } catch (error) {
            this.setState({
                error_message: error,
                success_message: null,
            })
        }
    }

    async save_Line(data, {resetForm}) {
        try {
            // Lines Data
            let line = await PurcharseOrder.create_Line(
                this.state.header.id, data)
            let lines = this.state.lines_data

            // Header Data
            let header = await PurcharseOrder.get(this.state.header.id)
            lines.push(line)
            // Form Options
            resetForm({})
            this.setState({
                header: header,
                error_message: null,
                success_message: 'Linea registrada.',
                lines_data: lines,
            })
        } catch (error) {
            this.setState({
                error_message: error,
                success_message: null,
            })
        }
    }

    async finish_Document() {
        try {
            // Header Data
            let header = await PurcharseOrder.finish(
                this.state.header.id)
            window.location.replace(Urls.purchase_orders)
            this.setState({
                header: header,
                success_message: 'Registro finalizado.',
                error_message: null,
            })
        } catch (error) {
            this.setState({
                error_message: error,
                success_message: null,
            })
        }
    }

    async close_PopupError() {
        this.setState({
            error_message: null,
        })
    }

    async close_PopupSuccess() {
        this.setState({
            success_message: null,
        })
    }

    render() {

        const {
            // Usability
            error_message,
            success_message,
            show_lines,

            // Header Data
            header,
            tax_data,
            supplier_data,
            warehouse_data,

            // Lines Data
            item_data,
            lines_data
        } = this.state

        return (
            <React.Fragment>
            <div className="content-header">
                <TitleMain
                    icon={<i className="fas fa-file-download"></i>}
                    url={Urls.purchase_orders}
                    url_label={"Ordenes de Compra"}
                    text={"Crear Order de Compra"}
                ></TitleMain>
            </div>
            <div className="content-body">
                <div className="content-body__section">
                    <div  className="card-form">
                        <div className="card-form-header">
                            Cabecera del Documento:
                        </div>
                    {header ?
                    <FormOrderHeaderUpdate
                        instance={header}
                        submit_handle={this.save_HeaderDescription}
                    ></FormOrderHeaderUpdate>
                    :<FormOrderHeader
                        submit_handle={this.save_Header}
                        warehouse_data={warehouse_data}
                        tax_data={tax_data}
                        supplier_data={supplier_data}
                    ></FormOrderHeader>
                    }
                    </div>
                    { show_lines && item_data.length > 0 &&
                    <div  className="card-form">
                        <div className="card-form-header">
                            Agregar Linea
                        </div>
                        <FormOrderLine
                            submit_handle={this.save_Line}
                            item_data={item_data}
                            tax_data={tax_data}
                            header={header}
                        ></FormOrderLine>
                    </div>
                    }
                    { show_lines && lines_data.length > 0 &&
                    <div className="card-list">
                        <div className="card-list__body">
                        <ListItem>
                            { lines_data.map((item, index)=> {
                                return (
                                    <PurchaseOrderLine
                                    key={index}
                                    instance={item}
                                    ></PurchaseOrderLine>
                                )
                            })}
                        </ListItem>
                        </div>
                    </div>
                    }
                    { show_lines && lines_data.length > 0 &&
                    <ActionControl>
                        <ActionButtonRight
                            handle_event={this.finish_Document}
                            tag={'Finalizar'}
                        ></ActionButtonRight>
                    </ActionControl>
                    }
                </div>
            </div>
            { error_message &&
            <PopupAlert
                close_handle={this.close_PopupError}
                msg={error_message}
                type={"ERROR"}
            />
            }
            { success_message &&
            <PopupAlert
                close_handle={this.close_PopupSuccess}
                msg={success_message}
                type={"SUCCESS"}
            />
            }
            </React.Fragment>
        )
    }
}


/* ---------------- GLOBAL  ---------------- */
var page = null


/* ---------------- LOAD  ---------------- */
window.onload = function () {
    page = new Page()
}

/* ---------------- Page Object  ---------------- */
class Page {
    constructor () {
        this.master = new MasterIn()
        this.master.spinner.stop()
        this.wrapper = document.getElementById("wrapper")
        ReactDOM.render(
            <PurchaseOrderNew />,
            this.wrapper
        )
    }
}
