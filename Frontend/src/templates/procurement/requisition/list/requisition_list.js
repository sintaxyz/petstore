import MasterIn from '../../../../vanillajs/MasterIn'
import Message from '../../../../vanillajs/Message'
import Toolbar from '../../../../vanillajs/Search'

import '../../../../styles/templates/_list.scss'



/* ---------------- GLOBAL  ---------------- */
var page = null


/* ---------------- LOAD  ---------------- */
window.onload = function () {
    page = new Page()
}

/* ---------------- Page Object  ---------------- */
class Page {
    constructor () {
        this.master = new MasterIn()
        this.searchtoolbar = new Toolbar()
        this.message = new Message()
        this.init()
    }

    init() {
        this.master.spinner.stop()
    }
}
