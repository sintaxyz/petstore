import React from 'react'
import ReactDOM from 'react-dom'
import 'normalize.css'
import './expense_edit.scss'

import MasterIn from '../../../../vanillajs/MasterIn'
import Urls from '../../../../app/server/urls'
import BarNavigation from '../../../../reactjs/components/BarNavigation'
import TitleMain from '../../../../reactjs/components/TitleMain'

import FormPayment from '../../../../reactjs/components/FormPayment'
import Payment from '../../../../app/bsn/Payment'
import Client from '../../../../app/bsn/Client'
import SaleOrder from '../../../../app/bsn/SaleOrder'
import BranchOffice from '../../../../app/bsn/BranchOffice'

import PopupAlert from '../../../../reactjs/components/PopupAlert'
import { set } from 'browser-cookies'
import AsideContent from '../../../../reactjs/components/AsideContent'
import AsideCard from '../../../../reactjs/components/AsideCard'
import AsideCancel from '../../../../reactjs/components/AsideCancel'


/* ---------------- MainComponent ---------------- */

class PaymentNew extends React.Component {

    constructor(props) {
        super(props)

        this.state = {
            // Usability
            error_message: null,
            success_message: null,

            clients: [],
            saleorders: [],
            branchoffices: [],
        }

        // Handle Events
        this.save_Payment = this.save_Payment.bind(this);
        this.close_handle = this.close_handle.bind(this);
        // this.cancel_Payment = this.cancel_Payment.bind(this);
    }

    async componentDidMount() {
        try {
            let clients_data = await Client.all()
            let saleorders_data = await SaleOrder.pendingpay_list()
            let branchoffices_data = await BranchOffice.all()
            this.setState({
                error_message: null,
                clients: clients_data,
                saleorders: saleorders_data,
                branchoffices: branchoffices_data,
            })
        } catch (error) {
            this.setState({
                error_message: error,
            })
        }
    }

    async save_Payment(data) {
        try {
            await Payment.create(data)
            window.location.replace(Urls.expense)
        } catch (error) {
            this.setState({
                error_message: error,
                success_message: null,
            })
        }
    }

    // async cancel_Payment() {
    //     try {
    //         // Header Data
    //         let header = await Payment.cancel(
    //             this.state.header.id)
    //         window.location.replace(Urls.sale_orders)
    //         this.setState({
    //             header: header,
    //             success_message: 'Orden de Venta Cancelada.',
    //             error_message: null,
    //         })
    //     } catch (error) {
    //         this.setState({
    //             error_message: error,
    //             success_message: null,
    //         })
    //     }
    // }

    async close_handle() {
        this.setState({
            error_message: null,
        })
    }

    render() {

        const {
            error_message,

            clients,
            saleorders,
            branchoffices,
        } = this.state

        return (
            <React.Fragment>
            <div className="content-header">
                <TitleMain
                    icon={<i className="fas fa-hand-holding-usd"></i>}
                    url={Urls.expense}
                    url_label={"Egresos"}
                    text={"Editar Egresos"}
                ></TitleMain>
            </div>
            <div className="content-body">
                <div className="content-body__section">
                    <div  className="card-form">
                        <div className="card-form-header">
                            Información del Pago:
                        </div>
                        <FormPayment
                            submit_handle={this.save_Payment}
                            clients_data={clients}
                            saleorders_data={saleorders}
                            branchoffices_data={branchoffices}
                        ></FormPayment>
                    </div>
                </div>
            </div>
            {error_message &&
                <PopupAlert
                    close_handle={this.close_handle}
                    msg={error_message}
                    type={'ERROR'}
                >
                </PopupAlert>
            }
            </React.Fragment>
        )
    }
}


/* ---------------- GLOBAL  ---------------- */
var page = null


/* ---------------- LOAD  ---------------- */
window.onload = function () {
    page = new Page()
}

/* ---------------- Page Object  ---------------- */
class Page {
    constructor () {
        this.master = new MasterIn()
        this.master.spinner.stop()
        this.wrapper = document.getElementById("wrapper")
        ReactDOM.render(
            <PaymentNew />,
            this.wrapper
        )
    }
}
