import React from 'react'
import ReactDOM from 'react-dom'
import 'normalize.css'
import './expense_new.scss'

import MasterIn from '../../../../vanillajs/MasterIn'
import Urls from '../../../../app/server/urls'
import BarNavigation from '../../../../reactjs/components/BarNavigation'
import TitleMain from '../../../../reactjs/components/TitleMain'

import FormExpenseHeader from '../../../../reactjs/components/FormExpenseHeader'
import FormExpenseHeaderUpdate from '../../../../reactjs/components/FormExpenseHeaderUpdate'
import FormExpense from '../../../../reactjs/components/FormExpense'
import Expense from '../../../../app/bsn/Expense'
import Company from '../../../../app/bsn/Company'

import PopupAlert from '../../../../reactjs/components/PopupAlert'
import { set } from 'browser-cookies'


/* ---------------- MainComponent ---------------- */

class PaymentNew extends React.Component {

    constructor(props) {
        super(props)

        this.state = {
            // Usability
            error_message: null,
            success_message: null,

            // Header Data
            company: null,
            company_data: [],
        }

        // Handle Events
        this.save_Header = this.save_Header.bind(this);
        this.save_HeaderDescription = this.save_HeaderDescription.bind(this);
        // this.save_Payment = this.save_Payment.bind(this);
        // this.close_PopupError = this.close_PopupError.bind(this);
        // this.close_PopupSuccess = this.close_PopupSuccess.bind(this);
    }

    async componentDidMount() {
        try {
            // Header Data
            let companies = await Company.active()
            console.log(companies);

            this.setState({
                // Header Data
                // error_message: null,
                company_data: companies,
            })
        } catch (error) {
            this.setState({
                error_message: error,
            })
        }
    }

    async save_Header(data) {
        try {
            // Header Data
            // Lines Data
            console.log(data);
            let company = await Company.get(data.company)

            this.setState({
                company: company,
                show_lines: true,
                error_message: null,
                success_message: 'Datos del Empresa obtenidos',
            })
        } catch (error) {
            this.setState({
                error_message: error,
                success_message: null,
            })
        }
    }

    async save_HeaderDescription(data) {
        console.log('HeaderDesciption');

        try {
            // Header Data
            let header = await Expense.create(data)
            window.location.replace(Urls.expense)
            this.setState({
                header: header,
                show_lines: true,
                error_message: null,
                success_message: 'Registro creado.',
            })
        } catch (error) {
            this.setState({
                error_message: error,
                success_message: null,
            })
        }
    }

    render() {

        const {
            // Usability
            error_message,
            success_message,
            show_lines,

            // Header Data
            company,
            company_data,
        } = this.state
        console.log(this.state);

        return (
            <React.Fragment>
            <div className="content-header">
                <TitleMain
                    icon={<i className="fas fa-hand-holding-usd"></i>}
                    url={Urls.expense}
                    url_label={"Egresos"}
                    text={"Crear Egresos"}
                ></TitleMain>
            </div>
            <div className="content-body">
                <div className="content-body__section">
                    <div className="card-info-header">
                        <div className="header-item">
                            Cabecera del Documento:
                        </div>
                    </div>
                <div  className="card-form">
                    {company ?
                        <FormExpenseHeaderUpdate
                        instance={company}
                        submit_handle={this.save_HeaderDescription}
                        ></FormExpenseHeaderUpdate>:<FormExpenseHeader
                        submit_handle={this.save_Header}
                        company_data={company_data}
                        ></FormExpenseHeader>
                    }
                </div>
            </div>
            </div>
            </React.Fragment>
        )
    }
}


/* ---------------- GLOBAL  ---------------- */
var page = null


/* ---------------- LOAD  ---------------- */
window.onload = function () {
    page = new Page()
}

/* ---------------- Page Object  ---------------- */
class Page {
    constructor () {
        this.master = new MasterIn()
        this.master.spinner.stop()
        this.wrapper = document.getElementById("wrapper")
        ReactDOM.render(
            <PaymentNew />,
            this.wrapper
        )
    }
}
