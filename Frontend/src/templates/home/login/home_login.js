import MasterOut from '../../../vanillajs/MasterOut'
import Message from '../../../vanillajs/Message'

import './home_login.scss'


/* ---------------- GLOBAL  ---------------- */
var page = null


/* ---------------- LOAD  ---------------- */
window.onload = function () {
    page = new Page()
}


/* ---------------- PAGE OBJECT  ---------------- */

class Page {
    constructor () {
        this.master = new MasterOut()
        this.message = null
        this.field_account = document.getElementById('id_username')
        this.field_password = document.getElementById('id_password')
        this.button_submit = document.getElementById("btn_submit")

        this.init()
        this.set_Events()
    }

    init () {
        this.message = new Message()
        this.master.spinner.stop()
    }

    set_Events() {
        this.button_submit.addEventListener(
            "click",
            this.click_ButtonSubmit.bind(this)
        )
    }

    click_ButtonSubmit(event) {
        this.master.spinner.start()
        if (this.field_account.value ==  "" ||
            this.field_password.value == "" )
        {
            this.master.spinner.stop()
        }
    }
}
