import React from 'react';
import ReactDOM from 'react-dom';
import 'normalize.css'
import './privacity.scss'

import Urls from '../../../app/server/urls';

import TopBar from '../../../reactjs/components/TopBar'
import Footer from '../../../reactjs/components/Footer'


/* ---------------- MainComponent ---------------- */

class EcommercePrivacity extends React.Component {

    render() {

        return (
            <React.Fragment>
                <main className="main">
                    <TopBar
                        url={Urls.dashboard}
                        title={"PetStore"}
                    />
                    <div className="content">
                        <div className="title">
                            AVISO DE PRIVACIDAD
                        </div>

                        <div className="question-wrap">
                            <div className="question">
                                Sint nostrud dolor reprehenderit reprehenderit.
                            </div>
                        </div>


                        <div className="question-wrap">
                            <div className="question">
                                Mollit eu veniam do ea aliqua eu quis.
                            </div>
                            <div className="answer">
                                Eu aute ut do id aliquip Lorem est est eiusmod commodo culpa magna. Amet non do cupidatat id nostrud qui dolor.
                            </div>

                            <div className="answer">
                                Ut mollit deserunt sit deserunt ea tempor. Tempor laboris laboris occaecat sint reprehenderit ex proident pariatur nisi amet incididunt consequat velit.
                            </div>
                        </div>

                        <div className="question-wrap">
                            <div className="question">
                                Duis culpa nisi aliquip cupidatat mollit ullamco eu aliquip ullamco dolor.
                            </div>

                            <div className="answer">
                                Cupidatat eiusmod cillum officia consectetur. Voluptate velit Lorem reprehenderit eu adipisicing.
                            </div>

                            <div className="answer">
                                Voluptate consequat consectetur aliqua anim ut non. Nisi laborum dolor voluptate incididunt ex et do Lorem aliqua qui laborum excepteur.
                            </div>

                            <div className="answer">
                                Sunt non ad incididunt reprehenderit aute id ipsum sit aliquip et nostrud dolore adipisicing. Ad exercitation pariatur do consequat mollit nisi duis do pariatur fugiat.
                            </div>

                            <div className="answer">
                                Eiusmod velit aute sit adipisicing incididunt proident eu aliquip eu do adipisicing consequat. Voluptate dolor enim dolore anim culpa enim adipisicing qui laborum tempor enim nostrud ullamco.
                            </div>

                            <div className="answer">
                                Duis cillum pariatur ad commodo. Quis duis quis voluptate irure ex sit duis aute anim sint aute minim eiusmod.
                            </div>

                            <div className="answer">
                                Dolore reprehenderit tempor irure elit dolor. Sint pariatur enim do eiusmod voluptate dolore nulla nostrud deserunt ex laboris ea ut.
                            </div>

                            <div className="answer">
                                Ea ipsum enim officia ut nisi Lorem commodo. Cupidatat labore labore nulla irure exercitation ipsum esse labore sint.
                            </div>
                        </div>

                        <div className="question-wrap">
                            <div className="question">
                                Incididunt veniam culpa Lorem ex.
                            </div>
                            <div className="answer">
                                Amet non veniam consectetur voluptate voluptate consequat mollit qui dolore aute deserunt voluptate duis. Commodo commodo aute adipisicing commodo.
                            </div>
                        </div>

                        <div className="question-wrap">
                            <div className="question">
                                Anim id eu non pariatur do eiusmod incididunt est fugiat exercitation eu aliquip.
                            </div>
                            <div className="answer">
                                Irure nostrud magna laboris dolor nisi elit elit eu elit. Irure reprehenderit exercitation officia sit velit incididunt veniam aliquip reprehenderit consequat laborum non fugiat.
                            </div>
                        </div>

                        <div className="question-wrap">
                            <div className="question">
                                Elit incididunt veniam do consequat irure exercitation nostrud ullamco aliquip est quis sit ut ullamco.
                            </div>
                            <div className="answer">
                                Fugiat est exercitation irure minim cillum quis officia nostrud aliqua ad et. Quis eiusmod elit do adipisicing laboris ullamco amet.
                            </div>

                            <div className="answer">
                                Occaecat pariatur ad consectetur anim ea cillum ad adipisicing nostrud Lorem enim ex. Aute laboris nulla sit ea irure incididunt exercitation cupidatat.
                            </div>

                            <div className="answer">
                                Minim sit nostrud cillum sit ullamco laborum ipsum in. Ipsum nostrud aute tempor ipsum veniam exercitation adipisicing amet eiusmod occaecat.
                            </div>
                        </div>

                        <div className="question-wrap">
                            <div className="question">
                                Deserunt eu ea voluptate culpa labore ex cillum.
                            </div>
                            <div className="answer">
                                Sit elit do duis sit proident dolore laborum sit reprehenderit laboris. Ipsum excepteur nostrud tempor nostrud consequat deserunt quis eiusmod dolore Lorem sunt.
                            </div>
                        </div>


                        <div className="question-wrap">
                            <div className="question">
                                Dolor cillum duis occaecat eiusmod cupidatat est.
                            </div>
                            <div className="answer">
                                Enim incididunt id deserunt laboris tempor officia consectetur laboris sit nostrud proident dolore labore. Consequat dolore excepteur non consequat aliquip quis.
                            </div>
                        </div>


                        <div className="question-wrap">
                            <div className="question">
                                Voluptate occaecat do cupidatat exercitation aliquip esse ea proident dolore irure.
                            </div>
                            <div className="answer">
                                Aute eu nisi cupidatat sunt dolore consequat velit esse laboris proident labore aute. Adipisicing voluptate in exercitation aute consectetur nisi irure.
                            </div>
                        </div>

                        <div className="question-wrap">
                            <div className="question">
                                Anim esse do laborum do sint labore.
                            </div>
                            <div className="answer">
                                Nulla eu laboris veniam aliquip ut et incididunt minim ea aliqua reprehenderit. Ullamco culpa sit pariatur pariatur cupidatat occaecat est Lorem esse.
                            </div>
                        </div>

                        <div className="leyend">
                            Última modificación: 13 de Enero 2019
                        </div>

                    </div>
                </main>
                <Footer />
            </React.Fragment>
        )
    }
}


/* ---------------- GLOBAL  ---------------- */
var page = null;


/* ---------------- LOAD  ---------------- */
window.onload = function () {
    page = new Page()
}

/* ---------------- Page Object  ---------------- */
class Page {
    constructor() {
        this.wrapper = document.getElementById("wrapper")
        ReactDOM.render(
            <EcommercePrivacity></EcommercePrivacity>,
            this.wrapper
        )
    }
}
