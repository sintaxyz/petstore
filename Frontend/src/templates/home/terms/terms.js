import React from 'react';
import ReactDOM from 'react-dom';
import 'normalize.css'
import './terms.scss'

import Urls from '../../../app/server/urls';

import TopBar from '../../../reactjs/components/TopBar'
import Footer from '../../../reactjs/components/Footer'


/* ---------------- MainComponent ---------------- */

class Terms extends React.Component {

    constructor(props) {
        super(props)
    }

    render() {

        return (
            <React.Fragment>
                <main className="main">
                    <TopBar
                        url={Urls.dashboard}
                        title={"PetStore"}
                    />
                    {/* <Header url_img={url_app_logo} option={""} /> */}
                    <div className="content">
                        <div className="title">
                            TÉRMINOS Y CONDICIONES DE USO
                        </div>

                        <div className="question-wrap">
                            <div className="answer">
                                Laborum aliquip magna excepteur sunt cupidatat voluptate ex aliquip. Aliqua voluptate ipsum ullamco qui officia pariatur veniam ullamco sunt velit labore proident velit.
                            </div>
                        </div>

                        <div className="question-wrap">
                            <div className="question">
                                Veniam mollit ipsum reprehenderit excepteur duis ex excepteur sunt.
                            </div>

                            <div className="answer">
                                Nulla id laborum non adipisicing eu commodo laboris deserunt sunt. Exercitation fugiat labore excepteur enim.
                            </div>

                            <div className="answer">
                                Do pariatur cupidatat elit mollit mollit ipsum velit id excepteur ipsum. Proident enim do cupidatat esse in dolor id esse velit anim pariatur enim aliqua.
                            </div>

                            <div className="answer">
                                Enim tempor id deserunt proident fugiat elit consectetur sunt id deserunt ullamco veniam dolor. Laborum laborum culpa exercitation consectetur incididunt aliqua.
                            </div>

                            <div className="answer">
                                In ullamco cillum ut eiusmod ullamco sit cillum elit laboris dolor proident nostrud. Officia enim tempor sit do incididunt consequat ipsum.
                            </div>

                            <div className="answer">
                                Esse commodo et in id velit sint et consectetur dolore exercitation. Ea Lorem sint pariatur deserunt eu ea ipsum consequat tempor adipisicing consectetur et est laboris.
                            </div>
                        </div>

                        <div className="question-wrap">
                            <div className="question">
                                Labore in mollit labore amet veniam non esse proident mollit do pariatur incididunt voluptate commodo.
                            </div>
                            <div className="answer">
                                Officia consequat reprehenderit aliqua sit ad non. Ea reprehenderit dolor esse et labore quis nostrud amet est veniam dolore sit ex consequat.
                            </div>
                        </div>

                        <div className="question-wrap">
                            <div className="question">
                                Irure incididunt aliqua ut exercitation.
                            </div>
                            <div className="answer">
                                Do magna duis incididunt nostrud et mollit ad excepteur ullamco elit sunt eu cupidatat. Sunt consequat incididunt labore quis ex aliquip eu tempor incididunt aliqua non.
                            </div>
                            <div className="answer">
                                Laborum cupidatat proident culpa veniam. Do Lorem occaecat do in elit occaecat cillum nostrud officia cupidatat.
                            </div>
                        </div>

                        <div className="question-wrap">
                            <div className="question">
                                Consequat minim reprehenderit ex dolore pariatur non ullamco sunt laboris incididunt.
                            </div>
                            <div className="answer">
                                Ipsum voluptate laborum incididunt irure aliquip dolor ex. Eu exercitation pariatur enim exercitation.
                            </div>
                            <div className="answer">
                                Aliquip ea est ipsum do velit reprehenderit quis. In proident amet labore sunt est labore nisi commodo commodo veniam.
                            </div>
                            <div className="answer">
                                Irure commodo deserunt reprehenderit consectetur amet eu nostrud proident cupidatat sit. Est sit pariatur ut dolor enim tempor exercitation laborum consequat fugiat fugiat qui.
                            </div>
                            <div className="answer">
                                Ut anim sint labore fugiat incididunt consequat veniam exercitation occaecat. Ullamco sunt non nostrud proident consequat dolore exercitation.
                            </div>
                        </div>

                        <div className="question-wrap">
                            <div className="question">
                                Cupidatat laborum id qui veniam commodo anim reprehenderit reprehenderit.
                            </div>
                            <div className="answer">
                                Cillum nulla pariatur exercitation exercitation eu ea laborum quis duis culpa nisi aliquip minim esse. Laboris minim voluptate Lorem laborum nostrud ad dolor.
                            </div>
                            <div className="answer">
                                Enim occaecat anim enim ipsum voluptate non commodo. Voluptate eiusmod et Lorem dolor anim esse.
                            </div>
                            <div className="answer">
                                Pariatur culpa irure voluptate ea non. Labore non irure id non aliqua eu voluptate elit sunt nostrud excepteur laborum amet.
                            </div>
                            <div className="answer">
                                Est laborum dolore ex officia duis elit non mollit esse duis ullamco minim magna cupidatat. Officia minim veniam sit cillum reprehenderit pariatur est consequat labore.
                            </div>
                            <div className="answer">
                                Ipsum dolor et eiusmod sit irure laborum et tempor cupidatat velit ullamco eu proident. Incididunt anim aliqua eu mollit officia consequat minim ex eu sit et exercitation cupidatat.
                            </div>
                            <div className="answer">
                                Ex est laborum excepteur fugiat do aliquip sint esse adipisicing veniam non. Laboris nisi pariatur elit est esse proident.
                            </div>
                            <div className="answer">
                                Cupidatat magna voluptate esse excepteur velit ullamco mollit. Non exercitation enim eiusmod pariatur Lorem consequat laboris non aute.
                            </div>
                            <div className="answer">
                                Commodo cillum aute excepteur pariatur non laborum ipsum dolor. Duis sunt laboris consectetur eu esse irure eiusmod ut minim aute eiusmod eiusmod quis.
                            </div>
                            <div className="answer">
                                Anim sunt sint enim occaecat sint cillum sint veniam cupidatat id amet mollit ex. Est irure consectetur occaecat consequat ullamco reprehenderit do enim nisi adipisicing excepteur labore nostrud duis.
                            </div>
                            <div className="answer">
                                Esse veniam aliqua laborum aute. Proident ad id laborum voluptate dolore ea amet non dolor culpa nisi nostrud.
                            </div>
                            <div className="answer">
                                Commodo sunt ex sit occaecat voluptate aliqua sint ea laboris do sunt ad commodo proident. Cillum dolore ea Lorem velit cupidatat nulla est magna adipisicing cupidatat deserunt aute ad.
                            </div>
                            <div className="answer">
                                Et quis nisi quis commodo aliqua culpa laboris. Dolore est in nulla incididunt sint consequat nulla sit minim.
                            </div>
                            <div className="answer">
                                Irure et magna adipisicing culpa qui voluptate qui et commodo ex ipsum nulla voluptate. Sunt et officia aliquip reprehenderit exercitation ad esse culpa eiusmod dolor do adipisicing.
                            </div>
                            <div className="answer">
                                Sint deserunt in ipsum consectetur qui cupidatat nisi dolore sit deserunt occaecat fugiat Lorem sit. Adipisicing do elit esse incididunt occaecat aliqua occaecat est laboris laboris esse ipsum sint eu.
                            </div>
                            <div className="answer">
                                Fugiat in cillum eu aliquip ut qui mollit pariatur sit consectetur id tempor. Sint aute minim commodo sit pariatur mollit consectetur.
                            </div>
                            <div className="answer">
                                Irure et eu sint cupidatat exercitation qui commodo mollit ex consectetur ullamco. Exercitation eiusmod exercitation in reprehenderit nostrud.
                            </div>
                            <div className="answer">
                                Voluptate mollit incididunt ea dolore duis elit laboris aute non nulla do eu officia pariatur. Id velit reprehenderit nisi esse nostrud labore pariatur eiusmod exercitation irure.
                            </div>
                        </div>

                        <div className="question-wrap">
                            <div className="question">
                                Irure incididunt reprehenderit ex officia ea sit.
                            </div>
                            <div className="answer">
                                Veniam pariatur dolor veniam dolore ut tempor eu laborum. Duis quis ipsum ut culpa cillum commodo exercitation incididunt nostrud laborum nostrud fugiat fugiat esse.
                            </div>
                            <div className="answer">
                                Ad et commodo cillum nulla ea deserunt deserunt esse eu enim voluptate. Quis sunt non veniam culpa consectetur deserunt ullamco.
                            </div>
                            <div className="answer">
                                Aliqua enim elit cupidatat sunt proident qui. Ullamco labore anim excepteur cupidatat officia esse.
                            </div>
                        </div>

                        <div className="question-wrap">
                            <div className="question">
                                Adipisicing pariatur anim veniam sint consectetur.
                            </div>
                            <div className="answer">
                                Proident esse Lorem eiusmod ex culpa aliquip cupidatat officia magna. Proident ut veniam mollit ex.
                            </div>
                            <div className="answer">
                                Elit esse consequat magna sint. Eu ea Lorem reprehenderit eu occaecat exercitation elit magna ut.
                            </div>
                            <div className="answer">
                                Irure ad proident eiusmod do ut occaecat magna sint quis pariatur. Reprehenderit eu sint reprehenderit ipsum enim nisi quis.
                            </div>
                            <div className="answer">
                                Nostrud proident Lorem officia adipisicing qui labore consequat in qui elit nisi reprehenderit. Voluptate ullamco aliquip deserunt minim commodo eu dolore ipsum qui dolor amet labore pariatur enim.
                            </div>
                        </div>

                        <div className="question-wrap">
                            <div className="question">
                                Fugiat dolor aliqua fugiat voluptate ex consequat velit esse fugiat aute.
                            </div>
                            <div className="answer">
                                Id incididunt pariatur ipsum officia anim officia. Et labore id eiusmod qui sint nostrud consequat ad deserunt nisi qui eiusmod ex.
                            </div>
                            <div className="answer">
                                Minim ea voluptate consequat Lorem cupidatat dolore exercitation elit ad eiusmod. Dolor reprehenderit velit minim irure veniam voluptate enim.
                            </div>
                            <div className="answer">
                                Occaecat qui laborum tempor pariatur nisi mollit aute sit magna enim. Proident est consectetur excepteur labore aute magna nulla consectetur consequat aliqua deserunt est voluptate qui.
                            </div>
                            <div className="answer">
                                Ad incididunt magna in proident aliquip laboris fugiat irure do. Ad ad do ex occaecat.
                            </div>
                        </div>

                        <div className="question-wrap">
                            <div className="question">
                                Irure dolore velit aliqua eiusmod est tempor in Lorem est.
                            </div>
                            <div className="answer">
                                Est et est ex anim cupidatat incididunt aliquip nisi in in elit nostrud commodo duis. Nulla ea deserunt in labore id dolor incididunt aute.
                            </div>
                            <div className="answer">
                                Quis eiusmod non ad laborum ut nulla irure irure duis aliquip. Voluptate culpa irure esse est duis.
                            </div>
                        </div>

                        <div className="question-wrap">
                            <div className="question">
                                Dolor voluptate excepteur ullamco voluptate amet.
                            </div>
                            <div className="answer">
                                Incididunt deserunt pariatur reprehenderit ea magna officia esse magna nostrud culpa deserunt laboris consectetur velit. Laborum tempor non commodo adipisicing nulla excepteur laboris reprehenderit proident dolor in mollit sunt minim.
                            </div>
                            <div className="answer">
                                Culpa aliqua commodo eiusmod irure adipisicing duis. Laborum aliqua dolor voluptate cupidatat aliquip velit velit dolore.
                            </div>
                            <div className="answer">
                                Nisi sunt aute exercitation velit. Ipsum sint reprehenderit eu enim pariatur esse Lorem mollit culpa commodo amet exercitation aliqua dolor.
                            </div>
                            <div className="answer">
                                Ipsum laborum enim occaecat aliquip qui sunt cillum eiusmod cupidatat ea. Tempor ad voluptate et Lorem sit velit consectetur duis elit laboris.
                            </div>
                        </div>

                        <div className="question-wrap">
                            <div className="question">
                                Nostrud id Lorem reprehenderit dolor Lorem Lorem consectetur quis quis.
                            </div>
                            <div className="answer">
                                Id reprehenderit et cillum anim velit duis cillum commodo anim. Ad ipsum consequat ex ipsum pariatur.
                            </div>
                        </div>

                        <div className="question-wrap">
                            <div className="question">
                                Amet sit amet nisi quis.
                            </div>
                            <div className="answer">
                                Ex irure dolore laborum sint dolor eiusmod duis reprehenderit ipsum incididunt. Consectetur ut esse consequat dolor nostrud laboris occaecat magna sint nulla cillum sint sint Lorem.
                            </div>
                        </div>

                        <div className="question-wrap">
                            <div className="question">
                                Adipisicing ad deserunt sit cupidatat aliquip officia duis eiusmod.
                            </div>
                            <div className="answer">
                                Esse laboris irure dolore non in sint eiusmod. Dolore voluptate laboris pariatur sint aute minim cupidatat reprehenderit consequat.
                            </div>
                            <div className="answer">
                                Deserunt nisi ipsum do eiusmod velit Lorem adipisicing esse. Culpa ad fugiat officia sit ut.
                            </div>
                        </div>

                        <div className="question-wrap">
                            <div className="question">
                                Quis dolore anim cillum nostrud ipsum dolor.
                            </div>
                            <div className="answer">
                                Deserunt duis do fugiat sint nulla sint enim deserunt irure eiusmod. Nulla mollit reprehenderit sint pariatur ea aute nisi.
                            </div>
                        </div>

                        <div className="leyend">
                            Última modificación: 13 de Enero 2019
                        </div>
                    </div>
                </main>
                <Footer />
            </React.Fragment>
        )
    }
}

/* ---------------- GLOBAL  ---------------- */
var page = null;


/* ---------------- LOAD  ---------------- */
window.onload = function () {
    page = new Page()
}

/* ---------------- Page Object  ---------------- */
class Page {
    constructor() {
        this.wrapper = document.getElementById("wrapper")
        ReactDOM.render(
            <Terms></Terms>,
            this.wrapper
        )
    }
}
