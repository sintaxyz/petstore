import React, { useState, useEffect } from 'react'
import ReactDOM from 'react-dom'
import 'normalize.css'

import MasterIn from '../../../vanillajs/MasterIn'
import PopupAlert from '../../../reactjs/components/PopupAlert'
import ChartColumn from '../../../reactjs/components/ChartColumn'
import ChartLine from '../../../reactjs/components/ChartLine'
import DashboardButtonsCards from '../../../reactjs/components/DashboardButtonsCards'
import FiltersHeader from '../../../reactjs/components/FiltersHeader'
import Filter from '../../../reactjs/components/Filter'
import FilterDate from '../../../reactjs/components/FilterDate'
import Item from '../../../app/bsn/Item'
import Warehouse from '../../../app/bsn/Warehouse'
import Company from '../../../app/bsn/Company'
import SaleOrder from '../../../app/bsn/SaleOrder'
import Payment from '../../../app/bsn/Payment'
import Expense from '../../../app/bsn/Expense'
import Client from '../../../app/bsn/Client'
import BranchOffice from '../../../app/bsn/BranchOffice'
import '../../../styles/templates/_simple.scss'
import 'stx-vanilla/src/Content.js'
import 'stx-vanilla/src/ListCards'
import 'stx-vanilla/src/ListCardBtns'
import './home_dashboard.scss'
import { toHtml } from '@fortawesome/fontawesome-svg-core'
import CardDashboard from '../../../reactjs/components/CardDashboard'

/* ---------------- MainComponent ---------------- */

const build_Options = (data, display_value, property_value) => {
    if (display_value) {
       var property_display = display_value
    } else {
        var property_display = 'name'
    }
    if (property_value) {
       var property_value = property_value
    } else {
        var property_value = 'id'
    }
    
    let options = data.map((item, index, array) => {
        let new_item = {}
        new_item.value = item[property_value]
        new_item.label = item[property_display]
        return new_item
    })

    return options
};

function close_Alert(fnc){
    fnc(null)
}

const DashboardImages = (props) => {

    const [company, setCompany] = useState(null)
    const [status, setStatus] = useState(null)
    const [error_message, setErrorMessage] = useState(null)
    const [success_message, setSucessMessage] = useState(null)
    const [data_pvse, setDataPVSE] = useState([])
    const [result_pvse, setResultPVSE] = useState(null)
    const [data_cxc, setDataCXC] = useState([])
    const [data_sell, setDataSell] = useState([])
    const [data_stock, setDataStock] = useState([])

    async function get_ItemSell(){
        let filters = await get_FiltersSell()
        let sell_records = await Item.most_Selled(filters)
        let base_sell = []
        sell_records.forEach(element => {
            base_sell.push([element.primary_number, element.total_amount])
        });
        return base_sell
    }
    
    async function get_ItemStock(){
        let filters = await get_FiltersStock()
        let stock_records = await Item.most_InStock(filters)
        let base_stock = []
        stock_records.forEach(element => {
            base_stock.push([element.primary_number, element.total_amount])
        });
        return base_stock
    }
    
    async function get_CountsReceivable(){
        let filters = await get_FiltersCXC()
        let cxc_records = await SaleOrder.accounts_Receivable(filters)
        let base_cxc = []
        cxc_records.forEach(element => {
            base_cxc.push([element.credit_days, element.amount_outstanding__sum])
        });
        return base_cxc        
    }

    async function get_PayVSExt(){
        let filters = await get_FiltersPVSE()
        let payment_records = await Payment.payments_InDays(filters)
        let expense_records = await Expense.expense_InDays(filters)
        let expense = []
        let payment = []
        let base_pvse = []
        payment_records.records.forEach(element => {
            payment.push([element.confirmed_date, element.amount__sum])
        });
        expense_records.records.forEach(element => {
            expense.push([element.date, element.amount__sum])
        });
        base_pvse.push({title: "Pagos", data: payment}, {title: "Egresos", data: expense})

        let payments = payment_records.total ? payment_records.total : 0.00
        let expenses = expense_records.total ? expense_records.total : 0.00
        let utility = payments - expenses
        console.log(payments - expenses);
        
        setResultPVSE({
            payments: payments,
            expenses: expenses,
            utility: utility
        })
        return base_pvse
    }

    async function get_FiltersSell(){
        let filters =[] 
        if (branchoffice_sell){
            filters.push({label: "branchoffice", value: branchoffice_sell})
        }
        if (client_sell){
            filters.push({label: "client", value: client_sell})
        }
        if (paymethod_sell){
            filters.push({label: "paymethod", value: paymethod_sell})
        }
        if (company){
            filters.push({label: "company", value: company})
        }
        return filters
    }

    async function get_FiltersStock(){
        let filters =[] 
        if (warehouse_stock){
            filters.push({label: "warehouse", value: warehouse_stock})
        }
        if (company){
            filters.push({label: "company", value: company})
        }
        return filters
    }

    async function get_FiltersCXC(){
        let filters =[] 
        if (branchoffice_cxc){
            filters.push({label: "branchoffice", value: branchoffice_cxc})
        }
        if (client_cxc){
            filters.push({label: "client", value: client_cxc})
        }
        if (company){
            filters.push({label: "company", value: company})
        }
        return filters
    }

    async function get_FiltersPVSE(){
        let filters =[] 
        if (start_date_pvse){
            filters.push({label: "start_date", value: start_date_pvse})
        }
        if (end_date_pvse){
            filters.push({label: "end_date", value: end_date_pvse})
        }        
        if (company){
            filters.push({label: "company", value: company})
        }
        return filters
    }

    async function get_FiltersGeneral(){
        let filters =[]
        if (company){
            filters.push({label: "company", value: company})
        }
        return filters
    }

    useEffect(() => {
        const fetchData = async () => {
            let cxc = await get_CountsReceivable()
            let pvse = await get_PayVSExt()
            let item_sell = await get_ItemSell()
            let item_stock = await get_ItemStock()

            setDataCXC(cxc)
            setDataPVSE(pvse)
            setDataSell(item_sell)
            setDataStock(item_stock)
        };
        fetchData();
    }, [company]);

    // General Filters
    const [companies_data, setCompanyData] = useState([])

    async function get_Companies(){
        var result = build_Options(await Company.all(), 'commercial_name')
        return result
    }

    useEffect(() => {
        const fetchData = async () => {
            const result = await get_Companies();
            setCompanyData(result);
        };
        fetchData();
    }, []);


    // FilterMostSelledItems
    const [client_sell, setClientSell] = useState(null);
    const [branchoffice_sell, setBranchofficeSell] = useState(null);
    const [paymethod_sell, setPaymethodSell] = useState(null);

    const [clients_data_sell, setClientDataSell] = useState([]);
    const [branchoffices_data_sell, setBranchofficeDataSell] = useState([]);
    const [paymethods_data_sell, setPaymethodDataSell] = useState([]);

    async function get_ClientsSell(){
        let filters = await get_FiltersGeneral()
        let result = build_Options(await Client.for_Chart(filters), 'commercial_name')
        console.log(result);
        return result
    }

    async function get_BranchOfficesSell(){
        let filters = await get_FiltersGeneral()
        var result = build_Options(await BranchOffice.for_Chart(filters), 'name')
        return result
    }

    useEffect(() => {
        const fetchData = async () => {
            const clients = await get_ClientsSell();
            const branchoffices = await get_BranchOfficesSell();
            const paymethods = [
                {value:'CA', label: 'Efectivo'},
                {value:'TA', label: 'Tarjeta'},
                {value:'CR', label: 'Credito'},
            ]
            
            setClientDataSell(clients);
            setBranchofficeDataSell(branchoffices);
            setPaymethodDataSell(paymethods)
        };
        fetchData();
    }, [company]);

    useEffect(() => {
        const fetchData = async () => {
            let data = await get_ItemSell()
            setDataSell(data)
        };
        fetchData();
    }, [client_sell, branchoffice_sell, paymethod_sell]);

    // FilterStocksLevels
    const [warehouse_stock, setWarehouseStock] = useState(null);
    const [warehouses_data_stock, setWarehouseDataStock] = useState([]);

    async function get_WarehousesStock(){
        let filters = await get_FiltersGeneral()
        var result = build_Options(await Warehouse.for_Chart(filters), 'name')
        return result
    }

    useEffect(() => {
        const fetchData = async () => {
            const warehouses = await get_WarehousesStock();
            setWarehouseDataStock(warehouses);
        };
        fetchData();
    }, [company]);

    useEffect(() => {
        const fetchData = async () => {
            let data = await get_ItemStock()
            setDataStock(data)
        };
        fetchData();
    }, [warehouse_stock]);

    // FilterAccountsReceivable
    const [client_cxc, setClientCXC] = useState(null);
    const [branchoffice_cxc, setBranchofficeCXC] = useState(null);

    const [clients_data_cxc, setClientDataCXC] = useState([]);
    const [branchoffices_data_cxc, setBranchofficeDataCXC] = useState([]);

    async function get_ClientsCXC(){
        let filters = await get_FiltersGeneral()
        let result = build_Options(await Client.for_Chart(filters), 'commercial_name')
        return result
    }

    async function get_BranchOfficesCXC(){
        let filters = await get_FiltersGeneral()
        var result = build_Options(await BranchOffice.for_Chart(filters), 'name')
        return result
    }

    useEffect(() => {
        const fetchData = async () => {
            const clients = await get_ClientsCXC();
            const branchoffices = await get_BranchOfficesCXC();
            
            setClientDataCXC(clients);
            setBranchofficeDataCXC(branchoffices);
        };
        fetchData();
    }, [company]);

    useEffect(() => {
        const fetchData = async () => {
            let data = await get_CountsReceivable()
            setDataCXC(data)
        };
        fetchData();
    }, [branchoffice_cxc, client_cxc]);
    const current_date = () => {
        let today = new Date();
        let dd = today.getDate();
        let formmated_dd = dd ? dd.length == 2 : `0${dd}`
        let mm = today.getMonth()+1;
        let formated_mm = mm ? mm.length == 2 : `0${mm}`
        let yyyy = today.getFullYear();
        console.log(formated_dd, formated_mm, yyyy);
        
        return `${formmated_dd}/${formated_mm}/${yyyy}`
    }

    // FilterPayVSExt
    const [start_date_pvse, setStartDatePVSE] = useState("");
    const [end_date_pvse, setEndDatePVSE] = useState("");

    function validate_DateFormat(date) {
        var RegExPattern = /^\d{2,4}\-\d{1,2}\-\d{1,2}$/;
        if ((date.match(RegExPattern)) && (date!='')) {
              return true;
        } else {
              return false
        }
    }

    function format_Date(dateString) {
        var format = dateString.split("-");
        var date = format[1]+'/'+format[2] +'/'+format[0]
        return date        
      }

    async function get_StartDatePVSE(startdate){
        try {
            if(validate_DateFormat(startdate)){
                setStartDatePVSE(startdate)
                validate_DateDifference(startdate=startdate)
            } else {
                setStartDatePVSE("")
            }
        } catch (error) {
            alert(error)
        }
    }

    async function get_EndDatePVSE(enddate){
        try {
            if (validate_DateFormat(enddate)){
                setEndDatePVSE(enddate)
                validate_DateDifference(enddate=enddate)
            } else {
                setEndDatePVSE("")
            }
        } catch (error) {
            alert(error)
        }
    }

    function validate_DateDifference(startdate=start_date_pvse, enddate=end_date_pvse){
        console.log("------------------");
        console.log(startdate, enddate);
        console.log("------------------");
        
        if (enddate === "" && startdate){
            console.log('No existe fecha final')
        } else if (startdate === "" && enddate) {
            console.log('No existe fecha inicial')
        } else if (startdate, enddate) {
            let formated_start = format_Date(startdate)
            let formated_end = format_Date(enddate)
            const date1 = new Date(formated_start);
            const date2 = new Date(formated_end);
            const diffTime = Math.abs(date2 - date1);
            const diffDays = Math.ceil(diffTime / (1000 * 60 * 60 * 24)); 
            if (diffDays > 30) {
                console.log("No es posible filtrar a mas de 30 dias")
            }
        }       
    }

    useEffect(() => {
        const fetchData = async () => {
            let data = await get_PayVSExt()
            setDataPVSE(data)
        };
        fetchData();
    }, [company, start_date_pvse, end_date_pvse]);

    // Usability Functions
    function blurHandle(){
        console.log('blur!');
    }

    var empty_text_sell = <div className="chart-card-empty">
            <p>1.-Crear Articulos</p>
            <p>2.-Crear Almacen</p>
            <p>3.-Crear Sucursal</p>
            <p>4.-Crear Clientes</p>
            <p>5.-Crear Ordenes de Ventas</p>
        </div>

    var empty_text_stock = <div className="chart-card-empty">
            <p>1.-Crear Articulos</p>
            <p>2.-Crear Almacen</p>
            <p>3.-Crear Carga Inicial</p>
            <p>4.-Crear Ordenes de Compra</p>
            <p>5.-Crear Recepción de Articulos</p>
        </div>

    var empty_text_cxc = <div className="chart-card-empty">
            <p>1.-Crear Articulos</p>
            <p>2.-Crear Almacen</p>
            <p>3.-Crear Sucursal</p>
            <p>4.-Crear Clientes con Capacidad de Crédito</p>
            <p>5.-Crear Ordenes de Ventas a Crédito</p>
        </div>


    var empty_text_pvse = <div className="chart-card-empty">
            <p>1.-Crear Articulos</p>
            <p>2.-Crear Almacen</p>
            <p>3.-Crear Sucursal</p>
            <p>4.-Crear Ordenes de Compra</p>
            <p>5.-Crear Ordenes de Ventas</p>
            <p>6.-Crear Ordenes de Egresos</p>
            <p>7.-Crear Ordenes de Pagos</p>
        </div>
    return (
        <div className="dashboard">
            <DashboardButtonsCards/>
            <div className="body-chart-cards">
                {companies_data.length > 1 &&
                <FiltersHeader>
                    <Filter
                        name={"company"}
                        title={"Compañia"}
                        value={company}
                        options={companies_data}
                        change_handle={setCompany}
                        blur_handle={blurHandle}
                        class_name={"chart-filter-header"}
                    />
                </FiltersHeader>
                }
                { data_pvse &&
                    <ChartLine
                        title="Ingresos Vs Egresos"
                        chart_data={data_pvse}
                        empty_text={empty_text_pvse}
                        >
                        <FiltersHeader>
                            <FilterDate
                                name={""}
                                title={"Fecha inicial"}
                                class_name={"field chart-filter-date"}
                                change_handle={get_StartDatePVSE}
                                optional={false}                                
                            />
                            <FilterDate
                                name={""}
                                title={"Fecha final"}
                                class_name={"field chart-filter-date"}
                                change_handle={get_EndDatePVSE}
                                optional={false}
                            />
                        </FiltersHeader>
                    </ChartLine>
                }
                { result_pvse &&
                    <CardDashboard
                        title="Total de Ingresos Vs Egresos"
                        data={result_pvse}
                    />
                }
                { data_sell &&
                    <ChartColumn
                    title="Resumen de Ventas"
                    item_type="Monto"
                    chart_data={data_sell}
                    empty_text={empty_text_sell}
                    >
                    <FiltersHeader>
                        <Filter
                            name={"client"}
                            title={"Cliente"}
                            value={client_sell}
                            options={clients_data_sell}
                            change_handle={setClientSell}
                            class_name={"chart-filter-select"}
                            blur_handle={blurHandle}
                        />
                        <Filter
                            name={"branchoffice"}
                            title={"Sucursal"}
                            value={branchoffice_sell}
                            options={branchoffices_data_sell}
                            change_handle={setBranchofficeSell}
                            class_name={"chart-filter-select"}
                            blur_handle={blurHandle}
                        />
                        <Filter
                            name={"paymethod"}
                            title={"Metodo de Pago"}
                            value={paymethod_sell}
                            options={paymethods_data_sell}
                            change_handle={setPaymethodSell}
                            class_name={"chart-filter-select"}
                            blur_handle={blurHandle}
                        />
                    </FiltersHeader>
                    </ChartColumn>
                }
                { data_stock &&
                    <ChartColumn
                    title="Niveles de Stock"
                    item_type="Cantidad"
                    chart_data={data_stock}
                    empty_text={empty_text_stock}
                    >
                    <FiltersHeader>
                        <Filter
                            name={"warehouse"}
                            title={"Almacen"}
                            value={warehouse_stock}
                            options={warehouses_data_stock}
                            change_handle={setWarehouseStock}
                            class_name={"chart-filter-select"}
                            blur_handle={blurHandle}
                        />
                    </FiltersHeader>
                    </ChartColumn>
                }
                { data_cxc &&
                    <ChartColumn
                        title="Cuentas por Cobrar"
                        item_type="Monto"
                        chart_data={data_cxc}
                        empty_text={empty_text_cxc}
                        >
                        <FiltersHeader>
                            <Filter
                                name={"client"}
                                title={"Cliente"}
                                value={client_cxc}
                                options={clients_data_cxc}
                                change_handle={setClientCXC}
                                class_name={"chart-filter-select"}
                                blur_handle={blurHandle}
                            />
                            <Filter
                                name={"branchoffice"}
                                title={"Sucursal"}
                                value={branchoffice_cxc}
                                options={branchoffices_data_cxc}
                                change_handle={setBranchofficeCXC}
                                class_name={"chart-filter-select"}
                                blur_handle={blurHandle}
                            />
                        </FiltersHeader>
                    </ChartColumn>
                }
            </div>
            {
                error_message &&
                <PopupAlert
                    msg={error_message}
                    type={"ERROR"}
                    clase_handle={close_Alert(setErrorMessage)}
                />
            }
        </div>
    )
}


/* ---------------- GLOBAL  ---------------- */
var page = null


/* ---------------- LOAD  ---------------- */
window.onload = function () {
    page = new Page()
}

/* ---------------- Page Object  ---------------- */
class Page {
    constructor () {
        this.master = new MasterIn()
        this.master.spinner.stop()
        this.wrapper = document.getElementById("wrapper")
        ReactDOM.render(
            <DashboardImages />,
            this.wrapper
        )
    }
}
