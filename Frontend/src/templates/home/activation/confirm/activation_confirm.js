import MasterOut from '../../../../vanillajs/MasterOut'
import Message from '../../../../vanillajs/Message'
import './activation_confirm.scss'

/* ---------------- GLOBAL  ---------------- */
var page = null


/* ---------------- LOAD  ---------------- */
window.onload = function () {
    page = new Page()
}


class Page {
    constructor() {
        this.master = new MasterOut()
        this.message = new Message()
        this.init()
    }

    init () {
        this.master.spinner.stop()
    }
}