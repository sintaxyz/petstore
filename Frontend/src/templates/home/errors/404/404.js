// import MasterPreOut from '../../../../vanillajs/components/MasterPreOut'
import 'stx-vanilla/src/Content.js'
import 'stx-vanilla/src/Breadcrum.js'
import 'stx-vanilla/src/CardAside.js'
import 'stx-vanilla/src/CardInfo.js'
import 'stx-vanilla/src/styles/marks.css'
import 'stx-vanilla/src/styles/colors.css'
import 'stx-vanilla/src/CardAside'
import 'stx-vanilla/src/styles/buttons.css'
import 'stx-vanilla/src/TitleMain'
import 'stx-vanilla/src/ListCardBtns'


/* ---------------- GLOBAL  ---------------- */
var page = null


/* ---------------- LOAD  ---------------- */
window.onload = function () {
    page = new Page()
}


/* ---------------- Page Object  ---------------- */
class Page {
    constructor () {
        this.master = new MasterPreOut()
        this.init()
    }

    init() {
        this.master.spinner.stop()
    }
}
