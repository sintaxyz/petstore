import React from 'react';
import ReactDOM from 'react-dom';
import 'normalize.css'
import './questions.scss'

import Urls from '../../../app/server/urls';

import TopBar from '../../../reactjs/components/TopBar'
import Footer from '../../../reactjs/components/Footer'


/* ---------------- MainComponent ---------------- */

class EcommerceQuestions extends React.Component {
    constructor(props) {
        super(props)
    }

    render() {

        return (
            <React.Fragment>
                <main className="main">
                    <TopBar
                        url={Urls.dashboard}
                        title={"PetStore"}
                    />
                    <div className="content">

                        <div className="title">
                            PREGUNTAS FRECUENTES
                        </div>

                        <div className="question-wrap">
                            <div className="question">
                                Irure labore dolor occaecat consequat non ut ut proident irure cupidatat proident eiusmod laboris cillum.
                            </div>
                            <div className="answer">
                                Deserunt et ad ullamco nisi elit quis amet labore excepteur elit pariatur ad. Non commodo irure ea veniam duis.
                            </div>
                        </div>

                        <div className="question-wrap">
                            <div className="question">
                                Sit non voluptate ipsum anim do incididunt aliqua non reprehenderit eiusmod.
                            </div>
                            <div className="answer">
                                Occaecat cillum minim sint cillum aute id irure culpa. Ad aliqua consequat consequat nisi irure duis.
                            </div>
                        </div>

                        <div className="question-wrap">
                            <div className="question">
                                Commodo id irure consequat mollit eiusmod consectetur adipisicing incididunt dolore do eiusmod.
                            </div>
                            <div className="answer">
                                Enim sit est eu incididunt do id ipsum veniam. Esse nulla occaecat reprehenderit eu ea fugiat id id ea deserunt amet consequat.
                            </div>
                        </div>

                        <div className="question-wrap">
                            <div className="question">
                                Occaecat ut amet aliqua adipisicing nisi.
                            </div>
                            <div className="answer">
                                Incididunt labore tempor anim excepteur voluptate et magna nostrud ipsum et commodo non nostrud. Excepteur nulla dolor eiusmod dolore tempor dolor dolore.
                            </div>
                        </div>

                        <div className="question-wrap">
                            <div className="question">
                                Qui aliqua exercitation tempor irure velit amet consectetur commodo laborum.
                            </div>
                            <div className="answer">
                                Consectetur elit ullamco ullamco consequat quis duis reprehenderit qui. Dolore duis dolor labore cillum nulla cupidatat enim aliqua laborum tempor et non.
                            </div>
                        </div>

                        <div className="question-wrap">
                            <div className="question">
                                Nostrud occaecat magna consequat eu minim minim magna dolor enim non do.
                            </div>
                            <div className="answer">
                                Cillum deserunt quis et ullamco ex sit ullamco do reprehenderit. Ad aliqua commodo magna commodo exercitation et minim ex.
                            </div>
                        </div>

                        <div className="question-wrap">
                            <div className="question">
                                Ea exercitation Lorem cillum enim.
                            </div>
                            <div className="answer">
                                Aliquip minim laboris tempor aliquip quis cupidatat tempor minim fugiat tempor elit irure. Lorem officia incididunt exercitation consectetur adipisicing amet exercitation Lorem tempor sit.
                            </div>
                        </div>

                        <div className="question-wrap">
                            <div className="question">
                                Enim amet duis reprehenderit commodo magna reprehenderit velit tempor exercitation incididunt ex irure.
                            </div>
                            <div className="answer">
                                Ullamco fugiat Lorem magna ullamco proident veniam reprehenderit minim ad aliquip esse. Deserunt reprehenderit occaecat duis duis et officia consequat elit.
                            </div>
                        </div>

                        <div className="question-wrap">
                            <div className="question">
                                Laborum aute commodo reprehenderit labore.
                            </div>
                            <div className="answer">
                                Dolor veniam sunt fugiat ea ullamco dolor aute. Est nulla esse et do sint.
                            </div>
                        </div>

                        <div className="question-wrap">
                            <div className="question">
                                Nostrud duis aute dolor magna dolor irure veniam.
                            </div>
                            <div className="answer">
                                Reprehenderit exercitation aute est veniam ea in cillum eu consequat commodo laborum voluptate dolore. Nostrud fugiat ipsum nostrud voluptate excepteur sint culpa incididunt duis.
                            </div>
                        </div>

                        <div className="question-wrap">
                            <div className="question">
                                Mollit do ut sunt duis esse commodo officia.
                            </div>
                            <div className="answer">
                                Minim nostrud nulla consectetur dolor irure consectetur fugiat ullamco qui cillum est exercitation cillum ullamco. Id sit aute exercitation veniam.
                            </div>
                        </div>

                        <div className="question-wrap">
                            <div className="question">
                                Cupidatat reprehenderit sunt laborum veniam ullamco.
                            </div>
                            <div className="answer">
                                Sint enim eiusmod dolor sit sint aliqua eiusmod esse cillum adipisicing. Minim tempor eu ipsum pariatur labore quis eiusmod proident cupidatat consectetur.
                            </div>
                        </div>

                        <div className="question-wrap">
                            <div className="question">
                                Enim dolor quis elit enim consectetur pariatur consequat minim ea.
                            </div>
                            <div className="answer">
                                Sit id nisi irure qui aliquip fugiat amet aute consectetur aliquip exercitation adipisicing pariatur laborum. Ad enim exercitation consequat ea duis tempor reprehenderit.
                            </div>
                        </div>

                        <div className="question-wrap">
                            <div className="question">
                                Consectetur fugiat ut commodo aliqua aliquip incididunt velit.
                            </div>
                            <div className="answer">
                                Esse velit qui incididunt velit quis dolor. Adipisicing sit fugiat officia eu sit consequat ad velit nostrud in ullamco irure.
                            </div>
                        </div>

                        <div className="question-wrap">
                            <div className="question">
                                Laborum occaecat reprehenderit laboris ea laborum consectetur fugiat enim officia sint Lorem.
                            </div>
                            <div className="answer">
                                Ea tempor deserunt ex veniam. Exercitation exercitation in sunt duis eiusmod.
                            </div>
                        </div>

                        <div className="question-wrap">
                            <div className="question">
                                Eiusmod reprehenderit aliqua pariatur consectetur ut minim cupidatat deserunt nisi.
                            </div>
                            <div className="answer">
                                Irure Lorem irure laboris aute voluptate. Laborum tempor quis eiusmod irure.
                            </div>
                        </div>

                        <div className="question-wrap">
                            <div className="question">
                                Tempor velit incididunt incididunt occaecat tempor dolor adipisicing dolore qui labore eu.
                            </div>
                            <div className="answer">
                                Sit nisi irure adipisicing incididunt excepteur aliquip in amet voluptate officia officia fugiat. Enim fugiat non labore dolor exercitation labore reprehenderit.
                            </div>
                        </div>

                        <div className="question-wrap">
                            <div className="question">
                                Exercitation officia ea cillum pariatur laborum culpa cupidatat adipisicing voluptate voluptate incididunt adipisicing pariatur.
                            </div>
                            <div className="answer">
                                Ipsum aliqua nisi id id laboris adipisicing laboris. Adipisicing nulla consequat tempor eu nostrud proident magna reprehenderit excepteur.
                            </div>
                        </div>

                        <div className="question-wrap">
                            <div className="question">
                                Mollit excepteur culpa dolor et magna magna id aliqua nostrud do tempor.
                            </div>
                            <div className="answer">
                                Lorem ut non voluptate quis magna non duis ea. Non dolore qui Lorem nisi ullamco do dolor anim.
                            </div>
                        </div>

                        <div className="question-wrap">
                            <div className="question">
                                Ipsum veniam reprehenderit laborum fugiat sit voluptate duis sint incididunt.
                            </div>
                            <div className="answer">
                                Dolore commodo sit id dolore minim aliquip reprehenderit pariatur cillum culpa sunt Lorem elit adipisicing. Deserunt sit exercitation aliqua eiusmod qui adipisicing mollit sint.
                            </div>
                        </div>

                        <div className="question-wrap">
                            <div className="question">
                                Ipsum adipisicing consectetur pariatur ut nostrud nisi.
                            </div>
                            <div className="answer">
                                Labore est eu ex dolore pariatur officia consectetur ad aliquip elit velit aliqua commodo et. Voluptate amet minim commodo commodo sunt sint pariatur amet non nostrud adipisicing non.
                            </div>
                        </div>

                        <div className="question-wrap">
                            <div className="question">
                                Elit labore ea irure quis proident commodo velit non cillum anim sint nulla sit nisi.
                            </div>
                            <div className="answer">
                                Veniam nostrud reprehenderit sit aliquip minim irure magna duis excepteur cupidatat nisi consequat exercitation. Sit eiusmod cupidatat nostrud id enim amet reprehenderit commodo.
                            </div>
                        </div>

                        <div className="question-wrap">
                            <div className="question">
                                Consequat duis magna id duis ea consequat non ullamco et ipsum elit dolore ea.
                            </div>
                            <div className="answer">
                                Dolore duis qui amet et ut commodo Lorem consectetur. Qui aliquip consequat veniam enim aute ullamco ipsum ad ipsum magna nulla.
                            </div>
                        </div>

                        <div className="question-wrap">
                            <div className="question">
                                Et et aute officia elit aliquip sint tempor quis magna qui occaecat nisi sunt anim.
                            </div>
                            <div className="answer">
                                Id aliquip cupidatat sunt commodo nostrud mollit sunt culpa labore occaecat. Esse nisi eu enim anim fugiat esse Lorem aute do.
                            </div>
                        </div>

                        <div className="leyend">
                            Última modificación: 13 de Enero 2019
                        </div>

                    </div>
                </main>
                <Footer />
            </React.Fragment>
        )
    }
}

/* ---------------- GLOBAL  ---------------- */
var page = null;


/* ---------------- LOAD  ---------------- */
window.onload = function () {
    page = new Page()
}

/* ---------------- Page Object  ---------------- */
class Page {
    constructor() {
        this.wrapper = document.getElementById("wrapper")
        ReactDOM.render(
            <EcommerceQuestions></EcommerceQuestions>,
            this.wrapper
        )
    }
}
