import MasterOut from '../../../../vanillajs/MasterOut'
import Message from '../../../../vanillajs/Message'

import './password_done.scss'

/* ---------------- GLOBAL  ---------------- */
var page = null


/* ---------------- LOAD  ---------------- */
window.onload = function () {
    page = new Page()
}

/* ---------------- Page Object  ---------------- */
class Page {
    constructor () {
        this.master = new MasterOut()
        this.message = new Message()

        this.init()
    }

    init () {
        this.master.spinner.stop()
    }
}
