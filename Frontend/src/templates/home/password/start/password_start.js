import MasterIn from '../../../../vanillajs/MasterIn'
import Message from '../../../../vanillajs/Message'

import '../../../../styles/templates/_form.scss'
import FieldSelect from '../../../../vanillajs/FieldSelect'


/* ---------------- GLOBAL  ---------------- */
var page = null


/* ---------------- LOAD  ---------------- */
window.onload = function () {
    page = new Page()
}


class Page {
    constructor() {
        this.master = new MasterOut()
        this.message = new Message()
        this.init()
    }

    init () {
        this.master.spinner.stop()
    }
}