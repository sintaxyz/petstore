import MasterIn from '../../../../vanillajs/MasterIn'
import Message from '../../../../vanillajs/Message'

import './item_edit.scss'
import FieldSelect from '../../../../vanillajs/FieldSelect'
import CardImage from '../../../../vanillajs/CardImage'
import FieldSelectSatProduct from '../../../../vanillajs/FieldSelectSatProduct'

/* ---------------- GLOBAL  ---------------- */
var page = null


/* ---------------- LOAD  ---------------- */
window.onload = function () {
    page = new Page()
}

/* ---------------- Page Object  ---------------- */
class Page {
    constructor () {
        this.master = new MasterIn()
        this.message = new Message()
        this.company = new FieldSelect({"id":"id_company"})
        this.sat_code = new FieldSelectSatProduct({"id":"id_sat_code"})
        this.udm = new FieldSelectSatProduct({"id":"id_udm"})
        this.image = new CardImage("id_image")
        this.init()
    }
    init () {
        this.master.spinner.stop()
    }
}
