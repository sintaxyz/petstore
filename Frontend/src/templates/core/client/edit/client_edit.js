import MasterIn from '../../../../vanillajs/MasterIn'
import Message from '../../../../vanillajs/Message'

import '../../../../styles/templates/_form.scss'
import FieldSelect from '../../../../vanillajs/FieldSelect'

/* ---------------- GLOBAL  ---------------- */
var page = null


/* ---------------- LOAD  ---------------- */
window.onload = function () {
    page = new Page()
}

/* ---------------- Page Object  ---------------- */
class Page {
    constructor () {
        this.master = new MasterIn()
        this.message = new Message()
        this.nationality = new FieldSelect({'id': 'id_nationality'})
        this.company = new FieldSelect({'id': 'id_company'})
        this.type = new FieldSelect({'id': 'id_type'})
        this.sector = new FieldSelect({'id': 'id_sector'})
        this.credit_days = new FieldSelect({'id': 'id_credit_days'})
        this.init()
    }
    init () {
        this.master.spinner.stop()
    }
}
