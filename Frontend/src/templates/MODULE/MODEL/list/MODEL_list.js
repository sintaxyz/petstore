import MasterIn from 'stx-vanilla/src/MasterIn'
import 'stx-vanilla/src/ContentList'
import 'stx-vanilla/src/Breadcrum'
import 'stx-vanilla/src/ListCards'
import Toolbar from 'stx-vanilla/src/Toolbar'
import FieldSelect from 'stx-vanilla/src/FieldSelect'
import PaginationBar from 'stx-vanilla/src/PaginationBar'
import 'stx-vanilla/src/styles/buttons.css'
import Message from 'stx-vanilla/src/Message'

import 'stx-vanilla/src/FieldText'


/* ---------------- GLOBAL  ---------------- */
var page = null


/* ---------------- LOAD  ---------------- */
window.onload = function () {
    page = new Page()
}

/* ---------------- Page Object  ---------------- */
class Page {
    constructor () {
        this.master = new MasterIn()
        this.searchtoolbar = new Toolbar()
        this.message = new Message()
        this.init()
    }

    init() {
        this.master.spinner.stop()
    }
}
