import MasterIn from 'stx-vanilla/src/MasterIn'

import 'stx-vanilla/src/Breadcrum'
import 'stx-vanilla/src/CardForm'
import 'stx-vanilla/src/CardAside'
import 'stx-vanilla/src/styles/marks.css'
import 'stx-vanilla/src/styles/colors.css'

import FieldSelect from 'stx-vanilla/src/FieldSelect'
import 'stx-vanilla/src/FieldText'
import 'stx-vanilla/src/FieldTextarea'
import 'stx-vanilla/src/FieldCheckbox'

import 'stx-vanilla/src/styles/buttons.css'

import Message from 'stx-vanilla/src/Message'

/* ---------------- GLOBAL  ---------------- */
var page = null


/* ---------------- LOAD  ---------------- */
window.onload = function () {
    page = new Page()
}

/* ---------------- Page Object  ---------------- */
class Page {
    constructor () {
        this.master = new MasterIn()
        this.message = new Message()
        this.init()
    }

    init () {
        this.master.spinner.stop()
    }
}
