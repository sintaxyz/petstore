import MasterIn from '../../../../vanillajs/MasterIn'
import Message from '../../../../vanillajs/Message'

import './company_new.scss'
import FieldSelect from '../../../../vanillajs/FieldSelect'
import CardImage from '../../../../vanillajs/CardImage'

/* ---------------- GLOBAL  ---------------- */
var page = null


/* ---------------- LOAD  ---------------- */
window.onload = function () {
    page = new Page()
}

/* ---------------- Page Object  ---------------- */
class Page {
    constructor () {
        this.master = new MasterIn()
        this.message = new Message()
        this.classification = new FieldSelect({'id': 'id_classification'})
        this.type = new FieldSelect({'id': 'id_type'})
        this.logo = new CardImage("id_logo")
        this.init()
    }
    init() {
        this.master.spinner.stop()
    }
}
