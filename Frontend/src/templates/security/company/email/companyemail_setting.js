import MasterIn from '../../../../vanillajs/MasterIn'
import Message from '../../../../vanillajs/Message'

import './companyemail_setting.scss'
import FieldSelect from '../../../../vanillajs/FieldSelect'
import CardImage from '../../../../vanillajs/CardImage'

/* ---------------- GLOBAL  ---------------- */
var page = null


/* ---------------- LOAD  ---------------- */
window.onload = function () {
    page = new Page()
}

/* ---------------- Page Object  ---------------- */
class Page {
    constructor () {
        this.master = new MasterIn()
        this.message = new Message()        
        this.init()
    }
    init() {
        this.master.spinner.stop()
    }
}
