import MasterIn from '../../../../vanillajs/MasterIn'
import Message from '../../../../vanillajs/Message'

import './user_permissions.scss'
import FieldSelect from '../../../../vanillajs/FieldSelect'


/* ---------------- GLOBAL  ---------------- */
var page = null


/* ---------------- LOAD  ---------------- */
window.onload = function () {
    page = new Page()
}

/* ---------------- Page Object  ---------------- */
class Page {
    constructor () {
        this.master = new MasterIn()
        this.permiso = new FieldSelect({'id': 'id_groups'})

        // let btn_image = document.getElementById("imagecard-footer-btn")
        // if ( btn_image != null ) {
        //     this.image = new ImageCard(btn_image)
        // }

        this.message = new Message()
        this.init()
    }
    init () {
        this.master.spinner.stop()
    }
}
