import MasterIn from '../../../../vanillajs/MasterIn'
import Message from '../../../../vanillajs/Message'

import '../../../../styles/templates/_form.scss'
import FieldSelect from '../../../../vanillajs/FieldSelect'

/* ---------------- GLOBAL  ---------------- */
var page = null


/* ---------------- LOAD  ---------------- */
window.onload = function () {
    page = new Page()
}

/* ---------------- Page Object  ---------------- */
class Page {
    constructor () {
        this.master = new MasterIn()
        this.message = new Message()
        this.puesto_padre = new FieldSelect({'id': 'id_position_father'})
        this.permissions = new FieldSelect({'id': 'id_permissions'})
        this.company = new FieldSelect({'id': 'id_company'})
        this.init()
    }

    init () {
        this.master.spinner.stop()
    }
}
