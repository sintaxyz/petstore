import React from 'react'
import ReactDOM from 'react-dom'
import 'normalize.css'
import './assignment.scss'

import MasterIn from '../../../../vanillajs/MasterIn'
import Urls from '../../../../app/server/urls'
import BarNavigation from '../../../../reactjs/components/BarNavigation'
import TitleMain from '../../../../reactjs/components/TitleMain'

import FormAssignmentHeader from '../../../../reactjs/components/FormAssignmentHeader'
import FormAssignmentHeaderUpdate from '../../../../reactjs/components/FormAssignmentHeaderUpdate'
import DetailAssignmentHeader from '../../../../reactjs/components/DetailAssignmentHeader'
// import FormAssignmentLine from '../../../../reactjs/components/FormAssignmentLine'
import Assignment from '../../../../app/bsn/Assignment'
import AssignmentLine from '../../../../reactjs/components/AssignmentLine'
import ListFormLines from '../../../../reactjs/components/ListFormLines'
import Company from '../../../../app/bsn/Company'
import Employee from '../../../../app/bsn/Employee'
import User from '../../../../app/bsn/User'

import ListItem from '../../../../reactjs/components/ListItem'
import ActionControl from '../../../../reactjs/components/ActionControl'
import ActionButtonRight from '../../../../reactjs/components/ActionButtonRight'
import { set } from 'browser-cookies'


/* ---------------- MainComponent ---------------- */


class AssignmentNew extends React.Component {


    constructor(props) {
        super(props)

        this.state = {
            // Usability
            error_message: null,
            success_message: null,
            show_lines: false,

            // Header Data
            // header: null,
            company: null,
            company_data: [],
            user_data: [],
            employee_data: [],
            branches_data: [],
            position_data: [],

            // Lines Data
            lines_data: [],
        }

        // Handle Events
        this.save_Header = this.save_Header.bind(this);
        this.save_HeaderDescription = this.save_HeaderDescription.bind(this);
    }

    // split
    async componentDidMount() {
        var array_url = [];
        array_url = document.URL.split("/");

        try {
            // Header Data
            let companies = await Company.all()
            let employee = await Employee.get(array_url[5])
            console.log(employee);

            let lines = await Assignment.get_ByUser(employee.user)
            console.log(lines);
            this.setState({
                // Header Data
                company_data: companies,
                user_data: employee,
                lines_data: lines,
                show_lines: true,
            })
        } catch (error) {
            this.setState({
                error_message: error,
            })
        }
    }

    async save_Header(data) {
        try {
            // Header Data
            console.log(data);

            let company_select = await Company.get(data.company)
            let positions = await Company.positions(data.company)
            let branches = await Company.branches(data.company)
            this.setState({
                company: company_select,
                position_data: positions,
                branches_data: branches,
                error_message: null,
                success_message: 'Datos para creacion obtenidos',
            })
        } catch (error) {
            this.setState({
                error_message: error,
                success_message: null,
            })
        }
    }

    async save_HeaderDescription(data, {resetForm}) {
        console.log('HeaderDesciption');

        try {
            let lines_data = this.state.lines_data

            // Header Data
            let header = await Assignment.create(data)
            lines_data.push(header)
            resetForm({})
            this.setState({
                header: header,
                company: null,
                lines_data: lines_data,
                show_lines: true,
                error_message: null,
                success_message: 'Registro creado.',
            })
        } catch (error) {
            this.setState({
                error_message: error,
                success_message: null,
            })
        }
    }

    render() {

        const {
            // Usability
            error_message,
            success_message,
            show_lines,

            // Header Data
            company_data,
            company,
            user_data,
            branches_data,
            position_data,

            // Lines Data
            lines_data
        } = this.state
        console.log(this.state);


        return (
            <React.Fragment>
            <div className="content-header">
                <TitleMain
                    icon={<i className="fas fa-map-marked-alt"></i>}
                    url={Urls.employees}
                    url_label={"Empleados"}
                    text={"Crear Asignacion"}
                ></TitleMain>
            </div>
            <div className="content-body">
                <div className="content-body__section">
                    <div className="card-info-header">
                        <div className="header-item">
                            Cabecera del Documento:
                        </div>
                    </div>
                <div  className="card-form">
                    {company ?
                        <FormAssignmentHeaderUpdate
                        instance={company}
                        user_data={user_data}
                        branches_data={branches_data}
                        position_data={position_data}
                        submit_handle={this.save_HeaderDescription}
                        ></FormAssignmentHeaderUpdate>:<FormAssignmentHeader
                        submit_handle={this.save_Header}
                        company_data={company_data}
                        ></FormAssignmentHeader>
                    }
                </div>
                { show_lines && lines_data.length > 0 &&
                <div className="card-list">
                    <div className="card-list__body">
                        <ListItem>
                            { lines_data.map((lines_data, index)=> {
                                return (
                                    <AssignmentLine
                                    key={index}
                                    instance={lines_data}
                                    ></AssignmentLine>
                                )
                            })}
                        </ListItem>
                    </div>
                </div>
                }

                {/* { show_lines && lines_data.length > 0 &&
                <ActionControl>
                    <ActionButtonRight
                        handle_event={this.finish_Document}
                        tag={'Finalizar'}
                        ></ActionButtonRight>
                </ActionControl>
                } */}
            </div>
            </div>
            </React.Fragment>
        )
    }
}


/* ---------------- GLOBAL  ---------------- */
var page = null


/* ---------------- LOAD  ---------------- */
window.onload = function () {
    page = new Page()
}

/* ---------------- Page Object  ---------------- */
class Page {
    constructor () {
        this.master = new MasterIn()
        this.master.spinner.stop()
        this.wrapper = document.getElementById("wrapper")
        ReactDOM.render(
            <AssignmentNew />,
            this.wrapper
        )
    }
}
