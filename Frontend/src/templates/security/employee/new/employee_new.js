import MasterIn from '../../../../vanillajs/MasterIn'
import Message from '../../../../vanillajs/Message'

import './employee_new.scss'
import CardImage from '../../../../vanillajs/CardImage'
import FieldDate from '../../../../vanillajs/FieldDate'
import FieldSelect from '../../../../vanillajs/FieldSelect'


/* ---------------- GLOBAL  ---------------- */
var page = null


/* ---------------- LOAD  ---------------- */
window.onload = function () {
    page = new Page()
}

/* ---------------- Page Object  ---------------- */
class Page {
    constructor () {
        this.master = new MasterIn()
        this.message = new Message()
        this.gender = new FieldSelect({"id":"id_gender"})
        this.companies = new FieldSelect({"id":"id_companies"})
        this.photography = new CardImage("id_photography")
        this.birthdate = new FieldDate('id_birthdate', 'date')
        this.init()
    }

    init () {
        this.master.spinner.stop()
    }
}
