import MasterIn from '../../../../vanillajs/MasterIn'
import '../../../../styles/templates/_list.scss'

import Message from '../../../../vanillajs/Message'
import Search from '../../../../vanillajs/Search'

import PaginationBar from '../../../../vanillajs/PaginationBar'


/* ---------------- GLOBAL  ---------------- */
var page = null


/* ---------------- LOAD  ---------------- */
window.onload = function () {
    page = new Page()
}

/* ---------------- Page Object  ---------------- */
class Page {
    constructor () {
        this.master = new MasterIn()
        this.search = new Search()
        this.pages = new PaginationBar()
        this.message = new Message()
        this.init()
    }

    init() {
        this.master.spinner.stop()
    }
}
