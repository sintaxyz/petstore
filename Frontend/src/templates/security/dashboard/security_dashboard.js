import MasterIn from '../../../vanillajs/MasterIn'
import '../../../styles/templates/_options.scss'

/* ---------------- GLOBAL  ---------------- */
var page = null


/* ---------------- LOAD  ---------------- */
window.onload = function () {
    page = new Page()
}


/* ---------------- Page Object  ---------------- */
class Page {
    constructor () {
        this.master = new MasterIn()
        this.init()
    }
    init () {
        this.master.spinner.stop()
    }
}
