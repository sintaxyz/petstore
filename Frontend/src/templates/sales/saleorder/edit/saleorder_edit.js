import React from 'react'
import ReactDOM from 'react-dom'
import 'normalize.css'
import './saleorder_edit.scss'

import MasterIn from '../../../../vanillajs/MasterIn'
import Urls from '../../../../app/server/urls'
import BarNavigation from '../../../../reactjs/components/BarNavigation'
import TitleMain from '../../../../reactjs/components/TitleMain'

import FormSaleOrderHeader from '../../../../reactjs/components/FormSaleOrderHeader'
import FormSaleOrderHeaderUpdate from '../../../../reactjs/components/FormSaleOrderHeaderUpdate'
import FormSaleOrderLine from '../../../../reactjs/components/FormSaleOrderLine'
import Tax from '../../../../app/bsn/Tax'
import Client from '../../../../app/bsn/Client'
import Warehouse from '../../../../app/bsn/Warehouse'
import Item from '../../../../app/bsn/Item'
import SaleOrder from '../../../../app/bsn/SaleOrder'
import SaleOrderLine from '../../../../reactjs/components/SaleOrderLine'

import PopupAlert from '../../../../reactjs/components/PopupAlert'
import ListItem from '../../../../reactjs/components/ListItem'
import ActionControl from '../../../../reactjs/components/ActionControl'
import ActionButtonRight from '../../../../reactjs/components/ActionButtonRight'
import { set } from 'browser-cookies'
import BranchOffice from '../../../../app/bsn/BranchOffice'
import AsideContent from '../../../../reactjs/components/AsideContent'
import AsideCard from '../../../../reactjs/components/AsideCard'
import AsideCancel from '../../../../reactjs/components/AsideCancel'


/* ---------------- MainComponent ---------------- */

class SaleOrderNew extends React.Component {

    constructor(props) {
        super(props)

        this.state = {
            // Usability
            error_message: null,
            success_message: null,
            show_lines: false,

            // Header Data
            header: null,
            warehouse_data: [],
            branchoffice_data: [],
            tax_data: [],
            client_data: [],

            // Lines Data
            lines_data: [],
            item_data: [],
        }

        // Handle Events
        this.save_Line = this.save_Line.bind(this);
        this.save_HeaderDescription = this.save_HeaderDescription.bind(this);
        this.finish_Document = this.finish_Document.bind(this);
        this.close_PopupError = this.close_PopupError.bind(this);
        this.close_PopupSuccess = this.close_PopupSuccess.bind(this);
        this.cancel_Order = this.cancel_Order.bind(this);
    }

    async componentDidMount() {
        try {
            var array_url = [];
            array_url = document.URL.split("/");

            // Header Data
            let header = await SaleOrder.get(array_url[5])
            console.log(header);

            let lines = header.in_saleorderitem
            let items = await Warehouse.items_InStockExist(header.warehouse)
            let taxes = await Tax.active()
            this.setState({
                // Header Data
                header: header,
                show_lines: true,
                item_data: items,
                lines_data: lines,
                tax_data: taxes,
            })
        } catch (error) {
            this.setState({
                error_message: error,
            })
        }
    }

    async save_HeaderDescription(data) {
        try {
            // Header Data
            let header = await SaleOrder.update_Description(
                this.state.header.id, data)
            this.setState({
                header: header,
                show_lines: true,
                error_message: null,
                success_message: 'Registro actualizado.',
            })
        } catch (error) {
            this.setState({
                error_message: error,
                success_message: null,
            })
        }
    }

    async save_Line(data, {resetForm}) {
        try {
            // Lines Data
            let line = await SaleOrder.create_Line(
                this.state.header.id, data)
            let lines = this.state.lines_data

            // Header Data
            let header = await SaleOrder.get(this.state.header.id)
            lines.push(line)
            // Form Options
            resetForm({})
            this.setState({
                header: header,
                error_message: null,
                success_message: 'Linea registrada.',
                lines_data: lines,
            })
        } catch (error) {
            this.setState({
                error_message: error,
                success_message: null,
            })
        }
    }

    async finish_Document() {
        try {
            // Header Data
            let header = await SaleOrder.finish(
                this.state.header.id)
            window.location.replace(Urls.sale_orders)
            this.setState({
                header: header,
                success_message: 'Registro finalizado.',
                error_message: null,
            })
        } catch (error) {
            this.setState({
                error_message: error,
                success_message: null,
            })
        }
    }

    async cancel_Order() {
        try {
            // Header Data
            let header = await SaleOrder.cancel(
                this.state.header.id)
            window.location.replace(Urls.sale_orders)
            this.setState({
                header: header,
                success_message: 'Orden de Venta Cancelada.',
                error_message: null,
            })
        } catch (error) {
            this.setState({
                error_message: error,
                success_message: null,
            })
        }
    }

    async close_PopupError() {
        this.setState({
            error_message: null,
        })
    }

    async close_PopupSuccess() {
        this.setState({
            success_message: null,
        })
    }

    render() {

        const {
            // Usability
            error_message,
            success_message,
            show_lines,

            // Header Data
            header,
            warehouse_data,
            branchoffice_data,
            tax_data,
            client_data,

            // Lines Data
            item_data,
            lines_data
        } = this.state

        return (
            <React.Fragment>
            <div className="content-header">
                <TitleMain
                    icon={<i className="fas fa-file-upload"></i>}
                    url={Urls.sale_orders}
                    url_label={"Ordenes de Venta"}
                    text={"Editar Orden de Venta"}
                ></TitleMain>
            </div>
            <div className="content-body">
                <div className="content-body__section">
                    <div  className="card-form">
                        <div className="card-form-header">
                            Cabecera del Documento:
                        </div>
                        {header &&
                            <FormSaleOrderHeaderUpdate
                                instance={header}
                                submit_handle={this.save_HeaderDescription}
                            ></FormSaleOrderHeaderUpdate>
                        }
                    </div>
                    { show_lines && item_data.length > 0 &&
                    <div  className="card-form">
                        <div className="card-form-header">
                            Agregar Linea
                        </div>
                        <FormSaleOrderLine
                            submit_handle={this.save_Line}
                            tax_data={tax_data}
                            item_data={item_data}
                        ></FormSaleOrderLine>
                    </div>
                    }
                    { show_lines && lines_data.length > 0 &&
                    <div className="card-list">
                        <div className="card-list__body">
                            <ListItem>
                                { lines_data.map((item, index)=> {
                                    return (
                                        <SaleOrderLine
                                        key={index}
                                        instance={item}
                                        ></SaleOrderLine>
                                    )
                                })}
                            </ListItem>
                        </div>
                    </div>
                    }
                    { show_lines && lines_data.length > 0 &&
                    <ActionControl>
                        <ActionButtonRight
                            handle_event={this.finish_Document}
                            tag={'Finalizar'}
                        ></ActionButtonRight>
                    </ActionControl>
                    }
                </div>
                <AsideContent>
                    <AsideCard title={'Otras Acciones'}>
                        <AsideCancel
                            icon={<i className="fas fa-window-close"></i>}
                            handle_event={this.cancel_Order}
                            tag={' Cancelar'}
                        ></AsideCancel>
                    </AsideCard>
                </AsideContent>
            </div>
            { error_message &&
            <PopupAlert
                close_handle={this.close_PopupError}
                msg={error_message}
                type={"ERROR"}
            />
            }
            { success_message &&
            <PopupAlert
                close_handle={this.close_PopupSuccess}
                msg={success_message}
                type={"SUCCESS"}
            />
            }
            </React.Fragment>
        )
    }
}


/* ---------------- GLOBAL  ---------------- */
var page = null


/* ---------------- LOAD  ---------------- */
window.onload = function () {
    page = new Page()
}

/* ---------------- Page Object  ---------------- */
class Page {
    constructor () {
        this.master = new MasterIn()
        this.master.spinner.stop()
        this.wrapper = document.getElementById("wrapper")
        ReactDOM.render(
            <SaleOrderNew />,
            this.wrapper
        )
    }
}