import MasterIn from '../../../../vanillajs/MasterIn'
import Message from '../../../../vanillajs/Message'

import '../../../../styles/templates/_retrieve.scss'


/* ---------------- GLOBAL  ---------------- */
var page = null


/* ---------------- LOAD  ---------------- */
window.onload = function () {
    page = new Page()
}

/* ---------------- Page Object  ---------------- */
class Page {
    constructor () {
        this.master = new MasterIn()
        this.message = new Message()
        this.master.spinner.stop()
    }
}