import React from 'react'
import ReactDOM from 'react-dom'
import 'normalize.css'
import './payment_new.scss'

import MasterIn from '../../../../vanillajs/MasterIn'
import Urls from '../../../../app/server/urls'
import BarNavigation from '../../../../reactjs/components/BarNavigation'
import TitleMain from '../../../../reactjs/components/TitleMain'

import FormPaymentHeader from '../../../../reactjs/components/FormPaymentHeader'
import FormPaymentHeaderUpdate from '../../../../reactjs/components/FormPaymentHeaderUpdate'
import FormPayment from '../../../../reactjs/components/FormPayment'
import Payment from '../../../../app/bsn/Payment'
import Client from '../../../../app/bsn/Client'
import SaleOrder from '../../../../app/bsn/SaleOrder'

import PopupAlert from '../../../../reactjs/components/PopupAlert'
import { set } from 'browser-cookies'


/* ---------------- MainComponent ---------------- */

class PaymentNew extends React.Component {

    constructor(props) {
        super(props)

        this.state = {
            // Usability
            error_message: null,
            success_message: null,

            // Header Data
            client: null,
            client_data: [],
            saleorders_data: [],
        }

        // Handle Events
        this.save_Header = this.save_Header.bind(this);
        this.save_HeaderDescription = this.save_HeaderDescription.bind(this);
        // this.save_Payment = this.save_Payment.bind(this);
        // this.close_PopupError = this.close_PopupError.bind(this);
        // this.close_PopupSuccess = this.close_PopupSuccess.bind(this);
    }

    async componentDidMount() {
        try {
            // Header Data
            let clients = await Client.client_debtor()
            console.log(clients);

            this.setState({
                // Header Data
                // error_message: null,
                client_data: clients,
            })
        } catch (error) {
            this.setState({
                error_message: error,
            })
        }
    }

    async save_Header(data) {
        try {
            // Header Data
            // Lines Data
            console.log(data);

            let client = await Client.retrieve(data.client)
            let saleorders = await SaleOrder.pendingpay_listByClient(data.client)

            this.setState({
                client: client,
                saleorders_data: saleorders,

                show_lines: true,
                error_message: null,
                success_message: 'Datos del cliente obtenidos',
            })
        } catch (error) {
            this.setState({
                error_message: error,
                success_message: null,
            })
        }
    }

    async save_HeaderDescription(data) {
        console.log('HeaderDesciption');

        try {
            // Header Data
            let header = await Payment.create(data)
            window.location.replace(Urls.payments)
            this.setState({
                header: header,
                show_lines: true,
                error_message: null,
                success_message: 'Registro creado.',
            })
        } catch (error) {
            this.setState({
                error_message: error,
                success_message: null,
            })
        }
    }

    render() {

        const {
            // Usability
            error_message,
            success_message,
            show_lines,

            // Header Data
            client,
            client_data,
            saleorders_data,
        } = this.state
        console.log(this.state);

        return (
            <React.Fragment>
            <div className="content-header">
                <TitleMain
                    icon={<i className="fas fa-file-invoice-dollar"></i>}
                    url={Urls.payments}
                    url_label={"Pagos"}
                    text={"Crear Pago"}
                ></TitleMain>
            </div>
            <div className="content-body">
                <div className="content-body__section">
                    <div className="card-info-header">
                        <div className="header-item">
                            Cabecera del Documento:
                        </div>
                    </div>
                <div  className="card-form">
                    {client ?
                        <FormPaymentHeaderUpdate
                        instance={client}
                        saleorders_data= {saleorders_data}
                        submit_handle={this.save_HeaderDescription}
                        ></FormPaymentHeaderUpdate>:<FormPaymentHeader
                        submit_handle={this.save_Header}
                        client_data={client_data}
                        ></FormPaymentHeader>
                    }
                </div>
            </div>
            </div>
            </React.Fragment>
        )
    }
}


/* ---------------- GLOBAL  ---------------- */
var page = null


/* ---------------- LOAD  ---------------- */
window.onload = function () {
    page = new Page()
}

/* ---------------- Page Object  ---------------- */
class Page {
    constructor () {
        this.master = new MasterIn()
        this.master.spinner.stop()
        this.wrapper = document.getElementById("wrapper")
        ReactDOM.render(
            <PaymentNew />,
            this.wrapper
        )
    }
}
