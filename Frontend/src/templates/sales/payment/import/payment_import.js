import MasterIn from '../../../../vanillajs/MasterIn'
import Message from '../../../../vanillajs/Message'
import ImportMessages from '../../../../vanillajs/ImportMessages'

import '../../../../styles/templates/_form.scss'


/* ---------------- GLOBAL  ---------------- */
var page = null


/* ---------------- LOAD  ---------------- */
window.onload = function () {
    page = new Page()
}

/* ---------------- Page Object  ---------------- */
class Page {
    constructor () {
        this.master = new MasterIn()
        this.message = new Message()

        let message_import_obj = document.getElementById("import_msg")
        if ( message_import_obj != null ) {
            this.import_message = new ImportMessages(message_import_obj)
        }
        this.init()
    }

    init () {
        this.master.spinner.stop()
    }
}
