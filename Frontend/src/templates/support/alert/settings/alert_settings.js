import MasterIn from '../../../../vanillajs/MasterIn'
import Message from '../../../../vanillajs/Message'

import '../../../../styles/templates/_form.scss'


/* ---------------- GLOBAL  ---------------- */
var page = null


/* ---------------- LOAD  ---------------- */
window.onload = function () {
    page = new Page()
}

/* ---------------- Page Object  ---------------- */
class Page {
    constructor () {
        this.master = new MasterIn()
        this.message = new Message()
        this.btn = document.getElementById("btn_subscription")

        this.btn.addEventListener(
            "click",
            this.main.bind(this)
        )

        this.init()
    }

    init() {
        // this.main()
        this.master.spinner.stop()
    }

    check() {
        if (!('serviceWorker' in navigator)) {
            throw new Error('No Service Worker support!')
        }
        if (!('PushManager' in window)) {
            throw new Error('No Push API Support!')
        }
    }

    async registerServiceWorker() {
        const swRegistration = await navigator.serviceWorker.register(
            'http://127.0.0.1:8000/static/build/service.min.js'
        )
        return swRegistration
    }

    async requestNotificationPermission() {
        const permission = await window.Notification.requestPermission();

        if(permission !== 'granted'){
            throw new Error('Permission not granted for Notification');
        }
    }

    showLocalNotification(title, body, swRegistration) {
        const options = {
            body,
        };
        swRegistration.showNotification(title, options);
    }

    async main() {
        console.log("entro")
        this.check()
        try {
            const swRegistration = await this.registerServiceWorker()
            const permission =  await this.requestNotificationPermission()
            console.log("Si entro aca")
            this.showLocalNotification(
                'Se agrega nueva nota',
                'Se agrego nueva nota al inmueble',
                swRegistration
            );
        } catch (error) {
            console.log(`Ocurrio error: ${error.message}`)
            console.log(error)
        }
    }



}