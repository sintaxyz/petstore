import MasterIn from '../../../../vanillajs/MasterIn'
import '../../../../styles/templates/_list.scss'

import Message from '../../../../vanillajs/Message'
import Toolbar from '../../../../vanillajs/Search'
import PaginationBar from '../../../../vanillajs/PaginationBar'

import FieldDate from '../../../../vanillajs/FieldDate'


/* ---------------- GLOBAL  ---------------- */
var page = null


/* ---------------- LOAD  ---------------- */
window.onload = function () {
    page = new Page()
}

/* ---------------- Page Object  ---------------- */
class Page {
    constructor () {
        this.master = new MasterIn()

        this.toolbar = new Toolbar()
        this.pages = new PaginationBar()
        this.message = new Message()

        this.created_date = new FieldDate('id_created_date', 'date')
        this.init()
    }
    init () {
        this.master.spinner.stop()
    }
}
