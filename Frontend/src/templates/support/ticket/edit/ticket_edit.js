import MasterIn from '../../../../vanillajs/MasterIn'
import Message from '../../../../vanillajs/Message'

import '../../../../styles/templates/_form.scss'
import FieldSelect from '../../../../vanillajs/FieldSelect'

/* ---------------- GLOBAL  ---------------- */
var page = null


/* ---------------- LOAD  ---------------- */
window.onload = function () {
    page = new Page()
}

/* ---------------- Page Object  ---------------- */
class Page {
    constructor () {
        this.master = new MasterIn()
        this.message = new Message()
        this.type = new FieldSelect({'id':'id_type'})
        this.status = new FieldSelect({'id':'id_status'})
        this.init()
    }
    init () {
        this.master.spinner.stop()
    }
}
