

function get_Domain() {
    if (PRODUCTION) {
        return `${window.location.origin}/`
    } else {
        return 'http://127.0.0.1:9001/'
    }
}


const App = {
    api_domain: `${window.location.origin}/`,
    logo_url: `${get_Domain()}static/images/logos/logo_crm_main.png`,
    logo_footer_url: `${get_Domain()}static/images/logos/logo_crm_footer.png`,
    img_carrusel_1: `${get_Domain()}static/images/carrusel/pharmacist1.jpg`,
    img_carrusel_2: `${get_Domain()}static/images/carrusel/pharmacist2.jpg`,
    img_carrusel_3: `${get_Domain()}static/images/carrusel/pharmacist3.jpg`,
    img_banner: `${get_Domain()}static/images/decorators/collection-bg.jpg`,
}

export default App