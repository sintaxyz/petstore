from channels.auth import AuthMiddlewareStack
from channels.routing import ProtocolTypeRouter, URLRouter
from communication.routing import websocket_urlpatterns
from channels.security.websocket import OriginValidator
from channels.security.websocket import AllowedHostsOriginValidator
from django.urls import path


application = ProtocolTypeRouter({
    # (http->django views is added by default)
    'websocket': AllowedHostsOriginValidator(
        AuthMiddlewareStack(
            URLRouter(
                websocket_urlpatterns
            )
        ),
    )
})
