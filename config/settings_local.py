from .settings_base import *

DEBUG = True

ALLOWED_HOSTS = ['*', ]

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2',
        'NAME': 'petstoredb',
        'USER': 'postgres',
        'PASSWORD': 'root12345',
        'HOST': 'localhost',
        'PORT': '5432',
    },
}

# App configuration
APP_NAME = "PetStore"
APP_PARTNER_NAME = ""
APP_YEAR_RELEASE = "2020"
APP_VERSION = "0.1"

# FireBase
FIREBASE_KEY = "thepiecesteak-firebase-adminsdk-7k16s-d0499bdf72.json"

# FACTURAMA
FACTURAMA_USERNAME = 'germain'
FACTURAMA_PASSWORD = 'raynor91'
FACTURAMA_SANDBOX = True

# MEDIA
MEDIA_URL = '/media/'

# STATICS
# STATIC_URL = '/static/'
STATIC_URL = 'http://127.0.0.1:9001/static/'

# Logs
# LOG_PATH = os.path.join(BASE_DIR.ancestor(1), "logs", "debug.log")

# LOGGING = {
#     'version': 1,
#     'disable_existing_loggers': False,
#     'handlers': {
#         'file': {
#             'level': 'DEBUG',
#             'class': 'logging.FileHandler',
#             # 'filename': LOG_PATH + "debug.log"
#             'filename': LOG_PATH
#         },
#         'console': {
#             'level': 'DEBUG',
#             'class': 'logging.StreamHandler',
#         }
#     },
#     'loggers': {
#         'error_logger': {
#             'handlers': ['file', 'console'],
#             'level': 'ERROR',
#             'propagate': True,
#         }
#     },
# }
