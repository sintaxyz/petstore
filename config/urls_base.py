# Django's Libraries
from django.conf import settings
from django.contrib import admin
from django.urls import path
from django.urls import include
from django.conf.urls import url
from django.conf.urls import handler404
from django.conf.urls import handler500
from tools.views import Stx404view
from tools.views import Stx500view

# Third-party Libraries
from rest_framework_simplejwt.views import TokenObtainPairView
from rest_framework_simplejwt.views import TokenRefreshView

admin.site.site_header = settings.APP_NAME

urlpatterns = [
    url(r'^jet/', include('jet.urls', 'jet')),  # Django JET URLS
    path('admin/', admin.site.urls),
    # path('api/token/', TokenObtainPairView.as_view(), name='token_obtain_pair'),
    # path('api/token/refresh/', TokenRefreshView.as_view(), name='token_refresh'),
    path('', include('home.urls', namespace='home')),
    # path('api/', include('home.urls_rest', namespace='home-api')),
    path('security/', include('security.urls', namespace='security')),
    path('security/api/', include('security.urls_rest', namespace='security-api')),
    path('support/', include('support.urls', namespace='support')),
    path(
        'support/api/',
        include('support.urls_rest', namespace='support-api')
    ),
    path('core/', include('core.urls', namespace='core')),
    path('core/api/', include('core.urls_rest', namespace='core-api')),

    path('sales/', include('sales.urls', namespace='sales')),
    path('sales/api/', include('sales.urls_rest', namespace='sales-api')),

    path('procurement/', include('procurement.urls', namespace='procurement')),
    path('procurement/api/', include(
        'procurement.urls_rest', namespace='procurement-api'
        )
    ),

    path('inventory/', include('inventory.urls', namespace='inventory')),
    path(
        'inventory/api/',
        include('inventory.urls_rest', namespace='inventory-api')
    ),
]

handler404 = Stx404view
handler500 = Stx500view
