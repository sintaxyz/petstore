# Thrid-party Libraries
from rest_framework import serializers

# Onw's Libraries
from .models import User
from .models import Employee
from .models import BranchOffice
from .models import Company
from .models import CompanyPosition
from .models import Assignment


class UserSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = [
            'id',
            'name',
            'email'
        ]


class UserLoggedSerializer(serializers.ModelSerializer):
    session_key = serializers.SerializerMethodField(
        method_name="get_SessionKey"
    )
    access_qty = serializers.SerializerMethodField(
        method_name="get_AccessQty"
    )
    last_activity = serializers.SerializerMethodField(
        method_name="get_LastActivity"
    )

    class Meta:
        model = User
        fields = [
            'id',
            'name',
            'is_staff',
            'is_active',
            'is_superuser',
            'session_key',
            'access_qty',
            'last_activity'
        ]

    def get_SessionKey(self, obj):
        try:
            return obj.logged_detail.session_key
        except Exception:
            return ""

    def get_AccessQty(self, obj):
        try:
            return obj.logged_detail.access_qty
        except Exception:
            return 0

    def get_LastActivity(self, obj):
        try:
            return obj.logged_detail.last_activity
        except Exception:
            return ""


class EmployeeSerializer(serializers.ModelSerializer):
    user_name = serializers.SerializerMethodField()

    class Meta:
        model = Employee
        fields = [
            'id',
            'user',
            'user_name',
        ]
        read_only_fields = [
            'id',
            'user_name',
        ]

    def get_user_name(self, obj):
        if obj.user.name:
            return obj.user.name
        else:
            return obj.user.email


class BranchOfficeSerializer(serializers.ModelSerializer):

    company_name = serializers.SerializerMethodField()

    class Meta:
        model = BranchOffice
        fields = [
            'id',
            'name',
            'company_name',
            'description',
        ]

    def get_company_name(self, obj):
        return obj.company.legal_entity


class CompanySerializer(serializers.ModelSerializer):

    classification_name = serializers.SerializerMethodField()
    type_desc = serializers.SerializerMethodField()

    class Meta:
        model = Company
        fields = [
            'id',
            'legal_entity',
            'commercial_name',
            'classification_name',
            'type_desc',
        ]

    def get_classification_name(self, obj):
        return obj.classification.name

    def get_type_desc(self, obj):
        return obj.get_type_display()


class CompanyPositionSerializer(serializers.ModelSerializer):
    
    company_name = serializers.SerializerMethodField()

    class Meta:
        model = CompanyPosition
        fields = [
            'id',
            'company_name',
            'name',            
            'position_father',
            'is_active',
        ]
        
    def get_company_name(self, obj):
        return obj.company.legal_entity


class AssignmentSerializer(serializers.ModelSerializer): 
    branches = BranchOfficeSerializer(required=False, many=True)
    company_name = serializers.SerializerMethodField()
    position_name = serializers.SerializerMethodField()
    user_name = serializers.SerializerMethodField()
    start_date_desc = serializers.SerializerMethodField()
    end_date_desc = serializers.SerializerMethodField()
    branches_desc = serializers.SerializerMethodField()

    class Meta:
        model = Assignment
        fields = [
            'id',
            'company',
            'company_name',
            'position',
            'position_name',
            'user',
            'user_name',
            'is_active',
            'start_date',
            'start_date_desc',
            'end_date',
            'end_date_desc',
            'status_text',
            'branches_desc',
            'branches',
        ]
        extra_kwargs = {
            'start_date': {'write_only': True},
            'end_date': {'write_only': True},
        }
        read_only_fields = [
            'id',
            'company_name',
            'position_name',
            'branches_desc',
            'user_name',
            'start_date_desc',
            'end_date_desc',
            'status_text',
        ]
        
    def get_company_name(self, obj):
        return obj.company.legal_entity

    def get_position_name(self, obj):
        return obj.position.name
   
    def get_user_name(self, obj):
        if obj.user.name:
            return obj.user.name
        else:
            return obj.user.email

    def get_start_date_desc(self, obj):
        if obj.start_date:
            return obj.start_date.strftime("%d/%m/%Y %H:%M")
        else:
            return None
    
    def get_end_date_desc(self, obj):
        if obj.end_date:
            return obj.end_date.strftime("%d/%m/%Y %H:%M")
        else:
            return None

    def get_branches_desc(self, obj):
        branches_name = ""
        if obj.branches.exists():
            for record in obj.branches.all():
                if obj.branches.all().last() == record:
                    branches_name = branches_name + record.name
                else:
                    branches_name = branches_name + record.name + ", "
            return branches_name
        else:
            return None


  