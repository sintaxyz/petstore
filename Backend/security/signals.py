
# Django's Libraries
from django.contrib.auth import user_logged_in
from django.contrib.auth import user_logged_out
from django.dispatch import receiver

# Own's Libraries
from .models import LoggedDetail


@receiver(user_logged_in)
def user_LoggedIn(sender, request, **kwargs):
    detail = LoggedDetail.objects.get_or_create(
        user=kwargs.get('user')
    )
    detail[0].access_qty += 1
    detail[0].save()


@receiver(user_logged_out)
def user_LoggedOut(sender, **kwargs):
    detail = LoggedDetail.objects.get(
        user=kwargs.get('user')
    )
    detail.session_key = None
    detail.save()
