# Django's Libraries
from django.urls import path

# Own's Librariesi
from .views_rest import UserAPIListView
from .views_rest import UserLoggedAPIListView
from .views_rest import CompanyAPIListView
from .views_rest import CompanyAPIRetrieveView
from .views_rest import CompanyHasPositionsAPIListView
from .views_rest import CompanyHasBranchesAPIListView
from .views_rest import BranchOfficeListAPIView
from .views_rest import CompanyPositionAPIListView
from .views_rest import AssignmentListAPIView
from .views_rest import AssignmentAPIListByUserView
from .views_rest import EmployeeAPIListView
from .views_rest import EmployeeIsActiveAPIListView
from .views_rest import EmployeeAPIRetrieveView
from .views_rest import CompanyAPIListView
from .views_rest import CompanyActiveAPIListView
from .views_rest import BranchOfficeChartListAPIView
from .views_rest import BranchOfficeIsActiveListAPIView

app_name = 'security'

urlpatterns = [
    path(
        'users/',
        UserAPIListView.as_view({
            'get': 'list',
        }),
        name="api-user-list"
    ),
    path(
        'companies/',
        CompanyAPIListView.as_view({
            'get': 'list',
        }),
        name="api-company-list"
    ),
    path(
        'users/logged/',
        UserLoggedAPIListView.as_view({
            'get': 'list',
        }),
        name="api-user-logged-list"
    ),
    path(
        'branchoffices/',
        BranchOfficeListAPIView.as_view({
            'get': 'list',
        }),
        name="api-branchoffice-list"
    ),
    path(
        'branchoffices/active/',
        BranchOfficeIsActiveListAPIView.as_view({
            'get': 'list',
        }),
        name="api-branchofficeactive-list"
    ),
    path(
        'companies/',
        CompanyAPIListView.as_view({
            'get': 'list',
        }),
        name="api-company-list"
    ),
    path(
        'companies/active',
        CompanyActiveAPIListView.as_view({
            'get': 'list',
        }),
        name="api-company-active"
    ),
    path(
        'companies/<int:pk>/',
        CompanyAPIRetrieveView.as_view({
            'get': 'retrieve',
        }),
        name="api-positions-retrieve"
    ),
    path(
        'companies/<int:pk>/companypositions/',
        CompanyHasPositionsAPIListView.as_view({
            'get': 'list',
        }),
        name="api-positions-incompany"
    ),
    path(
        'companies/<int:pk>/branchoffices/',
        CompanyHasBranchesAPIListView.as_view({
            'get': 'list',
        }),
        name="api-branches-incompany"
    ),
    path(
        'companypositions/',
        CompanyPositionAPIListView.as_view({
            'get': 'list',
        }),
        name="api-position-list"
    ),
    path(
        'employees/',
        EmployeeAPIListView.as_view({
            'get': 'list',
        }),
        name="api-employee-list"
    ),
    path(
        'employees/active/',
        EmployeeIsActiveAPIListView.as_view({
            'get': 'list',
        }),
        name="api-employee-active-list"
    ),
    path(
        'employees/<int:pk>/',
        EmployeeAPIRetrieveView.as_view({
            'get': 'retrieve',
        }),
        name="api-employee-retrieve"
    ),
    path(
        'assignments/',
        AssignmentListAPIView.as_view({
            'get': 'list',
            'post': 'create',
        }),
        name='api-assignment-list'
    ),
    path(
        'assignments/employee/<int:pk>/',
        AssignmentAPIListByUserView.as_view({
            'get': 'list',
        }),
        name="api-assignmentbyuser-list"
    ),
    path(     
        'branchoffices/charts/',
        BranchOfficeChartListAPIView.as_view({
            'get': 'list',
        }),
        name='api-branchoffice-charts'
    ),
]
