# Django's Libraries
from django.db import transaction
# Third-party Libraries
# from rest_framework.views import APIView
# from rest_framework.response import Response
from rest_framework import viewsets
# from rest_framework import mixins
from rest_framework import status
# from rest_framework.filters import OrderingFilter
from rest_framework.response import Response
# from django_filters.rest_framework import DjangoFilterBackend
from rest_framework.permissions import IsAuthenticated
# from rest_framework.permissions import AllowAny

# Own's Libraries
# from .models import User
from .serializers import UserSerializer
from .serializers import EmployeeSerializer
from .serializers import CompanySerializer
from .serializers import BranchOfficeSerializer
from .serializers import UserLoggedSerializer
from .serializers import CompanyPositionSerializer
from .serializers import AssignmentSerializer

from .business import UserBsn
from .business import BranchOfficeBsn
from .business import CompanyBsn
from .business import CompanyPositionBsn
from .business import AssignmentBsn
from .business import EmployeeBsn

from tools.views_rest import StxListAPIView


class UserAPIListView(viewsets.GenericViewSet):
    permission_classes = (IsAuthenticated,)
    serializer_class = UserSerializer

    def get_queryset(self):
        return UserBsn.get_All()

    def list(self, request, *args, **kwargs):
        queryset = self.filter_queryset(self.get_queryset())
        serializer = self.get_serializer(queryset, many=True)
        return Response(serializer.data)


class CompanyAPIListView(viewsets.GenericViewSet):
    permission_classes = (IsAuthenticated,)
    serializer_class = CompanySerializer

    def get_queryset(self, user):
        return CompanyBsn.get_Assigned(user)

    def list(self, request, *args, **kwargs):
        queryset = self.filter_queryset(self.get_queryset(request.user))
        serializer = self.get_serializer(queryset, many=True)
        return Response(serializer.data)


class CompanyActiveAPIListView(viewsets.GenericViewSet):
    permission_classes = (IsAuthenticated,)
    serializer_class = CompanySerializer

    def get_queryset(self, user):
        return CompanyBsn.get_Active(user)

    def list(self, request, *args, **kwargs):
        queryset = self.filter_queryset(self.get_queryset(request.user))
        serializer = self.get_serializer(queryset, many=True)
        return Response(serializer.data)


class CompanyAPIRetrieveView(viewsets.GenericViewSet):
    serializer_class = CompanySerializer

    def get_Object(self, pk):
        instance = CompanyBsn.get(pk)
        return instance

    def retrieve(self, request, pk, *args, **kwargs):
        instance = self.get_Object(pk)
        serializer = self.get_serializer(instance)
        return Response(serializer.data)


class CompanyHasPositionsAPIListView(viewsets.GenericViewSet):
    queryset = ''
    serializer_class = CompanyPositionSerializer

    def get_QuerySet(self, company_pk):
        records = CompanyBsn.get_PositionsInCompany(company_pk)
        return records

    def list(self, request, pk, *args, **kwargs):
        queryset = self.filter_queryset(
            self.get_QuerySet(pk)
        )
        serializer = self.get_serializer(queryset, many=True)
        return Response(serializer.data)


class CompanyHasBranchesAPIListView(viewsets.GenericViewSet):
    queryset = ''
    serializer_class = BranchOfficeSerializer

    def get_QuerySet(self, company_pk):
        records = CompanyBsn.get_BranchesInCompany(company_pk)
        return records

    def list(self, request, pk, *args, **kwargs):
        queryset = self.filter_queryset(
            self.get_QuerySet(pk)
        )
        serializer = self.get_serializer(queryset, many=True)
        return Response(serializer.data)


class BranchOfficeListAPIView(viewsets.GenericViewSet):
    permission_classes = (IsAuthenticated,)
    serializer_class = BranchOfficeSerializer

    def get_queryset(self, user):
        return BranchOfficeBsn.get_Assigned(user)

    def list(self, request, *args, **kwargs):
        queryset = self.filter_queryset(self.get_queryset(request.user))
        serializer = self.get_serializer(queryset, many=True)
        return Response(serializer.data)


class BranchOfficeIsActiveListAPIView(viewsets.GenericViewSet):
    permission_classes = (IsAuthenticated,)
    serializer_class = BranchOfficeSerializer

    def get_queryset(self, user):
        return BranchOfficeBsn.get_IsActive(user)

    def list(self, request, *args, **kwargs):
        queryset = self.filter_queryset(self.get_queryset(request.user))
        serializer = self.get_serializer(queryset, many=True)
        return Response(serializer.data)


class BranchOfficeChartListAPIView(viewsets.GenericViewSet):
    serializer_class = BranchOfficeSerializer

    def get_QuerySet(self, user, request):
        records = BranchOfficeBsn.get_ForCharts(user, request)
        return records

    def list(self, request, *args, **kwargs):
        queryset = self.filter_queryset(
            self.get_QuerySet(request.user, request)
        )
        serializer = self.get_serializer(queryset, many=True)
        return Response(serializer.data)


class UserLoggedAPIListView(StxListAPIView):
    serializer_class = UserLoggedSerializer
    business_class = UserBsn
    queryset_class = "get_LoggedList"


class EmployeeAPIListView(viewsets.GenericViewSet):
    permission_classes = (IsAuthenticated,)
    serializer_class = EmployeeSerializer

    def get_queryset(self, user):
        return EmployeeBsn.get_Assgined(user)

    def list(self, request, *args, **kwargs):
        queryset = self.filter_queryset(
            self.get_queryset(user=request.user)
        )
        serializer = self.get_serializer(queryset, many=True)
        return Response(serializer.data)


class EmployeeIsActiveAPIListView(viewsets.GenericViewSet):
    permission_classes = (IsAuthenticated,)
    serializer_class = EmployeeSerializer

    def get_queryset(self, user):
        return EmployeeBsn.get_IsActive(user)

    def list(self, request, *args, **kwargs):
        queryset = self.filter_queryset(
            self.get_queryset(user=request.user)
        )
        serializer = self.get_serializer(queryset, many=True)
        return Response(serializer.data)


class EmployeeAPIRetrieveView(viewsets.GenericViewSet):
    serializer_class = EmployeeSerializer

    def get_Object(self, pk):
        instance = EmployeeBsn.get(pk)
        return instance

    def retrieve(self, request, pk, *args, **kwargs):
        instance = self.get_Object(pk)
        serializer = self.get_serializer(instance)
        return Response(serializer.data)


# class CompanyAPIListView(viewsets.GenericViewSet):
#     permission_classes = (IsAuthenticated,)
#     serializer_class = CompanySerializer

#     def get_queryset(self):
#         return CompanyBsn.get_All()

#     def list(self, request, *args, **kwargs):
#         queryset = self.filter_queryset(self.get_queryset())
#         serializer = self.get_serializer(queryset, many=True)
#         return Response(serializer.data)


class CompanyPositionAPIListView(viewsets.GenericViewSet):
    permission_classes = (IsAuthenticated,)
    serializer_class = CompanyPositionSerializer

    def get_queryset(self):
        return CompanyPositionBsn.get_All()

    def list(self, request, *args, **kwargs):
        queryset = self.filter_queryset(self.get_queryset())
        serializer = self.get_serializer(queryset, many=True)
        return Response(serializer.data)


class AssignmentListAPIView(viewsets.GenericViewSet):
    serializer_class = AssignmentSerializer

    def get_QuerySet(self, user):
        records = AssignmentBsn.get_AssignmentList(user)
        return records

    def list(self, request, *args, **kwargs):
        queryset = self.filter_queryset(
            self.get_QuerySet(request.user)
        )
        serializer = self.get_serializer(queryset, many=True)
        return Response(serializer.data)

    def create(self, request, *args, **kwargs):
        serializer = AssignmentSerializer(data=request.data)
        if serializer.is_valid(raise_exception=False):
            try:
                data_list = serializer.validated_data
                with transaction.atomic():
                    record = AssignmentBsn.create(
                        data_list, request.user)
                serializer = AssignmentSerializer(record)
                return Response(
                    serializer.data,
                    status=status.HTTP_201_CREATED
                )
            except Exception as e:
                return Response(
                    {'detail': str(e)},
                    status=status.HTTP_400_BAD_REQUEST
                )
                print(e)
        else:
            return Response(
                serializer.errors,
                status=status.HTTP_400_BAD_REQUEST
            )


class AssignmentAPIListByUserView(viewsets.GenericViewSet):
    permission_classes = (IsAuthenticated,)
    serializer_class = AssignmentSerializer

    def get_queryset(self, pk):
        return AssignmentBsn.get_AssignmentsByUser(pk)

    def list(self, request, pk, *args, **kwargs):
        queryset = self.filter_queryset(
            self.get_queryset(pk)
        )
        serializer = self.get_serializer(queryset, many=True)
        return Response(serializer.data)
