# Django's Libraries
import django_filters

# Own's Libraries
from .models import Permission
from .models import User
from .models import Employee
from .models import Assignment
from .models import Company
from .models import CompanyClass
from .models import CompanyPosition
from .models import BranchOffice


def assigned_branchoffices(request):
    if request is None:
        return BranchOffice.objects.none()
    else:
        return BranchOffice.objects.filter(company__in=request.user.companies.all())


class PermissionFilter(django_filters.FilterSet):

    BOOLEAN_CHOICES = (
        ('Si', 'Si'),
        ('No', 'No')
    )

    is_active = django_filters.ChoiceFilter(
        choices=BOOLEAN_CHOICES,
        method='is_ActiveFilter'
    )

    class Meta:
        model = Permission
        fields = [
            'name',
            'is_active',
        ]

    def is_ActiveFilter(self, queryset, name, value):
        if value == 'Si':
            queryset = queryset.filter(is_active=True)
        elif value == 'No':
            queryset = queryset.filter(is_active=False)
        return queryset


class UserFilter(django_filters.FilterSet):

    class Meta:
        model = User
        fields = [
            'name',
            'email',
            'is_active',
            'is_superuser',
        ]


class EmployeeFilter(django_filters.FilterSet):
    BOOLEAN_CHOICES = (
        ('Si', 'Si'),
        ('No', 'No')
    )
    is_active = django_filters.ChoiceFilter(
        choices=BOOLEAN_CHOICES,
        method='is_ActiveFilter'
    )

    class Meta:
        model = Employee
        fields = [
            'is_active',
        ]

    def is_ActiveFilter(self, queryset, name, value):
        if value == 'Si':
            queryset = queryset.filter(is_active=True)
        elif value == 'No':
            queryset = queryset.filter(is_active=False)
        return queryset


class AssignmentFilter(django_filters.FilterSet):

    class Meta:
        model = Assignment
        fields = [
            'company',
            'branches',
            'is_active',
            'position',
        ]


class CompanyClassFilter(django_filters.FilterSet):
    class Meta:
        model = CompanyClass
        fields = [
            'name',
            'abbreviation',
        ]


class CompanyFilter(django_filters.FilterSet):

    BOOLEAN_CHOICES = (
        ('Si', 'Si'),
        ('No', 'No')
    )

    is_active = django_filters.ChoiceFilter(
        choices=BOOLEAN_CHOICES,
        method='is_ActiveFilter'
    )

    class Meta:
        model = Company
        fields = [
            'legal_entity',
            'commercial_name',
            'classification',
            'type',
            'email',
            'is_active',
        ]

    def is_ActiveFilter(self, queryset, name, value):
        if value == 'Si':
            queryset = queryset.filter(is_active=True)
        elif value == 'No':
            queryset = queryset.filter(is_active=False)
        return queryset


class CompanyPositionFilter(django_filters.FilterSet):

    BOOLEAN_CHOICES = (
        ('Si', 'Si'),
        ('No', 'No')
    )

    is_active = django_filters.ChoiceFilter(
        choices=BOOLEAN_CHOICES,
        method='is_ActiveFilter'
    )

    class Meta:
        model = CompanyPosition
        fields = [
            'name',
            'company',
            'is_active',
        ]

    def is_ActiveFilter(self, queryset, name, value):
        if value == 'Si':
            queryset = queryset.filter(is_active=True)
        elif value == 'No':
            queryset = queryset.filter(is_active=False)
        return queryset


class BranchOfficeFilter(django_filters.FilterSet):

    BOOLEAN_CHOICES = (
        ('Si', 'Si'),
        ('No', 'No')
    )

    is_active = django_filters.ChoiceFilter(
        choices=BOOLEAN_CHOICES,
        method='is_ActiveFilter'
    )

    class Meta:
        model = BranchOffice
        fields = [
            'company',
            'name',
            'is_active',
            'phone',
            'street',
            'zip_code',
        ]

    def is_ActiveFilter(self, queryset, name, value):
        if value == 'Si':
            queryset = queryset.filter(is_active=True)
        elif value == 'No':
            queryset = queryset.filter(is_active=False)
        return queryset
