# Python's Libraries
import io
# from datetime import datetime

# Django's Libraries
from django.db import transaction
from django.db.models import Q
# from django.contrib import messages
# from django.contrib.auth.models import Group
# from django.core.exceptions import ObjectDoesNotExist
from django.urls import reverse_lazy
from django.conf import settings
from django.utils.encoding import force_bytes
from django.utils.http import urlsafe_base64_encode
from django.contrib.auth.tokens import default_token_generator

import os
from django.core.files.storage import default_storage
from django.core.files.base import ContentFile

# Third-party Libraries
from xlsxwriter.workbook import Workbook
import facturama

# Own's Libraries
# from tools.utils import Helper
from tools.business import StxBsn
from .models import Permission
from .models import User
from .models import Employee
from .models import Company
from .models import CompanyClass
from .models import CompanyPosition
from .models import CompanyBilling
from .models import BranchOffice
from .models import Assignment

# from .filters import UserFilter
from .filters import PermissionFilter
from .filters import EmployeeFilter
from .filters import AssignmentFilter
from .filters import CompanyFilter
from .filters import CompanyClassFilter
from .filters import CompanyPositionFilter
from .filters import BranchOfficeFilter

from support.business import EmailBsn
from .resources import EmployeeResource
from .resources import CompanyResource
from .resources import BranchOfficeResource
from .resources import CompanyPositionResource
from tools.facturama import InvoiceMultiEmisor


class PermissionBsn(StxBsn):
    model = Permission
    model_filter = PermissionFilter
    key_word = "code"

    @classmethod
    def create(self, _data, _user):
        with transaction.atomic():
            record = self.get_ModelObject()()
            record.name = _data.get('name')
            record.code = _data.get('code')
            record.is_active = _data.get('is_active')
            record.created_by = _user
            record.full_clean()
            record.save()
            return record

    @classmethod
    def update(self, _record, _data, _user):
        with transaction.atomic():
            if _data.get('name'):
                _record.name = _data.get('name')

            if _data.get('code'):
                _record.code = _data.get('code')

            if _data.get('is_active') is not _record.is_active:
                _record.is_active = _data.get('is_active')

            _record.created_by = _user
            _record.full_clean()
            _record.save()
            return _record

    @classmethod
    def update_All(self, _record, _data, _user):
        with transaction.atomic():
            _record.name = _data.get('name')
            _record.is_active = _data.get('is_active')
            _record.created_by = _user
            _record.full_clean()
            _record.save()
            return _record

    @classmethod
    def get_UrlBack(self):
        url = reverse_lazy('home:settings')
        return url

    @classmethod
    def search(self, records, _value):
        try:
            _value = int(_value)
        except Exception:
            _value

        if type(_value) is str:
            records = records.filter(name__icontains=_value)
        elif type(_value) is int:
            records = records.filter(id=_value)
        return records


class UserBsn(StxBsn):
    key_word = "email"
    model = User

    @classmethod
    def get_ActiveByMail(self, _email):
        try:
            instance = User.objects.get(
                email__iexact=_email,
                is_active=True,
            )
            return instance

        except User.DoesNotExist:
            raise NameError(
                f"No existe cuenta con correo {_email}"
            )

    @classmethod
    def get_ByEmail(self, _value):
        try:
            record = User.objects.get(
                email__iexact=_value,
            )
            return record
        except User.DoesNotExist:
            return None
        return record

    @classmethod
    def create(self, _data, user=None):
        record = User.objects.create(
            name=_data.get('name'),
            email=_data.get('email'),
            is_active=_data.get('is_active', False),
            is_superuser=_data.get('is_superuser', False),
        )
        record.set_password("sintaxyz")
        record.full_clean()
        record.save()

        return record

    @classmethod
    def update(self, _record, _data, user=None):
        _record.name = _data.get('name')
        _record.email = _data.get('email')

        if 'is_activate' in _data:
            _record.is_active = _data.get('is_active')

        if 'is_superuser' in _data:
            _record.is_superuser = _data.get('is_superuser')

        # TODO updated_by?
        _record.save()

    # @classmethod

    @classmethod
    def get_CompanyByUser(self, user):
        record = self.get_ModelObject().objects.get(id=user.id).has_assignments.all()
        record.company

        # if record:
        #     for has_assignments.company in record:
        #         company = has_assignments.company

        return record


class EmployeeBsn(StxBsn):
    model = Employee
    model_resource = EmployeeResource
    model_filter = EmployeeFilter

    @classmethod
    def filter_ByPermission(self, records, user):
        return records.filter(user__companies__in=user.companies.all())

    @classmethod
    def get_Assgined(self, user):
        return self.get_All().filter(user__companies__in=user.companies.all())

    @classmethod
    def get_IsActive(self, user):
        employee = self.get_Assgined(user)
        return employee.filter(is_active=True).distinct()

    @classmethod
    def export_XLSX(self, records):
        return EmployeeResource().export(records).xlsx

    #@classmethod
    #def export_XLSX(self, **kwargs):
    #    records = kwargs.get('records')
    #    output = io.BytesIO()

    #    workbook = Workbook(output, {'in_memory': True})
    #    worksheet = workbook.add_worksheet('Empleados')

    #    header_font = workbook.add_format({'bold': True, 'font_size': 11})
    #    content_font = workbook.add_format({'bold': False, 'font_size': 10})


    #    width_a = worksheet.set_column(0, 0, 20)
    #    width_b = worksheet.set_column(1, 1, 20)
    #    width_c = worksheet.set_column(2, 2, 20)
    #    width_d = worksheet.set_column(3, 3, 6)
    #    width_e = worksheet.set_column(4, 4, 15)
    #    width_f = worksheet.set_column(5, 5, 15)
    #    width_g = worksheet.set_column(6, 6, 18)
    #    width_h = worksheet.set_column(7, 7, 6)

    #    row = 1

    #    worksheet.write(f'A{row}', 'Sucursal', header_font)
    #    worksheet.write(f'B{row}', 'Nombre', header_font)
    #    worksheet.write(f'C{row}', 'Puesto', header_font)
    #    worksheet.write(f'D{row}', 'Sexo', header_font)
    #    worksheet.write(f'E{row}', 'Teléfono de casa', header_font)
    #    worksheet.write(f'F{row}', 'Teléfono celular', header_font)
    #    worksheet.write(f'G{row}', 'Fecha de nacimiento', header_font)
    #    worksheet.write(f'H{row}', 'Activo', header_font)

    #    for record in records:
    #        row += 1
    #        # worksheet.write(f'A{row}', record.branchoffice.name, content_font)
    #        worksheet.write(f'B{row}', record.user.name, content_font)
    #        # worksheet.write(f'C{row}', record.position.name, content_font)
    #        worksheet.write(f'D{row}', record.get_gender_display() if record.get_gender_display() else 'No', content_font)
    #        worksheet.write(f'E{row}', str(record.home_phone) if record.home_phone else 'No', content_font)
    #        worksheet.write(f'F{row}', str(record.cel_phone) if record.cel_phone else 'No', content_font)
    #        worksheet.write(f'G{row}', str(record.birthdate.strftime('%d/''%m/''%Y')) if record.birthdate else 'No', content_font)
    #        worksheet.write(f'H{row}', 'Si' if record.is_active else 'No', content_font)

    #    workbook.close()
    #    output.seek(0)
    #    file = output.read()
    #    output.close()
    #    return file

    @classmethod
    def create_User(self, _data):
        user = UserBsn.get_ByEmail(_data.get('email'))
        if user:
            raise ValueError('Usuario no disponible.')
        else:
            user = UserBsn.create(_data)

        return user

    @classmethod
    def create(self, _data, _user, _current_site, _files=None):
        with transaction.atomic():
            if _data.get('companies'):
                # if _user.is_superuser is False:
                    approved = False
                    for employee_company in _data.get('companies'):
                        company = employee_company.id
                        access = self.validate_Access(_user, 'EDEP', company)
                        if access is True:
                            approved = True
                # else:
                #     approved = True
            else:
                raise ValueError('Es necesario asignar una compañia.')
            if approved is False:
                raise ValueError('No tiene permiso para crear empleado en esta compañia')
            user = self.create_User(_data)
            record = self.get_ModelObject()()
            if _files:
                record.photography = _files.get('photography', None)
            record.home_phone = _data.get('home_phone', None)
            record.cel_phone = _data.get('cel_phone', None)
            record.birthdate = _data.get('birthdate', None)
            record.gender = _data.get('gender', None)
            record.settlement = _data.get('settlement', None)
            record.municipality = _data.get('municipality', None)
            record.street = _data.get('street', None)
            record.outdoor_number = _data.get('outdoor_number', None)
            record.interior_number = _data.get('interior_number', None)
            record.between_street_a = _data.get('between_street_a', None)
            record.between_street_b = _data.get('between_street_b', None)
            record.zip_code = _data.get('zip_code', None)
            record.latitude = _data.get('latitude', None)
            record.longitude = _data.get('longitude', None)
            record.user = user
            record.created_by = _user
            record.full_clean()
            record.save()
            if _data.get('companies'):
                user.companies.set(_data.get('companies'))
            else:
                raise ValueError('Se necesita asignar por lo menos una Compañia')
            return record

    @classmethod
    def update(self, _record, _data, user, _files=None):
        with transaction.atomic():
            if _data.get('companies'):
                # if user.is_superuser is False:
                    approved = False
                    for employee_company in _data.get('companies'):
                        company = employee_company.id
                        access = self.validate_Access(user, 'EDEP', company)
                        if access is True:
                            approved = True
                # else:
                #     approved = True
            else:
                raise ValueError('Es necesario asignar una compañia.')
            if approved is False:
                raise ValueError('No tiene permiso para crear empleado en esta compañia')
            user_data = {
                'name': _data.get('name'),
                'email': _data.get('email'),
            }
            UserBsn.update(_record.user, user_data, user)
            if _files:
                _record.photography = _files.get('photography', None)
            _record.home_phone = _data.get('home_phone', None)
            _record.cel_phone = _data.get('cel_phone', None)
            _record.birthdate = _data.get('birthdate', None)
            _record.gender = _data.get('gender', None)
            _record.settlement = _data.get('settlement', None)
            _record.municipality = _data.get('municipality', None)
            _record.street = _data.get('street', None)
            _record.outdoor_number = _data.get('outdoor_number', None)
            _record.interior_number = _data.get('interior_number', None)
            _record.between_street_a = _data.get('between_street_a', None)
            _record.between_street_b = _data.get('between_street_b', None)
            _record.zip_code = _data.get('zip_code', None)
            _record.latitude = _data.get('latitude', None)
            _record.longitude = _data.get('longitude', None)
            _record.full_clean()
            _record.save()
            if _data.get('companies'):
                _record.user.companies.set(_data.get('companies'))
            else:
                raise ValueError('Se necesita asignar por lo menos una Compañia')
            return _record

    @classmethod
    def send_AccessEmail(self, user, current_site):
        subject_template = 'home/activation/email/activation_email_subject.txt',
        body_template = 'home/activation/email/activation_email_body.html',
        email = user.email
        site_name = current_site.name
        domain = current_site.domain
        use_https = False
        uid = urlsafe_base64_encode(force_bytes(user.pk))

        data = {
            'app': settings.APP_NAME,
            'email': email,
            'domain': domain,
            'site_name': site_name,
            'uid': uid,
            'user': user,
            'token': default_token_generator.make_token(user),
            'protocol': 'https' if use_https else 'http',
        }
        to_emails = [email]
        response = EmailBsn.send_Template(
            to_emails,
            data,
            subject_template,
            body_template
        )
        if response is False:
            raise NameError("Ocurrio un problema al enviar el correo, "
                            "favor de informar al administrador")

        return user

    @classmethod
    def search(self, records, _value):
        try:
            _value = int(_value)
        except Exception:
            _value

        if type(_value) is str:
            records = records.filter(name__icontains=_value)
        elif type(_value) is int:
            records = records.filter(id=_value)
        return records


class AssignmentBsn(StxBsn):
    model = Assignment
    model_filter = AssignmentFilter

    @classmethod
    def filter_ByPermission(self, records, user):
        return records.filter(user__companies__in=user.companies.all())

    @classmethod
    def get_Assgined(self, user):
        return self.get_All().filter(user__companies__in=user.companies.all())

    @classmethod
    def get_AssignmentList(self, user):
        records = self.get_ModelObject().objects.all()
        return records

    @classmethod
    def create(self, _data, _user):
        with transaction.atomic():
            if _data.get('company'):
                company = _data.get('company').id
            else:
                raise ValueError('Es necesario asignar una compañia.')
            approved = self.validate_Access(_user, ['CRAS', 'EDAS'], company)
            if approved is False:
                raise ValueError('No tiene permiso para crear asignación')
            record = self.get_ModelObject()()
            record.user = _data.get('user')
            record.company = _data.get('company')
            record.position = _data.get('position')
            record.start_date = _data.get('start_date')
            record.end_date = _data.get('end_date')
            record.created_by = _user
            record.full_clean()
            record.save()
            branches = {1,2}
            # record.branches.set(_data.get('branches'))
            record.branches.set(branches)
            record.save()
            return record

    @classmethod
    def update_Desc(self, _record, _data, _user):
        with transaction.atomic():
            company = _record.company.id
            approved = self.validate_Access(_user, ['CRAS', 'EDAS'], company)
            if approved is False:
                raise ValueError('No tiene permiso para crear asignación')
            _record.position = _data.get('position')
            _record.branches.set(_data.get('branches'))
            _record.star_date = _data.get('star_date')
            _record.end_date = _data.get('end_date')
            # _record.is_active = _data.get('is_active')
            _record.created_by = _user
            _record.full_clean()
            _record.save()
            return _record

    @classmethod
    def get_EmployeeUserByPk(self, pk):
        employee = EmployeeBsn.get(pk)
        record = employee.user
        return record

    @classmethod
    def get_AssignmentsByUser(self, pk):
        records = self.get_All().filter(user=pk)
        return records


class CompanyClassBsn(StxBsn):
    model = CompanyClass
    model_filter = CompanyClassFilter
    key_word = 'name'

    @classmethod
    def filter_ByPermission(self, records, user):
        return self.get_All()

    @classmethod
    def create(self, _data, _user, _files=None):
        with transaction.atomic():
            record = self.get_ModelObject()()
            record.name = _data.get('name')
            record.abbreviation = _data.get('abbreviation')
            record.created_by = _user
            record.full_clean()
            record.save()
            return record

    @classmethod
    def update(self, _record, _data, _user, _files=None):
        with transaction.atomic():
            _record.name = _data.get('name')
            _record.abbreviation = _data.get('abbreviation')
            _record.updated_by = _user
            _record.full_clean()
            _record.save()
            return _record

    @classmethod
    def search(self, records, _value):
        try:
            _value = int(_value)
        except Exception:
            _value

        if type(_value) is str:
            records = records.filter(name__icontains=_value)
        elif type(_value) is int:
            records = records.filter(id=_value)
        return records


class CompanyBsn(StxBsn):
    model = Company
    model_resource = CompanyResource
    model_filter = CompanyFilter
    key_word = 'legal_entity'

    @classmethod
    def get_Assigned(self, user):
        return self.get_ModelObject().objects.filter(assigned_users__in=[user])

    @classmethod
    def get_Active(self, user):
        companies = self.get_Assigned(user)
        records = companies.filter(is_active=True)
        return records

    @classmethod
    def filter_ByPermission(self, records, user):
        filtered_records = records.filter(assigned_users__in=[user])
        return filtered_records

    @classmethod
    def get_CompanyEmail(self, pk):
        try:
            email_settings = self.get(pk).email_settings
            return email_settings
        except Company.email_settings.RelatedObjectDoesNotExist:
            return None

    @classmethod
    def get_BillingSettings(self, pk):
        try:
            billing_settings = self.get(pk).billing_settings
            return billing_settings
        except Company.billing_settings.RelatedObjectDoesNotExist:
            return None

    @classmethod
    def export_XLSX(self, records):
        return CompanyResource().export(records).xlsx

    @classmethod
    def create_BillingSettings(self, _data, _user):
        # if _data.get('company'):
        #     company = _data.get('company')
        #     approved = self.validate_Access(_user, 'MOFA', company)
        #     if approved is False:
        #         raise ValueError('No tiene permiso para crear facturación')
        # else:
        #     raise ValueError('Es necesario asignar una compañia')
        record = CompanyBilling()
        
        try:
            invoice = InvoiceMultiEmisor()
            # invoice.create_Csds(_data)
            record.company = _data.get('company')
            record.rfc = _data.get('rfc')
            record.password = _data.get('password')
            record.fiscal_regimen = _data.get('fiscal_regimen')
            record.is_active = _data.get('is_active')
            record.full_clean()
            record.save()
        except Exception as error:
            raise ValueError(str(error))

    @classmethod
    def update_BillingSettings(self, _record, _data, _user):
        # if _data.get('company'):
        #     company = _data.get('company')
        #     approved = self.validate_Access(_user, 'MOFA', company)
        #     if approved is False:
        #         raise ValueError('No tiene permiso para editar facturación')
        # else:
        #     raise ValueError('Es necesario asignar una compañia')
        try:
            invoice = InvoiceMultiEmisor()
            if _record.rfc:
                invoice.update_Csds(_data)
            else:
                invoice.create_Csds(_data)
            _record.rfc = _data.get('rfc')
            _record.password = _data.get('password')
            _record.fiscal_regimen = _data.get('fiscal_regimen')
            _record.is_active = _data.get('is_active')
            _record.full_clean()
            _record.save()
        except Exception as error:
            raise ValueError(str(error))

    @classmethod
    def delete_BillingSettings(self, _record, _user):
        try:
            rfc = _record.rfc
            invoice = InvoiceMultiEmisor()
            invoice.delete_Csds(rfc)
            _record.rfc = None
            _record.password = None
            _record.password = None
            _record.fiscal_regime = None
            _record.save()
        except Exception as error:
            raise ValueError(str(error))

    @classmethod
    def get_PositionsInCompany(self, company_pk):
        records = self.get(company_pk).company_positions.all()
        return records

    @classmethod
    def get_BranchesInCompany(self, company_pk):
        records = self.get(company_pk).has_branchoffices.all()
        return records

    @classmethod
    def create(self, _data, _user, _files=None):
        with transaction.atomic():
            approved = self.validate_Access(_user, 'CREM')
            if approved is False:
                raise ValueError('No tiene permiso para crear empresas')
            record = self.get_ModelObject()()
            record.legal_entity = _data.get('legal_entity')
            record.commercial_name = _data.get('commercial_name')
            record.description = _data.get('description')
            record.classification = _data.get('classification')
            record.type = _data.get('type')
            record.email = _data.get('email')
            record.phone = _data.get('phone')
            record.website = _data.get('website')
            if _files:
                record.logo = _files.get('logo', None)
            record.comments = _data.get('comments')
            record.is_active = _data.get('is_active')
            record.settlement = _data.get('settlement')
            record.municipality = _data.get('municipality')
            record.street = _data.get('street')
            record.outdoor_number = _data.get('outdoor_number')
            record.interior_number = _data.get('interior_number')
            record.between_street_a = _data.get('between_street_a')
            record.between_street_b = _data.get('between_street_b')
            record.zip_code = _data.get('zip_code')
            record.latitude = _data.get('latitude')
            record.longitude = _data.get('longitude')
            record.created_by = _user
            record.full_clean()
            record.save()
            _user.companies.add(record)
            return record

    @classmethod
    def update(self, _record, _data, _user, _files=None):
        with transaction.atomic():
            company = _record.id
            approved = self.validate_Access(_user, 'EDEM', company)
            if approved is False:
                raise ValueError('No tiene permiso para editar la empresa')
            _record.legal_entity = _data.get('legal_entity')
            _record.commercial_name = _data.get('commercial_name')
            _record.description = _data.get('description')
            _record.classification = _data.get('classification')
            _record.type = _data.get('type')
            _record.email = _data.get('email')
            _record.phone = _data.get('phone')
            _record.website = _data.get('website')
            if _files:
                _record.logo = _files.get('logo', None)
            _record.comments = _data.get('comments')
            _record.is_active = _data.get('is_active')
            _record.ssettlementtreet = _data.get('settlement')
            _record.municipality = _data.get('municipality')
            _record.street = _data.get('street')
            _record.outdoor_number = _data.get('outdoor_number')
            _record.interior_number = _data.get('interior_number')
            _record.between_street_a = _data.get('between_street_a')
            _record.between_street_b = _data.get('between_street_b')
            _record.zip_code = _data.get('zip_code')
            _record.latitude = _data.get('latitude')
            _record.longitude = _data.get('longitude')
            _record.updated_by = _user
            _record.full_clean()
            _record.save()
            return _record

    @classmethod
    def search(self, records, _value):
        try:
            _value = int(_value)
        except Exception:
            _value

        if type(_value) is str:
            records = records.filter(legal_entity__icontains=_value)
        elif type(_value) is int:
            records = records.filter(id=_value)
        return records


class CompanyPositionBsn(StxBsn):
    model = CompanyPosition
    model_resource = CompanyPositionResource
    model_filter = CompanyPositionFilter
    key_word = "name"

    @classmethod
    def get_Assigned(self, user):
        return self.get_All().filter(company__in=user.companies.all())

    @classmethod
    def get_UrlBack(self):
        url = reverse_lazy('home:settings')
        return url

    @classmethod
    def export_XLSX(self, records):
        return CompanyPositionResource().export(records).xlsx

    @classmethod
    def create(self, _data, _user):
        with transaction.atomic():
            record = self.get_ModelObject()()
            record.name = _data.get('name')
            record.position_father = _data.get('position_father')
            if _data.get('position_father') and not _data.get('company'):
                record.company = _data.get('position_father').company
            elif _data.get('company') and not _data.get('position_father'):
                record.company = _data.get('company')
            elif _data.get('position_father') and _data.get('company'):
                if _data.get('position_father').company == _data.get('company'):
                    record.company = _data.get('company')
                else:
                    raise ValueError('El valor de la Compañia debe coincidir.')
            else:
                raise ValueError('Se debe un enlace a la Compañia(Posición padre o Compañia).')
            record.is_active = _data.get('is_active')
            record.created_by = _user
            record.full_clean()
            record.save()

            record.permissions.set(_data.get('permissions'))
            return record

    @classmethod
    def update(self, _record, _data, _user):
        with transaction.atomic():
            if _data.get('name'):
                _record.name = _data.get('name')
            if _data.get('position_father'):
                _record.position_father = _data.get('position_father')
            if _data.get('is_active') is not _record.is_active:
                _record.is_active = _data.get('is_active')
            _record.created_by = _user
            _record.full_clean()
            _record.save()

            _record.permissions.set(_data.get('permissions'))
            return _record

    @classmethod
    def update_All(self, _record, _data, _user):
        with transaction.atomic():
            _record.name = _data.get('name')
            _record.position_father = _data.get('position_father')
            _record.is_active = _data.get('is_active')
            _record.permissions.set(_data.get('permissions'))
            _record.created_by = _user
            _record.full_clean()
            _record.save()
            return _record

    @classmethod
    def search(self, records, _value):
        try:
            _value = int(_value)
        except Exception:
            _value

        if type(_value) is str:
            records = records.filter(name__icontains=_value)
        elif type(_value) is int:
            records = records.filter(id=_value)
        return records


class BranchOfficeBsn(StxBsn):
    model = BranchOffice
    model_resource = BranchOfficeResource
    model_filter = BranchOfficeFilter
    key_word = 'name'

    @classmethod
    def get_Assigned(self, user):
        return self.get_All().filter(company__in=user.companies.all())

    @classmethod
    def get_IsActive(self, user):
        return self.get_All().filter(
            company__in=user.companies.all(),
            is_active=True
        )

    @classmethod
    def get_ForCharts(self, user, request):
        records = self.get_Assigned(user)
        if request.GET.get('company', None):
            records = records.filter(company__id=request.GET.get('company'))
        return records

    @classmethod
    def export_XLSX(self, records):
        return BranchOfficeResource().export(records).xlsx

    @classmethod
    def filter_ByPermission(self, records, user):
        filtered_records = records.filter(company__in=user.companies.all())
        return filtered_records

    @classmethod
    def create(self, _data, _user, _files=None):
        with transaction.atomic():
            if _data.get('company'):
                company = _data.get('company').id
            else:
                raise ValueError('Es necesario asignar una compañia.')
            approved = self.validate_Access(_user, 'CRSU', company)
            if approved is False:
                raise ValueError('No tiene permiso para crear sucursales en esta compañia')
            record = self.get_ModelObject()()
            record.company = _data.get('company')
            record.name = _data.get('name')
            record.description = _data.get('description')
            record.phone = _data.get('phone')
            record.is_active = _data.get('is_active')
            record.settlement = _data.get('settlement')
            record.municipality = _data.get('municipality')
            record.street = _data.get('street')
            record.outdoor_number = _data.get('outdoor_number')
            record.interior_number = _data.get('interior_number')
            record.between_street_a = _data.get('between_street_a')
            record.between_street_b = _data.get('between_street_b')
            record.zip_code = _data.get('zip_code')
            record.latitude = _data.get('latitude')
            record.longitude = _data.get('longitude')
            record.created_by = _user
            record.full_clean()
            record.save()
            return record

    @classmethod
    def update(self, _record, _data, _user, _files=None):
        with transaction.atomic():
            if _data.get('company'):
                company = _data.get('company').id
            else:
                raise ValueError('Es necesario asignar una compañia.')
            approved = self.validate_Access(_user, 'EDSU', company)
            if approved is False:
                raise ValueError('No tiene permiso para editar esta sucursal')
            _record.company = _data.get('company')
            _record.name = _data.get('name')
            _record.description = _data.get('description')
            _record.phone = _data.get('phone')
            _record.is_active = _data.get('is_active')
            _record.settlement = _data.get('street')
            _record.municipality = _data.get('settlement')
            _record.street = _data.get('municipality')
            _record.outdoor_number = _data.get('outdoor_number')
            _record.interior_number = _data.get('interior_number')
            _record.between_street_a = _data.get('between_street_a')
            _record.between_street_b = _data.get('between_street_b')
            _record.zip_code = _data.get('zip_code')
            _record.latitude = _data.get('latitude')
            _record.longitude = _data.get('longitude')
            _record.updated_by = _user
            _record.full_clean()
            _record.save()
            return _record

    @classmethod
    def search(self, records, _value):
        try:
            _value = int(_value)
        except Exception:
            _value

        if type(_value) is str:
            records = records.filter(name__icontains=_value)
        elif type(_value) is int:
            records = records.filter(id=_value)
        return records
