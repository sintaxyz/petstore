
# Django's Libraries
from django.db import transaction
from django.http import Http404

from django.contrib import messages
from django.shortcuts import redirect
from django.shortcuts import render
from django.urls import reverse_lazy
from django.urls import reverse

# Own's Libraries
from tools.views import AppNewView

from tools.utils import List
from tools.views import StxView
from tools.views import StxListView
from tools.views import StxRetrieveView
from tools.views import StxNewView
from tools.views import StxEditView
from tools.views import StxExportToExcel
from tools.views import StxReactView
from tools.views import StxReactRelatedNewView

from .business import PermissionBsn
from .business import UserBsn
from .business import EmployeeBsn
from .business import AssignmentBsn
from .business import CompanyBsn
from .business import CompanyClassBsn
from .business import CompanyPositionBsn
from .business import BranchOfficeBsn

from support.business import EmailBsn

from .filters import UserFilter

from .forms import PermissionForm
from .forms import UserForm
from .forms import UserGroupForm
from .forms import UserPasswordForm
from .forms import UserPasswordResetForm
from .forms import EmployeeCreateForm
from .forms import EmployeeUpdateForm
from .forms import AssignmentForm
from .forms import CompanyForm
from .forms import CompanyClassForm
from .forms import CompanyPositionForm
from .forms import BranchOfficeForm
from .forms import CompanyBillingForm
from support.forms import EmailSettingForm
from tools.views import ResourceModelImportView
from tools.views import StxEmptyImportView


class PermissionListView(StxListView):
    template_name = 'security/permission/list/permission_list.html'
    business_class = PermissionBsn


class PermissionNewView(StxNewView):
    template_name = "security/permission/new/permission_new.html"
    business_class = PermissionBsn
    form_class = PermissionForm


class PermissionEditView(StxEditView):
    template_name = "security/permission/edit/permission_edit.html"
    business_class = PermissionBsn
    form_class = PermissionForm
    update_record_method = 'update'


class UserListView(StxView):
    template_name = 'security/user/list/user_list.html'
    page_title = "Listado de Usuarios"
    back_url = reverse_lazy('security:user-list')
    back_name = 'Usuario'

    def get(self, request):
        if 'from_save' in request.GET:
            messages.success(
                request,
                "El usuario '{}' se guardo exitosamente".format(
                    request.GET['from_save'].upper()
                )
            )

        records = UserBsn.get_All()
        search_value = request.GET.get('search_input', "")
        filters = UserFilter(request.GET, queryset=records)

        if search_value:
            filtered_records = UserBsn.search(search_value)
        else:
            filtered_records = filters.qs

        page = request.GET.get('page', 1)
        paginated_records = List.get_Pagination(filtered_records, page)

        context = {
            'back_url': self.back_url,
            'search_value': search_value,
            'filters': filters,
            'page_title': self.page_title,
            'records': paginated_records,
            'counter': records.count()
        }

        return render(request, self.template_name, context)


class UserNewView(StxNewView):
    template_name = 'security/user/new/user_new.html'
    business_class = UserBsn
    form_class = UserForm


class UserEditView(StxEditView):
    template_name = "security/user/edit/user_edit.html"
    update_record_method = "update"
    business_class = UserBsn
    form_class = UserForm


class UserPermissionsView(StxView):
    template_name = "security/user/permissions/user_permissions.html"
    page_title = "Permisos del Usuario"

    def get(self, request, pk):
        instance = UserBsn.get(pk)
        form = UserGroupForm(instance=instance)
        context = {
            'form': form,
            'page_title': self.page_title,
        }
        return render(request, self.template_name, context)

    def post(self, request, pk):
        instance = UserBsn.get(pk)
        form = UserGroupForm(data=request.POST, instance=instance)
        if form.is_valid():
            form.save()
            messages.success(
                request,
                "Se actualizaron los permisos del usuario"
            )

        context = {
            'form': form,
            'page_title': self.page_title,
        }
        return render(request, self.template_name, context)


class UserPasswordView(StxView):
    template_name = "security/user/password/user_password.html"
    page_title = "Cambiar contraseña al Usuario"

    def get(self, request, pk):
        instance = UserBsn.get(pk)
        form = UserPasswordForm(
            user=instance
        )
        context = {
            'form': form,
            'instance': instance,
            'page_title': self.page_title,
        }
        return render(request, self.template_name, context)

    def post(self, request, pk):
        instance = UserBsn.get(pk)
        form = UserPasswordForm(
            data=request.POST,
            user=instance
        )

        if form.is_valid():
            data = form.cleaned_data
            UserBsn.change_Password(instance, data)
            messages.success(
                request, "Se actualizo la contraseña con exito")
            return redirect(reverse(
                'security:user-list'
            ))

        context = {
            'form': form,
            'page_title': self.page_title,
        }
        return render(request, self.template_name, context)


class UserPasswordResetView(StxView):
    template_name = "security/user/password_reset/user_password_reset.html"
    page_title = "Cambiar contraseña al Usuario"

    def get(self, request, pk):
        instance = UserBsn.get(pk)
        form = UserPasswordResetForm(
            user=instance
        )
        context = {
            'form': form,
            'instance': instance,
            'page_title': self.page_title,
        }
        return render(request, self.template_name, context)

    def post(self, request, pk):
        instance = UserBsn.get(pk)
        form = UserPasswordResetForm(
            data=request.POST,
            user=instance
        )

        if form.is_valid():
            data = form.cleaned_data
            UserBsn.change_Password(instance, data)
            messages.success(
                request, "Se actualizo la contraseña con exito")

        context = {
            'form': form,
            'page_title': self.page_title,
        }
        return render(request, self.template_name, context)


class UserProfileView(StxRetrieveView):
    template_name = "security/user/profile/user_profile.html"
    business_class = UserBsn
    extra_permission_method = "permission_Retrieve"
    page_title = "Información personal"


class EmployeeListView(StxListView):
    permissions = ['VEEP']
    template_name = 'security/employee/list/employee_list.html'
    business_class = EmployeeBsn


class EmployeeNewView(StxNewView):
    permissions = ['CREP']
    template_name = "security/employee/new/employee_new.html"
    business_class = EmployeeBsn
    form_class = EmployeeCreateForm


class EmployeeEditView(StxEditView):
    permissions = ['EDEP']
    path_many_related_company = ['user', 'companies']
    template_name = "security/employee/edit/employee_edit.html"
    business_class = EmployeeBsn
    form_class = EmployeeUpdateForm


class EmployeeExportExcel(StxExportToExcel):
    permissions = ['EXEP']
    business_class = EmployeeBsn
    dataset_method = 'export_XLSX'
    name_file = 'listado de empleados.xls'


class EmployeeImportExcel(ResourceModelImportView):
    permissions = ['IMEP']
    template_name = 'security/employee/import/employee_import.html'
    business_class = EmployeeBsn
    empty_filename = 'Importar Empleados'


class EmployeeEmptyImportView(StxEmptyImportView):
    permissions = ['IMEP']
    business_class = EmployeeBsn


class EmployeeAssignmentView(StxReactRelatedNewView):
    permissions = ['CRAS']
    template_name = "security/employee/assignment/assignment.html"
    business_class = AssignmentBsn
    get_record_method = "get_EmployeeUserByPk"


class CompanyClassListView(StxListView):
    template_name = 'security/companyclass/list/companyclass_list.html'
    business_class = CompanyClassBsn


class CompanyClassNewView(StxNewView):
    template_name = 'security/companyclass/new/companyclass_new.html'
    business_class = CompanyClassBsn
    form_class = CompanyClassForm


class CompanyClassEditView(StxEditView):
    update_record_method = 'update'
    template_name = 'security/companyclass/edit/companyclass_edit.html'
    business_class = CompanyClassBsn
    form_class = CompanyClassForm
    page_title = 'Editar Clase de Empresa'


class CompanyListView(StxListView):
    permissions = ['VEEM']
    template_name = 'security/company/list/company_list.html'
    business_class = CompanyBsn


class CompanyNewView(StxNewView):
    permissions = ['CREM']
    template_name = 'security/company/new/company_new.html'
    business_class = CompanyBsn
    form_class = CompanyForm


class CompanyEditView(StxEditView):
    permissions = ['EDEM']
    path_related_company = ['id']
    update_record_method = 'update'
    template_name = 'security/company/edit/company_edit.html'
    business_class = CompanyBsn
    form_class = CompanyForm
    page_title = 'Editar Empresa'


class CompanyExportExcel(StxExportToExcel):
    permissions = ['EXEM']
    business_class = CompanyBsn
    dataset_method = 'export_XLSX'
    name_file = 'listado de Empresas.xls'


class CompanyImportExcel(ResourceModelImportView):
    permissions = ['IMEM']
    template_name = 'security/company/import/company_import.html'
    business_class = CompanyBsn
    empty_filename = 'Importar Empresas'


class CompanyEmptyImportView(StxEmptyImportView):
    permissions = ['IMEM']
    business_class = CompanyBsn


class CompanyEmailView(StxView):
    permissions = ['CECC']
    path_related_company = ['company', 'id']
    template_name = "security/company/email/companyemail_setting.html"
    page_title = "Configuración de Correo Electronico"

    def get(self, request, pk):
        instance = CompanyBsn.get_CompanyEmail(pk)

        self.back_url = CompanyBsn.get_UrlBack()
        self.back_text = "Empresas"

        if instance is None:
            messages.error(
                request,
                "Aún no se cuenta con configuración inicial."
            )
        if instance:
            if self.path_related_company:
                self.company = self.get_Company(instance)
                CompanyBsn.validate_Access(request.user, self.company)
        form = EmailSettingForm(
            instance=instance,
            user=request.user
        )
        context = self.set_Context(
            pk=pk,
            instance=instance,
            form=form,
            url_list=self.back_url
        )
        return render(request, self.template_name, context)

    def post(self, request, pk):
        instance = CompanyBsn.get_CompanyEmail(pk)

        if instance is None:
            form = EmailSettingForm(
                request.POST,
                user=request.user,
            )
        else:
            form = EmailSettingForm(
                request.POST,
                instance=instance,
                user=request.user,
            )

        self.back_url = CompanyBsn.get_UrlBack()
        self.back_text = "Empresas"

        if form.is_valid():
            try:
                data = form.cleaned_data
                if instance is None:
                    data['company'] = CompanyBsn.get(pk)
                    EmailBsn.create_Settings(
                        data,
                        request.user
                    )
                else:
                    EmailBsn.update_Settings(
                        instance,
                        data,
                        request.user
                    )
                messages.success(
                    request,
                    "La configuración se guardo correctamente."
                )

            except Exception as error:
                messages.error(
                    request,
                    str(error)
                )

        context = self.set_Context(
            pk=pk,
            instance=instance,
            form=form,
            url_list=self.back_url
        )
        return render(request, self.template_name, context)


class CompanyBillingView(StxView):
    permissions = ['MOFA']
    path_related_company = ['company', 'id']
    template_name = "security/company/billing/company_billing.html"
    page_title = "Configuración de Facturación"

    def get(self, request, pk):
        instance = CompanyBsn.get_BillingSettings(pk)

        self.back_url = CompanyBsn.get_UrlBack()
        self.back_text = "Empresas"

        if instance is None:
            messages.error(
                request,
                "Aún no se cuenta con configuración inicial."
            )
        if instance:
            if self.path_related_company:
                self.company = self.get_Company(instance)
                CompanyBsn.validate_Access(request.user, self.company)
        form = CompanyBillingForm(
            instance=instance,
            user=request.user
        )
        context = self.set_Context(
            pk=pk,
            instance=instance,
            form=form,
            url_list=self.back_url
        )
        return render(request, self.template_name, context)

    def post(self, request, pk):
        instance = CompanyBsn.get_BillingSettings(pk)

        if instance is None:
            form = CompanyBillingForm(
                request.POST,
                request.FILES,
                user=request.user,
            )
        else:
            if instance.rfc:
                form = CompanyBillingForm(
                    request.POST,
                    request.FILES,
                    instance=instance,
                    user=request.user,
                )
            else:
                form = CompanyBillingForm(
                    request.POST,
                    request.FILES,
                    user=request.user,
                )

        self.back_url = CompanyBsn.get_UrlBack()
        self.back_text = "Empresas"

        if form.is_valid():
            try:
                data = form.cleaned_data
                if instance is None:
                    data['company'] = CompanyBsn.get(pk)
                    CompanyBsn.create_BillingSettings(
                        data,
                        request.user
                    )
                else:
                    CompanyBsn.update_BillingSettings(
                        instance,
                        data,
                        request.user
                    )
                messages.success(
                    request,
                    "La configuración se guardo correctamente."
                )

            except Exception as error:
                messages.error(
                    request,
                    str(error)
                )

        context = self.set_Context(
            pk=pk,
            instance=instance,
            form=form,
            url_list=self.back_url
        )
        return render(request, self.template_name, context)


class CompanyBillingDeleteView(StxView):
    business_class = CompanyBsn

    def get(self, request, pk):
        record = self.business_class.get_BillingSettings(pk)
        self.business_class.delete_BillingSettings(record, request.user)
        messages.success(
            request, "Se eliminó la información de Facturación correctamente")
        return redirect(reverse_lazy(
            'security:company-billing',
            kwargs={'pk':record.company.pk}
        ))


class CompanyPositionListView(StxListView):
    permissions = ['VEPU']
    template_name = 'security/position/list/position_list.html'
    business_class = CompanyPositionBsn


class CompanyPositionNewView(StxNewView):
    permissions = ['CRPU']
    template_name = "security/position/new/position_new.html"
    business_class = CompanyPositionBsn
    form_class = CompanyPositionForm


class CompanyPositionEditView(StxEditView):
    permissions = ['EDPU']
    path_related_company = ['company', 'id']
    update_record_method = 'update'
    template_name = "security/position/edit/position_edit.html"
    business_class = CompanyPositionBsn
    form_class = CompanyPositionForm


class CompanyPositionExportExcel(StxExportToExcel):
    permissions = ['EXPU']
    business_class = CompanyPositionBsn
    dataset_method = 'export_XLSX'
    name_file = 'listado de puestos.xls'


class CompanyPositionImportExcel(ResourceModelImportView):
    permissions = ['IMPU']
    template_name = 'security/companyposition/import/companyposition_import.html'
    business_class = CompanyPositionBsn
    empty_filename = 'Importar puestos'


class CompanyPositionEmptyImportView(StxEmptyImportView):
    permissions = ['IMPU']
    business_class = CompanyPositionBsn


class BranchOfficeListView(StxListView):
    permissions = ['VESU']
    template_name = 'security/branchoffice/list/branchoffice_list.html'
    business_class = BranchOfficeBsn


class BranchOfficeNewView(StxNewView):
    permissions = ['CRSU']
    template_name = 'security/branchoffice/new/branchoffice_new.html'
    business_class = BranchOfficeBsn
    form_class = BranchOfficeForm


class BranchOfficeEditView(StxEditView):
    permissions = ['EDSU']
    path_related_company = ['company', 'id']
    update_record_method = 'update'
    template_name = 'security/branchoffice/edit/branchoffice_edit.html'
    business_class = BranchOfficeBsn
    form_class = BranchOfficeForm
    page_title = 'Editar sucursal'


class BranchOfficeExportExcel(StxExportToExcel):
    permissions = ['EXSU']
    business_class = BranchOfficeBsn
    dataset_method = 'export_XLSX'
    name_file = 'listado de Sucursales.xls'


class BranchOfficeImportExcel(ResourceModelImportView):
    permissions = ['IMSU']
    template_name = 'security/branchoffice/import/branchoffice_import.html'
    business_class = BranchOfficeBsn
    empty_filename = 'Importar Sucursales'


class BranchOfficeEmptyImportView(StxEmptyImportView):
    permissions = ['IMSU']
    business_class = BranchOfficeBsn


