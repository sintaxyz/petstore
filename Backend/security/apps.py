from django.apps import AppConfig


class SecurityConfig(AppConfig):
    name = 'security'
    verbose_name = "Seguridad"

    def ready(self):
        import security.signals
