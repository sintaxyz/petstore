# Python Libraries
import datetime

# Third Party Libraries
from import_export import resources
from import_export import fields
from import_export.widgets import ForeignKeyWidget
from import_export.widgets import ManyToManyWidget
from import_export.widgets import IntegerWidget
from import_export.widgets import DecimalWidget
from import_export.widgets import DateWidget
from import_export.widgets import Widget
from import_export.widgets import CharWidget

# Own Libraries

from core.models import Warehouse
from core.models import StockItem
from core.models import Supplier
from core.models import Tax
from security.models import User
from tools.resources import BooleanWidget
from tools.resources import ChoicesWidget
from tools.resources import GenericResource
from security.models import Company
from security.models import CompanyClass
from security.models import Employee
from security.models import BranchOffice
from security.models import Permission
from security.models import CompanyPosition


class EmployeeResource(GenericResource):
    id = fields.Field(
        column_name='ID',
        attribute='id',
        widget=IntegerWidget()
    )
    user = fields.Field(
        column_name='usuario',
        attribute='user',
        widget=ForeignKeyWidget(User, 'id')
    )
    is_active = fields.Field(
        column_name="activo",
        attribute='is_active',
        widget=BooleanWidget()
    )
    home_phone = fields.Field(
        column_name='telefono de casa',
        attribute='home_phone',
        widget=IntegerWidget()
    )
    cel_phone = fields.Field(
        column_name='telefono celular',
        attribute='cel_phone',
        widget=IntegerWidget()
    )
    birthdate = fields.Field(
        column_name='fecha de nacimiento',
        attribute='birthdate',
        widget=DateWidget()
    )
    gender = fields.Field(
        column_name='genero',
        attribute='gender',
        widget=ChoicesWidget(choices=Employee.GENDER_CHOICES)
    )
    photography = fields.Field(
        column_name="foto",
        attribute='photography',
    )

    class Meta:
        model = Employee
        skip_unchanged = True
        report_skipped = True
        import_id_fields = ['id', ]
        fields = (
            'id',
            'user',
            'is_active',
            'home_phone',
            'cel_phone',
            'birthdate',
            'gender',
            'photography',
        )
        export_order = (
            'id',
            'user',
            'is_active',
            'home_phone',
            'cel_phone',
            'birthdate',
            'gender',
            'photography',
        )


class CompanyResource(GenericResource):
    id = fields.Field(
        column_name='ID',
        attribute='id',
        widget=IntegerWidget()
    )
    legal_entity = fields.Field(
        column_name='nombre legal',
        attribute='legal_entity',
    )
    commercial_name = fields.Field(
        column_name='nombre comercial',
        attribute='commercial_name',
    )
    description = fields.Field(
        column_name='descripción',
        attribute='description',
    )
    classification = fields.Field(
        column_name='clasificación',
        attribute='classification',
        widget=ForeignKeyWidget(CompanyClass, 'id')
    )
    type = fields.Field(
        column_name='Tipo',
        attribute='type',
        widget=ChoicesWidget(choices=Company.TYPE_CHOICES)
    )
    email = fields.Field(
        column_name='email',
        attribute='email',
    )
    phone = fields.Field(
        column_name='Teléfono',
        attribute='phone',
        widget=IntegerWidget()
    )
    website = fields.Field(
        column_name='Sitio web',
        attribute='website',
    )
    logo = fields.Field(
        column_name='Logo de Compañia',
        attribute='logo',
    )
    comments = fields.Field(
        column_name='Comentarios',
        attribute='comments',
    )
    is_active = fields.Field(
        column_name='Activo',
        attribute='is_active',
        widget=BooleanWidget()
    )

    class Meta:
        model = Company
        skip_unchanged = True
        report_skipped = True
        import_id_fields = ['id', ]
        fields = (
            'id',
            'legal_entity',
            'commercial_name',
            'description',
            'classification',
            'type',
            'email',
            'phone',
            'website',
            'logo',
            'comments',
            'is_active',
        )
        export_order = (
            'id',
            'legal_entity',
            'commercial_name',
            'description',
            'classification',
            'type',
            'email',
            'phone',
            'website',
            'logo',
            'comments',
            'is_active',
        )


class BranchOfficeResource(GenericResource):
    id = fields.Field(
        column_name='ID',
        attribute='id',
        widget=IntegerWidget()
    )
    company = fields.Field(
        column_name='compañía',
        attribute='company',
        widget=ForeignKeyWidget(Company, 'id')
    )
    name = fields.Field(
        column_name="nombre",
        attribute='name',
    )
    description = fields.Field(
        column_name='descripción',
        attribute='description',
    )
    is_active = fields.Field(
        column_name="activo",
        attribute='is_active',
        widget=BooleanWidget()
    )
    phone = fields.Field(
        column_name='teléfono',
        attribute='phone',
        widget=IntegerWidget()
    )

    class Meta:
        model = BranchOffice
        skip_unchanged = True
        report_skipped = True
        import_id_fields = ['id', ]
        fields = (
            'id',
            'company',
            'name',
            'description',
            'is_active',
            'phone',
        )
        export_order = (
            'id',
            'company',
            'name',
            'description',
            'is_active',
            'phone',
        )


class CompanyPositionResource(GenericResource):
    id = fields.Field(
        column_name='ID',
        attribute='id',
        widget=IntegerWidget()
    )
    company = fields.Field(
        column_name='compañia',
        attribute='company',
        widget=ForeignKeyWidget(Company, 'id')
    )
    name = fields.Field(
        column_name='nombre',
        attribute='name',
    )
    description = fields.Field(
        column_name='descripción',
        attribute='description',
    )
    is_active = fields.Field(
        column_name="activo",
        attribute='is_active',
        widget=BooleanWidget()
    )
    position_father = fields.Field(
        column_name='puesto superior',
        attribute='position_father',
        widget=ForeignKeyWidget(CompanyPosition, 'id')
    )
    permissions = fields.Field(
        column_name='Permisos',
        attribute='permissions',
        widget=ManyToManyWidget(Permission, ', ', 'id')
    )

    class Meta:
        model = CompanyPosition
        skip_unchanged = True
        report_skipped = True
        import_id_fields = ['id', ]
        fields = (
            'id',
            'company',
            'name',
            'description',
            'is_active',
            'position_father',
            'permissions',
        )
        export_order = (
            'id',
            'company',
            'name',
            'description',
            'is_active',
            'position_father',
            'permissions',
        )
