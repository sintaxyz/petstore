# Django's Libraries
from django.db import models
from django.utils import timezone
from django.utils.translation import ugettext_lazy as _
from django.contrib.auth.models import PermissionsMixin
from django.contrib.auth.base_user import AbstractBaseUser
from django.urls import reverse_lazy

# # Third-party Libraries
# from simple_history.models import HistoricalRecords

# Own's Libraries
from tools.models import AppModel
from tools.models import AddressModel
from tools.utils import FileAdmin

from .managers import UserManager


class Subscription(models.Model):
    name = models.CharField(
        verbose_name="nombre",
        max_length=254,
        help_text='nombre del cliente',
        unique=True
    )
    key = models.CharField(
        verbose_name="key",
        max_length=254,
        help_text='clave de subscriptcion',
        unique=True
    )
    comments = models.TextField(
        verbose_name="comentarios",
        null=True,
        blank=True
    )
    is_active = models.BooleanField(
        verbose_name="activo",
        blank=False,
        null=False,
        default=True,
    )
    created_date = models.DateTimeField(
        verbose_name="fecha registro",
        auto_now=False,
        auto_now_add=True,
        null=True,
        blank=True
    )
    updated_date = models.DateTimeField(
        verbose_name="fecha actualización",
        auto_now=True,
        auto_now_add=False,
        null=True,
        blank=True
    )
    # history = HistoricalRecords()

    class Meta:
        verbose_name = 'suscripcion'
        verbose_name_plural = 'suscripciones'

    def __str__(self):
        value = "{}: {}".format(
            self.key,
            self.name
        )
        return value


class Permission(models.Model):
    code = models.CharField(
        verbose_name='clave',
        max_length=50,
        unique=True,
        blank=False,
        null=True,
    )
    name = models.CharField(
        verbose_name='nombre',
        max_length=100,
        blank=False,
        null=True,
    )
    description = models.TextField(
        verbose_name='descripción',
        blank=False,
        null=True,
    )
    is_active = models.BooleanField(
        verbose_name="active",
        blank=False,
        null=False,
        default=True,
    )

    class Meta:
        verbose_name = 'permiso'
        verbose_name_plural = 'permisos'

    @property
    def url_edit(self):
        url = reverse_lazy(
            'security:permission-edit',
            kwargs={'pk': self.pk}
        )
        return url

    @property
    def status_text(self):
        if self.is_active:
            return "Activo"
        else:
            return "Inactivo"

    @property
    def status_color(self):
        if self.is_active:
            return "green"
        else:
            return "gray"

    def __str__(self):
        return self.name


class User(AbstractBaseUser, PermissionsMixin):
    name = models.CharField(
        _('nombre'),
        max_length=100,
        blank=True,
    )
    email = models.EmailField(
        _('email'),
        unique=True,
        blank=False,
        null=True,
        error_messages={
            'unique': "Ya existe otra cuenta asociada a '\
            'este correo electrónico.",
        },
    )
    is_staff = models.BooleanField(
        _('staff status'),
        default=False,
        help_text=_('Al ser Staff se permite acceso al administrador.'),
    )
    is_active = models.BooleanField(
        _('active'),
        default=True,
    )
    date_joined = models.DateTimeField(
        _('date joined'),
        default=timezone.now,
    )
    date_activated = models.DateTimeField(
        _('date activated'),
        auto_now=False,
        auto_now_add=False,
        null=True,
        blank=True
    )
    is_superuser = models.BooleanField(
        _('superusuario'),
        default=False,
        help_text=_('Un usuario Superusuario tienen privilegios totales.')
    )
    first_login = models.BooleanField(
        "¿Primer ingreso?",
        default=True
    )
    companies = models.ManyToManyField(
        'security.Company',
        verbose_name='Compañias',
        related_name='assigned_users',
        blank=True,
    )
    subscription = models.ForeignKey(
        Subscription,
        verbose_name='subscripcion',
        related_name='users',
        on_delete=models.PROTECT,
        null=True,
        blank=True,
    )

    objects = UserManager()
    EMAIL_FIELD = 'email'
    USERNAME_FIELD = 'email'
    REQUIRED_FIELDS = []

    class Meta:
        verbose_name = _('user')
        verbose_name_plural = _('users')

    @property
    def short_name(self):
        return self.name

    @property
    def url_start(self):
        return 'home:dashboard'

    @property
    def url_list(self):
        return 'security:user-list'

    @property
    def url_profile(self):
        url = reverse_lazy(
            'home:profile-detail',
            kwargs={'pk': self.pk}
        )
        return url

    @property
    def employee_profile_edit(self):
        # definir en caso de no haber employeeprofile
        url = reverse_lazy(
            'security:employee-personal-edit',
            kwargs={'pk': self.employeeprofile.pk}
        )
        return url

    @property
    def url_profile_password(self):
        url = reverse_lazy(
            'home:profile-password',
            kwargs={'pk': self.pk}
        )
        return url

    @property
    def url_new(self):
        url = reverse_lazy(
            'security:user-new',
        )
        return url

    @property
    def url_edit(self):
        url = reverse_lazy(
            'security:user-edit',
            kwargs={'pk': self.pk}
        )
        return url

    @property
    def status_value(self):
        if self.is_active:
            return "Activa"
        else:
            return "Inactiva"

    @property
    def status_color(self):
        if self.is_active:
            return "green"
        else:
            return "gray"

    def __str__(self):
        if self.name:
            return self.name
        else:
            return self.email


class Employee(AppModel, AddressModel):
    GENDER_CHOICES = (
        ('H', 'Hombre'),
        ('M', 'Mujer'),
    )
    user = models.OneToOneField(
        User,
        verbose_name='usuario',
        related_name='employee_profile',
        on_delete=models.CASCADE,
        blank=False,
        null=True,
    )
    is_active = models.BooleanField(
        verbose_name="activo",
        blank=False,
        null=False,
        default=False,
    )
    home_phone = models.BigIntegerField(
        verbose_name='telefono de casa',
        blank=True,
        null=True,
    )
    cel_phone = models.BigIntegerField(
        verbose_name='telefono celular',
        blank=True,
        null=True,
    )
    birthdate = models.DateField(
        verbose_name='fecha de nacimiento',
        blank=True,
        null=True,
    )
    gender = models.CharField(
        verbose_name='genero',
        choices=GENDER_CHOICES,
        max_length=10,
        blank=True,
        null=True,
    )
    photography = models.ImageField(
        verbose_name="foto",
        blank=True,
        null=True,
        upload_to=FileAdmin.path_Photograpy,
    )

    class Meta:
        verbose_name = 'Empleado'
        verbose_name_plural = 'Empleados'

    @property
    def url_edit(self):
        url = reverse_lazy(
            'security:employee-edit',
            kwargs={'pk': self.pk}
        )
        return url

    @property
    def status_text(self):
        if self.is_active:
            return "Activo"
        else:
            return "Inactivo"

    @property
    def status_color(self):
        if self.is_active:
            return "green"
        else:
            return "gray"

    def __str__(self):
        if self.user:
            if self.user.name:
                return self.user.name
            else:
                return 'Unknow'
        else:
            return 'Sin Usuario'


class LoggedDetail(models.Model):

    user = models.OneToOneField(
        User,
        related_name="logged_detail",
        on_delete=models.CASCADE
    )
    session_key = models.CharField(
        max_length=40,
        null=True,
        blank=True
    )
    access_qty = models.PositiveIntegerField(
        null=True,
        blank=True,
        default=0
    )
    last_activity = models.DateTimeField(
        null=True,
        blank=True
    )

    def __str__(self):
        return self.user.email


class CompanyClass(AppModel):
    name = models.CharField(
        verbose_name='nombre',
        max_length=50,
        blank=False,
        null=True
    )
    abbreviation = models.CharField(
        verbose_name='abreviación',
        max_length=3,
        blank=False,
        null=True,
    )

    class Meta:
        verbose_name = 'Clase de Empresa'
        verbose_name_plural = 'Clases de Empresas'

    @property
    def url_edit(self):
        url = reverse_lazy(
            'security:companyclass-edit',
            kwargs={'pk': self.pk}
        )
        return url

    def __str__(self):
        return self.name


class Company(AddressModel, AppModel):
    TYPE_CHOICES = [
        ('PUB', 'Publica'),
        ('PRI', 'Privada'),
    ]
    legal_entity = models.CharField(
        verbose_name='nombre legal',
        max_length=150,
        blank=False,
        null=True,
    )
    commercial_name = models.CharField(
        verbose_name='nombre comercial',
        max_length=150,
        blank=True,
        null=True,
    )
    description = models.TextField(
        verbose_name='descripción',
        blank=True,
        null=True,
    )
    classification = models.ForeignKey(
        CompanyClass,
        verbose_name='clasificación',
        related_name='has_classification',
        on_delete=models.PROTECT,
        blank=False,
        null=True
    )
    type = models.CharField(
        verbose_name='Tipo',
        max_length=3,
        choices=TYPE_CHOICES,
        default='PUB',
    )
    email = models.EmailField(
        verbose_name='email',
        unique=True,
        blank=False,
        null=True,
        error_messages={
            'unique': 'Ya existe este correo asociado a otra cuenta.'
        }
    )
    phone = models.CharField(
        verbose_name='Teléfono',
        max_length=144,
        blank=True,
        null=True,
    )
    website = models.URLField(
        verbose_name='Sitio web',
        max_length=80,
        blank=True,
        null=True,
    )
    logo = models.ImageField(
        verbose_name='Logo de Compañia',
        blank=True,
        null=True,
        upload_to="media/storeItems/logos",
    )
    comments = models.TextField(
        verbose_name='Comentarios',
        blank=True,
        null=True,
    )
    is_active = models.BooleanField(
        verbose_name='Activo',
        blank=False,
        null=False,
        default=True,
    )

    class Meta:
        verbose_name = 'Empresa'
        verbose_name_plural = 'Empresas'

    @property
    def status_text(self):
        if self.is_active:
            return "Activo"
        else:
            return "Inactivo"

    @property
    def status_color(self):
        if self.is_active:
            return "green"
        else:
            return "gray"

    @property
    def url_edit(self):
        url = reverse_lazy(
            'security:company-edit',
            kwargs={'pk': self.pk}
        )
        return url

    def __str__(self):
        return self.legal_entity


class CompanyPosition(AppModel):
    company = models.ForeignKey(
        Company,
        verbose_name='compañia',
        related_name='company_positions',
        on_delete=models.PROTECT,
        blank=False,
        null=True,
    )
    name = models.CharField(
        verbose_name='nombre',
        max_length=50,
        blank=False,
        null=True,
    )
    description = models.TextField(
        verbose_name='descripción',
        blank=True,
        null=True,
    )
    is_active = models.BooleanField(
        verbose_name="activo",
        blank=False,
        null=False,
        default=True,
    )
    position_father = models.ForeignKey(
        'self',
        verbose_name='puesto superior',
        related_name='child_positions',
        on_delete=models.PROTECT,
        blank=True,
        null=True,
    )
    permissions = models.ManyToManyField(
        Permission,
        verbose_name='Permisos',
        related_name='assigned_to',
        blank=True,
    )

    class Meta:
        unique_together = (('company', 'name'),)
        verbose_name = 'Puesto'
        verbose_name_plural = 'Puestos'

    @property
    def url_edit(self):
        url = reverse_lazy(
            'security:companyposition-edit',
            kwargs={'pk': self.pk}
        )
        return url

    @property
    def status_text(self):
        if self.is_active:
            return "Activo"
        else:
            return "Inactivo"

    @property
    def status_color(self):
        if self.is_active:
            return "green"
        else:
            return "gray"

    def __str__(self):
        return self.name


class CompanyBilling(AppModel):
    FISCAL_REGIMEN_CHOICES = (
        ('601', 'General de Ley Personas Morales'),
        ('603', 'Personas Morales con Fines no Lucrativos'),
        ('605', 'Sueldos y Salarios e Ingresos Asimilados a Salarios'),
        ('606', 'Arrendamiento'),
        ('607', 'Régimen de Enajenación o Adquisición de Bienes'),
        ('608', 'Demás ingresos'),
        ('609', 'Consolidación'),
        ('610', 'Residentes en el Extranjero sin Establecimiento Permanente en México'),
        ('611', 'Ingresos por Dividendos (socios y accionistas)'),
        ('612', 'Personas Físicas con Actividades Empresariales y Profesionales'),
        ('614', 'Ingresos por intereses'),
        ('615', 'Régimen de los ingresos por obtención de premios'),
        ('616', 'Sin obligaciones fiscales'),
        ('620', 'Sociedades Cooperativas de Producción que optan por diferir sus ingresos'),
        ('621', 'Incorporación Fiscal'),
        ('622', 'Actividades Agrícolas, Ganaderas, Silvícolas y Pesqueras'),
        ('623', 'Opcional para Grupos de Sociedades'),
        ('624', 'Coordinados'),
        ('628', 'Hidrocarburos'),
        ('629', 'De los Regímenes Fiscales Preferentes y de las Empresas Multinacionales'),
        ('630', 'Enajenación de acciones en bolsa de valore')
    )
    company = models.OneToOneField(
        Company,
        verbose_name='Compañia',
        related_name='billing_settings',
        on_delete=models.CASCADE,
        blank=False,
        null=True
    )
    rfc = models.CharField(
        verbose_name='RFC',
        max_length=13,
        unique=True,
        blank=False,
        null=True,
    )
    fiscal_regimen = models.CharField(
        verbose_name='Regimen Fiscal',
        max_length=3,
        choices=FISCAL_REGIMEN_CHOICES,
        blank=True,
        null=True,
    )
    password = models.CharField(
        verbose_name='Contraseña',
        max_length=128,
        blank=False,
        null=True,
    )
    is_active = models.BooleanField(
        verbose_name='Activo',
        default=True
    )

    class Meta:
        verbose_name = 'Facturación'
        verbose_name_plural = 'Facturaciones'

    def __str__(self):
        return f'{self.id}: {self.company.commercial_name}'


class BranchOffice(AddressModel, AppModel):
    company = models.ForeignKey(
        Company,
        verbose_name='compañía',
        related_name='has_branchoffices',
        on_delete=models.PROTECT,
        blank=False,
        null=True,
    )
    name = models.CharField(
        verbose_name="nombre",
        max_length=255,
    )
    description = models.TextField(
        verbose_name='descripción',
        blank=True,
        null=True,
    )
    is_active = models.BooleanField(
        verbose_name="activo",
        blank=False,
        null=False,
        default=True,
    )
    phone = models.CharField(
        max_length=144,
        verbose_name='teléfono',
        blank=True,
        null=True,
    )

    class Meta:
        unique_together = (('company', 'name'),)
        verbose_name = 'Sucursal'
        verbose_name_plural = 'Sucursales'

    def __str__(self):
        return self.name

    @property
    def status_text(self):
        if self.is_active:
            return "Activo"
        else:
            return "Inactivo"

    @property
    def status_color(self):
        if self.is_active:
            return "green"
        else:
            return "gray"

    @property
    def url_edit(self):
        url = reverse_lazy(
            'security:branchoffice-edit',
            kwargs={'pk': self.pk}
        )
        return url

    @property
    def url_retrieve(self):
        url = reverse_lazy(
            'security:branchoffice-retrieve',
            kwargs={'pk': self.pk}
        )
        return url


class Assignment(AppModel):
    user = models.ForeignKey(
        User,
        verbose_name='usuarios',
        related_name='has_assignments',
        on_delete=models.PROTECT,
        blank=True,
        null=True,
    )
    company = models.ForeignKey(
        Company,
        verbose_name='compañía',
        related_name='has_assignment',
        on_delete=models.PROTECT,
        blank=True,
        null=True,
    )
    position = models.ForeignKey(
        CompanyPosition,
        verbose_name='puesto',
        related_name='position_assignment',
        on_delete=models.PROTECT,
        blank=True,
        null=True,
    )
    branches = models.ManyToManyField(
        BranchOffice,
        verbose_name="sucursales",
        related_name="branches_assignment",
        blank=True,
    )
    is_active = models.BooleanField(
        verbose_name="activo",
        blank=False,
        null=False,
        default=True,
    )
    start_date = models.DateTimeField(
        verbose_name="Fecha Inicio",
        blank=True,
        null=True

    )
    end_date = models.DateTimeField(
        verbose_name="Fecha de Finalización",
        blank=True,
        null=True
    )

    class Meta:
        unique_together = (('company', 'user', 'position'),)
        verbose_name = 'asinación'
        verbose_name_plural = 'asignaciones'

    def __str__(self):
        value = "{}".format(
            self.id
        )
        return value

    @property
    def status_text(self):
        if self.is_active:
            return "Activo"
        else:
            return "Inactivo"

    @property
    def status_color(self):
        if self.is_active:
            return "green"
        else:
            return "gray"
