# Django's Libraries
from django.urls import path

# Own's Libraries
from .views import PermissionListView
from .views import PermissionNewView
from .views import PermissionEditView

from .views import UserListView
from .views import UserNewView
from .views import UserEditView
from .views import UserPermissionsView
from .views import UserPasswordView

from .views import EmployeeListView
from .views import EmployeeNewView
from .views import EmployeeEditView
from .views import EmployeeExportExcel
from .views import EmployeeImportExcel
from .views import EmployeeEmptyImportView

from .views import EmployeeAssignmentView

from .views import CompanyListView
from .views import CompanyNewView
from .views import CompanyEditView
from .views import CompanyEmailView
from .views import CompanyBillingView
from .views import CompanyExportExcel
from .views import CompanyEmptyImportView
from .views import CompanyImportExcel
from .views import CompanyPositionImportExcel
from .views import CompanyPositionExportExcel
from .views import CompanyPositionEmptyImportView
from .views import CompanyBillingDeleteView
from .views import CompanyClassListView
from .views import CompanyClassNewView
from .views import CompanyClassEditView

from .views import CompanyPositionListView
from .views import CompanyPositionNewView
from .views import CompanyPositionEditView

from .views import BranchOfficeListView
from .views import BranchOfficeNewView
from .views import BranchOfficeEditView
from .views import BranchOfficeEmptyImportView
from .views import BranchOfficeExportExcel
from .views import BranchOfficeImportExcel


app_name = 'security'

urlpatterns = [
    path(
        'permissions/',
        PermissionListView.as_view(),
        name='permission-list'
    ),
    path(
        'permissions/new/',
        PermissionNewView.as_view(),
        name='permission-new'
    ),
    path(
        'permissions/<int:pk>/edit/',
        PermissionEditView.as_view(),
        name='permission-edit'
    ),
    path(
        'users/',
        UserListView.as_view(),
        name="user-list"
    ),
    path(
        'users/new/',
        UserNewView.as_view(),
        name="user-new"
    ),
    path(
        'users/<int:pk>/edit/',
        UserEditView.as_view(),
        name="user-edit"
    ),
    path(
        'users/<int:pk>/permissions/',
        UserPermissionsView.as_view(),
        name="user-permissions"
    ),
    path(
        'users/<int:pk>/password/',
        UserPasswordView.as_view(),
        name="user-password"
    ),
    path(
        'employees/',
        EmployeeListView.as_view(),
        name='employee-list',
    ),
    path(
        'employees/new/',
        EmployeeNewView.as_view(),
        name='employee-new',
    ),
    path(
        'employees/<int:pk>/edit/',
        EmployeeEditView.as_view(),
        name='employee-edit',
    ),
    path(
        'employees/xls',
        EmployeeExportExcel.as_view(),
        name='employee-list-xls'
    ),
    path(
        'employees/import/',
        EmployeeImportExcel.as_view(),
        name='employees-import'
    ),
    path(
        'employees/import/empty/',
        EmployeeEmptyImportView.as_view(),
        name='employees-import-empty'
    ),
    path(
        'employees/<int:pk>/assignments/',
        EmployeeAssignmentView.as_view(),
        name='employee-assignment',
    ),
    path(
        'companies/',
        CompanyListView.as_view(),
        name='company-list'
    ),
    path(
        'companies/new/',
        CompanyNewView.as_view(),
        name='company-new'
    ),
    path(
        'companies/<int:pk>/edit/',
        CompanyEditView.as_view(),
        name='company-edit'
    ),
    path(
        'companies/<int:pk>/email/settings/',
        CompanyEmailView.as_view(),
        name='company-email'
    ),
    path(
        'companies/<int:pk>/billing/settings/',
        CompanyBillingView.as_view(),
        name='company-billing'
    ),
    path(
        'companies/<int:pk>/billing/settings/delete/',
        CompanyBillingDeleteView.as_view(),
        name='company-billing-delete'
    ),
    path(
        'companies/xls',
        CompanyExportExcel.as_view(),
        name='company-list-xls'
    ),
    path(
        'companies/import/',
        CompanyImportExcel.as_view(),
        name='company-import'
    ),
    path(
        'companies/import/empty/',
        CompanyEmptyImportView.as_view(),
        name='companies-import-empty'
    ),
    path(
        'companyclassifications/',
        CompanyClassListView.as_view(),
        name='companyclass-list'
    ),
    path(
        'companyclassifications/new/',
        CompanyClassNewView.as_view(),
        name='companyclass-new'
    ),
    path(
        'companyclassifications/<int:pk>/edit/',
        CompanyClassEditView.as_view(),
        name='companyclass-edit'
    ),
    path(
        'positions/',
        CompanyPositionListView.as_view(),
        name='companyposition-list'
    ),
    path(
        'positions/new/',
        CompanyPositionNewView.as_view(),
        name='companyposition-new'
    ),
    path(
        'positions/<int:pk>/edit/',
        CompanyPositionEditView.as_view(),
        name='companyposition-edit'
    ),
    path(
        'branchoffices/',
        BranchOfficeListView.as_view(),
        name='branchoffice-list',
    ),
    path(
        'branchoffices/new/',
        BranchOfficeNewView.as_view(),
        name='branchoffice-new',
    ),
    path(
        'branchoffices/<int:pk>/edit/',
        BranchOfficeEditView.as_view(),
        name='branchoffice-edit',
    ),
    path(
        'branchoffices/xls',
        BranchOfficeExportExcel.as_view(),
        name='branchoffice-list-xls'
    ),
    path(
        'branchoffices/import/',
        BranchOfficeImportExcel.as_view(),
        name='branchoffices-import'
    ),
    path(
        'branchoffices/import/empty/',
        BranchOfficeEmptyImportView.as_view(),
        name='branchoffice-import-empty'
    ),
    path(
        'companypositions/xls',
        CompanyPositionExportExcel.as_view(),
        name='companyposition-list-xls'
    ),
    path(
        'companypositions/import/',
        CompanyPositionImportExcel.as_view(),
        name='companyposition-import'
    ),
    path(
        'companypositions/import/empty/',
        CompanyPositionEmptyImportView.as_view(),
        name='companypositions-import-empty'
    ),
]
