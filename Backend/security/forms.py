# Django's Libraries
from django.contrib import messages
from django.forms import FileField
from django.forms import ModelMultipleChoiceField
from django.forms import ModelForm
from django.forms import CharField
from django.forms import EmailField
from django.forms import PasswordInput
from django.forms import BooleanField
from django.forms import PasswordInput

from django.contrib.auth.forms import AdminPasswordChangeForm
from django.utils.safestring import mark_safe
from django.core.exceptions import ImproperlyConfigured
from django.contrib.sites.shortcuts import get_current_site

# Own's Libraries
from tools.forms import StxModelCreateForm

from .models import Permission
from .models import User
from .models import Employee
from .models import Assignment
from .models import Company
from .models import CompanyClass
from .models import CompanyPosition
from .models import BranchOffice
from .models import CompanyBilling

from .business import PermissionBsn
from .business import UserBsn
from .business import EmployeeBsn
from .business import CompanyBsn
from .business import CompanyClassBsn
from .business import CompanyPositionBsn
from .business import BranchOfficeBsn


class PermissionForm(StxModelCreateForm):
    business_class = PermissionBsn

    class Meta:
        model = Permission
        fields = [
            'code',
            'name',
            'description',
            'is_active',
        ]


class UserForm(StxModelCreateForm):
    business_class = UserBsn

    class Meta:
        model = User
        fields = [
            'name',
            'email',
            'email',
            'is_active',
            'is_superuser',
        ]
        exclude = [
            'date_joined',
            'is_staff',
        ]


class EmployeeCreateForm(StxModelCreateForm):
    business_class = EmployeeBsn
    companies = ModelMultipleChoiceField(
        label="Compañias",
        queryset=Company.objects.none(),
        required=False
    )
    email = EmailField(
        label='Correo Electronico',
        required=True,
    )
    name = CharField(
        label='Nombre Completo',
        max_length=144,
        required=True,
    )

    class Meta:
        model = Employee
        fields = [
            'email',
            'name',
            'companies',
            'home_phone',
            'cel_phone',
            'birthdate',
            'gender',
            'settlement',
            'municipality',
            'street',
            'outdoor_number',
            'interior_number',
            'between_street_a',
            'between_street_b',
            'zip_code',
            'latitude',
            'longitude',
            'photography',
        ]

    def save(self, request):
        if self.business_class is not None:
            data = self.cleaned_data
            record = self.business_class.create(
                data,
                self.user,
                get_current_site(request),
                request.FILES
            )
            url = self.business_class.get_UrlOnSave(
                request,
                record
            )
            return url
        else:
            raise ImproperlyConfigured(
                "Favor de especificar una clase de negocio"
            )

    def configurate_Fields(self):
        self.fields['companies'].queryset = CompanyBsn.get_Assigned(self.user)


class EmployeeUpdateForm(StxModelCreateForm):
    business_class = EmployeeBsn

    companies = ModelMultipleChoiceField(
        label="Compañias",
        queryset=Company.objects.none(),
        required=False
    )
    email = EmailField(
        label='Correo Electronico',
        required=True,
    )
    name = CharField(
        label='Nombre(s)',
        max_length=144,
        required=True,
    )

    class Meta:
        model = Employee
        fields = [
            'email',
            'name',
            'companies',
            'home_phone',
            'cel_phone',
            'birthdate',
            'gender',
            'settlement',
            'municipality',
            'street',
            'outdoor_number',
            'interior_number',
            'between_street_a',
            'between_street_b',
            'zip_code',
            'latitude',
            'longitude',
            'photography',
        ]

    def configurate_Fields(self):
        self.fields['email'].initial = self.instance.user.email
        self.fields['name'].initial = self.instance.user.name
        self.fields['email'].widget.attrs['readonly'] = True
        self.fields['companies'].queryset = CompanyBsn.get_Assigned(self.user)
        self.fields['companies'].initial = self.instance.user.companies.all().values_list(
            'id', flat=True
        )

    def save(self, request):
        if self.business_class is not None:
            data = self.cleaned_data
            if 'save' in request.POST:
                record = self.business_class.update(
                    self.instance,
                    data,
                    self.user,
                    request.FILES
                )
                url = self.business_class.get_UrlOnSave(
                    request,
                    record
                )
                return url
            else:
                self.business_class.send_AccessEmail(
                    self.instance.user,
                    get_current_site(request),
                )
                messages.success(
                    request,
                    "Se reenvio correo para activación de cuenta"
                )
        else:
            raise ImproperlyConfigured(
                "Favor de especificar una clase de negocio"
            )


class AssignmentForm(ModelForm):
    class Meta:
        model = Assignment
        fields = (
            'user',
            'company',
            'position',
            'branches',
            'is_active',
            'start_date',
            'end_date',
        )


class UserGroupForm(ModelForm):
    class Meta:
        model = User
        fields = (      
            'groups',
        )


class UserPasswordForm(AdminPasswordChangeForm):
    password1 = CharField(
        label='Nueva contraseña',
        help_text=mark_safe(
            '<ul><li>La contraseña no puede ser similar a su otra '
            'información personal.</li>'
            '<li>La contraseña debe contener al menos 8 caracteres.</li>'
            '<li>La contraseña no puede ser una contraseña común.</li>'
            '<li>La contraseña no puede ser enteramente numérica.</li></ul>'),
        widget=PasswordInput()
    )
    password2 = CharField(
        label='Confirmar contraseña',
        help_text="Para verificar, introduzca la misma "
                  "contraseña que introdujo antes.",
        widget=PasswordInput()
    )
    reset_password = BooleanField(
        label='¿Solicitar cambiar al iniciar?',
    )

    def __init__(self, *args, **kwargs):
        super(UserPasswordForm, self).__init__(*args, **kwargs)
        self.fields['reset_password'].required = False
        self.fields['reset_password'].initial = self.user.reset_password


class UserPasswordResetForm(AdminPasswordChangeForm):
    password1 = CharField(
        label='Nueva contraseña',
        help_text=mark_safe(
            '<ul><li>La contraseña no puede ser similar a su otra '
            'información personal.</li>'
            '<li>La contraseña debe contener al menos 8 caracteres.</li>'
            '<li>La contraseña no puede ser una contraseña común.</li>'
            '<li>La contraseña no puede ser enteramente numérica.</li></ul>'),
        widget=PasswordInput()
    )
    password2 = CharField(
        label='Confirmar contraseña',
        help_text="Para verificar, introduzca la misma "
                  "contraseña que introdujo antes.",
        widget=PasswordInput()
    )


class CompanyClassForm(StxModelCreateForm):
    business_class = CompanyClassBsn

    class Meta:
        model = CompanyClass
        fields = [
            'name',
            'abbreviation',
        ]


class CompanyForm(StxModelCreateForm):
    business_class = CompanyBsn

    class Meta:
        model = Company
        fields = [
            'legal_entity',
            'commercial_name',
            'description',
            'classification',
            'type',
            'email',
            'phone',
            'website',
            'is_active',
            'logo',
            'comments',
            'settlement',
            'municipality',
            'street',
            'outdoor_number',
            'interior_number',
            'between_street_a',
            'between_street_b',
            'zip_code',
            'latitude',
            'longitude',
        ]


class CompanyPositionForm(StxModelCreateForm):
    business_class = CompanyPositionBsn

    class Meta:
        model = CompanyPosition
        fields = [
            'company',
            'name',
            'position_father',
            'is_active',
            'permissions',
        ]

    def configurate_Fields(self):
        self.fields['company'].queryset = CompanyBsn.get_Assigned(self.user)
        if self.instance:
            self.fields['position_father'].queryset = CompanyPositionBsn.get_Assigned(self.user).exclude(id=self.instance.id)
        else:
            self.fields['position_father'].queryset = CompanyPositionBsn.get_Assigned(self.user)


class CompanyBillingForm(StxModelCreateForm):

    cer = FileField(label='Archivo CER', required=False)
    key = FileField(label='Archivo KEY', required=False)

    class Meta:
        model = CompanyBilling
        fields = [
            'rfc',
            'password',
            'is_active',
            'fiscal_regimen'
        ]
        widgets = {
            'password': PasswordInput(),
        }


class BranchOfficeForm(StxModelCreateForm):
    business_class = BranchOfficeBsn

    class Meta:
        model = BranchOffice
        fields = [
            'company',
            'name',
            'description',
            'phone',
            'settlement',
            'municipality',
            'street',
            'outdoor_number',
            'interior_number',
            'between_street_a',
            'between_street_b',
            'zip_code',
            'latitude',
            'longitude',
            'is_active',
        ]

    def configurate_Fields(self):
        self.fields['company'].queryset = CompanyBsn.get_Assigned(self.user)
