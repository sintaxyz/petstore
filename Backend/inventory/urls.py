# DJango's Libraries
from django.urls import path

from .views import DocShrinkageListView
from .views import DocShrinkageNewView
from .views import DocShrinkageEditView
from .views import DocShrinkageRetrieveView
from .views import DocAdjustmentListView
from .views import DocAdjustmentNewView
from .views import DocAdjustmentEditView
from .views import DocAdjustmentRetrieveView
from .views import DocDispatchListView
from .views import DocDispatchNewView
from .views import DocDispatchEditView
from .views import DocDispatchRetrieveView
from .views import DocInitialEntryListView
from .views import DocInitialEntryNewView
from .views import DocInitialEntryEditView
from .views import DocInitialEntryRetrieveView
from .views import DocReceptionListView
from .views import DocReceptionNewView
from .views import DocReceptionEditView
from .views import DocReceptionRetrieveView
from .views import DocInitialEntryExportExcel
from .views import DocReceptionExportExcel


app_name = 'inventory'

urlpatterns = [
    path(
        'docadjustments/',
        DocAdjustmentListView.as_view(),
        name='docadjustment-list',
    ),
    path(
        'docadjustments/new/',
        DocAdjustmentNewView.as_view(),
        name='docadjustment-new',
    ),
    path(
        'docadjustments/<int:pk>/edit/',
        DocAdjustmentEditView.as_view(),
        name='docadjustment-edit',
    ),
    path(
        'docadjustments/<int:pk>/retrieve/',
        DocAdjustmentRetrieveView.as_view(),
        name='docadjustment-retrieve',
    ),
    path(
        'docinitialentries/',
        DocInitialEntryListView.as_view(),
        name='docinitialentry-list',
    ),
    path(
        'docinitialentries/new/',
        DocInitialEntryNewView.as_view(),
        name='docinitialentry-new',
    ),
    path(
        'docinitialentries/<int:pk>/edit/',
        DocInitialEntryEditView.as_view(),
        name='docinitialentry-edit',
    ),
    path(
        'docinitialentries/<int:pk>/retrieve/',
        DocInitialEntryRetrieveView.as_view(),
        name='docinitialentry-retrieve',
    ),
    path(
        'docshrinkages/',
        DocShrinkageListView.as_view(),
        name='docshrinkage-list',
    ),
    path(
        'docshrinkages/new/',
        DocShrinkageNewView.as_view(),
        name='docshrinkage-new',
    ),
    path(
        'docshrinkages/<int:pk>/edit/',
        DocShrinkageEditView.as_view(),
        name='docshrinkage-edit',
    ),
    path(
        'docshrinkages/<int:pk>/retrieve/',
        DocShrinkageRetrieveView.as_view(),
        name='docshrinkage-retrieve',
    ),
    path(
        'docreceptions/',
        DocReceptionListView.as_view(),
        name='docreception-list',
    ),
    path(
        'docreceptions/new/',
        DocReceptionNewView.as_view(),
        name='docreception-new',
    ),
    path(
        'docreceptions/<int:pk>/edit/',
        DocReceptionEditView.as_view(),
        name='docreception-edit',
    ),
    path(
        'docreceptions/<int:pk>/retrieve/',
        DocReceptionRetrieveView.as_view(),
        name='docreception-retrieve',
    ),
    path(
        'docdispatches/',
        DocDispatchListView.as_view(),
        name='docdispatch-list',
    ),
    path(
        'docdispatches/new/',
        DocDispatchNewView.as_view(),
        name='docdispatch-new',
    ),
    path(
        'docdispatches/<int:pk>/edit/',
        DocDispatchEditView.as_view(),
        name='docdispatch-edit',
    ),
    path(
        'docdispatches/<int:pk>/retrieve/',
        DocDispatchRetrieveView.as_view(),
        name='docdispatch-retrieve',
    ),
    path(
        'docinicialentry/export/xlsx/',
        DocInitialEntryExportExcel.as_view(),
        name='docinicialentry-export-xlsx'
    ),
    path(
        'docreception/export/xlsx/',
        DocReceptionExportExcel.as_view(),
        name='docreception-export-xlsx'
    ),
    
]
