# Django's Libraries
from django.urls import path

# Own's Libraries
from .views_rest import DocReceptionListAPIView
from .views_rest import DocReceptionRetrieveAPIView
from .views_rest import DocReceptionLinesUpdateAPIView
from .views_rest import DocReceptionUpdateDescAPIView
from .views_rest import DocReceptionFinishAPIView
from .views_rest import DocReceptionCancelAPIView
from .views_rest import DocReceptionListLinesAPIView
from .views_rest import DocDispatchListAPIView
from .views_rest import DocDispatchRetrieveAPIView
from .views_rest import DocDispatchLinesUpdateAPIView
from .views_rest import DocDispatchUpdateDescAPIView
from .views_rest import DocDispatchFinishAPIView
from .views_rest import DocDispatchListLinesAPIView
from .views_rest import DocInitialEntryListAPIView
from .views_rest import DocInitialEntryRetrieveAPIView
from .views_rest import DocInitialEntryListLinesAPIView
from .views_rest import DocInitialEntryLinesUpdateAPIView
from .views_rest import DocInitialEntryUpdateDescAPIView
from .views_rest import DocInitialEntryFinishAPIView
from .views_rest import DocInitialEntryCancelAPIView
from .views_rest import DocAdjustmentListAPIView
from .views_rest import DocAdjustmentRetrieveAPIView
from .views_rest import DocAdjustmentLinesUpdateAPIView
from .views_rest import DocAdjustmentUpdateDescAPIView
from .views_rest import DocAdjustmentFinishAPIView
from .views_rest import DocAdjustmentListLinesAPIView
from .views_rest import DocShrinkageListAPIView
from .views_rest import DocShrinkageRetrieveAPIView
from .views_rest import DocShrinkageLinesUpdateAPIView
from .views_rest import DocShrinkageUpdateDescAPIView
from .views_rest import DocShrinkageFinishAPIView
from .views_rest import DocShrinkageListLinesAPIView


app_name = 'inventory-api'

urlpatterns = [
    path(
        'docadjustments/',
        DocAdjustmentListAPIView.as_view({
            'get': 'list',
            'post': 'create',
        }),
        name='docadjustment-list'
    ),
    path(
        'docadjustments/<int:pk>/',
        DocAdjustmentRetrieveAPIView.as_view({
            'get': 'retrieve',
            'put': 'update',
            'post': 'create',
        }),
        name='docadjustment-retrieve'
    ),
    path(
        'docadjustments/<int:pk>/desc/',
        DocAdjustmentUpdateDescAPIView.as_view({
            'post': 'update',
        }),
        name='docadjustment-update-desc'
    ),    
    path(
        'docadjustments/<int:pk>/finish/',
        DocAdjustmentFinishAPIView.as_view({
            'get': 'finish',
        }),
        name='docadjustment-finish'
    ),
    path(
        'docadjustments/<int:pk>/lines/<int:line_pk>/',
        DocAdjustmentLinesUpdateAPIView.as_view({
            'put': 'update',
        }),
        name='docadjustmentline-update'
    ),    
    path(
        'docreceptions/',
        DocReceptionListAPIView.as_view({
            'get': 'list',
            'post': 'create',
        }),
        name='docreception-list'
    ),
    path(
        'docreceptions/<int:pk>/desc/',
        DocReceptionUpdateDescAPIView.as_view({
            'post': 'update',
        }),
        name='docreception-update-desc'
    ),
    path(
        'docreceptions/<int:pk>/lines/',
        DocReceptionListLinesAPIView.as_view({
            'get': 'list',
        }),
        name='docreception-lines'
    ),
    path(
        'docreceptions/<int:pk>/lines/<int:line_pk>/',
        DocReceptionLinesUpdateAPIView.as_view({
            'put': 'update',
        }),
        name='docreceptionline-update'
    ),
    path(
        'docreceptions/<int:pk>/finish/',
        DocReceptionFinishAPIView.as_view({
            'get': 'finish',
        }),
        name='docreception-finish'
    ),
    path(
        'docreceptions/<int:pk>/cancel/',
        DocReceptionCancelAPIView.as_view({
            'get': 'cancel',
        }),
        name='docreception-cancel'
    ),
    path(
        'docreceptions/<int:pk>/',
        DocReceptionRetrieveAPIView.as_view({
            'get': 'retrieve',
            'put': 'update',
            'post': 'create',
        }),
        name='docreception-retrieve'
    ),    
    path(
        'docdispatches/',
        DocDispatchListAPIView.as_view({
            'get': 'list',
            'post': 'create',
        }),
        name='docdispatch-list'
    ),
    path(
        'docdispatches/<int:pk>/',
        DocDispatchRetrieveAPIView.as_view({
            'get': 'retrieve',
            'put': 'update',
            'post': 'create',
        }),
        name='docdispatch-retrieve'
    ),
    path(
        'docdispatches/<int:pk>/desc/',
        DocDispatchUpdateDescAPIView.as_view({
            'post': 'update',
        }),
        name='docdispatch-update-desc'
    ),    
    path(
        'docdispatches/<int:pk>/finish/',
        DocDispatchFinishAPIView.as_view({
            'get': 'finish',
        }),
        name='docdispatch-finish'
    ),
    path(
        'docdispatches/<int:pk>/lines/<int:line_pk>/',
        DocDispatchLinesUpdateAPIView.as_view({
            'put': 'update',
        }),
        name='docdispatchline-update'
    ),    
    path(
        'docinitialentries/',
        DocInitialEntryListAPIView.as_view({
            'get': 'list',
            'post': 'create',
        }),
        name='docinitialentry-list'
    ),
    path(
        'docinitialentries/<int:pk>/',
        DocInitialEntryRetrieveAPIView.as_view({
            'get': 'retrieve',
            'put': 'update',
            'post': 'create',
        }),
        name='docinitialentry-retrieve'
    ),
    path(
        'docinitialentriess/<int:pk>/lines/',
        DocInitialEntryListLinesAPIView.as_view({
            'get': 'list',
        }),
        name='docinitialentry-lines'
    ),
    path(
        'docinitialentries/<int:pk>/desc/',
        DocInitialEntryUpdateDescAPIView.as_view({
            'post': 'update',
        }),
        name='docinitialentry-update-desc'
    ),    
    path(
        'docinitialentries/<int:pk>/finish/',
        DocInitialEntryFinishAPIView.as_view({
            'get': 'finish',
        }),
        name='docinitialentry-finish'
    ),
    path(
        'docinitialentries/<int:pk>/cancel/',
        DocInitialEntryCancelAPIView.as_view({
            'get': 'cancel',
        }),
        name='docinitialentry-cancel'
    ),
    path(
        'docinitialentries/<int:pk>/lines/<int:line_pk>/',
        DocInitialEntryLinesUpdateAPIView.as_view({
            'put': 'update',
        }),
        name='docinitialentryline-update'
    ),
    path(
        'docshrinkages/',
        DocShrinkageListAPIView.as_view({
            'get': 'list',
            'post': 'create',
        }),
        name='docshrinkage-list'
    ),
    path(
        'docshrinkages/<int:pk>/',
        DocShrinkageRetrieveAPIView.as_view({
            'get': 'retrieve',
            'put': 'update',
            'post': 'create',
        }),
        name='docshrinkage-retrieve'
    ),
    path(
        'docshrinkages/<int:pk>/desc/',
        DocShrinkageUpdateDescAPIView.as_view({
            'post': 'update',
        }),
        name='docshrinkage-update-desc'
    ),    
    path(
        'docshrinkages/<int:pk>/finish/',
        DocShrinkageFinishAPIView.as_view({
            'get': 'finish',
        }),
        name='docshrinkage-finish'
    ),
    path(
        'docshrinkages/<int:pk>/lines/<int:line_pk>/',
        DocShrinkageLinesUpdateAPIView.as_view({
            'put': 'update',
        }),
        name='docshrinkageline-update'
    ),    
]
