# Django Libraries
from django.shortcuts import render
from django.shortcuts import redirect
from django.urls import reverse_lazy
from django.contrib import messages
from django.db.models import Sum

# Own's Libraries
from tools.views import StxListView
from tools.views import StxEditView
from tools.views import AppListView
from tools.views import AppNewView
from tools.views import AppEditView
from tools.views import StxRetrieveView
from tools.views import StxReactView
from tools.views import StxExportToExcel
from tools.views import ResourceModelImportView
from tools.views import StxEmptyImportView


from .business import DocShrinkageBsn
from .business import DocAdjustmentBsn
from .business import DocDispatchBsn
from .business import DocInitialEntryBsn
from .business import DocReceptionBsn

from .forms import DocShrinkageForm
from .forms import DocDispatchForm
from .forms import DocInitialEntryForm
from .forms import DocReceptionForm
from .forms import DocAdjustmentForm


class DocAdjustmentListView(StxListView):
    template_name = 'inventory/docadjustment/list/docadjustment_list.html'
    business_class = DocAdjustmentBsn


class DocAdjustmentNewView(StxReactView):
    template_name = 'inventory/docadjustment/new/docadjustment_new.html'


class DocAdjustmentEditView(StxEditView):
    update_record_method = 'update'
    template_name = 'inventory/docadjustment/edit/docadjustment_edit.html'
    business_class = DocAdjustmentBsn
    form_class = DocAdjustmentForm
    page_title = 'Editar Ajustes'


class DocAdjustmentRetrieveView(StxRetrieveView):
    template_name = "inventory/docadjustment/retrieve/docadjustment_retrieve.html"
    business_class = DocAdjustmentBsn


class DocInitialEntryListView(StxListView):
    permissions = ['VECI']
    template_name = 'inventory/docinitialentry/list/docinitialentry_list.html'
    business_class = DocInitialEntryBsn

    def pre_Return(self):
        records = self.context['queryset']
        if len(records) < 1000:
            totals = records.aggregate(Sum('total_amount'))
            self.append_Context(total_amount=totals['total_amount__sum'])


class DocInitialEntryNewView(StxReactView):
    permissions = ['CRCI']
    template_name = 'inventory/docinitialentry/new/docinitialentry_new.html'


class DocInitialEntryEditView(StxEditView):
    permissions = ['EDCI']
    path_related_company = ['warehouse', 'company', 'id']
    update_record_method = 'update'
    template_name = 'inventory/docinitialentry/edit/docinitialentry_edit.html'
    business_class = DocInitialEntryBsn
    form_class = DocInitialEntryForm
    page_title = 'Editar Carga Inicial'


class DocInitialEntryRetrieveView(StxRetrieveView):
    permissions = ['VECI']
    path_related_company = ['warehouse', 'company', 'id']
    template_name = "inventory/docinitialentry/retrieve/docinitialentry_retrieve.html"
    business_class = DocInitialEntryBsn


class DocInitialEntryExportExcel(StxExportToExcel):
    permissions = ['EXCI']
    business_class = DocInitialEntryBsn
    dataset_method = 'export_XLSX'
    name_file = 'Listado de Carga Inicial.xlsx'


class DocShrinkageListView(StxListView):
    template_name = 'inventory/shrinkage/list/shrinkage_list.html'
    business_class = DocShrinkageBsn


class DocShrinkageNewView(StxReactView):
    template_name = 'inventory/shrinkage/new/shrinkage_new.html'


class DocShrinkageEditView(StxEditView):
    update_record_method = 'update'
    template_name = 'inventory/shrinkage/edit/shrinkage_edit.html'
    business_class = DocShrinkageBsn
    form_class = DocShrinkageForm
    page_title = 'Editar DocShrinkage'


class DocShrinkageRetrieveView(StxRetrieveView):
    template_name = "inventory/shrinkage/retrieve/shrinkage_retrieve.html"
    business_class = DocShrinkageBsn


class DocReceptionListView(StxListView):
    permissions = ['VERP']
    template_name = 'inventory/docreception/list/docreception_list.html'
    business_class = DocReceptionBsn


class DocReceptionNewView(StxReactView):
    permissions = ['CRRP']
    template_name = 'inventory/docreception/new/docreception_new.html'


class DocReceptionEditView(StxEditView):
    permissions = ['EDRP']
    path_related_company = ['warehouse', 'company', 'id']
    update_record_method = 'update'
    template_name = 'inventory/docreception/edit/docreception_edit.html'
    business_class = DocReceptionBsn
    form_class = DocReceptionForm
    page_title = 'Editar Recepcion de Articulos'


class DocReceptionRetrieveView(StxRetrieveView):
    permissions = ['VERP']
    path_related_company = ['warehouse', 'company', 'id']
    template_name = "inventory/docreception/retrieve/docreception_retrieve.html"
    business_class = DocReceptionBsn


class DocReceptionExportExcel(StxExportToExcel):
    permissions = ['EXRP']
    business_class = DocReceptionBsn
    dataset_method = 'export_XLSX'
    name_file = 'Listado de Recepcion.xlsx'


class DocDispatchListView(StxListView):
    template_name = 'inventory/docdispatch/list/docdispatch_list.html'
    business_class = DocDispatchBsn


class DocDispatchNewView(StxReactView):
    template_name = 'inventory/docdispatch/new/docdispatch_new.html'


class DocDispatchEditView(StxEditView):
    update_record_method = 'update'
    template_name = 'inventory/docdispatch/edit/docdispatch_edit.html'
    business_class = DocDispatchBsn
    form_class = DocDispatchForm
    page_title = 'Editar Despacho de Articulos '


class DocDispatchRetrieveView(StxRetrieveView):
    template_name = "inventory/docdispatch/retrieve/docdispatch_retrieve.html"
    business_class = DocDispatchBsn
