# Python's Libraries
from decimal import Decimal

# Django's Libraries
from django.db import models
from django.urls import reverse_lazy

# Own's Libraries
from tools.models import AppModel
from tools.validators import MinValueValidator

from security.models import User

from core.models import Warehouse
from core.models import Supplier
from core.models import StockItem
from core.models import Item

# from procurement.models import PurchaseOrderLine


class DocAdjustment(AppModel):
    warehouse = models.ForeignKey(
        Warehouse,
        verbose_name='almacen',
        related_name='has_docadjustment',
        on_delete=models.PROTECT,
        blank=False,
        null=True,
    )
    description = models.CharField(
        max_length=100,
        verbose_name='descripción',
        blank=True,
        null=True,
    )
    comments = models.TextField(
        verbose_name='comentarios',
        blank=True,
        null=True,
    )
    type = models.CharField(
        max_length=50,
        verbose_name='tipo',
        blank=False,
        null=True,
    )
    adjusted_by = models.ForeignKey(
        User,
        verbose_name='realizado por',
        related_name='adjustments_made',
        on_delete=models.PROTECT,
        blank=False,
        null=True,
    )
    date = models.DateTimeField(
        verbose_name='fecha',
        blank=True,
        null=True,
    )

    class Meta:
        verbose_name = 'Ajuste'
        verbose_name_plural = 'Ajustes'

    @property
    def url_edit(self):
        url = reverse_lazy(
            'inventory:docadjustment-edit',
            kwargs={'pk': self.pk}
        )
        return url

    @property
    def url_retrieve(self):
        url = reverse_lazy(
            'inventory:docadjustment-retrieve',
            kwargs={'pk': self.pk}
        )
        return url

    def __str__(self):
        return f'{self.warehouse} - {self.pk}'


class DocAdjustmentLine(AppModel):
    document = models.ForeignKey(
        DocAdjustment,
        verbose_name='documento',
        related_name='in_docadjustment',
        on_delete=models.PROTECT,
        blank=False,
        null=True,
    )
    line = models.SlugField(
        verbose_name='linea del documento',
        max_length=1000,
        null=True,
        blank=True
    )
    item = models.ForeignKey(
        StockItem,
        verbose_name='artículo',
        related_name='has_itemadjustment',
        on_delete=models.PROTECT,
        blank=False,
        null=True,
    )
    comments = models.TextField(
        verbose_name='motivo del ajuste',
        blank=True,
        null=True,
    )
    qty_old = models.DecimalField(
        verbose_name='cantidad anterior',
        max_digits=8,
        decimal_places=2,
        default=0,
        validators=[MinValueValidator(Decimal('0'))],
        null=True,
        blank=False,
    )
    qty_new = models.DecimalField(
        verbose_name='cantidad nueva',
        max_digits=8,
        decimal_places=2,
        default=0,
        validators=[MinValueValidator(Decimal('0'))],
        null=True,
        blank=False,
    )

    class Meta:
        unique_together = (('document', 'item'), ('document', 'line'))
        verbose_name = 'Articulo de Ajuste'
        verbose_name_plural = 'Articulos de ajustes'

    def __str__(self):
        return f'{self.document.warehouse} - {self.pk} - {self.line}'


class DocInitialEntry(AppModel):
    STATUS_CHOICES = (
        ('INP', 'EN PROCESO'),
        ('PEN', 'PENDIENTE'),
        ('APR', 'APROBADA'),
        ('REJ', 'RECHAZADA'),
        ('CAN', 'CANCELADA'),
    )
    warehouse = models.ForeignKey(
        Warehouse,
        verbose_name='almacen',
        related_name='has_docinicitalentry',
        on_delete=models.PROTECT,
        blank=False,
        null=True,
    )
    status = models.CharField(
        verbose_name='Estado',
        max_length=3,
        choices=STATUS_CHOICES,
        default='INP',
        null=True,
        blank=True,
    )
    description = models.CharField(
        max_length=100,
        verbose_name='descripción',
        blank=True,
        null=True,
    )
    comments = models.TextField(
        verbose_name='comentarios',
        blank=True,
        null=True,
    )
    qty_items = models.DecimalField(
        verbose_name='total de articulos',
        max_digits=10,
        decimal_places=2,
        default=0,
        validators=[MinValueValidator(Decimal('0'))],
        null=True,
        blank=False,
    )
    entry_by = models.ForeignKey(
        User,
        verbose_name='ingresado por',
        related_name='entries_made',
        on_delete=models.PROTECT,
        blank=False,
        null=True,
    )
    total_amount = models.DecimalField(
        verbose_name='precio total',
        max_digits=15,
        decimal_places=2,
        blank=True,
        null=True,
    )
    date = models.DateTimeField(
        verbose_name='fecha',
        blank=True,
        null=True,
    )

    class Meta:
        verbose_name = 'Carga Inicial '
        verbose_name_plural = 'Cargas Iniciales'

    @property
    def url_edit(self):
        url = reverse_lazy(
            'inventory:docinitialentry-edit',
            kwargs={'pk': self.pk}
        )
        return url

    @property
    def url_retrieve(self):
        url = reverse_lazy(
            'inventory:docinitialentry-retrieve',
            kwargs={'pk': self.pk}
        )
        return url

    @property
    def status_text(self):
        choices = {
            'INP': 'EN PROCESO',
            'PEN': 'PENDIENTE',
            'APR': 'APROBADA',
            'REJ': 'RECHAZADA',
            'CAN': 'CANCELADA',
        }
        return choices.get(self.status, 'No definido')

    @property
    def status_color(self):
        choices = {
            'INP': 'gray',
            'PEN': 'yellow',
            'APR': 'green',
            'REJ': 'red',
            'CAN': 'red',
        }
        return choices.get(self.status, 'gray')

    def __str__(self):
        return f'{self.warehouse} - {self.pk}'


class DocInitialEntryLine(AppModel):
    STATUS_CHOICES = (
        ('INP', 'EN PROCESO')
    )
    document = models.ForeignKey(
        DocInitialEntry,
        verbose_name='documento',
        related_name='has_entrylines',
        on_delete=models.CASCADE,
        blank=False,
        null=True,
    )
    line = models.SlugField(
        verbose_name='linea del documento',
        max_length=1000,
        null=True,
        blank=True
    )
    item = models.ForeignKey(
        Item,
        verbose_name='artículo',
        related_name='has_itementry',
        on_delete=models.PROTECT,
        blank=False,
        null=True,
    )
    comments = models.TextField(
        verbose_name='comentarios',
        blank=True,
        null=True,
    )
    qty = models.DecimalField(
        verbose_name='cantidad',
        max_digits=8,
        decimal_places=2,
        default=0,
        validators=[MinValueValidator(Decimal('0'))],
        null=True,
        blank=False
    )
    minimun_qty = models.DecimalField(
        verbose_name='minimo en almacen',
        max_digits=8,
        decimal_places=2,
        default=0,
        validators=[MinValueValidator(Decimal('0'))],
        null=True,
        blank=False
    )
    price = models.DecimalField(
        verbose_name='precio unitario',
        max_digits=12,
        decimal_places=2,
        blank=False,
        null=True,
    )
    sell_price = models.DecimalField(
        verbose_name='precio de venta',
        max_digits=12,
        decimal_places=2,
        blank=False,
        null=True,
    )
    total_amount = models.DecimalField(
        verbose_name='precio total',
        max_digits=12,
        decimal_places=2,
        blank=False,
        null=True,
    )
    total_sell_price = models.DecimalField(
        verbose_name='precio venta total',
        max_digits=12,
        decimal_places=2,
        blank=False,
        null=True,
    )

    class Meta:
        unique_together = (('document', 'item'), ('document', 'line'))
        verbose_name = 'Articulo Carga Inicial '
        verbose_name_plural = 'Articulos Carga Inicial'

    def __str__(self):
        return f'{self.document.warehouse} - {self.pk} - {self.line}'


class DocReception(AppModel):
    STATUS_CHOICES = (
        ('INP', 'EN PROCESO'),
        ('PEN', 'PENDIENTE'),
        ('APR', 'APROBADA'),
        ('REJ', 'RECHAZADA'),
        ('CAN', 'CANCELADA'),
    )

    status = models.CharField(
        verbose_name='Estado',
        max_length=3,
        choices=STATUS_CHOICES,
        default='INP',
        null=True,
        blank=True,
    )
    warehouse = models.ForeignKey(
        Warehouse,
        verbose_name='almacen',
        related_name='has_itemreception',
        on_delete=models.PROTECT,
        blank=False,
        null=True,
    )
    order = models.ForeignKey(
        'procurement.PurchaseOrder',
        verbose_name='orden de compra',
        related_name='receptions',
        on_delete=models.CASCADE,
        blank=True,
        null=True,
    )
    number = models.SlugField(
        verbose_name='no. recepción',
        max_length=100,
        null=True,
        blank=True
    )
    description = models.CharField(
        max_length=100,
        verbose_name='descripción',
        blank=True,
        null=True,
    )
    comments = models.TextField(
        verbose_name='comentarios',
        blank=True,
        null=True,
    )
    received_by = models.ForeignKey(
        User,
        verbose_name='realizado por',
        related_name='receptions_received',
        on_delete=models.PROTECT,
        blank=False,
        null=True,
    )
    date = models.DateTimeField(
        verbose_name='fecha',
        blank=True,
        null=True,
    )

    class Meta:
        unique_together = (('order', 'number'),)
        verbose_name = 'Recepcion'
        verbose_name_plural = 'Recepciones'

    @property
    def url_edit(self):
        url = reverse_lazy(
            'inventory:docreception-edit',
            kwargs={'pk': self.pk}
        )
        return url

    @property
    def url_retrieve(self):
        url = reverse_lazy(
            'inventory:docreception-retrieve',
            kwargs={'pk': self.pk}
        )
        return url

    @property
    def status_text(self):
        choices = {
            'INP': 'EN PROCESO',
            'PEN': 'PENDIENTE',
            'APR': 'APROBADA',
            'REJ': 'RECHAZADA',
            'CAN': 'CANCELADA',
        }
        return choices.get(self.status, 'No definido')

    @property
    def status_color(self):
        choices = {
            'INP': 'gray',
            'PEN': 'yellow',
            'APR': 'green',
            'REJ': 'red',
            'CAN': 'red',
        }
        return choices.get(self.status, 'gray')

    def __str__(self):
        return f'{self.order} - {self.number}'


class DocReceptionLine(AppModel):
    document = models.ForeignKey(
        'DocReception',
        verbose_name='documento',
        related_name='in_docreception',
        on_delete=models.CASCADE,
        blank=False,
        null=True,
    )
    item = models.ForeignKey(
        StockItem,
        verbose_name='artículo',
        related_name='has_receptionline',
        on_delete=models.PROTECT,
        blank=False,
        null=True,
    )
    line = models.SlugField(
        verbose_name='linea del documento',
        max_length=1000,
        null=True,
        blank=True
    )
    order_price = models.DecimalField(
        verbose_name='Costo en orden',
        max_digits=12,
        decimal_places=2,
        blank=False,
        null=True,
    )
    order_line = models.ForeignKey(
        'procurement.PurchaseOrderLine',
        verbose_name='linea de la orden',
        related_name='reception_lines',
        on_delete=models.CASCADE,
        null=True,
        blank=True
    )
    qty_received = models.DecimalField(
        verbose_name='cantidad recibida',
        max_digits=8,
        decimal_places=2,
        default=0,
        validators=[MinValueValidator(Decimal('0'))],
        null=True,
        blank=False
    )
    comments = models.TextField(
        verbose_name="comentarios",
        null=True,
        blank=True,
    )

    class Meta:
        unique_together = (('document', 'item'), ('document', 'line'))
        verbose_name = 'Articulo Recibido'
        verbose_name_plural = 'Articulos Recibidos'

    def __str__(self):
        return f'{self.document.number} - {self.pk} - {self.line}'


class DocDispatch(AppModel):
    warehouse = models.ForeignKey(
        Warehouse,
        verbose_name='almacen',
        related_name='has_itemdispatch',
        on_delete=models.PROTECT,
        blank=False,
        null=True,
    )
    description = models.CharField(
        max_length=100,
        verbose_name='descripción',
        blank=True,
        null=True,
    )
    comments = models.TextField(
        verbose_name='comentarios',
        blank=True,
        null=True,
    )
    delivery_by = models.ForeignKey(
        User,
        verbose_name='entregado por',
        related_name='has_delivery_by',
        on_delete=models.PROTECT,
        blank=False,
        null=True,
    )
    received_by = models.ForeignKey(
        User,
        verbose_name='recibido por',
        related_name='has_received_by',
        on_delete=models.PROTECT,
        blank=False,
        null=True,
    )
    date = models.DateTimeField(
        verbose_name='fecha',
        blank=True,
        null=True,
    )

    class Meta:
        verbose_name = 'Despacho'
        verbose_name_plural = 'Despachos'

    @property
    def url_edit(self):
        url = reverse_lazy(
            'inventory:docdispatch-edit',
            kwargs={'pk': self.pk}
        )
        return url

    @property
    def url_retrieve(self):
        url = reverse_lazy(
            'inventory:docdispatch-retrieve',
            kwargs={'pk': self.pk}
        )
        return url

    def __str__(self):
        return f'{self.warehouse} - {self.pk}'


class DocDispatchLine(AppModel):
    document = models.ForeignKey(
        'DocDispatch',
        verbose_name='documento',
        related_name='in_doc',
        on_delete=models.CASCADE,
        blank=False,
        null=True,
    )
    item = models.ForeignKey(
        StockItem,
        verbose_name='artículo',
        related_name='has_itemline',
        on_delete=models.PROTECT,
        blank=False,
        null=True,
    )
    line = models.SlugField(
        verbose_name='linea del documento',
        max_length=1000,
        null=True,
        blank=True
    )
    qty = models.DecimalField(
        verbose_name='cantidad',
        max_digits=8,
        decimal_places=2,
        default=0,
        validators=[MinValueValidator(Decimal('0'))],
        null=True,
        blank=False
    )
    price = models.DecimalField(
        verbose_name='precio unitario',
        max_digits=12,
        decimal_places=2,
        blank=False,
        null=True,
    )
    comments = models.TextField(
        verbose_name='comentarios',
        blank=True,
        null=True,
    )

    class Meta:
        unique_together = (('document', 'item'), ('document', 'line'))
        verbose_name = 'Articulo despachado'
        verbose_name_plural = 'Articulos despachados'

    def __str__(self):
        return f'{self.document.warehouse} - {self.pk} - {self.line}'


class DocShrinkage(AppModel):
    SHRINKAGE_TYPES = [
        ('ADM', 'Administrativa'),
        ('OPE', 'Operativa'),
        ('NAT', 'Natural'),
        ('PRO', 'Producción'),
    ]
    warehouse = models.ForeignKey(
        Warehouse,
        verbose_name='almacén',
        related_name='has_mermas',
        on_delete=models.PROTECT,
        blank=False,
        null=True,
    )
    description = models.CharField(
        max_length=100,
        verbose_name='descripción',
        blank=True,
        null=True,
    )
    comments = models.TextField(
        verbose_name='comentarios',
        blank=True,
        null=True,
    )
    type = models.CharField(
        verbose_name='tipo',
        max_length=3,
        choices=SHRINKAGE_TYPES,
        default='ADM',
        blank=False,
        null=True,
    )
    date = models.DateTimeField(
        verbose_name='fecha',
        blank=True,
        null=True,
    )
    register_by = models.ForeignKey(
        User,
        verbose_name='registrado por',
        related_name='create_merma',
        on_delete=models.PROTECT,
        blank=False,
        null=True,
    )

    class Meta:
        verbose_name = 'Merma'
        verbose_name_plural = 'Mermas'

    @property
    def url_edit(self):
        url = reverse_lazy(
            'inventory:docshrinkage-edit',
            kwargs={'pk': self.pk}
        )
        return url

    @property
    def url_retrieve(self):
        url = reverse_lazy(
            'inventory:docshrinkage-retrieve',
            kwargs={'pk': self.pk}
        )
        return url

    def __str__(self):
        return f'{self.warehouse} - {self.pk}'


class DocShrinkageLine(AppModel):
    document = models.ForeignKey(
        'DocShrinkage',
        verbose_name='merma',
        related_name='has_mermaline',
        on_delete=models.CASCADE,
        blank=False,
        null=True,
    )
    line = models.SlugField(
        verbose_name='linea del documento',
        max_length=1000,
        null=True,
        blank=True
    )
    item = models.ForeignKey(
        StockItem,
        verbose_name='artículo',
        related_name='mermaline_item',
        on_delete=models.PROTECT,
        blank=False,
        null=True,
    )
    qty = models.DecimalField(
        verbose_name='cantidad',
        max_digits=8,
        decimal_places=2,
        default=0,
        validators=[MinValueValidator(Decimal('0'))],
        blank=False,
        null=True,
    )
    comments = models.TextField(
        verbose_name='comentarios',
        blank=True,
        null=True,
    )

    class Meta:
        unique_together = (('document', 'item'), ('document', 'line'))
        verbose_name = 'Linea de merma'
        verbose_name_plural = 'Lineas de merma'

    def __str__(self):
        return f'{self.document.warehouse} - {self.pk} - {self.line}'
