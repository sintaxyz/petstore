# Python Libraries
import datetime

# Third Party Libraries
from import_export import resources
from import_export import fields
from import_export.widgets import ForeignKeyWidget
from import_export.widgets import ManyToManyWidget
from import_export.widgets import IntegerWidget
from import_export.widgets import DecimalWidget
from import_export.widgets import DateWidget
from import_export.widgets import Widget

# Own Libraries
from inventory.models import DocInitialEntry
from inventory.models import DocReception
from procurement.models import PurchaseOrder
from procurement.models import RequisitionOrder
from .models import Warehouse
from security.models import User
from tools.resources import BooleanWidget
from tools.resources import ChoicesWidget
from tools.resources import GenericResource


class DocInitialEntryResource(GenericResource):
    id = fields.Field(
        column_name='ID',
        attribute='id',
        widget=IntegerWidget()
    )
    warehouse = fields.Field(
        column_name='almacen',
        attribute='warehouse',
        widget=ForeignKeyWidget(Warehouse, 'id')
    )
    status = fields.Field(
        column_name='Estado',
        attribute='status',
        widget=ChoicesWidget(choices=DocInitialEntry.STATUS_CHOICES)
    )
    description = fields.Field(
        column_name='descripción',
        attribute='description',
    )
    comments = fields.Field(
        column_name='comentarios',
        attribute='comments',
    )
    qty_items = fields.Field(
        column_name='total de articulos',
        attribute='qty_items',
        widget=DecimalWidget()
    )
    entry_by = fields.Field(
        column_name='ingresado por',
        attribute='entry_by',
        widget=ForeignKeyWidget(User, 'id')
    )
    total_amount = fields.Field(
        column_name='precio total',
        attribute='total_amount',
        widget=DecimalWidget()
    )
    date = fields.Field(
        column_name='fecha',
        attribute='date',
        widget=DateWidget()
    )

    class Meta:
        model = DocInitialEntry
        skip_unchanged = True
        report_skipped = True
        fields = (
            'id',
            'warehouse',
            'status',
            'description',
            'qty_items',
            'entry_by',
            'total_amount',
            'date',
        )
        exclude = (
            'comments',
        )
        export_order = (
            'id',
            'warehouse',
            'status',
            'description',
            'qty_items',
            'entry_by',
            'total_amount',
            'date',
        )


class DocReceptionResource(GenericResource):
    id = fields.Field(
        column_name='ID',
        attribute='id',
        widget=IntegerWidget()
    )
    status = fields.Field(
        column_name='Estado',
        attribute='status',
        widget=ChoicesWidget(choices=DocReception.STATUS_CHOICES)
    )
    warehouse = fields.Field(
        column_name='almacen',
        attribute='warehouse',
        widget=ForeignKeyWidget(Warehouse, 'id')
    )
    order = fields.Field(
        column_name='ID orden de compra',
        attribute='order',
        widget=ForeignKeyWidget(PurchaseOrder, 'id')
    )
    number = fields.Field(
        column_name='no. recepción',
        attribute='number',
    )
    description = fields.Field(
        column_name='descripcion',
        attribute='description',
    )
    comments = fields.Field(
        column_name='comentarios',
        attribute='comments',
    )
    received_by = fields.Field(
        column_name='realizado por',
        attribute='received_by',
        widget=ForeignKeyWidget(User, 'id')
    )
    date = fields.Field(
        column_name='fecha',
        attribute='date',
        widget=DateWidget()
    )

    class Meta:
        model = DocReception
        skip_unchanged = True
        report_skipped = True
        fields = (
            'id',
            'status',
            'warehouse',
            'number',
            'description',
            'received_by',
            'date',
            'order',
        )
        exclude = (
            'comments',
        )
        export_order = (
            'id',
            'status',
            'warehouse',
            'number',
            'description',
            'received_by',
            'date',
            'order',

        )

