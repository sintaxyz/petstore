# Django's Libraries
import django_filters


# Own's Libraries
from core.filters import assigned_warehouses

from .models import DocShrinkage
from .models import DocAdjustment
from .models import DocDispatch
from .models import DocInitialEntry
from .models import DocReception


class DocAdjustmentFilter(django_filters.FilterSet):

    class Meta:
        model = DocAdjustment
        fields = [
            'warehouse',
            'type',
        ]


class DocInitialEntryFilter(django_filters.FilterSet):

    class Meta:
        model = DocInitialEntry
        fields = [
            'warehouse',
            'entry_by',
            'qty_items',
        ]


class DocShrinkageFilter(django_filters.FilterSet):

    class Meta:
        model = DocShrinkage
        fields = [
            'warehouse',
            'type',
        ]


class DocReceptionFilter(django_filters.FilterSet):

    class Meta:
        model = DocReception
        fields = [
            'warehouse',
        ]


class DocDispatchFilter(django_filters.FilterSet):

    class Meta:
        model = DocDispatch
        fields = [
            'warehouse',
            'delivery_by',
            'received_by',
        ]
