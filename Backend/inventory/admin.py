# Django's Libraries
from django.contrib import admin

# Own's Libraries
from .models import DocShrinkage
from .models import DocShrinkageLine
from .models import DocDispatch
from .models import DocDispatchLine
from .models import DocInitialEntry
from .models import DocInitialEntryLine
from .models import DocAdjustment
from .models import DocAdjustmentLine
from .models import DocReception
from .models import DocReceptionLine


class DocAdjustmentLineInLine(admin.StackedInline):
    model = DocAdjustmentLine
    extra = 1


@admin.register(DocAdjustment)
class DocAdjustmentAdmin(admin.ModelAdmin):
    list_display = (
        'id',
        'type',
    )
    search_fields = (
        'id',
        'type',
    )
    inlines = [DocAdjustmentLineInLine, ]


class DocInitialEntryLineInLine(admin.StackedInline):
    model = DocInitialEntryLine
    extra = 1


@admin.register(DocInitialEntry)
class DocInitialEntryAdmin(admin.ModelAdmin):
    list_display = (
        'id',
        'warehouse',
        'entry_by',
    )
    search_fields = (
        'id',
    )
    inlines = [DocInitialEntryLineInLine, ]


class DocShrinkageLineInLine(admin.StackedInline):
    model = DocShrinkageLine
    extra = 1


@admin.register(DocShrinkage)
class DocShrinkageAdmin(admin.ModelAdmin):
    list_display = (
        'id',
        'warehouse',
        )
    search_fields = (
        'id',
        'warehouse',
    )
    inlines = [DocShrinkageLineInLine, ]


class DocDispatchLineInLine(admin.TabularInline):
    model = DocDispatchLine
    extra = 1


@admin.register(DocDispatch)
class DocDispatchAdmin(admin.ModelAdmin):
    list_display = (
        'id',
        'warehouse',
    )
    inlines = [
        DocDispatchLineInLine,
    ]


class DocReceptionLineInLine(admin.TabularInline):
    model = DocReceptionLine
    extra = 1


@admin.register(DocReception)
class DocReceptionAdmin(admin.ModelAdmin):
    list_display = (
        'id',
        'warehouse',
    )
    search_fields = (
        'id',
        'warehouse',
    )
    inlines = [
        DocReceptionLineInLine,
    ]
