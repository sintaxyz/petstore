# Python's Libraries
# Django's Libraries

# Third-party Libraries
from rest_framework import serializers
# import json

# Own's Libraries
from .models import DocDispatch
from .models import DocDispatchLine
from .models import DocInitialEntry
from .models import DocInitialEntryLine
from .models import DocAdjustment
from .models import DocAdjustmentLine
from .models import DocReception
from .models import DocReceptionLine
from .models import DocShrinkage
from .models import DocShrinkageLine


class DocAdjustmentLineSerializer(serializers.ModelSerializer):
    item_name = serializers.SerializerMethodField()

    class Meta:
        model = DocAdjustmentLine
        fields = [
            'id',
            'item',
            'item_name',
            'line',
            'qty_old',
            'qty_new',
            'comments',
        ]
        read_only_fields = [
            'id',
            'item_name',
        ]

    def get_item_name(self, obj):
        return obj.item.item.name


class DocAdjustmentSerializer(serializers.ModelSerializer):
    in_docadjustment = DocAdjustmentLineSerializer(
        required=False, many=True)
    company_name = serializers.SerializerMethodField()
    warehouse_name = serializers.SerializerMethodField()
    adjusted_by_name = serializers.SerializerMethodField()
    date_desc = serializers.SerializerMethodField()

    class Meta:
        model = DocAdjustment
        fields = [
            'id',
            'company_name',
            'warehouse',
            'warehouse_name',
            'adjusted_by',
            'adjusted_by_name',
            'date',
            'date_desc',
            'type',
            'description',
            'comments',
            'in_docadjustment',
        ]
        extra_kwargs = {
            'date': {'write_only': True},
        }
        read_only_fields = [
            'id',
            'company_name',
            'warehouse_name',
            'adjusted_by_name',
            'date_desc',
        ]

    def get_company_name(self, obj):
        return obj.warehouse.company.commercial_name

    def get_warehouse_name(self, obj):
        return obj.warehouse.name

    def get_adjusted_by_name(self, obj):
        return obj.adjusted_by.name

    def get_date_desc(self, obj):
        return obj.date.strftime("%d/%m/%Y %H:%M")


class DocAdjustmentUpdateDescSerializer(serializers.ModelSerializer):
    class Meta:
        model = DocAdjustment
        exclude = (
            'id',
            'warehouse',
            'adjusted_by',
            'date',
        )


class DocAdjustmentLineUpdateDescSerializer(serializers.ModelSerializer):
    class Meta:
        model = DocAdjustmentLine
        exclude = (
            'id',
            'item',
            'line',
            'qty_old',
        )
        read_only_fields = [
            'item',
        ]


class DocAdjustmentUpdateSerializer(serializers.ModelSerializer):
    company_name = serializers.SerializerMethodField()
    warehouse_name = serializers.SerializerMethodField()
    adjusted_by_name = serializers.SerializerMethodField()
    date_desc = serializers.SerializerMethodField()

    class Meta:
        model = DocAdjustment
        fields = [
            'id',
            'company_name',
            'warehouse',
            'warehouse_name',
            'adjusted_by',
            'adjusted_by_name',
            'date',
            'date_desc',
            'type',
            'description',
            'comments',
        ]
        extra_kwargs = {
            'date': {'write_only': True},
        }
        read_only_fields = [
            'id',
            'company_name',
            'warehouse_name',
            'adjusted_by_name',
            'date_desc',
        ]

    def get_company_name(self, obj):
        return obj.warehouse.company.commercial_name

    def get_warehouse_name(self, obj):
        return obj.warehouse.name

    def get_adjusted_by_name(self, obj):
        return obj.adjusted_by.name

    def get_date_desc(self, obj):
        return obj.date.strftime("%d/%m/%Y %H:%M")


class DocReceptionLineSerializer(serializers.ModelSerializer):
    item_name = serializers.SerializerMethodField()
    required = serializers.SerializerMethodField()
    received = serializers.SerializerMethodField()
    pending = serializers.SerializerMethodField()

    class Meta:
        model = DocReceptionLine
        fields = [
            'id',
            'item',
            'item_name',
            'line',
            'order_price',
            'order_line',
            'qty_received',
            'required',
            'received',
            'pending',
            'comments',
        ]
        read_only_fields = [
            'id',
            'item_name',        
            'required',
            'received',
            'pending',
            'item',
            'line',
            'order_price',
            'order_line',
        ]

    def get_item_name(self, obj):
        return obj.item.item.name

    def get_required(self, obj):
        return obj.order_line.qty

    def get_pending(self, obj):
        return obj.order_line.qty_pending

    def get_received(self, obj):
        required = obj.order_line.qty
        pending = obj.order_line.qty_pending
        received = required - pending
        return received


class DocReceptionSerializer(serializers.ModelSerializer):
    in_docreception = DocReceptionLineSerializer(
        required=False, many=True)
    company_name = serializers.SerializerMethodField()
    warehouse_name = serializers.SerializerMethodField()
    supplier_name = serializers.SerializerMethodField()
    received_by_name = serializers.SerializerMethodField()
    date_desc = serializers.SerializerMethodField()
    order_desc = serializers.SerializerMethodField()
    order_date_desc = serializers.SerializerMethodField()

    class Meta:
        model = DocReception
        fields = [
            'id',
            'company_name',
            'warehouse',
            'warehouse_name',
            'order',
            'order_desc',
            'order_date_desc',
            'date',
            'date_desc',
            'number',
            # 'supplier',
            'supplier_name',
            'received_by',
            'received_by_name',
            'description',
            'comments',
            'in_docreception',
        ]
        extra_kwargs = {
            'date': {'write_only': True},
        }
        read_only_fields = [
            'id',
            'number',
            'company_name',
            'warehouse_name',
            'supplier_name',
            'received_by_name',
            'order_desc',
            'order_date_desc',
            'date_desc',
        ]

    def get_company_name(self, obj):
        return obj.order.warehouse.company.commercial_name

    def get_warehouse_name(self, obj):
        return obj.order.warehouse.name

    def get_supplier_name(self, obj):
        return obj.order.supplier.commercial_name

    def get_received_by_name(self, obj):
        return obj.received_by.name

    def get_order_desc(self, obj):
        return obj.order.description

    def get_order_date_desc(self, obj):
        return obj.order.date.strftime("%d/%m/%Y %H:%M")

    def get_date_desc(self, obj):
        return obj.date.strftime("%d/%m/%Y %H:%M")


class DocReceptionUpdateDescSerializer(serializers.ModelSerializer):
    class Meta:
        model = DocReception
        exclude = (
            'id',
            'status',
            'warehouse',
            'order',
            'number',
            'received_by',
            'date',
        )


class DocReceptionLineUpdateDescSerializer(serializers.ModelSerializer):
    class Meta:
        model = DocReceptionLine
        exclude = (
            'id',
            'item',
            'line',
            'order_price',
            'order_line',
        )
        read_only_fields = [
            'item',
        ]


class DocReceptionUpdateSerializer(serializers.ModelSerializer):
    company_name = serializers.SerializerMethodField()
    warehouse_name = serializers.SerializerMethodField()
    supplier_name = serializers.SerializerMethodField()
    received_by_name = serializers.SerializerMethodField()
    date_desc = serializers.SerializerMethodField()
    order_desc = serializers.SerializerMethodField()
    order_date_desc = serializers.SerializerMethodField()

    class Meta:
        model = DocReception
        fields = [
            'id',
            'company_name',
            'warehouse',
            'warehouse_name',
            'order',
            'order_desc',
            'order_date_desc',
            'date',
            'date_desc',
            'number',
            'supplier',
            'supplier_name',
            'received_by',
            'received_by_name',
            'description',
            'comments',
        ]
        extra_kwargs = {
            'date': {'write_only': True},
        }
        read_only_fields = [
            'id',
            'company_name',
            'warehouse_name',
            'supplier_name',
            'received_by_name',
            'order_desc',
            'order_date_desc',
            'date_desc',
        ]

    def get_company_name(self, obj):
        return obj.warehouse.company.commercial_name

    def get_warehouse_name(self, obj):
        return obj.warehouse.name

    def get_supplier_name(self, obj):
        return obj.supplier.commercial_name

    def get_received_by_name(self, obj):
        return obj.received_by.name

    def get_order_desc(self, obj):
        return obj.order.description

    def get_order_date_desc(self, obj):
        return obj.order.date.strftime("%d/%m/%Y %H:%M")

    def get_date_desc(self, obj):
        return obj.date.strftime("%d/%m/%Y %H:%M")


class DocDispatchLineSerializer(serializers.ModelSerializer):
    item_name = serializers.SerializerMethodField()

    class Meta:
        model = DocDispatchLine
        fields = [
            'id',
            'item',
            'item_name',
            'line',
            'qty',
            'price',
            'comments',
        ]
        read_only_fields = [
            'id',
            'item_name',
            'price',
        ]

    def get_item_name(self, obj):
        return obj.item.item.name


class DocDispatchSerializer(serializers.ModelSerializer):
    in_doc = DocDispatchLineSerializer(
        required=False, many=True)
    company_name = serializers.SerializerMethodField()
    warehouse_name = serializers.SerializerMethodField()
    delivery_by_name = serializers.SerializerMethodField()
    received_by_name = serializers.SerializerMethodField()
    date_desc = serializers.SerializerMethodField()

    class Meta:
        model = DocDispatch
        fields = [
            'id',
            'company_name',
            'warehouse',
            'warehouse_name',
            'delivery_by',
            'delivery_by_name',
            'received_by',
            'received_by_name',
            'date',
            'date_desc',
            'description',
            'comments',
            'in_doc',
        ]
        extra_kwargs = {
            'date': {'write_only': True},
        }
        read_only_fields = [
            'id',
            'company_name',
            'warehouse_name',
            'delivery_by_name',
            'received_by_name',
            'date_desc',
        ]

    def get_company_name(self, obj):
        return obj.warehouse.company.commercial_name

    def get_warehouse_name(self, obj):
        return obj.warehouse.name

    def get_delivery_by_name(self, obj):
        return obj.delivery_by.name

    def get_received_by_name(self, obj):
        return obj.received_by.name

    def get_date_desc(self, obj):
        return obj.date.strftime("%d/%m/%Y %H:%M")


class DocDispatchUpdateDescSerializer(serializers.ModelSerializer):
    class Meta:
        model = DocDispatch
        exclude = (
            'id',
            'warehouse',
            'delivery_by',
            'date',
        )


class DocDispatchLineUpdateDescSerializer(serializers.ModelSerializer):    
    class Meta:
        model = DocDispatchLine
        exclude = (
            'id',
            'item',
            'line',
            'price',
        )
        read_only_fields = [
            'item',
        ]


class DocDispatchUpdateSerializer(serializers.ModelSerializer):
    company_name = serializers.SerializerMethodField()
    warehouse_name = serializers.SerializerMethodField()
    delivery_by_name = serializers.SerializerMethodField()
    received_by_name = serializers.SerializerMethodField()
    date_desc = serializers.SerializerMethodField()

    class Meta:
        model = DocDispatch
        fields = [
            'id',
            'company_name',
            'warehouse',
            'warehouse_name',
            'delivery_by',
            'delivery_by_name',
            'received_by',
            'received_by_name',
            'date',
            'date_desc',
            'description',
            'comments',
        ]
        extra_kwargs = {
            'date': {'write_only': True},
        }
        read_only_fields = [
            'id',
            'company_name',
            'warehouse_name',
            'delivery_by_name',
            'received_by_name',
            'date_desc',
        ]

    def get_company_name(self, obj):
        return obj.warehouse.company.commercial_name

    def get_warehouse_name(self, obj):
        return obj.warehouse.name

    def get_delivery_by_name(self, obj):
        return obj.delivery_by.name

    def get_received_by_name(self, obj):
        return obj.received_by.name

    def get_date_desc(self, obj):
        return obj.date.strftime("%d/%m/%Y %H:%M")


class DocInitialEntryLineSerializer(serializers.ModelSerializer):
    item_name = serializers.SerializerMethodField()

    class Meta:
        model = DocInitialEntryLine
        fields = [
            'id',
            'item',
            'item_name',
            'line',
            'comments',
            'qty',
            'minimun_qty',
            'price',
            'sell_price',
            'total_amount',
            'total_sell_price',
        ]
        read_only_fields = [
            'id',
            'item_name',
            'line',
        ]

    def get_item_name(self, obj):
        return obj.item.name


class DocInitialEntrySerializer(serializers.ModelSerializer):
    has_entrylines = DocInitialEntryLineSerializer(
        required=False, many=True)
    company_name = serializers.SerializerMethodField()
    warehouse_name = serializers.SerializerMethodField()
    entry_by_name = serializers.SerializerMethodField()
    date_desc = serializers.SerializerMethodField()

    class Meta:
        model = DocInitialEntry
        fields = [
            'id',
            'company_name',
            'warehouse',
            'warehouse_name',
            'entry_by',
            'entry_by_name',
            'date',
            'date_desc',
            'description',
            'comments',
            'qty_items',
            'total_amount',
            'has_entrylines',
        ]
        extra_kwargs = {
            'date': {'write_only': True},
        }
        read_only_fields = [
            'id',
            'company_name',
            'warehouse_name',
            'entry_by_name',
            'qty_items',
            'total_amount',
            'date_desc',
        ]

    def get_company_name(self, obj):
        return obj.warehouse.company.commercial_name

    def get_warehouse_name(self, obj):
        return obj.warehouse.name

    def get_entry_by_name(self, obj):
        return obj.entry_by.name

    def get_date_desc(self, obj):
        return obj.date.strftime("%d/%m/%Y %H:%M") if obj.date else None


class DocInitialEntryUpdateSerializer(serializers.ModelSerializer):
    company_name = serializers.SerializerMethodField()
    warehouse_name = serializers.SerializerMethodField()
    entry_by_name = serializers.SerializerMethodField()
    date_desc = serializers.SerializerMethodField()

    class Meta:
        model = DocInitialEntry
        fields = [
            'id',
            'company_name',
            'warehouse',
            'warehouse_name',
            'entry_by',
            'entry_by_name',
            'date',
            'date_desc',
            'description',
            'comments',
            'qty_items',
            'total_amount',
        ]
        extra_kwargs = {
            'date': {'write_only': True},
        }
        read_only_fields = [
            'id',
            'company_name',
            'warehouse_name',
            'entry_by_name',
            'qty_items',
            'total_amount',
            'date_desc',
        ]

    def get_company_name(self, obj):
        return obj.warehouse.company.commercial_name

    def get_warehouse_name(self, obj):
        return obj.warehouse.name

    def get_entry_by_name(self, obj):
        return obj.entry_by.name

    def get_date_desc(self, obj):
        return obj.date.strftime("%d/%m/%Y %H:%M")


class DocInitialEntryUpdateDescSerializer(serializers.ModelSerializer):
    class Meta:
        model = DocInitialEntry
        exclude = (
            'id',
            'warehouse',
            'qty_items',
            'entry_by',
            'total_amount',
            'date',
        )


class DocShrinkageLineSerializer(serializers.ModelSerializer):
    item_name = serializers.SerializerMethodField()

    class Meta:
        model = DocShrinkageLine
        fields = [
            'id',
            'item',
            'item_name',
            'qty',
            'line',
            'comments',
        ]
        read_only_fields = [
            'id',
            'item_name',
        ]

    def get_item_name(self, obj):
        return obj.item.item.name


class DocShrinkageSerializer(serializers.ModelSerializer):
    has_mermaline = DocShrinkageLineSerializer(
        required=False, many=True)
    company_name = serializers.SerializerMethodField()
    warehouse_name = serializers.SerializerMethodField()
    register_by_name = serializers.SerializerMethodField()
    date_desc = serializers.SerializerMethodField()
    type_desc = serializers.SerializerMethodField()

    class Meta:
        model = DocShrinkage
        fields = [
            'id',
            'company_name',
            'warehouse',
            'warehouse_name',
            'register_by',
            'register_by_name',
            'date',
            'date_desc',
            'description',
            'comments',
            'type',
            'type_desc',
            'has_mermaline',
        ]
        extra_kwargs = {
            'date': {'write_only': True},
            'type': {'write_only': True},
        }
        read_only_fields = [
            'id',
            'company_name',
            'warehouse_name',
            'register_by_name',
            'type_desc',
            'date_desc',
        ]

    def get_company_name(self, obj):
        return obj.warehouse.company.commercial_name

    def get_warehouse_name(self, obj):
        return obj.warehouse.name

    def get_register_by_name(self, obj):
        return obj.register_by.name

    def get_date_desc(self, obj):
        return obj.date.strftime("%d/%m/%Y %H:%M")

    def get_type_desc(self, obj):
        return obj.get_type_display()


class DocShrinkageUpdateDescSerializer(serializers.ModelSerializer):
    class Meta:
        model = DocShrinkage
        exclude = (
            'id',
            'warehouse',
            'register_by',
            'date',
        )


class DocShrinkageLineUpdateDescSerializer(serializers.ModelSerializer):
    class Meta:
        model = DocShrinkageLine
        exclude = (
            'id',
            'item',
            'line',
        )
        read_only_fields = [
            'item',
        ]


class DocShrinkageUpdateSerializer(serializers.ModelSerializer):
    company_name = serializers.SerializerMethodField()
    warehouse_name = serializers.SerializerMethodField()
    register_by_name = serializers.SerializerMethodField()
    date_desc = serializers.SerializerMethodField()
    type_desc = serializers.SerializerMethodField()

    class Meta:
        model = DocShrinkage
        fields = [
            'id',
            'company_name',
            'warehouse',
            'warehouse_name',
            'register_by',
            'register_by_name',
            'date',
            'date_desc',
            'description',
            'comments',
            'type',
            'type_desc',
        ]
        extra_kwargs = {
            'date': {'write_only': True},
            'type': {'write_only': True},
        }
        read_only_fields = [
            'id',
            'company_name',
            'warehouse_name',
            'register_by_name',
            'type_desc',
            'date_desc',
        ]

    def get_company_name(self, obj):
        return obj.warehouse.company.commercial_name

    def get_warehouse_name(self, obj):
        return obj.warehouse.name

    def get_register_by_name(self, obj):
        return obj.register_by.name

    def get_date_desc(self, obj):
        return obj.date.strftime("%d/%m/%Y %H:%M")

    def get_type_desc(self, obj):
        return obj.get_type_display()
