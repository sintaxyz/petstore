# Python's Libraries
from decimal import Decimal

# Django's Libraries
from django.db import transaction
from django.urls import reverse_lazy
from django.db.models import Avg, Count, Min, Sum

# Own's Libraries
from tools.business import StxBsn

from core.models import StockItem
from .models import DocShrinkage
from .models import DocShrinkageLine
from .models import DocAdjustment
from .models import DocAdjustmentLine
from .models import DocDispatch
from .models import DocDispatchLine
from .models import DocInitialEntry
from .models import DocInitialEntryLine
from .models import DocReception
from .models import DocReceptionLine

from .filters import DocShrinkageFilter
from .filters import DocDispatchFilter
from .filters import DocInitialEntryFilter
from .filters import DocReceptionFilter
from .filters import DocAdjustmentFilter

from .resources import DocReceptionResource
from .resources import DocInitialEntryResource
from core.business import StockItemBsn
from procurement.business import PurchaseOrderBsn


class DocAdjustmentBsn(StxBsn):
    model = DocAdjustment
    model_filter = DocAdjustmentFilter

    @classmethod
    def get_Assigned(self, user):
        return self.get_All().filter(warehouse_company__in=user.companies.all())

    @classmethod
    def filter_ByPermission(self, records, user):
        return records.filter(warehouse__company__in=user.companies.all())

    def update_Desc(self, _record, _data, _user):
        with transaction.atomic():
            _record.description = _data.get('description')
            _record.comments = _data.get('comments')
            _record.type = _data.get('type')
            _record.updated_by = _user
            _record.full_clean()
            _record.save()
            return _record

    @classmethod
    def update_AdjustmentLineDesc(self, _record, _data, _user, _files=None):
        with transaction.atomic():
            _record.qty_new = _data.get('qty_new')
            _record.comments = _data.get('comments')
            _record.updated_by = _user
            _record.full_clean()
            _record.save()
            return _record

    @classmethod
    def affect_Stock(self, _record, _user):
        with transaction.atomic():
            for line in _record.in_docadjustment.all():
                stock = StockItemBsn.get_ItemStock(
                    line.item, _record.warehouse
                    )
                stock.qty = line.qty_new
                stock.updated_by = _user
                stock.save()

    @classmethod
    def finish(self, _record, _user):
        with transaction.atomic():
            self.affect_Stock(_record, _user)
            _record.updated_by = _user
            _record.full_clean()
            _record.save()
            return _record

    @classmethod
    def get_Lines(self, pk, user):
        records = self.get_ModelObject().objects.get(
            id=pk).in_docadjustment.all()
        return records

    @classmethod
    def get_AdjustmentList(self, pk):
        records = self.get_ModelObject().objects.all()
        return records

    @classmethod
    def create(self, _data, _user, _files=None):
        with transaction.atomic():
            record = self.get_ModelObject()()
            record.warehouse = _data.get('warehouse')
            record.description = _data.get('description')
            record.comments = _data.get('comments')
            record.date = _data.get('date')
            record.type = _data.get('type')
            record.adjusted_by = _data.get('adjusted_by')
            record.created_by = _user
            record.full_clean()
            record.save()
            return record

    @classmethod
    def create_AdjustmentLine(self, _record, _data, _user, _files=None):
        with transaction.atomic():
            record = DocAdjustmentLine()
            record.document = _record
            record.item = _data.get('item')
            record.qty_new = _data.get('qty_new')
            qty_stock = StockItemBsn.get_ItemStock(
                record.item, record.document.warehouse
                )
            if qty_stock:
                record.qty_old = qty_stock.qty
            else:
                raise NameError('No se encontro el articulo.')
            record.comments = _data.get('comments')
            record.created_by = _user
            record.full_clean()
            record.save()
            record.line = "AJ{}LN{}".format(
                str(record.document.id), str(record.id)
                )
            record.save()
            return record

    @classmethod
    def update_Adjustment(self, _record, _data, _user, _files=None):
        with transaction.atomic():
            _record.warehouse = _data.get('warehouse')
            _record.description = _data.get('description')
            _record.comments = _data.get('comments')
            _record.adjusted_by = _data.get('adjusted_by')
            _record.date = _data.get('date')
            _record.type = _data.get('type')
            _record.updated_by = _user
            _record.full_clean()
            _record.save()
            return _record

    @classmethod
    def update_AdjustmentLine(self, _record, _data, _user, _files=None):
        with transaction.atomic():
            if _record.item == _data.get('item'):
                qty_stock = StockItemBsn.get_ItemStock(
                    _record.item, _record.document.warehouse
                    )
                if qty_stock:
                    _record.qty_old = qty_stock.qty
                    qty_stock.qty = _record.qty_new
                    qty_stock.updated_by = _user
                    qty_stock.save()
                else:
                    raise NameError('No se encontro el articulo.')
            else:
                qty_stock = StockItemBsn.get_ItemStock(
                    _record.item, _record.document.warehouse
                    )
                if qty_stock:
                    qty_stock.qty = _record.qty_old
                    qty_stock.updated_by = _user
                    qty_stock.save()
                else:
                    raise NameError('No se encontro el articulo.')
                qty_stock = StockItemBsn.get_ItemStock(
                    _data.get('item'), _record.document.warehouse
                    )
                if qty_stock:
                    _record.qty_old = qty_stock.qty
                    qty_stock.qty = _data.get('qty_new')
                    qty_stock.updated_by = _user
                    qty_stock.save()
                else:
                    raise NameError('No se encontro el articulo.')

            _record.item = _data.get('item')
            _record.qty_new = _data.get('qty_new')
            _record.line = _data.get('line')
            _record.comments = _data.get('comments')
            _record.updated_by = _user
            _record.full_clean()
            _record.save()
            return _record

    @classmethod
    def get_Adjustment(self, pk):
        record = self.get_ModelObject().objects.get(id=pk)
        if record:
            return record
        else:
            raise ValueError('No existe este ajuste de articulos')

    @classmethod
    def get_AdjustmentLine(self, pk, line_pk):
        adjustment = self.get_ModelObject().objects.get(id=pk)
        if adjustment:
            record = adjustment.in_docadjustment.get(id=line_pk)
            return record
        else:
            raise ValueError('No existe esta linea de artículo.')

    @classmethod
    def search(self, records, _value):
        try:
            _value = int(_value)
        except Exception:
            _value

        if type(_value) is str:
            records = records.filter(warehouse_name__icontains=_value)
        elif type(_value) is int:
            records = records.filter(id=_value)
        return records


class DocInitialEntryBsn(StxBsn):
    model = DocInitialEntry
    model_filter = DocInitialEntryFilter

    @classmethod
    def filter_ByPermission(self, records, user):
        return records.filter(warehouse__company__in=user.companies.all())

    @classmethod
    def get_EntryList(self, user):
        records = self.get_ModelObject().objects.all()
        return records

    @classmethod
    def export_XLSX(self, records):
        return DocInitialEntryResource().export(records).xlsx

    @classmethod
    def create(self, _data, _user, _files=None):
        with transaction.atomic():
            if _data.get('warehouse'):
                company = _data.get('warehouse').company.id
            else:
                raise ValueError('Es necesario asignar un almacen.')
            approved = self.validate_Access(_user, 'CRCI', company)
            if approved is False:
                raise ValueError('No tiene permiso para crear carga inicial')
            record = self.get_ModelObject()()
            record.warehouse = _data.get('warehouse')
            record.entry_by = _user
            record.description = _data.get('description')
            record.comments = _data.get('comments')
            record.date = _data.get('date')
            record.created_by = _user
            record.full_clean()
            record.save()

            self.set_TotalItemsValues(record, _user, True)

            return record

    @classmethod
    def update_Entry(self, _record, _data, _user, _files=None):
        with transaction.atomic():
            _record.warehouse = _data.get('warehouse')
            _record.entry_by = _user
            _record.description = _data.get('description')
            _record.comments = _data.get('comments')
            _record.date = _data.get('date')
            _record.updated_by = _user
            _record.full_clean()
            _record.save()
            return _record

    @classmethod
    def update_Desc(self, _record, _data, _user, _files=None):
        with transaction.atomic():
            company = _record.warehouse.company.id
            approved = self.validate_Access(_user, 'EDCI', company)
            if approved is False:
                raise ValueError('No tiene permiso para editar carga inicial')
            if _record.status != 'INP':
                raise PermissionError(
                    'No se puede modificar este registro.')
            _record.description = _data.get('description')
            _record.comments = _data.get('comments')
            _record.updated_by = _user
            _record.full_clean()
            _record.save()
            return _record

    @classmethod
    def affect_Stock(self, _record, _user):
        with transaction.atomic():
            if _record.status != 'INP':
                raise PermissionError(
                    'No se puede modificar este registro.')
            for line in _record.has_entrylines.all():
                StockItem.objects.create(
                    warehouse=line.document.warehouse,
                    item=line.item,
                    qty=line.qty,
                    minimun_qty=line.minimun_qty,
                    price=line.price,
                    sell_price=line.sell_price
                )

    @classmethod
    def finish(self, _record, _user):
        with transaction.atomic():
            if _record.warehouse:
                company = _record.warehouse.company.id
            else:
                raise PermissionError('Es necesario asignar un almacen.')
            approved = self.validate_Access(_user, ['CRCI', 'EDCI'], company)
            if approved is False:
                raise PermissionError('No tiene permiso para finalizar carga inicial')
            if _record.status != 'INP':
                raise PermissionError(
                    'No se puede modificar este registro.')
            self.affect_Stock(_record, _user)
            _record.status = 'APR'
            _record.updated_by = _user
            _record.full_clean()
            _record.save()
            return _record

    @classmethod
    def cancel(self, _record, _user):
        with transaction.atomic():
            if _record.warehouse:
                company = _record.warehouse.company.id
            else:
                raise ValueError('Es necesario asignar un almacen.')
            approved = self.validate_Access(_user, 'CACI', company)
            if approved is False:
                raise ValueError('No tiene permiso para crear carga inicial')
            if _record.status == 'INP'\
                or _record.status == 'PEN':
                _record.status = 'CAN'
                _record.updated_by = _user
                _record.full_clean()
                _record.save()
                return _record
            else:
                raise PermissionError(
                    'No se puede modificar este registro.')

    @classmethod
    def create_EntryLine(self, _record, _data, _user, _files=None):
        with transaction.atomic():
            if _record.warehouse:
                company = _record.warehouse.company.id
            else:
                raise ValueError('Es necesario asignar un almacen.')
            approved = self.validate_Access(_user, ['CRCI', 'EDCI'], company)
            if approved is False:
                raise ValueError('No tiene permiso para crear o editar carga inicial')
            if _record.status != 'INP':
                raise PermissionError(
                    'No se puede modificar este registro.')
            record = DocInitialEntryLine()
            record.document = _record
            record.item = _data.get('item')
            record.comments = _data.get('comments')
            record.qty = _data.get('qty')
            record.minimun_qty = _data.get('minimun_qty')
            record.price = _data.get('price')
            record.sell_price = _data.get('sell_price')
            record.total_amount = Decimal(format(
                record.price * record.qty, '.2f'))
            record.total_sell_price = Decimal(format(
                record.sell_price * record.qty, '.2f'))
            record.created_by = _user
            record.full_clean()
            record.save()
            record.line = "CI{}LN{}".format(
                str(record.document.id), str(record.id)
                )
            record.save()
            stock = StockItemBsn.get_ItemStock(
                record.item, record.document.warehouse
                )
            if not stock:
                self.set_TotalItemsValues(record.document, _user)
                return record
            else:
                raise ValueError('Este articulo ya cuenta con Stock en este almacen.')


    @classmethod
    def update_EntryLine(self, _record, _data, _user, _files=None):
        with transaction.atomic():
            if _record.status != 'INP':
                raise PermissionError(
                    'No se puede modificar este registro.')
            _record.comments = _data.get('comments')
            _record.qty = _data.get('qty')
            _record.minimun_qty = _data.get('minimun_qty')
            _record.price = _data.get('price')
            _record.sell_price = _data.get('sell_price')
            _record.total_amount = Decimal(format(
                _record.price * _record.qty, '.2f'))
            _record.total_sell_price = Decimal(format(
                _record.sell_price * _record.qty, '.2f'))
            _record.updated_by = _user
            _record.full_clean()
            _record.save()

            _stock = StockItemBsn.get_ItemStock(
                _record.item, _record.document.warehouse
                )
            if _stock:
                _stock.qty = _record.qty
                _stock.price = _record.price
                _stock.sell_price = _record.sell_price
                _stock.updated_by = _user
                _stock.save()
            else:
                raise NameError('No se encontro el articulo.')

            self.set_TotalItemsValues(_record.document, _user)
            return _record

    @classmethod
    def set_TotalItemsValues(self, _record, _user, is_created=False):
        _record.qty_items = sum(
                [i.qty for i in _record.has_entrylines.all()]
            )
        _record.total_amount = sum(
                [i.total_amount for i in _record.has_entrylines.all()]
            )

        if is_created:
            pass
        else:
            _record.updated_by = _user
        _record.full_clean()
        _record.save()
        return _record

    @classmethod
    def get_Entry(self, pk):
        record = self.get_ModelObject().objects.get(id=pk)
        if record:
            return record
        else:
            raise ValueError('No existe esta carga inicial.')

    @classmethod
    def get_Lines(self, pk, user):
        records = self.get_ModelObject().objects.get(
            id=pk).has_entrylines.all()
        return records

    @classmethod
    def get_EntryLine(self, pk, line_pk):
        entry = self.get_ModelObject().objects.get(id=pk)
        if entry:
            record = entry.has_entrylines.get(id=line_pk)
            return record
        else:
            raise ValueError('No existe esta linea de artículo.')

    @classmethod
    def search(self, records, _value):
        try:
            _value = int(_value)
        except Exception:
            _value

        if type(_value) is str:
            records = records.filter(warehouse_name__icontains=_value)
        elif type(_value) is int:
            records = records.filter(id=_value)
        return records


class DocShrinkageBsn(StxBsn):
    model = DocShrinkage
    model_filter = DocShrinkageFilter

    @classmethod
    def get_Assigned(self, user):
        return self.get_All().filter(warehouse_company__in=user.companies.all())

    @classmethod
    def filter_ByPermission(self, records, user):
        return records.filter(warehouse__company__in=user.companies.all())

    @classmethod
    def update_Desc(self, _record, _data, _user, _files=None):
        with transaction.atomic():
            _record.qty = _data.get('qty')
            _record.comments = _data.get('comments')
            _record.updated_by = _user
            _record.full_clean()
            _record.save()
            return _record

    @classmethod
    def get_Lines(self, pk, user):
        records = self.get_ModelObject().objects.get(
            id=pk).has_mermaline.all()
        return records

    @classmethod
    def update_ShrinkageLineDesc(self, _record, _data, _user, _files=None):
        with transaction.atomic():
            _record.qty = _data.get('qty')
            _record.comments = _data.get('comments')
            _record.updated_by = _user
            _record.full_clean()
            _record.save()
            return _record

    @classmethod
    def update_StockQty(self, _data, _record, _user):
        qty_stock = StockItemBsn.get_ItemStock(
            _data.item, _record
            )
        if qty_stock:
            qty_temp = qty_stock.qty - _data.qty
            qty_stock.qty = qty_temp
            qty_stock.updated_by = _user
            qty_stock.save()
        return qty_stock

    @classmethod
    def affect_Stock(self, _record, _user):
        with transaction.atomic():
            for line in _record.has_mermaline.all():
                stock = StockItemBsn.get_ItemStock(
                    line.item, _record.warehouse
                )
                temp = stock.qty - line.qty
                stock.qty = temp
                stock.updated_by = _user
                stock.save()

    @classmethod
    def finish(self, _record, _user):
        with transaction.atomic():
            self.affect_Stock(_record, _user)
            _record.updated_by = _user
            _record.full_clean()
            _record.save()
            return _record

    @classmethod
    def get_ShrinkageList(self, pk):
        records = self.get_ModelObject().objects.all()
        return records

    @classmethod
    def create(self, _data, _user, _files=None):
        with transaction.atomic():
            record = self.get_ModelObject()()
            record.warehouse = _data.get('warehouse')
            record.description = _data.get('description')
            record.comments = _data.get('comments')
            record.date = _data.get('date')
            record.type = _data.get('type')
            record.register_by = _data.get('register_by')
            record.created_by = _user
            record.full_clean()
            record.save()
            # if _data.get('has_mermaline', None):
            #     for item in _data.get('has_mermaline'):
            #         product = item.get('item')
            #         qty = item.get('qty')
            #         comments = item.get('comments')
            #         line = item.get('line')
            #         lines = DocShrinkageLine.objects.create(
            #             item=product,
            #             comments=comments,
            #             line=line,
            #             qty=qty,
            #             document=record,
            #         )
            #         self.update_StockQty(
            #             lines, record.warehouse, _user
            #             )
            # else:
            #     raise NameError('No se puede crear Pedido sin articulos.')
            # record.save()
            return record

    @classmethod
    def create_ShrinkageLine(self, _record, _data, _user, _files=None):
        with transaction.atomic():
            record = DocShrinkageLine()
            record.document = _record
            record.item = _data.get('item')
            record.qty = _data.get('qty')
            record.comments = _data.get('comments')
            record.created_by = _user
            record.full_clean()
            record.save()
            record.line = "ME{}LN{}".format(
                str(record.document.id), str(record.id)
                )
            record.save()
            return record

    @classmethod
    def update_Shrinkage(self, _record, _data, _user, _files=None):
        with transaction.atomic():
            _record.description = _data.get('description')
            _record.comments = _data.get('comments')
            _record.type = _data.get('type')
            _record.updated_by = _user
            _record.full_clean()
            _record.save()
            return _record

    @classmethod
    def update_ShrinkageLine(self, _record, _data, _user, _files=None):
        with transaction.atomic():
            if _record.item == _data.get('item'):
                qty_stock = StockItemBsn.get_ItemStock(
                    _record.item, _record.document.warehouse
                    )
                if qty_stock:
                    qty_temp = qty_stock.qty - _data.get('qty')
                    qty_stock.qty = qty_temp
                    qty_stock.updated_by = _user
                    qty_stock.save()
                else:
                    raise NameError('No se encontro el articulo.')
            else:
                qty_stock = StockItemBsn.get_ItemStock(
                    _record.item, _record.document.warehouse
                    )
                if qty_stock:
                    qty_temp = qty_stock.qty + _record.qty
                    qty_stock.qty = qty_temp
                    qty_stock.updated_by = _user
                    qty_stock.save()
                else:
                    raise NameError('No se encontro el articulo.')
                qty_stock = StockItemBsn.get_ItemStock(
                    _data.get('item'), _record.document.warehouse
                    )
                if qty_stock:
                    qty_temp = qty_stock.qty - _data.get('qty')
                    qty_stock.qty = qty_temp
                    qty_stock.updated_by = _user
                    qty_stock.save()
                else:
                    raise NameError('No se encontro el articulo.')

            _record.item = _data.get('item')
            _record.qty = _data.get('qty')
            _record.line = _data.get('line')
            _record.comments = _data.get('comments')
            _record.updated_by = _user
            _record.full_clean()
            _record.save()
            return _record

    @classmethod
    def get_Shrinkage(self, pk):
        record = self.get_ModelObject().objects.get(id=pk)
        if record:
            return record
        else:
            raise ValueError('No existe este ajuste de articulos')

    @classmethod
    def get_ShrinkageLine(self, pk, line_pk):
        shrinkage = self.get_ModelObject().objects.get(id=pk)
        if shrinkage:
            record = shrinkage.has_mermaline.get(id=line_pk)
            return record
        else:
            raise ValueError('No existe esta linea de artículo.')

    @classmethod
    def search(self, records, _value):
        try:
            _value = int(_value)
        except Exception:
            _value

        if type(_value) is str:
            records = records.filter(warehouse_name__icontains=_value)
        elif type(_value) is int:
            records = records.filter(id=_value)
        return records


class DocReceptionBsn(StxBsn):
    model = DocReception
    model_filter = DocReceptionFilter

    @classmethod
    def get_Assigned(self, user):
        return self.get_All().filter(warehouse_company__in=user.companies.all())

    @classmethod
    def filter_ByPermission(self, records, user):
        return records.filter(warehouse__company__in=user.companies.all())

    @classmethod
    def export_XLSX(self, records):
        return DocReceptionResource().export(records).xlsx

    @classmethod
    def update_Desc(self, _record, _data, _user, _files=None):
        with transaction.atomic():
            company = _record.warehouse.company.id
            approved = self.validate_Access(_user, ['CRRP', 'EDRP'], company)
            if approved is False:
                raise ValueError('No tiene permiso para editar recepciones.')
            if _record.status != 'INP':
                raise PermissionError(
                    'No se puede modificar este registro.')
            _record.description = _data.get('description')
            _record.comments = _data.get('comments')
            _record.updated_by = _user
            _record.full_clean()
            _record.save()
            return _record

    @classmethod
    def affect_Stock(self, _record, _user):
        with transaction.atomic():
            if _record.status == 'INP' or \
                _record.status == 'PEN':
                raise PermissionError(
                    'No se puede modificar este registro.')

            order_supplied = True

            for line in _record.in_docreception.all():
                order_line = line.order_line
                stock = line.item
                stock.qty += line.qty_received
                stock.save()
                order_line.qty_pending -= line.qty_received
                order_line.save()
                if order_line.qty_pending > 0:
                    order_supplied = False

            if order_supplied:
                order = _record.order
                order.full_supplied = True
                order.save()

    @classmethod
    def finish(self, _record, _user):
        with transaction.atomic():
            company = _record.warehouse.company.id
            approved = self.validate_Access(_user, ['CRRP', 'EDRP'], company)
            if approved is False:
                raise PermissionError('No tiene permiso para terminar recepcion.')
            if _record.status == 'INP' or \
                _record.status == 'PEN':
                _record.status = 'APR'
                _record.updated_by = _user
                self.affect_Stock(_record, _user)
                _record.full_clean()
                _record.save()
                return _record
            else:
                raise PermissionError(
                    'No se puede modificar este registro.')

    @classmethod
    def cancel(self, _record, _user):
        with transaction.atomic():
            company = _record.warehouse.company.id
            approved = self.validate_Access(_user, 'CARP', company)
            if approved is False:
                raise PermissionError('No tiene permiso para cacelar recepcion.')
            if _record.status == 'INP' or \
                _record.status == 'PEN':
                _record.status = 'CAN'
                _record.updated_by = _user
                _record.full_clean()
                _record.save()
                return _record
            else:
                raise PermissionError(
                    'No se puede modificar este registro.')

    @classmethod
    def get_Lines(self, pk, user):
        records = self.get_ModelObject().objects.get(
            id=pk).in_docreception.all()
        return records

    @classmethod
    def get_ReceptionList(self, user):
        records = self.get_ModelObject().objects.all()
        return records

    @classmethod
    def create(self, _data, _user, _files=None):
        with transaction.atomic():
            if _data.get('order'):
                company = _data.get('order').warehouse.company.id
            else:
                raise ValueError('Es necesario asignar una orden de compra.')
            approved = self.validate_Access(_user, 'CRRP', company)
            if approved is False:
                raise ValueError('No tiene permiso para crear recepciones.')
            record = self.get_ModelObject()()
            record.warehouse = _data.get('order').warehouse
            record.date = _data.get('date')
            record.order = _data.get('order')
            record.received_by = _user
            record.description = _data.get('description')
            record.comments = _data.get('comments')
            record.created_by = _user
            record.full_clean()
            record.save()
            record.number = "OC{}RE{}".format(
                str(record.order.id), str(record.id)
            )
            record.save()
            lines = 0
            for order_line in record.order.order_line.all():
                if order_line.qty_pending > 0:
                    line = DocReceptionLine.objects.create(
                        document=record,
                        item=order_line.item,
                        order_line=order_line,
                        order_price=order_line.price,
                        qty_received=order_line.qty_pending,
                        created_by=_user
                    )
                    line.line = "RE{}LN{}".format(
                    str(line.document.id), str(line.id)
                    )
                    line.save()
                    lines += 1
            if lines > 0:
                return record
            else:
                raise ValueError(
                    'La Orden de Compra no tiene lineas pendientes.')

    @classmethod
    def create_ReceptionLine(self, _record, _data, _user, _files=None):
        with transaction.atomic():
            record = DocReceptionLine()
            record.document = _record
            item_order = PurchaseOrderBsn.get_Item(
                record.document.order, _data.get('item')
                )
            if item_order:
                record.order_line = item_order.line
                record.order_price = item_order.price
                record.qty_total_order = item_order.qty
            record.item = _data.get('item')
            record.qty_received = _data.get('qty_received')
            record.comments = _data.get('comments')
            record.qty_pending_order = (
                record.qty_total_order - record.qty_received
                )
            record.created_by = _user
            record.full_clean()
            record.save()
            record.line = "RE{}LN{}".format(
                str(record.document.id), str(record.id)
                )
            record.save()
            self.update_StockQty(record, record.document.warehouse, _user)
            return record

    @classmethod
    def update_Reception(self, _record, _data, _user, _files=None):
        with transaction.atomic():
            _record.warehouse = _data.get('warehouse')
            _record.supplier = _data.get('supplier')
            _record.received_by = _data.get('received_by')
            _record.order = _data.get('order')
            _record.number = _data.get('number')
            _record.description = _data.get('description')
            _record.comments = _data.get('comments')
            _record.date = _data.get('date')
            _record.updated_by = _user
            _record.full_clean()
            _record.save()
            return _record

    @classmethod
    def update_ReceptionLine(self, _record, _data, _user, _files=None):
        with transaction.atomic():
            _record.item = _data.get('item')
            _record.qty_received = _data.get('qty_received')
            _record.comments = _data.get('comments')
            _record.updated_by = _user
            _record.full_clean()
            _record.save()
            return _record

    @classmethod
    def update_ReceptionLineDesc(self, _record, _data, _user, _files=None):
        with transaction.atomic():
            company = _record.document.warehouse.company.id
            approved = self.validate_Access(_user, ['CRRP', 'EDRP'], company)
            if approved is False:
                raise ValueError('No tiene permiso para crear recepciones.')
            if _record.document.status != 'INP':
                raise ValueError('Este registro no puede ser modificado.')
            _record.qty_received = _data.get('qty_received')
            _record.comments = _data.get('comments')
            _record.updated_by = _user
            _record.full_clean()
            _record.save()
            return _record

    @classmethod
    def update_StockQty(self, _data, _record, _user):
        qty_stock = StockItemBsn.get_ItemStock(
            _data.item, _record
            )
        if qty_stock:
            qty_temp = qty_stock.qty + _data.qty_received
            qty_stock.qty = qty_temp
            qty_stock.updated_by = _user
            qty_stock.save()
        return qty_stock


    @classmethod
    def get_Reception(self, pk):
        record = self.get_ModelObject().objects.get(id=pk)
        if record:
            return record
        else:
            raise ValueError('No existe este despacho de articulos.')

    @classmethod
    def get_ReceptionLine(self, pk, line_pk):
        reception = self.get_ModelObject().objects.get(id=pk)
        if reception:
            record = reception.in_docreception.get(id=line_pk)
            return record
        else:
            raise ValueError('No existe esta linea de artículo.')

    @classmethod
    def search(self, records, _value):
        try:
            _value = int(_value)
        except Exception:
            _value

        if type(_value) is str:
            records = records.filter(warehouse_name__icontains=_value)
        elif type(_value) is int:
            records = records.filter(id=_value)
        return records


class DocDispatchBsn(StxBsn):
    model = DocDispatch
    model_filter = DocDispatchFilter

    @classmethod
    def get_Assigned(self, user):
        return self.get_All().filter(warehouse_company__in=user.companies.all())

    @classmethod
    def filter_ByPermission(self, records, user):
        return records.filter(warehouse__company__in=user.companies.all())


    @classmethod
    def update_Desc(self, _record, _data, _user, _files=None):
        with transaction.atomic():
            _record.description = _data.get('description')
            _record.comments = _data.get('comments')
            _record.updated_by = _user
            _record.full_clean()
            _record.save()
            return _record

    @classmethod
    def affect_Stock(self, _record, _user):
        with transaction.atomic():
            for line in _record.in_doc.all():
                stock = StockItemBsn.get_ItemStock(
                    line.item, _record.warehouse
                )
                temp = stock.qty - line.qty
                stock.qty = temp
                stock.updated_by = _user
                stock.save()

    @classmethod
    def finish(self, _record, _user):
        with transaction.atomic():
            self.affect_Stock(_record, _user)
            _record.updated_by = _user
            _record.full_clean()
            _record.save()
            return _record

    @classmethod
    def get_Lines(self, pk, user):
        records = self.get_ModelObject().objects.get(
            id=pk).in_doc.all()
        return records

    @classmethod
    def get_DispatchList(self, user):
        records = self.get_ModelObject().objects.all()
        return records

    @classmethod
    def create(self, _data, _user, _files=None):
        with transaction.atomic():
            record = self.get_ModelObject()()
            record.warehouse = _data.get('warehouse')
            record.delivery_by = _data.get('delivery_by')
            record.received_by = _data.get('received_by')
            record.description = _data.get('description')
            record.comments = _data.get('comments')
            record.date = _data.get('date')
            record.created_by = _user
            record.full_clean()
            record.save()
            return record

    @classmethod
    def update_Dispatch(self, _record, _data, _user, _files=None):
        with transaction.atomic():
            _record.received_by = _data.get('received_by')
            _record.description = _data.get('description')
            _record.comments = _data.get('comments')
            _record.updated_by = _user
            _record.full_clean()
            _record.save()
            return _record

    @classmethod
    def create_DispatchLine(self, _record, _data, _user, _files=None):
        with transaction.atomic():
            record = DocDispatchLine()
            record.document = _record
            record.comments = _data.get('comments')
            qty_stock = StockItemBsn.get_ItemStock(
                _data.get('item'), record.document.warehouse
                )
            record.price = qty_stock.price
            record.qty = _data.get('qty')
            record.item = _data.get('item')
            record.created_by = _user
            record.full_clean()
            record.save()
            record.line = "DE{}LN{}".format(
                str(record.document.id), str(record.id)
                )
            record.save()
            return record

    @classmethod
    def update_DispatchLine(self, _record, _data, _user, _files=None):
        with transaction.atomic():
            _record.comments = _data.get('comments')
            if _record.item == _data.get('item'):
                qty_stock = StockItemBsn.get_ItemStock(
                    _record.item, _record.document.warehouse
                    )
                if qty_stock:
                    qty_temp = qty_stock.qty + _record.qty
                    qty_stock.qty = qty_temp - _data.get('qty')
                    qty_stock.updated_by = _user
                    qty_stock.save()
            else:
                qty_stock = StockItemBsn.get_ItemStock(
                    _record.item, _record.document.warehouse
                    )
                if qty_stock:
                    qty_temp = qty_stock.qty + _record.qty
                    qty_stock.qty = qty_temp
                    qty_stock.updated_by = _user
                    qty_stock.save()

                qty_stock = StockItemBsn.get_ItemStock(
                    _data.get('item'), _record.document.warehouse
                    )
                if qty_stock:
                    qty_temp = qty_stock.qty - _data.get('qty')
                    qty_stock.qty = qty_temp
                    qty_stock.updated_by = _user
                    qty_stock.save()

            _record.price = qty_stock.price
            _record.qty = _data.get('qty')
            _record.item = _data.get('item')
            _record.updated_by = _user
            _record.full_clean()
            _record.save()
            return _record

    @classmethod
    def get_Dispatch(self, pk):
        record = self.get_ModelObject().objects.get(id=pk)
        if record:
            return record
        else:
            raise ValueError('No existe este despacho de articulos.')

    @classmethod
    def get_DispatchLine(self, pk, line_pk):
        dispatch = self.get_ModelObject().objects.get(id=pk)
        if dispatch:
            record = dispatch.in_doc.get(id=line_pk)
            return record
        else:
            raise ValueError('No existe esta linea de artículo.')

    @classmethod
    def search(self, records, _value):
        try:
            _value = int(_value)
        except Exception:
            _value

        if type(_value) is str:
            records = records.filter(warehouse_name__icontains=_value)
        elif type(_value) is int:
            records = records.filter(id=_value)
        return records
