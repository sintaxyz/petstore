# Own's Libraries
from tools.forms import StxModelCreateForm

from .models import DocShrinkage
from .models import DocAdjustment
from .models import DocDispatch
from .models import DocInitialEntry
from .models import DocReception

from .business import DocShrinkageBsn
from .business import DocAdjustmentBsn
from .business import DocDispatchBsn
from .business import DocInitialEntryBsn
from .business import DocReceptionBsn


class DocAdjustmentForm(StxModelCreateForm):
    business_class = DocAdjustmentBsn

    class Meta:
        model = DocAdjustment
        fields = [
            'warehouse',
            'adjusted_by',
            'date',
            'type',
            'description',
            'comments',
        ]


class DocInitialEntryForm(StxModelCreateForm):
    business_class = DocInitialEntryBsn

    class Meta:
        model = DocInitialEntry
        fields = [
            'warehouse',
            'entry_by',
            'qty_items',
            'total_amount',
            'description',
            'comments',
            'date',
        ]


class DocShrinkageForm(StxModelCreateForm):
    business_class = DocShrinkageBsn

    class Meta:
        model = DocShrinkage
        fields = [
            'warehouse',
            'register_by',
            'description',
            'comments',
            'type',
            'date',
        ]


class DocReceptionForm(StxModelCreateForm):
    business_class = DocReceptionBsn

    class Meta:
        model = DocReception
        fields = [
            'warehouse',
            'received_by',
            'order',
            'date',
            'number',
            'comments',
        ]


class DocDispatchForm(StxModelCreateForm):
    business_class = DocDispatchBsn

    class Meta:
        model = DocDispatch
        fields = [
            'warehouse',
            'delivery_by',
            'received_by',
        ]
