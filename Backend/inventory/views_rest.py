# Python's Libraries

# Django's Libraries
from django.db import transaction
# from django_filters.rest_framework import DjangoFilterBackend

# Third-party Libraries
from rest_framework import viewsets
from rest_framework import status
from rest_framework.response import Response
from rest_framework.permissions import IsAuthenticated
# from rest_framework.permissions import AllowAny

# Own's Libraries
from .business import DocReceptionBsn
from .business import DocDispatchBsn
from .business import DocInitialEntryBsn
from .business import DocAdjustmentBsn
from .business import DocShrinkageBsn

from .serializers import DocReceptionLineSerializer
from .serializers import DocReceptionSerializer
from .serializers import DocReceptionUpdateDescSerializer
from .serializers import DocReceptionLineUpdateDescSerializer
from .serializers import DocReceptionUpdateSerializer
from .serializers import DocDispatchSerializer
from .serializers import DocDispatchUpdateSerializer
from .serializers import DocDispatchLineSerializer
from .serializers import DocDispatchUpdateDescSerializer
from .serializers import DocDispatchLineUpdateDescSerializer
from .serializers import DocInitialEntrySerializer
from .serializers import DocInitialEntryUpdateDescSerializer
from .serializers import DocInitialEntryLineSerializer
from .serializers import DocInitialEntryUpdateSerializer
from .serializers import DocAdjustmentSerializer
from .serializers import DocAdjustmentUpdateSerializer
from .serializers import DocAdjustmentLineSerializer
from .serializers import DocAdjustmentUpdateDescSerializer
from .serializers import DocAdjustmentLineUpdateDescSerializer
from .serializers import DocShrinkageLineSerializer
from .serializers import DocShrinkageSerializer
from .serializers import DocShrinkageUpdateDescSerializer
from .serializers import DocShrinkageLineUpdateDescSerializer
from .serializers import DocShrinkageUpdateSerializer


class DocAdjustmentListAPIView(viewsets.GenericViewSet):
    serializer_class = DocAdjustmentSerializer

    def get_QuerySet(self, user):
        records = DocAdjustmentBsn.get_AdjustmentList(user)
        return records

    def list(self, request, *args, **kwargs):
        queryset = self.filter_queryset(
            self.get_QuerySet(user=request.user)
        )
        serializer = self.get_serializer(queryset, many=True)
        return Response(serializer.data)

    def create(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        if serializer.is_valid(raise_exception=False):
            try:
                data_list = serializer.validated_data
                with transaction.atomic():
                    record = DocAdjustmentBsn.create(
                        data_list, request.user)
                serializer = DocAdjustmentSerializer(record)
                return Response(
                    serializer.data,
                    status=status.HTTP_201_CREATED
                )
            except Exception as e:
                return Response(
                    {'detail': str(e)},
                    status=status.HTTP_400_BAD_REQUEST
                )
                print(e)
        else:
            return Response(
                serializer.errors,
                status=status.HTTP_400_BAD_REQUEST
            )


class DocAdjustmentUpdateDescAPIView(viewsets.GenericViewSet):
    serializer_class = DocAdjustmentSerializer

    def get_Object(self, pk):
        instance = DocAdjustmentBsn.get(pk)
        return instance

    def update(self, request, pk, *args, **kwargs):
        instance = self.get_Object(pk)
        serializer = DocAdjustmentUpdateDescSerializer(data=request.data)
        if serializer.is_valid(raise_exception=False):
            try:
                data_list = serializer.validated_data
                record = DocAdjustmentBsn.update_Desc(
                    instance,
                    data_list,
                    request.user
                )
                serializer = self.get_serializer(record)
                return Response(
                    serializer.data,
                    status=status.HTTP_200_OK
                )
            except Exception as e:
                return Response(
                    {'detail': str(e)},
                    status=status.HTTP_400_BAD_REQUEST
                )
        else:
            return Response(
                serializer.errors,
                status=status.HTTP_400_BAD_REQUEST
            )


class DocAdjustmentListLinesAPIView(viewsets.GenericViewSet):
    serializer_class = DocAdjustmentLineSerializer

    def get_QuerySet(self, pk, user):
        records = DocAdjustmentBsn.get_Lines(pk, user)
        return records

    def list(self, request, pk, *args, **kwargs):
        queryset = self.filter_queryset(
            self.get_QuerySet(pk, user=request.user)
        )
        serializer = self.get_serializer(queryset, many=True)
        return Response(serializer.data)


class DocAdjustmentLinesUpdateAPIView(viewsets.GenericViewSet):
    serializer_class = DocAdjustmentLineSerializer

    def get_Object(self, pk, line_pk):
        instance = DocAdjustmentBsn.get_AdjustmentLine(pk, line_pk)
        return instance

    def update(self, request, pk, line_pk, *args, **kwargs):
        instance = self.get_Object(pk, line_pk)
        serializer = DocAdjustmentLineUpdateDescSerializer(data=request.data)
        if serializer.is_valid(raise_exception=False):
            try:
                data_list = serializer.validated_data
                record = DocAdjustmentBsn.update_AdjustmentLineDesc(
                    instance,
                    data_list,
                    request.user
                )
                serializer = self.get_serializer(record)
                return Response(
                    serializer.data,
                    status=status.HTTP_200_OK
                )
            except Exception as e:
                return Response(
                    {'detail': str(e)},
                    status=status.HTTP_400_BAD_REQUEST
                )
        else:
            return Response(
                serializer.errors,
                status=status.HTTP_400_BAD_REQUEST
            )


class DocAdjustmentFinishAPIView(viewsets.GenericViewSet):
    serializer_class = DocAdjustmentSerializer

    def get_Object(self, pk):
        instance = DocAdjustmentBsn.get(pk)
        return instance

    def finish(self, request, pk):
        instance = self.get_Object(pk)
        try:
            record = DocAdjustmentBsn.finish(instance, request.user)
            serializer = self.serializer_class(record)
            return Response(
                serializer.data,
                status=status.HTTP_200_OK
            )
        except Exception as e:
            return Response(
                {'detail': str(e)},
                status=status.HTTP_400_BAD_REQUEST
            )


class DocAdjustmentRetrieveAPIView(viewsets.GenericViewSet):
    serializer_class = DocAdjustmentSerializer

    def get_Object(self, pk):
        instance = DocAdjustmentBsn.get_Adjustment(pk)
        return instance

    def retrieve(self, request, pk, *args, **kwargs):
        instance = self.get_Object(pk)
        serializer = self.get_serializer(instance)
        return Response(serializer.data)

    def update(self, request, pk, *args, **kwargs):
        instance = self.get_Object(pk)
        serializer = DocAdjustmentUpdateSerializer(data=request.data)
        if serializer.is_valid(raise_exception=False):
            try:
                data_list = serializer.validated_data
                record = DocAdjustmentBsn.update_Adjustment(
                    instance,
                    data_list,
                    request.user
                )
                serializer = DocAdjustmentUpdateSerializer(record)
                return Response(
                    serializer.data,
                    status=status.HTTP_200_OK
                )
            except Exception as e:
                return Response(
                    {'detail': str(e)},
                    status=status.HTTP_400_BAD_REQUEST
                )
        else:
            return Response(
                serializer.errors,
                status=status.HTTP_400_BAD_REQUEST
            )

    def create(self, request, pk, *args, **kwargs):
        instance = self.get_Object(pk)
        serializer = DocAdjustmentLineSerializer(data=request.data)
        if serializer.is_valid(raise_exception=False):
            try:
                data_list = serializer.validated_data
                with transaction.atomic():
                    record = DocAdjustmentBsn.create_AdjustmentLine(
                        instance,
                        data_list,
                        request.user
                    )
                    serializer = DocAdjustmentLineSerializer(record)
                return Response(
                    serializer.data,
                    status=status.HTTP_200_OK
                )
            except Exception as e:
                return Response(
                    {'detail': str(e)},
                    status=status.HTTP_400_BAD_REQUEST
                )
        else:
            return Response(
                serializer.errors,
                status=status.HTTP_400_BAD_REQUEST
            )


class DocReceptionListAPIView(viewsets.GenericViewSet):
    serializer_class = DocReceptionSerializer

    def get_QuerySet(self, user):
        records = DocReceptionBsn.get_ReceptionList(user)
        return records

    def list(self, request, *args, **kwargs):
        queryset = self.filter_queryset(
            self.get_QuerySet(user=request.user)
        )
        serializer = self.get_serializer(queryset, many=True)
        return Response(serializer.data)

    def create(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        if serializer.is_valid(raise_exception=False):
            try:
                data_list = serializer.validated_data
                with transaction.atomic():
                    record = DocReceptionBsn.create(
                        data_list, request.user)
                serializer = DocReceptionSerializer(record)
                return Response(
                    serializer.data,
                    status=status.HTTP_201_CREATED
                )
            except Exception as e:
                return Response(
                    {'detail': str(e)},
                    status=status.HTTP_400_BAD_REQUEST
                )
                print(e)
        else:
            return Response(
                serializer.errors,
                status=status.HTTP_400_BAD_REQUEST
            )


class DocReceptionUpdateDescAPIView(viewsets.GenericViewSet):
    serializer_class = DocReceptionSerializer

    def get_Object(self, pk):
        instance = DocReceptionBsn.get(pk)
        return instance

    def update(self, request, pk, *args, **kwargs):
        instance = self.get_Object(pk)
        serializer = DocReceptionUpdateDescSerializer(data=request.data)
        if serializer.is_valid(raise_exception=False):
            try:
                data_list = serializer.validated_data
                record = DocReceptionBsn.update_Desc(
                    instance,
                    data_list,
                    request.user
                )
                serializer = self.get_serializer(record)
                return Response(
                    serializer.data,
                    status=status.HTTP_200_OK
                )
            except Exception as e:
                return Response(
                    {'detail': str(e)},
                    status=status.HTTP_400_BAD_REQUEST
                )
        else:
            return Response(
                serializer.errors,
                status=status.HTTP_400_BAD_REQUEST
            )


class DocReceptionListLinesAPIView(viewsets.GenericViewSet):
    serializer_class = DocReceptionLineSerializer

    def get_QuerySet(self, pk, user):
        records = DocReceptionBsn.get_Lines(pk, user)
        return records

    def list(self, request, pk, *args, **kwargs):
        queryset = self.filter_queryset(
            self.get_QuerySet(pk, user=request.user)
        )
        serializer = self.get_serializer(queryset, many=True)
        return Response(serializer.data)


class DocReceptionLinesUpdateAPIView(viewsets.GenericViewSet):
    serializer_class = DocReceptionLineSerializer

    def get_Object(self, pk, line_pk):
        instance = DocReceptionBsn.get_ReceptionLine(
            pk, line_pk
            )
        return instance

    def update(self, request, pk, line_pk, *args, **kwargs):
        instance = self.get_Object(pk, line_pk)
        serializer = DocReceptionLineUpdateDescSerializer(data=request.data)
        if serializer.is_valid(raise_exception=False):
            try:
                data_list = serializer.validated_data
                record = DocReceptionBsn.update_ReceptionLineDesc(
                    instance,
                    data_list,
                    request.user
                )
                serializer = self.get_serializer(record)
                return Response(
                    serializer.data,
                    status=status.HTTP_200_OK
                )
            except Exception as e:
                return Response(
                    {'detail': str(e)},
                    status=status.HTTP_400_BAD_REQUEST
                )
        else:
            return Response(
                serializer.errors,
                status=status.HTTP_400_BAD_REQUEST
            )


class DocReceptionFinishAPIView(viewsets.GenericViewSet):
    serializer_class = DocReceptionSerializer

    def get_Object(self, pk):
        instance = DocReceptionBsn.get(pk)
        return instance

    def finish(self, request, pk):
        instance = self.get_Object(pk)
        try:
            record = DocReceptionBsn.finish(instance, request.user)
            serializer = self.serializer_class(record)
            return Response(
                serializer.data,
                status=status.HTTP_200_OK
            )
        except Exception as e:
            return Response(
                {'detail': str(e)},
                status=status.HTTP_400_BAD_REQUEST
            )


class DocReceptionCancelAPIView(viewsets.GenericViewSet):
    serializer_class = DocReceptionSerializer

    def get_Object(self, pk):
        instance = DocReceptionBsn.get(pk)
        return instance

    def cancel(self, request, pk):
        instance = self.get_Object(pk)
        try:
            record = DocReceptionBsn.cancel(instance, request.user)
            serializer = self.serializer_class(record)
            return Response(
                serializer.data,
                status=status.HTTP_200_OK
            )
        except Exception as e:
            return Response(
                {'detail': str(e)},
                status=status.HTTP_400_BAD_REQUEST
            )


class DocReceptionRetrieveAPIView(viewsets.GenericViewSet):
    serializer_class = DocReceptionSerializer

    def get_Object(self, pk):
        instance = DocReceptionBsn.get_Reception(pk)
        return instance

    def retrieve(self, request, pk, *args, **kwargs):
        instance = self.get_Object(pk)
        serializer = self.get_serializer(instance)
        return Response(serializer.data)

    def update(self, request, pk, *args, **kwargs):
        instance = self.get_Object(pk)
        serializer = DocReceptionUpdateSerializer(data=request.data)
        if serializer.is_valid(raise_exception=False):
            try:
                data_list = serializer.validated_data
                record = DocReceptionBsn.update_Reception(
                    instance,
                    data_list,
                    request.user
                )
                serializer = DocReceptionUpdateSerializer(record)
                return Response(
                    serializer.data,
                    status=status.HTTP_200_OK
                )
            except Exception as e:
                return Response(
                    {'detail': str(e)},
                    status=status.HTTP_400_BAD_REQUEST
                )
        else:
            return Response(
                serializer.errors,
                status=status.HTTP_400_BAD_REQUEST
            )

    def create(self, request, pk, *args, **kwargs):
        instance = self.get_Object(pk)
        serializer = DocReceptionLineSerializer(data=request.data)
        if serializer.is_valid(raise_exception=False):
            try:
                data_list = serializer.validated_data
                with transaction.atomic():
                    record = DocReceptionBsn.create_ReceptionLine(
                        instance,
                        data_list,
                        request.user
                    )
                    serializer = DocReceptionLineSerializer(record)
                return Response(
                    serializer.data,
                    status=status.HTTP_200_OK
                )
            except Exception as e:
                return Response(
                    {'detail': str(e)},
                    status=status.HTTP_400_BAD_REQUEST
                )
        else:
            return Response(
                serializer.errors,
                status=status.HTTP_400_BAD_REQUEST
            )


class DocDispatchListAPIView(viewsets.GenericViewSet):
    serializer_class = DocDispatchSerializer

    def get_QuerySet(self, user):
        records = DocDispatchBsn.get_DispatchList(user)
        return records

    def list(self, request, *args, **kwargs):
        queryset = self.filter_queryset(
            self.get_QuerySet(request.user)
        )
        serializer = self.get_serializer(queryset, many=True)
        return Response(serializer.data)

    def create(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        if serializer.is_valid(raise_exception=False):
            try:
                data_list = serializer.validated_data
                with transaction.atomic():
                    record = DocDispatchBsn.create(
                        data_list, request.user)
                serializer = DocDispatchSerializer(record)
                return Response(
                    serializer.data,
                    status=status.HTTP_201_CREATED
                )
            except Exception as e:
                return Response(
                    {'detail': str(e)},
                    status=status.HTTP_400_BAD_REQUEST
                )
                print(e)
        else:
            return Response(
                serializer.errors,
                status=status.HTTP_400_BAD_REQUEST
            )


class DocDispatchRetrieveAPIView(viewsets.GenericViewSet):
    serializer_class = DocDispatchSerializer

    def get_Object(self, pk):
        instance = DocDispatchBsn.get_Dispatch(pk)
        return instance

    def retrieve(self, request, pk, *args, **kwargs):
        instance = self.get_Object(pk)
        serializer = self.get_serializer(instance)
        return Response(serializer.data)

    def update(self, request, pk, *args, **kwargs):
        instance = self.get_Object(pk)
        serializer = DocDispatchUpdateSerializer(data=request.data)
        if serializer.is_valid(raise_exception=False):
            try:
                data_list = serializer.validated_data
                record = DocDispatchBsn.update_Dispatch(
                    instance,
                    data_list,
                    request.user
                )
                serializer = DocDispatchUpdateSerializer(record)
                return Response(
                    serializer.data,
                    status=status.HTTP_200_OK
                )
            except Exception as e:
                return Response(
                    {'detail': str(e)},
                    status=status.HTTP_400_BAD_REQUEST
                )
        else:
            return Response(
                serializer.errors,
                status=status.HTTP_400_BAD_REQUEST
            )

    def create(self, request, pk, *args, **kwargs):
        instance = self.get_Object(pk)
        serializer = DocDispatchLineSerializer(data=request.data)
        if serializer.is_valid(raise_exception=False):
            try:
                data_list = serializer.validated_data
                with transaction.atomic():
                    record = DocDispatchBsn.create_DispatchLine(
                        instance,
                        data_list,
                        request.user
                    )
                    serializer = DocDispatchLineSerializer(record)
                return Response(
                    serializer.data,
                    status=status.HTTP_200_OK
                )
            except Exception as e:
                return Response(
                    {'detail': str(e)},
                    status=status.HTTP_400_BAD_REQUEST
                )
        else:
            return Response(
                serializer.errors,
                status=status.HTTP_400_BAD_REQUEST
            )


class DocDispatchLinesUpdateAPIView(viewsets.GenericViewSet):
    serializer_class = DocDispatchLineSerializer

    def get_Object(self, pk, line_pk):
        instance = DocDispatchBsn.get_DispatchLine(
            pk, line_pk
            )
        return instance

    def update(self, request, pk, line_pk, *args, **kwargs):
        instance = self.get_Object(pk, line_pk)
        serializer = self.get_serializer(data=request.data)
        if serializer.is_valid(raise_exception=False):
            try:
                data_list = serializer.validated_data
                record = DocDispatchBsn.update_DispatchLine(
                    instance,
                    data_list,
                    request.user
                )
                serializer = self.get_serializer(record)
                return Response(
                    serializer.data,
                    status=status.HTTP_200_OK
                )
            except Exception as e:
                return Response(
                    {'detail': str(e)},
                    status=status.HTTP_400_BAD_REQUEST
                )
        else:
            return Response(
                serializer.errors,
                status=status.HTTP_400_BAD_REQUEST
            )


class DocDispatchFinishAPIView(viewsets.GenericViewSet):
    serializer_class = DocDispatchSerializer

    def get_Object(self, pk):
        instance = DocDispatchBsn.get(pk)
        return instance

    def finish(self, request, pk):
        instance = self.get_Object(pk)
        try:
            record = DocDispatchBsn.finish(instance, request.user)
            serializer = self.serializer_class(record)
            return Response(
                serializer.data,
                status=status.HTTP_200_OK
            )
        except Exception as e:
            return Response(
                {'detail': str(e)},
                status=status.HTTP_400_BAD_REQUEST
            )


class DocDispatchUpdateDescAPIView(viewsets.GenericViewSet):
    serializer_class = DocDispatchSerializer

    def get_Object(self, pk):
        instance = DocDispatchBsn.get(pk)
        return instance

    def update(self, request, pk, *args, **kwargs):
        instance = self.get_Object(pk)
        serializer = DocDispatchUpdateDescSerializer(data=request.data)
        if serializer.is_valid(raise_exception=False):
            try:
                data_list = serializer.validated_data
                record = DocDispatchBsn.update_Desc(
                    instance,
                    data_list,
                    request.user
                )
                serializer = self.get_serializer(record)
                return Response(
                    serializer.data,
                    status=status.HTTP_200_OK
                )
            except Exception as e:
                return Response(
                    {'detail': str(e)},
                    status=status.HTTP_400_BAD_REQUEST
                )
        else:
            return Response(
                serializer.errors,
                status=status.HTTP_400_BAD_REQUEST
            )


class DocDispatchListLinesAPIView(viewsets.GenericViewSet):
    serializer_class = DocDispatchLineSerializer

    def get_QuerySet(self, pk, user):
        records = DocDispatchBsn.get_Lines(pk, user)
        return records

    def list(self, request, pk, *args, **kwargs):
        queryset = self.filter_queryset(
            self.get_QuerySet(pk, user=request.user)
        )
        serializer = self.get_serializer(queryset, many=True)
        return Response(serializer.data)


class DocInitialEntryListAPIView(viewsets.GenericViewSet):
    queryset = ''
    serializer_class = DocInitialEntrySerializer

    def get_QuerySet(self, user):
        records = DocInitialEntryBsn.get_EntryList(user)
        return records

    def list(self, request, *args, **kwargs):
        queryset = self.filter_queryset(
            self.get_QuerySet(user=request.user)
        )
        serializer = self.get_serializer(queryset, many=True)
        return Response(serializer.data)

    def create(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        if serializer.is_valid(raise_exception=False):
            try:
                data_list = serializer.validated_data
                with transaction.atomic():
                    record = DocInitialEntryBsn.create(
                        data_list, request.user)
                serializer = DocInitialEntrySerializer(record)
                return Response(
                    serializer.data,
                    status=status.HTTP_201_CREATED
                )
            except Exception as e:
                return Response(
                    {'detail': str(e)},
                    status=status.HTTP_400_BAD_REQUEST
                )
                print(e)
        else:
            return Response(
                serializer.errors,
                status=status.HTTP_400_BAD_REQUEST
            )


class DocInitialEntryRetrieveAPIView(viewsets.GenericViewSet):
    serializer_class = DocInitialEntrySerializer

    def get_Object(self, pk):
        instance = DocInitialEntryBsn.get_Entry(pk)
        return instance

    def retrieve(self, request, pk, *args, **kwargs):
        instance = self.get_Object(pk)
        serializer = self.get_serializer(instance)
        return Response(serializer.data)

    def update(self, request, pk, *args, **kwargs):
        instance = self.get_Object(pk)
        serializer = DocInitialEntryUpdateSerializer(data=request.data)
        if serializer.is_valid(raise_exception=False):
            try:
                data_list = serializer.validated_data
                record = DocInitialEntryBsn.update_Entry(
                    instance,
                    data_list,
                    request.user
                )
                serializer = DocInitialEntryUpdateSerializer(record)
                return Response(
                    serializer.data,
                    status=status.HTTP_200_OK
                )
            except Exception as e:
                return Response(
                    {'detail': str(e)},
                    status=status.HTTP_400_BAD_REQUEST
                )
        else:
            return Response(
                serializer.errors,
                status=status.HTTP_400_BAD_REQUEST
            )

    def create(self, request, pk, *args, **kwargs):
        instance = self.get_Object(pk)
        serializer = DocInitialEntryLineSerializer(data=request.data)
        if serializer.is_valid(raise_exception=False):
            try:
                data_list = serializer.validated_data
                with transaction.atomic():
                    record = DocInitialEntryBsn.create_EntryLine(
                        instance,
                        data_list,
                        request.user
                    )
                    serializer = DocInitialEntryLineSerializer(record)
                return Response(
                    serializer.data,
                    status=status.HTTP_200_OK
                )
            except Exception as e:
                return Response(
                    {'detail': str(e)},
                    status=status.HTTP_400_BAD_REQUEST
                )
        else:
            return Response(
                serializer.errors,
                status=status.HTTP_400_BAD_REQUEST
            )


class DocInitialEntryListLinesAPIView(viewsets.GenericViewSet):
    serializer_class = DocInitialEntryLineSerializer

    def get_QuerySet(self, pk, user):
        records = DocInitialEntryBsn.get_Lines(pk, user)
        return records

    def list(self, request, pk, *args, **kwargs):
        queryset = self.filter_queryset(
            self.get_QuerySet(pk, user=request.user)
        )
        serializer = self.get_serializer(queryset, many=True)
        return Response(serializer.data)    


class DocInitialEntryLinesUpdateAPIView(viewsets.GenericViewSet):
    serializer_class = DocInitialEntryLineSerializer

    def get_Object(self, pk, line_pk):
        instance = DocInitialEntryBsn.get_EntryLine(pk, line_pk)
        return instance

    def update(self, request, pk, line_pk, *args, **kwargs):
        instance = self.get_Object(pk, line_pk)
        serializer = self.get_serializer(data=request.data)
        if serializer.is_valid(raise_exception=False):
            try:
                data_list = serializer.validated_data
                record = DocInitialEntryBsn.update_EntryLine(
                    instance,
                    data_list,
                    request.user
                )
                serializer = self.get_serializer(record)
                return Response(
                    serializer.data,
                    status=status.HTTP_200_OK
                )
            except Exception as e:
                return Response(
                    {'detail': str(e)},
                    status=status.HTTP_400_BAD_REQUEST
                )
        else:
            return Response(
                serializer.errors,
                status=status.HTTP_400_BAD_REQUEST
            )


class DocInitialEntryFinishAPIView(viewsets.GenericViewSet):
    serializer_class = DocInitialEntrySerializer

    def get_Object(self, pk):
        instance = DocInitialEntryBsn.get(pk)
        return instance

    def finish(self, request, pk):
        record = self.get_Object(pk)
        try:
            record = DocInitialEntryBsn.finish(record, request.user)
            serializer = self.serializer_class(record)
            return Response(
                serializer.data,
                status=status.HTTP_200_OK
            )
        except Exception as e:
            return Response(
                {'detail': str(e)},
                status=status.HTTP_400_BAD_REQUEST
            )


class DocInitialEntryCancelAPIView(viewsets.GenericViewSet):
    serializer_class = DocInitialEntrySerializer

    def get_Object(self, pk):
        instance = DocInitialEntryBsn.get(pk)
        return instance

    def cancel(self, request, pk):
        record = self.get_Object(pk)
        try:
            record = DocInitialEntryBsn.cancel(record, request.user)
            serializer = self.serializer_class(record)
            return Response(
                serializer.data,
                status=status.HTTP_200_OK
            )
        except Exception as e:
            return Response(
                {'detail': str(e)},
                status=status.HTTP_400_BAD_REQUEST
            )


class DocInitialEntryUpdateDescAPIView(viewsets.GenericViewSet):
    serializer_class = DocInitialEntrySerializer

    def get_Object(self, pk):
        instance = DocInitialEntryBsn.get(pk)
        return instance

    def update(self, request, pk, *args, **kwargs):
        instance = self.get_Object(pk)
        serializer = DocInitialEntryUpdateDescSerializer(data=request.data)
        if serializer.is_valid(raise_exception=False):
            try:
                data_list = serializer.validated_data
                record = DocInitialEntryBsn.update_Desc(
                    instance,
                    data_list,
                    request.user
                )
                serializer = self.get_serializer(record)
                return Response(
                    serializer.data,
                    status=status.HTTP_200_OK
                )
            except Exception as e:
                return Response(
                    {'detail': str(e)},
                    status=status.HTTP_400_BAD_REQUEST
                )
        else:
            return Response(
                serializer.errors,
                status=status.HTTP_400_BAD_REQUEST
            )


class DocShrinkageListAPIView(viewsets.GenericViewSet):
    serializer_class = DocShrinkageSerializer

    def get_QuerySet(self, user):
        records = DocShrinkageBsn.get_ShrinkageList(user)
        return records

    def list(self, request, *args, **kwargs):
        queryset = self.filter_queryset(
            self.get_QuerySet(user=request.user)
        )
        serializer = self.get_serializer(queryset, many=True)
        return Response(serializer.data)

    def create(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        if serializer.is_valid(raise_exception=False):
            try:
                data_list = serializer.validated_data
                with transaction.atomic():
                    record = DocShrinkageBsn.create(
                        data_list, request.user)
                serializer = DocShrinkageSerializer(record)
                return Response(
                    serializer.data,
                    status=status.HTTP_201_CREATED
                )
            except Exception as e:
                return Response(
                    {'detail': str(e)},
                    status=status.HTTP_400_BAD_REQUEST
                )
                print(e)
        else:
            return Response(
                serializer.errors,
                status=status.HTTP_400_BAD_REQUEST
            )


class DocShrinkageRetrieveAPIView(viewsets.GenericViewSet):
    serializer_class = DocShrinkageSerializer

    def get_Object(self, pk):
        instance = DocShrinkageBsn.get_Shrinkage(pk)
        return instance

    def retrieve(self, request, pk, *args, **kwargs):
        instance = self.get_Object(pk)
        serializer = self.get_serializer(instance)
        return Response(serializer.data)

    def update(self, request, pk, *args, **kwargs):
        instance = self.get_Object(pk)
        serializer = DocShrinkageUpdateSerializer(data=request.data)
        if serializer.is_valid(raise_exception=False):
            try:
                data_list = serializer.validated_data
                record = DocShrinkageBsn.update_Shrinkage(
                    instance,
                    data_list,
                    request.user
                )
                serializer = DocShrinkageUpdateSerializer(record)
                return Response(
                    serializer.data,
                    status=status.HTTP_200_OK
                )
            except Exception as e:
                return Response(
                    {'detail': str(e)},
                    status=status.HTTP_400_BAD_REQUEST
                )
        else:
            return Response(
                serializer.errors,
                status=status.HTTP_400_BAD_REQUEST
            )

    def create(self, request, pk, *args, **kwargs):
        instance = self.get_Object(pk)
        serializer = DocShrinkageLineSerializer(data=request.data)
        if serializer.is_valid(raise_exception=False):
            try:
                data_list = serializer.validated_data
                with transaction.atomic():
                    record = DocShrinkageBsn.create_ShrinkageLine(
                        instance,
                        data_list,
                        request.user
                    )
                    serializer = DocShrinkageLineSerializer(record)
                return Response(
                    serializer.data,
                    status=status.HTTP_200_OK
                )
            except Exception as e:
                return Response(
                    {'detail': str(e)},
                    status=status.HTTP_400_BAD_REQUEST
                )
        else:
            return Response(
                serializer.errors,
                status=status.HTTP_400_BAD_REQUEST
            )


class DocShrinkageLinesUpdateAPIView(viewsets.GenericViewSet):
    serializer_class = DocShrinkageLineSerializer

    def get_Object(self, pk, line_pk):
        instance = DocShrinkageBsn.get_ShrinkageLine(pk, line_pk)
        return instance

    def update(self, request, pk, line_pk, *args, **kwargs):
        instance = self.get_Object(pk, line_pk)
        serializer = self.get_serializer(data=request.data)
        if serializer.is_valid(raise_exception=False):
            try:
                data_list = serializer.validated_data
                record = DocShrinkageBsn.update_ShrinkageLine(
                    instance,
                    data_list,
                    request.user
                )
                serializer = self.get_serializer(record)
                return Response(
                    serializer.data,
                    status=status.HTTP_200_OK
                )
            except Exception as e:
                return Response(
                    {'detail': str(e)},
                    status=status.HTTP_400_BAD_REQUEST
                )
        else:
            return Response(
                serializer.errors,
                status=status.HTTP_400_BAD_REQUEST
            )


class DocShrinkageFinishAPIView(viewsets.GenericViewSet):
    serializer_class = DocShrinkageSerializer

    def get_Object(self, pk):
        instance = DocShrinkageBsn.get(pk)
        return instance

    def finish(self, request, pk):
        instance = self.get_Object(pk)
        try:
            record = DocShrinkageBsn.finish(instance, request.user)
            serializer = self.serializer_class(record)
            return Response(
                serializer.data,
                status=status.HTTP_200_OK
            )
        except Exception as e:
            return Response(
                {'detail': str(e)},
                status=status.HTTP_400_BAD_REQUEST
            )


class DocShrinkageUpdateDescAPIView(viewsets.GenericViewSet):
    serializer_class = DocShrinkageSerializer

    def get_Object(self, pk):
        instance = DocShrinkageBsn.get(pk)
        return instance

    def update(self, request, pk, *args, **kwargs):
        instance = self.get_Object(pk)
        serializer = DocShrinkageUpdateDescSerializer(data=request.data)
        if serializer.is_valid(raise_exception=False):
            try:
                data_list = serializer.validated_data
                record = DocShrinkageBsn.update_Desc(
                    instance,
                    data_list,
                    request.user
                )
                serializer = self.get_serializer(record)
                return Response(
                    serializer.data,
                    status=status.HTTP_200_OK
                )
            except Exception as e:
                return Response(
                    {'detail': str(e)},
                    status=status.HTTP_400_BAD_REQUEST
                )
        else:
            return Response(
                serializer.errors,
                status=status.HTTP_400_BAD_REQUEST
            )


class DocShrinkageListLinesAPIView(viewsets.GenericViewSet):
    serializer_class = DocShrinkageLineSerializer

    def get_QuerySet(self, pk, user):
        records = DocShrinkageBsn.get_Lines(pk, user)
        return records

    def list(self, request, pk, *args, **kwargs):
        queryset = self.filter_queryset(
            self.get_QuerySet(pk, user=request.user)
        )
        serializer = self.get_serializer(queryset, many=True)
        return Response(serializer.data)
