# Python's Libraries

# Django's Libraries
from django.contrib import admin

# Third-party Libraries

# Own's Libraries

from .models import RequisitionOrder
from .models import RequisitionOrderLine
# from .models import QuotationRequest
# from .models import QuotationResponse
from .models import PurchaseOrderLine
from .models import PurchaseOrder
from .models import Expense


class RequisitionOrderLineInLine(admin.StackedInline):
    model = RequisitionOrderLine
    extra = 1


@admin.register(RequisitionOrder)
class RequisitionOrderAdmin(admin.ModelAdmin):
    list_display = (
        'id',
        'warehouse',
        'requisited_by',
        'status',
    )
    search_fields = (
        'warehouse',
        'requisited_by',
        'status',
    )
    inlines = [RequisitionOrderLineInLine, ]


class PurchaseOrderLineInLine(admin.StackedInline):
    model = PurchaseOrderLine
    extra = 1


@admin.register(PurchaseOrder)
class PurchaseOrderAdmin(admin.ModelAdmin):
    list_display = (
        'id',
        'warehouse',
        'supplier',
        'status',
    )
    search_fields = (
        'warehouse',
        'supplier',
        'status',
    )
    inlines = [PurchaseOrderLineInLine, ]


@admin.register(Expense)
class ExpenseAdmin(admin.ModelAdmin):
    list_display = (
        'id',
        'company',
        'concept',
        'status',
        'amount',
    )
    search_fields = (
        'company',
        'concept',
        'status',
    )
