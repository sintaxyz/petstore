# Python's Libraries

# Django's Libraries
import django_filters

# Third-party Libraries

# Own's Libraries
from .models import RequisitionOrder
from .models import PurchaseOrder
from .models import Expense

from core.filters import assigned_warehouses
from core.filters import assigned_suppliers


class ExpenseFilter(django_filters.FilterSet):

    class Meta:
        model = Expense
        fields = [
            'key',
            'concept',
            'status',
        ]


class RequisitionOrderFilter(django_filters.FilterSet):

    class Meta:
        model = RequisitionOrder
        fields = [
            'warehouse',
            'requisited_by',
            'status',
        ]


# class QuotationFilter(django_filters.FilterSet):

#     class Meta:
#         model = Quotation
#         fields = [
#             'quoted_by',
#             'requisition',
#             'quotation_lines',
#         ]


class PurchaseOrderFilter(django_filters.FilterSet):

    warehouse = django_filters.ModelChoiceFilter(queryset=assigned_warehouses)
    supplier = django_filters.ModelChoiceFilter(queryset=assigned_suppliers)

    class Meta:
        model = PurchaseOrder
        fields = [
            'warehouse',
            'status',
            'supplier',
            'full_supplied',
        ]
