# Python's Libraries
# Django's Libraries
# Third-party Libraries
# Own's Libraries
from tools.forms import StxModelCreateForm

from .models import RequisitionOrder
# from .models import Quotation
from .models import PurchaseOrder
from .models import Expense

from .business import RequisitionOrderBsn
# from .business import QuotationBsn
from .business import PurchaseOrderBsn
from .business import ExpenseBsn


class RequisitionOrderForm(StxModelCreateForm):
    business_class = RequisitionOrderBsn

    class Meta:
        model = RequisitionOrder
        fields = [
            'warehouse',
            'requisited_by',
            'description',
            'comments',
            'date',
            'status',
            'qty_items',
        ]


# class QuotationForm(StxModelCreateForm):
#     business_class = QuotationBsn

#     class Meta:
#         model = Quotation
#         fields = [
#             'quoted_by',
#             'requisition',
#             'quotation_lines',
#         ]


class ExpenseForm(StxModelCreateForm):
    business_class = ExpenseBsn

    class Meta:
        model = Expense
        fields = [
            'company',
            'concept',
            'key',
            'comments',
            'amount',
            'status',
        ]


class PurchaseOrderForm(StxModelCreateForm):
    business_class = PurchaseOrderBsn

    class Meta:
        model = PurchaseOrder
        fields = [
            'warehouse',
            'date',
            'request_by',
            'approved_by',
            'canceled_by',
            'rejected_by',
            'approved_date',
            'canceled_date',
            'rejected_date',
            'cancellation_reason',
            'rejection_reason',
            'status',
            'description',
            'comments',
            'supplier',
            'qty_items',
            'total_amount',
        ]
