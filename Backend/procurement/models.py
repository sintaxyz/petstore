# Python's Libraries
from decimal import Decimal
import datetime

# Django's Libraries
from django.db import models
from django.urls import reverse_lazy

# Third-party Libraries

# Own's Libraries
from tools.models import AppModel
from tools.validators import MinValueValidator

from security.models import User
from security.models import Company

from core.models import Supplier
from core.models import Warehouse
from core.models import Tax
from inventory.models import StockItem


class RequisitionOrder(AppModel):
    STATUS_CHOICES = (
        ('PEN', 'PENDIENTE'),
        ('COT', 'COMPLETAMENTE COTIZADA'),
        ('PCT', 'PARCIALMENTE COTIZADA'),
        ('CAN', 'CANCELADA'),
    )
    warehouse = models.ForeignKey(
        Warehouse,
        verbose_name='almacén',
        related_name='in_requisition',
        on_delete=models.PROTECT,
        blank=False,
        null=True,
    )
    description = models.CharField(
        max_length=100,
        verbose_name='descripción',
        blank=True,
        null=True,
    )
    comments = models.TextField(
        verbose_name='comentarios',
        blank=True,
        null=True,
    )
    date = models.DateTimeField(
        verbose_name='fecha',
        blank=True,
        null=True,
    )
    requisited_by = models.ForeignKey(
        User,
        verbose_name='pedido por',
        related_name='has_requisitedby',
        on_delete=models.PROTECT,
        blank=False,
        null=True,
    )
    status = models.CharField(
        verbose_name='status',
        choices=STATUS_CHOICES,
        max_length=10,
        default='PEN',
        null=True,
        blank=False,
    )
    qty_items = models.DecimalField(
        verbose_name='Cant. Articulos',
        max_digits=10,
        decimal_places=2,
        default=0,
        validators=[MinValueValidator(Decimal('0'))],
        null=True,
        blank=False,
    )

    class Meta:
        verbose_name = 'Solicitud de compra'
        verbose_name_plural = 'Solicitudes de compra'

    @property
    def url_edit(self):
        url = reverse_lazy(
            'procurement:requisitionorder-edit',
            kwargs={'pk': self.pk}
        )
        return url

    @property
    def url_retrieve(self):
        url = reverse_lazy(
            'procurement:requisitionorder-retrieve',
            kwargs={'pk': self.pk}
        )
        return url

    def __str__(self):
        return f'{self.warehouse.name} - {self.pk}'


class RequisitionOrderLine(AppModel):
    order = models.ForeignKey(
        RequisitionOrder,
        verbose_name='orden',
        related_name='order_line',
        on_delete=models.CASCADE,
        blank=False,
        null=True,
    )
    line = models.SlugField(
        verbose_name='linea de la orden',
        max_length=1000,
        null=True,
        blank=True
    )
    item = models.ForeignKey(
        StockItem,
        verbose_name='artículo',
        related_name='in_requisitionlines',
        on_delete=models.PROTECT,
        blank=False,
        null=True,
    )
    qty = models.DecimalField(
        verbose_name='cantidad',
        max_digits=8,
        decimal_places=2,
        default=0,
        validators=[MinValueValidator(Decimal('0'))],
        blank=False,
        null=True,
    )
    comments = models.TextField(
        verbose_name='comentarios',
        blank=True,
        null=True,
    )

    class Meta:
        unique_together = (('order', 'line'))
        verbose_name = 'Linea de Solicitud Compra'
        verbose_name_plural = 'Lineas de Solicitud de Compra'

    def __str__(self):
        return f'{self.order.warehouse.name} - {self.pk}'


class PurchaseOrder(AppModel):
    STATUS_CHOICES = (
        ('INP', 'EN PROCESO'),
        ('PEN', 'PENDIENTE'),
        ('APR', 'APROBADA'),
        ('REJ', 'RECHAZADA'),
        ('CAN', 'CANCELADA'),
    )
    PAYMETHOD_CHOICES = (
        ('CA', 'Efectivo'),
        ('TA', 'Tarjeta'),
        ('CR', 'Crédito'),
    )
    description = models.CharField(
        max_length=100,
        verbose_name='descripción',
        blank=True,
        null=True,
    )
    comments = models.TextField(
        verbose_name='comentarios',
        blank=True,
        null=True,
    )
    warehouse = models.ForeignKey(
        Warehouse,
        verbose_name='almacén',
        related_name='in_purchaseorder',
        on_delete=models.PROTECT,
        blank=False,
        null=True,
    )
    supplier = models.ForeignKey(
        Supplier,
        verbose_name='proveedor',
        related_name='in_purchaseorder',
        on_delete=models.PROTECT,
        blank=False,
        null=True,
    )
    status = models.CharField(
        verbose_name='Estado',
        choices=STATUS_CHOICES,
        max_length=3,
        default='INP',
        blank=False,
    )
    paymethod = models.CharField(
        verbose_name='metodo de pago',
        max_length=3,
        choices=PAYMETHOD_CHOICES,
        default='CA',
        blank=True,
        null=True,
    )
    credit_days = models.SmallIntegerField(
        verbose_name='dias de credito',
        default=0,
        validators=[MinValueValidator(Decimal('0'))],
        blank=True,
        null=True,
    )
    tax = models.ForeignKey(
        Tax,
        verbose_name='impuestos',
        related_name='in_orders',
        on_delete=models.PROTECT,
        blank=False,
        null=True
    )
    qty_items = models.DecimalField(
        verbose_name='Cant. Articulos',
        max_digits=10,
        decimal_places=2,
        default=0,
        validators=[MinValueValidator(Decimal('0'))],
        null=True,
        blank=True,
    )
    total_amount = models.DecimalField(
        verbose_name='Cant. Total de Articulos',
        max_digits=12,
        decimal_places=2,
        default=0,
        validators=[MinValueValidator(Decimal('0'))],
        null=True,
        blank=False,
    )
    amount_outstanding = models.DecimalField(
        verbose_name='Pendiente por pagar',
        max_digits=12,
        decimal_places=2,
        default=0,
        validators=[MinValueValidator(
            Decimal('0'),
            "El valor de monto pendiente por pagar no puede ser menor a 0")],
        null=True,
        blank=False,
    )
    date = models.DateTimeField(
        verbose_name='fecha',
        blank=True,
        null=True,
    )
    request_by = models.ForeignKey(
        User,
        verbose_name='solicitada por',
        related_name='orders_requested',
        on_delete=models.PROTECT,
        blank=False,
        null=True,
    )
    approved_date = models.DateTimeField(
        verbose_name='fecha de aprobación',
        blank=True,
        null=True,
    )
    approved_by = models.ForeignKey(
        User,
        verbose_name='aprobada por',
        related_name='orders_approved',
        on_delete=models.PROTECT,
        blank=True,
        null=True,
    )
    canceled_date = models.DateTimeField(
        verbose_name='fecha de cancelacion',
        blank=True,
        null=True,
    )
    canceled_by = models.ForeignKey(
        User,
        verbose_name='cancelada por',
        related_name='orders_canceled',
        on_delete=models.PROTECT,
        blank=True,
        null=True,
    )
    cancellation_reason = models.TextField(
        verbose_name='motivo de cancelación',
        blank=True,
        null=True,
    )
    rejected_date = models.DateTimeField(
        verbose_name='fecha de rechazo',
        blank=True,
        null=True,
    )
    rejected_by = models.ForeignKey(
        User,
        verbose_name='rechazada por',
        related_name='orders_rejected',
        on_delete=models.PROTECT,
        blank=True,
        null=True,
    )
    rejection_reason = models.TextField(
        verbose_name='motivo de rechazo',
        blank=True,
        null=True,
    )
    full_supplied = models.BooleanField(
        verbose_name='Surtida Totalmente',
        default=False,
    )

    class Meta:
        verbose_name = 'Order de Compra'
        verbose_name_plural = 'Orderes de Compra'

    @property
    def url_retrieve(self):
        url = reverse_lazy(
            'procurement:purchaseorder-retrieve',
            kwargs={'pk': self.pk}
        )
        return url

    @property
    def url_edit(self):
        url = reverse_lazy(
            'procurement:purchaseorder-edit',
            kwargs={'pk': self.pk}
        )
        return url

    @property
    def status_text(self):
        choices = {
            'INP': 'EN PROCESO',
            'PEN': 'PENDIENTE',
            'APR': 'APROBADA',
            'REJ': 'RECHAZADA',
            'CAN': 'CANCELADA',
        }
        return choices.get(self.status, 'No definido')

    @property
    def status_color(self):
        choices = {
            'INP': 'gray',
            'PEN': 'yellow',
            'APR': 'green',
            'REJ': 'red',
            'CAN': 'red',
        }
        return choices.get(self.status, 'gray')

    @property
    def subtotal_price(self):
        record_value = 0
        if self.order_line.exists() is True:
            record = Decimal(record_value)
            for line in self.order_line.all():
                record += Decimal(line.subtotal_price)
            return record
        else:
            return record_value

    @property
    def total_tax(self):
        record_value = 0
        if self.order_line.exists() is True:
            record = Decimal(record_value)
            for line in self.order_line.all():
                record += Decimal(line.total_tax)
            return record
        else:
            return record_value

    def __str__(self):
        return f'{self.warehouse.name} - {self.pk}'

    @property
    def tax_value(self):
        if self.tax:
            record = format(
                Decimal(self.tax.factor * 100), '.2f')
            return record

    @property
    def total_item(self):

        if self.tax:
            record = format(
                Decimal(self.tax.factor + self.subtotal_price), '.2f')
            return record


class PurchaseOrderLine(AppModel):
    order = models.ForeignKey(
        PurchaseOrder,
        verbose_name='orden',
        related_name='order_line',
        on_delete=models.CASCADE,
        blank=False,
        null=True,
    )
    line = models.SlugField(
        verbose_name='linea de la orden',
        max_length=1000,
        null=True,
        blank=True
    )
    item = models.ForeignKey(
        StockItem,
        verbose_name='artículo',
        related_name='in_orderline',
        on_delete=models.PROTECT,
        blank=False,
        null=True,
    )
    delivery_date = models.DateTimeField(
        verbose_name='Fecha de entrega',
        null=True,
        blank=True
    )
    qty = models.DecimalField(
        verbose_name='cantidad',
        max_digits=8,
        decimal_places=2,
        default=0,
        validators=[MinValueValidator(Decimal('0'))],
        null=True,
        blank=False
    )
    price = models.DecimalField(
        verbose_name='Costo unitario',
        max_digits=12,
        decimal_places=2,
        blank=False,
        null=True,
    )
    tax = models.ForeignKey(
        Tax,
        verbose_name='impuestos',
        related_name='in_orderlines',
        on_delete=models.PROTECT,
        blank=True,
        null=True,
    )
    total_amount = models.DecimalField(
        verbose_name='total',
        max_digits=12,
        decimal_places=2,
        blank=False,
        null=True,
    )
    comments = models.TextField(
        verbose_name='comentarios',
        blank=True,
        null=True,
    )
    qty_pending = models.DecimalField(
        verbose_name='cantidad pendiente orden',
        max_digits=8,
        decimal_places=2,
        default=0,
        validators=[MinValueValidator(Decimal('0'))],
        null=True,
        blank=False
    )

    class Meta:
        unique_together = (('order', 'line'))
        verbose_name = 'Linea de Order de compra'
        verbose_name_plural = 'Lineas de Ordene de Compra'

    @property
    def subtotal_price(self):
        record = format(self.price * self.qty, '.2f')
        return record

    @property
    def total_tax(self):
        if self.tax:
            record = format(
                Decimal(self.subtotal_price) * (self.tax.factor / 100), '.2f')
            return record
        else:
            record = 0
            return record

    def __str__(self):
        return f'{self.order.warehouse.name} - {self.pk}:{self.line}'

    @property
    def tax_value(self):
        if self.tax:
            record = format(
                Decimal(self.tax.factor * 100), '.2f')
            return record


class Expense(AppModel):
    STATUS_CHOICES = (
        ('INP', 'En proceso'),
        ('CO', 'Confirmado'),
        ('CA', 'Cancelado'),
    )
    CONCEPT_CHOICES = (
        ('PUO', 'Orden de Compra'),
        ('PAR', 'Nómina'),
        ('LIS', 'Servicio de Luz'),
        ('WAS', 'Servicio de Agua'),
        ('OTH', 'Otros'),
    )
    company = models.ForeignKey(
        Company,
        verbose_name='compañia',
        related_name='expenses',
        on_delete=models.CASCADE,
        blank=True,
        null=True,
    )
    concept = models.CharField(
        verbose_name='concepto',
        max_length=3,
        choices=CONCEPT_CHOICES,
        default='PUO',
        blank=False,
        null=True,
    )
    key = models.CharField(
        verbose_name='clave',
        max_length=50,
        blank=False,
        null=True,
    )
    comments = models.TextField(
        verbose_name='comentarios',
        blank=True,
        null=True,
    )
    amount = models.DecimalField(
        verbose_name='cantidad',
        max_digits=12,
        decimal_places=2,
        default=0,
        validators=[MinValueValidator(Decimal('0'))],
        blank=False,
        null=True,
    )
    date = models.DateTimeField(
        verbose_name='Fecha',
        default=datetime.datetime.now
    )
    status = models.CharField(
        verbose_name='estado',
        max_length=3,
        choices=STATUS_CHOICES,
        default='CO',
        blank=False,
        null=True,
    )

    class Meta:
        verbose_name = 'Egreso'
        verbose_name_plural = 'Egresos'

    def __str__(self):
        return f'{self.id}: ${self.amount}'

    @property
    def status_text(self):
        choices = {
            'INP': 'En proceso',
            'CO': 'Confirmado',
            'CA': 'Cancelado'
        }
        return choices.get(self.status, 'No definido')

    @property
    def status_color(self):
        choices = {
            'INP': 'yellow',
            'CO': 'green',
            'CA': 'red'
        }
        return choices.get(self.status, 'gray')

    @property
    def url_retrieve(self):
        url = reverse_lazy(
            'procurement:expense-retrieve',
            kwargs={'pk': self.pk}
        )
        return url

    @property
    def url_edit(self):
        url = reverse_lazy(
            'procurement:expense-edit',
            kwargs={'pk': self.pk}
        )
        return url
