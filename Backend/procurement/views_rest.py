# Python's Libraries

# Django's Libraries
from django.db import transaction
# from django_filters.rest_framework import DjangoFilterBackend

# Third-party Libraries
from rest_framework import viewsets
from rest_framework import status
from rest_framework.response import Response
from rest_framework.permissions import IsAuthenticated
# from rest_framework.permissions import AllowAny

# Own's Libraries
from security.business import CompanyBsn
from .business import RequisitionOrderBsn
from .business import PurchaseOrderBsn
from .business import ExpenseBsn

from .serializers import RequisitionOrderLineSerializer
from .serializers import RequisitionOrderSerializer
from .serializers import RequisitionOrderUpdateSerializer
from .serializers import PurchaseOrderLineSerializer
from .serializers import PurchaseOrderSerializer
from .serializers import PurchaseOrderRetrieveSerializer
from .serializers import PurchaseOrderUpdateSerializer
from .serializers import PurchaseOrderUpdateDescSerializer
from .serializers import ExpenseSerializer
from .serializers import ExpensesDashboardSerializer


class RequisitionListAPIView(viewsets.GenericViewSet):
    serializer_class = RequisitionOrderSerializer

    def get_QuerySet(self, user):
        records = RequisitionOrderBsn.get_RequisitionList(user)
        return records

    def list(self, request, *args, **kwargs):
        queryset = self.filter_queryset(
            self.get_QuerySet(user=request.user)
        )
        serializer = self.get_serializer(queryset, many=True)
        return Response(serializer.data)

    def create(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        if serializer.is_valid(raise_exception=False):
            try:
                data_list = serializer.validated_data
                with transaction.atomic():
                    record = RequisitionOrderBsn.create(
                        data_list, request.user)
                serializer = RequisitionOrderSerializer(record)
                return Response(
                    serializer.data,
                    status=status.HTTP_201_CREATED
                )
            except Exception as e:
                return Response(
                    {'detail': str(e)},
                    status=status.HTTP_400_BAD_REQUEST
                )
                print(e)
        else:
            return Response(
                serializer.errors,
                status=status.HTTP_400_BAD_REQUEST
            )


class RequisitionRetrieveAPIView(viewsets.GenericViewSet):
    serializer_class = RequisitionOrderSerializer

    def get_Object(self, pk):
        instance = RequisitionOrderBsn.get_Requisition(pk)
        return instance

    def retrieve(self, request, pk, *args, **kwargs):
        instance = self.get_Object(pk)
        serializer = self.get_serializer(instance)
        return Response(serializer.data)

    def update(self, request, pk, *args, **kwargs):
        instance = self.get_Object(pk)
        serializer = RequisitionOrderUpdateSerializer(data=request.data)
        if serializer.is_valid(raise_exception=False):
            try:
                data_list = serializer.validated_data
                record = RequisitionOrderBsn.update_Requisition(
                    instance,
                    data_list,
                    request.user
                )
                serializer = RequisitionOrderUpdateSerializer(record)
                return Response(
                    serializer.data,
                    status=status.HTTP_200_OK
                )
            except Exception as e:
                return Response(
                    {'detail': str(e)},
                    status=status.HTTP_400_BAD_REQUEST
                )
        else:
            return Response(
                serializer.errors,
                status=status.HTTP_400_BAD_REQUEST
            )

    def create(self, request, pk, *args, **kwargs):
        instance = self.get_Object(pk)
        serializer = RequisitionOrderLineSerializer(data=request.data)
        if serializer.is_valid(raise_exception=False):
            try:
                data_list = serializer.validated_data
                with transaction.atomic():
                    record = RequisitionOrderBsn.create_RequisitionLine(
                        instance,
                        data_list,
                        request.user
                    )
                    serializer = RequisitionOrderLineSerializer(record)
                return Response(
                    serializer.data,
                    status=status.HTTP_200_OK
                )
            except Exception as e:
                return Response(
                    {'detail': str(e)},
                    status=status.HTTP_400_BAD_REQUEST
                )
        else:
            return Response(
                serializer.errors,
                status=status.HTTP_400_BAD_REQUEST
            )


class RequisitionLinesUpdateAPIView(viewsets.GenericViewSet):
    serializer_class = RequisitionOrderLineSerializer

    def get_Object(self, pk, line_pk):
        instance = RequisitionOrderBsn.get_RequisitionLine(
            pk, line_pk
            )
        return instance

    def update(self, request, pk, line_pk, *args, **kwargs):
        instance = self.get_Object(pk, line_pk)
        serializer = self.get_serializer(data=request.data)
        if serializer.is_valid(raise_exception=False):
            try:
                data_list = serializer.validated_data
                record = RequisitionOrderBsn.update_RequisitionLine(
                    instance,
                    data_list,
                    request.user
                )
                serializer = self.get_serializer(record)
                return Response(
                    serializer.data,
                    status=status.HTTP_200_OK
                )
            except Exception as e:
                return Response(
                    {'detail': str(e)},
                    status=status.HTTP_400_BAD_REQUEST
                )
        else:
            return Response(
                serializer.errors,
                status=status.HTTP_400_BAD_REQUEST
            )


class PurchaseOrderListAPIView(viewsets.GenericViewSet):
    serializer_class = PurchaseOrderSerializer

    def get_QuerySet(self, user):
        records = PurchaseOrderBsn.get_PurchaseList(user)
        return records

    def list(self, request, *args, **kwargs):
        queryset = self.filter_queryset(
            self.get_QuerySet(request.user)
        )
        serializer = PurchaseOrderRetrieveSerializer(queryset, many=True)
        return Response(serializer.data)

    def create(self, request, *args, **kwargs):
        serializer = self.serializer_class(data=request.data)
        if serializer.is_valid(raise_exception=False):
            try:
                data_list = serializer.validated_data
                with transaction.atomic():
                    record = PurchaseOrderBsn.create(
                        data_list, request.user)
                serializer = PurchaseOrderSerializer(record)
                return Response(
                    serializer.data,
                    status=status.HTTP_201_CREATED
                )
            except Exception as e:
                return Response(
                    {'detail': str(e)},
                    status=status.HTTP_400_BAD_REQUEST
                )
                print(e)
        else:
            return Response(
                serializer.errors,
                status=status.HTTP_400_BAD_REQUEST
            )


class PurchaseOrderListApprovedAPIView(viewsets.GenericViewSet):
    serializer_class = PurchaseOrderSerializer

    def get_QuerySet(self, user):
        records = PurchaseOrderBsn.get_Approved(user)
        return records

    def list(self, request, *args, **kwargs):
        queryset = self.filter_queryset(
            self.get_QuerySet(request.user)
        )
        serializer = PurchaseOrderRetrieveSerializer(queryset, many=True)
        return Response(serializer.data)


class PurchaseOrderRetrieveAPIView(viewsets.GenericViewSet):
    serializer_class = PurchaseOrderRetrieveSerializer

    def get_Object(self, pk):
        instance = PurchaseOrderBsn.get_Purchase(pk)
        return instance

    def retrieve(self, request, pk, *args, **kwargs):
        instance = self.get_Object(pk,)
        serializer = self.get_serializer(instance)
        return Response(serializer.data)

    def update(self, request, pk, *args, **kwargs):
        instance = self.get_Object(pk)
        serializer = PurchaseOrderUpdateSerializer(data=request.data)
        if serializer.is_valid(raise_exception=False):
            try:
                data_list = serializer.validated_data
                record = PurchaseOrderBsn.update_Purchase(
                    instance,
                    data_list,
                    request.user
                )
                serializer = PurchaseOrderUpdateSerializer(record)
                return Response(
                    serializer.data,
                    status=status.HTTP_200_OK
                )
            except Exception as e:
                return Response(
                    {'detail': str(e)},
                    status=status.HTTP_400_BAD_REQUEST
                )
        else:
            return Response(
                serializer.errors,
                status=status.HTTP_400_BAD_REQUEST
            )

    def create(self, request, pk, *args, **kwargs):
        instance = self.get_Object(pk)
        serializer = PurchaseOrderLineSerializer(data=request.data)
        if serializer.is_valid(raise_exception=False):
            try:
                data_list = serializer.validated_data
                data_list['order'] = instance
                with transaction.atomic():
                    record = PurchaseOrderBsn.create_PurchaseLine(
                        data_list,
                        request.user
                    )
                    serializer = PurchaseOrderLineSerializer(record)
                return Response(
                    serializer.data,
                    status=status.HTTP_200_OK
                )
            except Exception as e:
                return Response(
                    {'detail': str(e)},
                    status=status.HTTP_400_BAD_REQUEST
                )
        else:
            return Response(
                serializer.errors,
                status=status.HTTP_400_BAD_REQUEST
            )


class PurchaseOrderUpdateDescAPIView(viewsets.GenericViewSet):
    serializer_class = PurchaseOrderSerializer

    def get_Object(self, pk):
        instance = PurchaseOrderBsn.get(pk)
        return instance

    def update(self, request, pk):
        instance = self.get_Object(pk)
        serializer = self.serializer_class(data=request.data)
        if serializer.is_valid(raise_exception=False):
            data_list = serializer.validated_data
            try:
                record = PurchaseOrderBsn.update_Desc(
                    instance, data_list, request.user)
                serializer = PurchaseOrderSerializer(record)
                return Response(
                    serializer.data,
                    status=status.HTTP_200_OK
                )
            except Exception as e:
                return Response(
                    {'detail': str(e)},
                    status=status.HTTP_400_BAD_REQUEST
                )
        else:
            return Response(
                serializer.errors,
                status=status.HTTP_400_BAD_REQUEST
            )


class PurchaseOrderApproveAPIView(viewsets.GenericViewSet):
    serializer_class = PurchaseOrderUpdateDescSerializer

    def get_Object(self, pk):
        instance = PurchaseOrderBsn.get(pk)
        return instance

    def approve(self, request, pk):
        instance = self.get_Object(pk)
        try:
            record = PurchaseOrderBsn.approve(
                instance, request.user)
            serializer = PurchaseOrderSerializer(record)
            return Response(
                serializer.data,
                status=status.HTTP_200_OK
            )
        except Exception as e:
            return Response(
                {'detail': str(e)},
                status=status.HTTP_400_BAD_REQUEST
            )


class PurchaseOrderCancelAPIView(viewsets.GenericViewSet):
    serializer_class = PurchaseOrderUpdateDescSerializer

    def get_Object(self, pk):
        instance = PurchaseOrderBsn.get(pk)
        return instance

    def cancel(self, request, pk):
        instance = self.get_Object(pk)
        try:
            record = PurchaseOrderBsn.cancel(
                instance, request.user)
            serializer = PurchaseOrderSerializer(record)
            return Response(
                serializer.data,
                status=status.HTTP_200_OK
            )
        except Exception as e:
            return Response(
                {'detail': str(e)},
                status=status.HTTP_400_BAD_REQUEST
            )
        

class PurchaseOrderLinesUpdateAPIView(viewsets.GenericViewSet):
    serializer_class = PurchaseOrderLineSerializer

    def get_Object(self, pk, line_pk):
        instance = PurchaseOrderBsn.get_PurchaseLine(pk, line_pk)
        return instance

    def update(self, request, pk, line_pk, *args, **kwargs):
        instance = self.get_Object(pk, line_pk)
        serializer = self.get_serializer(data=request.data)
        if serializer.is_valid(raise_exception=False):
            try:
                data_list = serializer.validated_data
                record = PurchaseOrderBsn.update_PurchaseLine(
                    instance,
                    data_list,
                    request.user
                )
                serializer = self.get_serializer(record)
                return Response(
                    serializer.data,
                    status=status.HTTP_200_OK
                )
            except Exception as e:
                return Response(
                    {'detail': str(e)},
                    status=status.HTTP_400_BAD_REQUEST
                )
        else:
            return Response(
                serializer.errors,
                status=status.HTTP_400_BAD_REQUEST
            )


class ExpenseListAPIView(viewsets.GenericViewSet):
    serializer_class = ExpenseSerializer

    def get_QuerySet(self, user):
        records = ExpenseBsn.get_Assigned()
        return records

    def list(self, request, *args, **kwargs):
        queryset = self.filter_queryset(
            self.get_QuerySet(user=request.user)
        )
        serializer = ExpenseSerializer(queryset, many=True)
        return Response(serializer.data)

    def create(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        if serializer.is_valid(raise_exception=False):
            try:
                data_list = serializer.validated_data
                with transaction.atomic():
                    record = ExpenseBsn.create(
                        data_list, request.user)
                serializer = ExpenseSerializer(record)
                return Response(
                    serializer.data,
                    status=status.HTTP_201_CREATED
                )
            except Exception as e:
                return Response(
                    {'detail': str(e)},
                    status=status.HTTP_400_BAD_REQUEST
                )
                print(e)
        else:
            return Response(
                serializer.errors,
                status=status.HTTP_400_BAD_REQUEST
            )


class ExpensesInDaysAPIView(viewsets.GenericViewSet):
    queryset = ''
    serializer_class = ExpensesDashboardSerializer

    def get_queryset(self, user, request):
        records = ExpenseBsn.get_ExpensesInDays(user, request)
        return records

    def list(self, request, *args, **kwargs):
        queryset = self.get_queryset(request.user, request)
        serializer = self.serializer_class(queryset)
        return Response(serializer.data)
