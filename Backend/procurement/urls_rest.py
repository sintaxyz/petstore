# Django's Libraries
from django.urls import path

# Own's Libraries
from .views_rest import RequisitionListAPIView
from .views_rest import RequisitionRetrieveAPIView
from .views_rest import RequisitionLinesUpdateAPIView
from .views_rest import PurchaseOrderListAPIView
from .views_rest import PurchaseOrderRetrieveAPIView
from .views_rest import PurchaseOrderLinesUpdateAPIView
from .views_rest import PurchaseOrderUpdateDescAPIView
from .views_rest import PurchaseOrderApproveAPIView
from .views_rest import PurchaseOrderCancelAPIView
from .views_rest import PurchaseOrderListApprovedAPIView
from .views_rest import ExpenseListAPIView
from .views_rest import ExpensesInDaysAPIView

app_name = 'procurement-api'

urlpatterns = [
    path(
        'requisitions/',
        RequisitionListAPIView.as_view({
            'get': 'list',
            'post': 'create',
        }),
        name='requisitionorder-list'
    ),
    path(
        'requisitions/<int:pk>/',
        RequisitionRetrieveAPIView.as_view({
            'get': 'retrieve',
            'put': 'update',
            'post': 'create',
        }),
        name='requisitionorder-retrieve'
    ),
    path(
        'requisitions/<int:pk>/lines/<int:line_pk>/',
        RequisitionLinesUpdateAPIView.as_view({
            'put': 'update',
        }),
        name='requisitionline-update'
    ),
    path(
        'purchaseorders/',
        PurchaseOrderListAPIView.as_view({
            'get': 'list',
            'post': 'create',
        }),
        name='purchaseorder-list'
    ),
    path(
        'purchaseorders/approved/',
        PurchaseOrderListApprovedAPIView.as_view({
            'get': 'list',
        }),
        name='purchaseorder-list-approved'
    ),
    path(
        'purchaseorders/<int:pk>/',
        PurchaseOrderRetrieveAPIView.as_view({
            'get': 'retrieve',
            'put': 'update',
            'post': 'create',
        }),
        name='purchaseorder-retrieve'
    ),
    path(
        'purchaseorders/<int:pk>/desc/',
        PurchaseOrderUpdateDescAPIView.as_view({
            'put': 'update',
        }),
        name='purchaseorder-update-desc'
    ),
    path(
        'purchaseorders/<int:pk>/approve/',
        PurchaseOrderApproveAPIView.as_view({
            'get': 'approve',
        }),
        name='purchaseorder-approve'
    ),
    path(
        'purchaseorders/<int:pk>/cancel/',
        PurchaseOrderCancelAPIView.as_view({
            'get': 'cancel',
        }),
        name='purchaseorder-cancel'
    ),
    path(
        'purchaseorders/<int:pk>/lines/<int:line_pk>/',
        PurchaseOrderLinesUpdateAPIView.as_view({
            'put': 'update',
        }),
        name='purchaseorderline-update'
    ),
    path(
        'expenses/',
        ExpenseListAPIView.as_view({
            'get': 'list',
            'post': 'create',
        }),
        name='expense-list'
    ),
    path(
        'expenses/indays/',
        ExpensesInDaysAPIView.as_view({
            'get': 'list',
        }),
        name='expenses-indays'
    ),
]
