# Python Libraries
import datetime

# Third Party Libraries
from import_export import resources
from import_export import fields
from import_export.widgets import ForeignKeyWidget
from import_export.widgets import ManyToManyWidget
from import_export.widgets import IntegerWidget
from import_export.widgets import DecimalWidget
from import_export.widgets import DateWidget
from import_export.widgets import Widget

# Own Libraries
from .models import RequisitionOrder
from .models import RequisitionOrderLine
from .models import PurchaseOrder
from .models import PurchaseOrderLine
from .models import Expense
from core.models import Warehouse
from core.models import StockItem
from core.models import Supplier
from core.models import Tax
from security.models import Company
from security.models import User
from tools.resources import BooleanWidget
from tools.resources import ChoicesWidget
from tools.resources import GenericResource


class RequisitionOrderResource(GenericResource):

    id = fields.Field(
        column_name='Folio',
        attribute='id',
        widget=IntegerWidget()
    )
    warehouse = fields.Field(
        column_name='almacén',
        attribute='warehouse',
        widget=ForeignKeyWidget(Warehouse, 'name')
    )
    description = fields.Field(
        column_name='descripción',
        attribute='description',
    )
    comments = fields.Field(
        column_name='comentarios',
        attribute='comments',
    )
    date = fields.Field(
        column_name='fecha',
        attribute='date',
        widget=DateWidget()
    )
    requisited_by = fields.Field(
        column_name='pedido por',
        attribute='requisited_by',
        widget=ForeignKeyWidget(User, 'name')
    )
    status = fields.Field(
        column_name='estado',
        attribute='status',
        widget=ChoicesWidget(choices=RequisitionOrder.STATUS_CHOICES)
    )
    qty_items = fields.Field(
        column_name='Cant. Articulos',
        attribute='qty_items',
        widget=DecimalWidget()
    )

    class Meta:
        model = RequisitionOrder
        skip_unchanged = True
        report_skipped = True
        fields = (
            'warehouse',
            'description ',
            'comments',
            'date',
            'requisited_by  ',
            'status',
            'qty_items',
        )
        export_order = (
            'warehouse',
            'description ',
            'comments',
            'date',
            'requisited_by  ',
            'status',
            'qty_items',
        )


class RequisitionOrderLineResource(GenericResource):

    id = fields.Field(
        column_name='Folio',
        attribute='id',
        widget=IntegerWidget()
    )
    order = fields.Field(
        column_name='orden',
        attribute='order',
        widget=ForeignKeyWidget(RequisitionOrder, 'name')
    )
    line = fields.Field(
        column_name='linea de la orden',
        attribute='line',
    )
    item = fields.Field(
        column_name='artículo',
        attribute='item',
        widget=ForeignKeyWidget(StockItem, 'name')
    )
    qty = fields.Field(
        column_name='cantidad',
        attribute='qty',
        widget=DecimalWidget()
    )
    comments = fields.Field(
        column_name='comentarios',
        attribute='comments',
    )

    class Meta:
        model = RequisitionOrderLine
        skip_unchanged = True
        report_skipped = True
        fields = (
            'order',
            'line',
            'item',
            'qty',
        )
        export_order = (
            'order',
            'line',
            'item',
            'qty',
        )


class PurchaseOrderResource(GenericResource):
    id = fields.Field(
        column_name='Folio',
        attribute='id',
        widget=IntegerWidget()
    )
    description = fields.Field(
        column_name='descripción',
        attribute='description',
    )
    comments = fields.Field(
        column_name='comentarios',
        attribute='comments',
    )
    warehouse = fields.Field(
        column_name='almacén',
        attribute='warehouse',
        widget=ForeignKeyWidget(Warehouse, 'id')
    )
    supplier = fields.Field(
        column_name='proveedor',
        attribute='supplier',
        widget=ForeignKeyWidget(Supplier, 'id')
    )
    status = fields.Field(
        column_name='Estado',
        attribute='status',
        widget=ChoicesWidget(choices=PurchaseOrder.STATUS_CHOICES)
    )
    paymethod = fields.Field(
        column_name='metodo de pago',
        attribute='paymethod',
        widget=ChoicesWidget(choices=PurchaseOrder.PAYMETHOD_CHOICES)
    )
    credit_days = fields.Field(
        column_name='dias de credito',
        attribute='credit_days',
        widget=IntegerWidget()
    )
    tax = fields.Field(
        column_name='impuestos',
        attribute='tax',
        widget=ForeignKeyWidget(Tax, 'factor')
    )
    qty_items = fields.Field(
        column_name='Cant. Articulos',
        attribute='qty_items',
        widget=DecimalWidget()
    )
    total_amount = fields.Field(
        column_name='Cant. Total de Articulos',
        attribute='total_amount',
        widget=DecimalWidget()
    )
    date = fields.Field(
        column_name='fecha',
        attribute='date',
        widget=DateWidget()
    )
    request_by = fields.Field(
        column_name='solicitada por',
        attribute='request_by',
        widget=ForeignKeyWidget(User, 'id')
    )
    approved_date = fields.Field(
        column_name='fecha de aprobación',
        attribute='approved_date',
        widget=DateWidget()
    )
    approved_by = fields.Field(
        column_name='aprovada por',
        attribute='approved_by',
        widget=ForeignKeyWidget(User, 'id')
    )
    canceled_date = fields.Field(
        column_name='fecha de cancelacion',
        attribute='canceled_date',
        widget=DateWidget()
    )
    canceled_by = fields.Field(
        column_name='cancelada por',
        attribute='canceled_by',
        widget=ForeignKeyWidget(User, 'id')
    )
    cancellation_reason = fields.Field(
        column_name='motivo de cancelación',
        attribute='cancellation_reason',
    )
    rejected_date = fields.Field(
        column_name='fecha de rechazo',
        attribute='rejected_date',
        widget=DateWidget()
    )
    rejected_by = fields.Field(
        column_name='rechazada por',
        attribute='rejected_by',
        widget=ForeignKeyWidget(User, 'id')
    )
    rejection_reason = fields.Field(
        column_name='motivo de rechazo',
        attribute='rejection_reason',
    )
    full_supplied = fields.Field(
        column_name='Surtida Totalmente',
        attribute='full_supplied',
        widget=BooleanWidget()
    )

    class Meta:
        model = PurchaseOrder
        skip_unchanged = True
        report_skipped = True
        fields = (
            'id',
            'warehouse',
            'supplier',
            'status',
            'paymethod',
            'credit_days',
            'tax',
            'qty_items',
            'total_amount',
            'date',
            'request_by',
            'approved_date',
            'approved_by',
            'canceled_date',
            'canceled_by',
            'cancellation_reason',
            'rejected_date',
            'rejected_by',
            'rejection_reason',
            'full_supplied',
        )
        export_order = (
            'id',
            'warehouse',
            'supplier',
            'status',
            'paymethod',
            'credit_days',
            'tax',
            'qty_items',
            'total_amount',
            'date',
            'request_by',
            'approved_date',
            'approved_by',
            'canceled_date',
            'canceled_by',
            'cancellation_reason',
            'rejected_date',
            'rejected_by',
            'rejection_reason',
            'full_supplied',
        )


class PurchaseOrderLineResource(GenericResource):

    id = fields.Field(
        column_name='ID',
        attribute='id',
        widget=IntegerWidget()
    )
    order = fields.Field(
        column_name='ID Orden de compra',
        attribute='order',
        widget=ForeignKeyWidget(RequisitionOrderLine, 'id')
    )
    line = fields.Field(
        column_name='linea de la orden',
        attribute='line',
    )
    item = fields.Field(
        column_name='artículo',
        attribute='item',
        widget=ForeignKeyWidget(StockItem, 'id')
    )
    delivery_date = fields.Field(
        column_name='Fecha de entrega',
        attribute='delivery_date',
    )
    qty = fields.Field(
        column_name='cantidad',
        attribute='qty',
        widget=DecimalWidget()
    )
    price = fields.Field(
        column_name='Costo unitario',
        attribute='price',
        widget=DecimalWidget()
    )
    tax = fields.Field(
        column_name='impuestos',
        attribute='tax',
        widget=ForeignKeyWidget(Tax, 'factor')
    )
    total_amount = fields.Field(
        column_name='total',
        attribute='price',
        widget=DecimalWidget()
    )
    comments = fields.Field(
        column_name='comentarios',
        attribute='comments',
    )
    qty_pending = fields.Field(
        column_name='cantidad pendiente orden',
        attribute='qty_pending',
        widget=DecimalWidget()
    )

    class Meta:
        model = PurchaseOrderLine
        skip_unchanged = True
        report_skipped = True
        fields = (
            'order',
            'line',
            'item',
            'delivery_date',
            'qty',
            'price',
            'tax',
            'total_amount',
            'qty_pending',
        )
        export_order = (
            'order',
            'line',
            'item',
            'delivery_date',
            'qty',
            'price',
            'tax',
            'total_amount',
            'qty_pending',
        )


class ExpenseResource(GenericResource):

    id = fields.Field(
        column_name='ID',
        attribute='id',
        widget=IntegerWidget()
    )
    company = fields.Field(
        column_name='compañia',
        attribute='company',
        widget=ForeignKeyWidget(Company, 'id')
    )
    concept = fields.Field(
        column_name='concepto',
        attribute='concept',
        widget=ChoicesWidget(choices=Expense.CONCEPT_CHOICES)
    )
    date = fields.Field(
        column_name='Fecha',
        attribute='date',
        widget=DateWidget()
    )
    key = fields.Field(
        column_name='clave',
        attribute='key',
    )
    comments = fields.Field(
        column_name='comentarios',
        attribute='comments',
    )
    amount = fields.Field(
        column_name='cantidad',
        attribute='amount',
        widget=DecimalWidget()
    )
    status = fields.Field(
        column_name='estado',
        attribute='status',
        widget=ChoicesWidget(choices=Expense.STATUS_CHOICES)
    )

    class Meta:
        model = Expense
        skip_unchanged = True
        report_skipped = True
        import_id_fields = ['id', ]
        fields = (
            'id',
            'company',
            'concept',
            'key',
            'comments',
            'amount',
            'date',
            'status',
        )
        export_order = (
            'id',
            'company',
            'concept',
            'key',
            'comments',
            'amount',
            'date',
            'status',
        )

