# Python's Libraries

# DJango's Libraries
from django.urls import path

# Third-party Libraries

# Own's Libraries
from .views import RequisitionOrderListView
from .views import RequisitionOrderNewView
from .views import RequisitionOrderEditView
from .views import RequisitionOrderRetrieveView
from .views import PurchaseOrderListView
from .views import PurchaseOrderNewView
from .views import PurchaseOrderEditView
from .views import PurchaseOrderRetrieveView
from .views import PurchaseOrderCancelView
from .views import PurchaseOrderToPdfView
from .views import PurchaseOrderExportExcel
from .views import ExpenseExportExcel
from .views import ExpenseEmptyImportView
from .views import ExpenseImportExcel
from .views import ExpenseListView
from .views import ExpenseNewView
from .views import ExpenseEditView
from .views import ExpenseRetrieveView

app_name = 'procurement'

urlpatterns = [
    path(
        'requisitions/',
        RequisitionOrderListView.as_view(),
        name='requisitionorder-list'
    ),
    path(
        'requisitions/new/',
        RequisitionOrderNewView.as_view(),
        name='requisitionorder-new'
    ),
    path(
        'requisitions/<int:pk>/edit/',
        RequisitionOrderEditView.as_view(),
        name='requisitionorder-edit'
    ),
    path(
        'requisitions/<int:pk>/retrieve/',
        RequisitionOrderRetrieveView.as_view(),
        name='requisitionorder-retrieve',
    ),
    path(
        'purchaseorders/',
        PurchaseOrderListView.as_view(),
        name='purchaseorder-list'
    ),
    path(
        'purchaseorders/new/',
        PurchaseOrderNewView.as_view(),
        name='purchaseorder-new'
    ),
    path(
        'purchaseorders/<int:pk>/edit/',
        PurchaseOrderEditView.as_view(),
        name='purchaseorder-edit'
    ),
    path(
        'purchaseorders/<int:pk>/retrieve/',
        PurchaseOrderRetrieveView.as_view(),
        name='purchaseorder-retrieve'
    ),
    path(
        'purchaseorders/<int:pk>/cancel/',
        PurchaseOrderCancelView.as_view(),
        name='purchaseorder-cancel'
    ),
    path(
        'purchaseorders/<int:pk>/export/pdf',
        PurchaseOrderToPdfView.as_view(),
        name='purchaseorder-export-pdf'
    ),
    path(
        'purchaseorders/export/xlsx/',
        PurchaseOrderExportExcel.as_view(),
        name='purchaseorder-export-xlsx'
    ),
    path(
        'expenses/',
        ExpenseListView.as_view(),
        name='expense-list'
    ),
    path(
        'expenses/new/',
        ExpenseNewView.as_view(),
        name='expense-new'
    ),
    path(
        'expenses/<int:pk>/edit/',
        ExpenseEditView.as_view(),
        name='expense-edit'
    ),
    path(
        'expenses/<int:pk>/retrieve/',
        ExpenseRetrieveView.as_view(),
        name='expense-retrieve'
    ),
    path(
        'expenses/export/xlsx/',
        ExpenseExportExcel.as_view(),
        name='expense-export-xlsx'
    ),
    path(
        'expenses/import/',
        ExpenseImportExcel.as_view(),
        name='expense-import'
    ),
    path(
        'expenses/import/empty/',
        ExpenseEmptyImportView.as_view(),
        name='expense-import-empty'
    ),
]
