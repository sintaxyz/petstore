# Python's Libraries
from decimal import Decimal
from datetime import datetime
from datetime import timedelta
from datetime import date

# Django's Libraries
from django.db import transaction
from django.db.models import Sum

# Third-party Libraries

# Own's Libraries
from tools.business import StxBsn

from .models import RequisitionOrder
from .models import RequisitionOrderLine
from .models import PurchaseOrder
from .models import PurchaseOrderLine
from .models import Expense

from .resources import PurchaseOrderResource
from .resources import ExpenseResource

from .filters import RequisitionOrderFilter
from .filters import PurchaseOrderFilter
from .filters import ExpenseFilter


class RequisitionOrderBsn(StxBsn):
    model = RequisitionOrder
    model_filter = RequisitionOrderFilter

    @classmethod
    def get_RequisitionList(self, pk):
        records = self.get_ModelObject().objects.all()
        return records

    @classmethod
    def filter_ByPermission(self, records, user):
        return records.filter(warehouse__company__in=user.companies.all())

    @classmethod
    def get_Assigned(self, user):
        return self.get_All().filter(warehouse__company__in=user.companies.all())

    @classmethod
    def create(self, _data, _user, _files=None):
        with transaction.atomic():
            record = self.get_ModelObject()()
            record.warehouse = _data.get('warehouse')
            record.requisited_by = _data.get('requisited_by')
            record.description = _data.get('description')
            record.comments = _data.get('comments')
            record.date = _data.get('date')
            record.created_by = _user
            record.full_clean()
            record.save()
            if _data.get('order_line', None):
                for item in _data.get('order_line'):
                    product = item.get('item')
                    line = item.get('line')
                    comments = item.get('comments')
                    qty = item.get('qty')
                    RequisitionOrderLine.objects.create(
                        item=product,
                        qty=qty,
                        line=line,
                        comments=comments,
                        order=record,
                    )
            else:
                raise NameError('No se puede crear Pedido sin articulos.')
            record.save()
            self.set_TotalItems(record, _user, True)
            return record

    @classmethod
    def create_RequisitionLine(self, _record, _data, _user, _files=None):
        with transaction.atomic():
            record = RequisitionOrderLine()
            record.order = _record
            record.item = _data.get('item')
            record.qty = _data.get('qty')
            record.comments = _data.get('comments')
            record.created_by = _user
            record.full_clean()
            record.save()
            record.line = "OR{}LN{}".format(
                str(record.order.id), str(record.id)
                )
            record.save()
            self.set_TotalItems(record.order, _user)
            return record

    @classmethod
    def update_Requisition(self, _record, _data, _user, _files=None):
        with transaction.atomic():
            _record.warehouse = _data.get('warehouse')
            _record.requisited_by = _data.get('requisited_by')
            # _record.status = _data.get('status')
            _record.description = _data.get('description')
            _record.comments = _data.get('comments')
            _record.date = _data.get('date')
            _record.updated_by = _user
            _record.full_clean()
            _record.save()
            return _record

    @classmethod
    def update_RequisitionLine(self, _record, _data, _user, _files=None):
        with transaction.atomic():
            _record.item = _data.get('item')
            _record.qty = _data.get('qty')
            _record.comments = _data.get('comments')
            _record.updated_by = _user
            _record.full_clean()
            _record.save()
            self.set_TotalItems(_record.order, _user)
            return _record

    @classmethod
    def set_TotalItems(self, _record, _user, is_created=False):
        _record.qty_items = sum(
                [i.qty for i in _record.order_line.all()]
            )
        if is_created:
            pass
        else:
            _record.updated_by = _user
        _record.full_clean()
        _record.save()
        return _record

    @classmethod
    def get_Requisition(self, pk):
        record = self.get_ModelObject().objects.get(id=pk)
        if record:
            return record
        else:
            raise ValueError('No existe este ajuste de articulos')

    @classmethod
    def get_RequisitionLine(self, pk, line_pk):
        requisition = self.get_ModelObject().objects.get(id=pk)
        if requisition:
            record = requisition.order_line.get(id=line_pk)
            return record
        else:
            raise ValueError('No existe esta linea de artículo.')

    @classmethod
    def search(self, records, _value):
        try:
            _value = int(_value)
        except Exception:
            _value

        if type(_value) is str:
            records = records.filter(warehouse_name__icontains=_value)
        elif type(_value) is int:
            records = records.filter(id=_value)
        return records


class PurchaseOrderBsn(StxBsn):
    model = PurchaseOrder
    model_filter = PurchaseOrderFilter
    model_resource = PurchaseOrderResource

    @classmethod
    def filter_ByPermission(self, records, user):
        return records.filter(warehouse__company__in=user.companies.all())

    @classmethod
    def get_Assigned(self, user):
        return self.get_All().filter(warehouse__company__in=user.companies.all())

    @classmethod
    def update_StatusPay(self, _id, _status, _user):
        record = self.get(_id)
        record.status_pay = _status
        record.updated_by = _user
        record.save()

    @classmethod
    def get_PurchaseList(self, user):
        records = self.get_ModelObject().objects.all()
        return records

    @classmethod
    def export_XLSX(self, records):
        return PurchaseOrderResource().export(records).xlsx

    @classmethod
    def get_Approved(self, user):
        records = self.get_Assigned(user).filter(
            status='APR', full_supplied=False
        ).exclude(order_line=None)
        return records

    @classmethod
    def create(self, _data, _user, _files=None):
        with transaction.atomic():
            if _data.get('warehouse'):
                company = _data.get('warehouse').company.id
            else:
                raise ValueError('Es necesario asignar un almacen.')
            approved = self.validate_Access(_user, 'CROC', company)
            if approved is False:
                raise ValueError('No tiene permiso para crear orden compra.')
            record = self.get_ModelObject()()
            record.warehouse = _data.get('warehouse')
            record.supplier = _data.get('supplier')
            record.date = _data.get('date')
            record.request_by = _user
            record.description = _data.get('description')
            record.comments = _data.get('comments')
            record.tax = _data.get('tax')
            record.created_by = _user
            record.full_clean()
            record.save()
            return record

    @classmethod
    def approve(self, _record, _user):
        with transaction.atomic():
            company = _record.warehouse.company.id
            approved = self.validate_Access(_user, ['CROC', 'EDOC'], company)
            if approved is False:
                raise ValueError('No tiene permiso para aprovar orden de comprar.')
            if _record.status != 'INP':
                raise PermissionError(
                    'No se puede modificar este registro.')
            _record.status = 'APR'
            _record.approved_date = datetime.now()
            _record.approved_by = _user
            _record.updated_by = _user
            _record.save()
            ExpenseBsn.expense_PurchaseOrder(_record, _user)
            return _record

    @classmethod
    def cancel(self, _record, _user):
        with transaction.atomic():
            company = _record.warehouse.company.id
            approved = self.validate_Access(_user, 'CAOC', company)
            if approved is False:
                raise ValueError('No tiene permiso para cancelar orden de comprar.')
            if _record.receptions.exists is True:
                raise PermissionError(
                    'Esta orden de compra ya ha sido entregada.')
            if _record.status == 'REJ' or\
                _record.status == 'CAN':
                raise PermissionError(
                    'Esta orden de compra ya fue rechazada.')
            if _record.status == 'APR' and _record.paymethod != 'CR':
                expense = ExpenseBsn.get_ExpenseByKey(str(_record.id))
                expense.status = 'CA'
                expense.updated_by = _user
                expense.save()
                _record.canceled_date = datetime.now()
                _record.canceled_by = _user
                _record.updated_by = _user
                _record.status = 'CAN'
                _record.save()
                return _record
            else:
                _record.canceled_date = datetime.now()
                _record.canceled_by = _user
                _record.updated_by = _user
                _record.status = 'CAN'
                _record.save()
                return _record


    @classmethod
    def update_Desc(self, _record, _data, _user):
        with transaction.atomic():
            company = _record.warehouse.company.id
            approved = self.validate_Access(_user, 'EDOC', company)
            if approved is False:
                raise ValueError('No tiene permiso para editar orden de compra')
            if _record.status != 'INP':
                raise PermissionError('No se puede modificar este registro.')
            _record.description = _data.get('description')
            _record.comments = _data.get('comments')
            _record.paymethod = _data.get('paymethod')
            _record.credit_days = _data.get('credit_days')
            _record.updated_by = _user
            _record.save()
            return _record

    @classmethod
    def create_PurchaseLine(self, _data, _user, _files=None):
        with transaction.atomic():
            if _data.get('order').warehouse:
                company = _data.get('order').warehouse.company.id
            else:
                raise ValueError('Es necesario asignar un almacen.')
            approved = self.validate_Access(_user, ['CROC', 'EDOC'], company)
            if approved is False:
                raise ValueError('No tiene permiso para editar orden de compra')
            if _data.get('order').status != 'INP':
                raise PermissionError('No se puede modificar este registro.')
            record = PurchaseOrderLine()
            record.order = _data.get('order')
            record.item = _data.get('item')
            record.qty = _data.get('qty')
            record.qty_pending = _data.get('qty')
            record.supplier = _data.get('supplier')
            record.delivery_date = _data.get('delivery_date')
            record.price = _data.get('price')
            record.comments = _data.get('comments')
            record.tax = _data.get('tax')
            total_amount = Decimal(
                    format(record.price * record.qty, '.2f')
                    )
            if _data.get('tax'):
                record.total_amount = format(
                    total_amount + (total_amount * (
                        _data.get('tax').factor / 100)), '.2f')
            else:
                record.total_amount = total_amount
            record.save()
            record.created_by = _user
            record.full_clean()
            record.save()
            record.line = "OC{}LN{}".format(
                str(record.order.id), str(record.id)
                )
            record.save()
            self.update_OrderByLine(record.order, _user)
            return record

    @classmethod
    def update_Purchase(self, _record, _data, _user, _files=None):
        with transaction.atomic():
            _record.warehouse = _data.get('warehouse')
            _record.supplier = _data.get('supplier')
            _record.date = _data.get('date')
            _record.request_by = _data.get('request_by')
            _record.approved_date = _data.get('approved_date')
            _record.approved_by = _data.get('approved_by')
            _record.canceled_date = _data.get('canceled_date')
            _record.canceled_by = _data.get('canceled_by')
            _record.cancellation_reason = _data.get('cancellation_reason')
            _record.rejected_date = _data.get('rejected_date')
            _record.rejected_by = _data.get('rejected_by')
            _record.rejection_reason = _data.get('rejection_reason')
            _record.description = _data.get('description')
            _record.comments = _data.get('comments')
            _record.paymethod = _data.get('paymethod')
            _record.days_credit = _data.get('days_credit')
            _record.tax = _data.get('tax')
            _record.updated_by = _user
            _record.full_clean()
            _record.save()
            return _record

    @classmethod
    def update_PurchaseLine(self, _record, _data, _user, _files=None):
        with transaction.atomic():
            _record.item = _data.get('item')
            _record.qty = _data.get('qty')
            _record.supplier = _data.get('supplier')
            _record.delivery_date = _data.get('delivery_date')
            _record.price = _data.get('price')
            _record.comments = _data.get('comments')
            _record.total_amount = Decimal(
                format(_record.price * _record.qty, '.2f')
                )
            _record.tax = _data.get('tax')
            _record.updated_by = _user
            _record.full_clean()
            _record.save()
            self.update_OrderByLine(_record.order, _user)
            return _record

    @classmethod
    def update_OrderByLine(self, _record, _user):
        _record.qty_items = sum(
                [i.qty for i in _record.order_line.all()]
            )
        _record.total_amount = sum(
                [Decimal(i.subtotal_price) + Decimal(i.total_tax) for i in _record.order_line.all()]
            )
        _record.amount_outstanding = _record.total_amount
        _record.updated_by = _user
        _record.save()
        return _record

    @classmethod
    def get_Purchase(self, pk):
        record = self.get_ModelObject().objects.get(id=pk)
        if record:
            return record
        else:
            raise ValueError('No existe este ajuste de articulos')

    @classmethod
    def get_PurchaseLine(self, pk, line_pk):
        purchase = self.get_ModelObject().objects.get(id=pk)
        if purchase:
            record = purchase.order_line.get(id=line_pk)
            return record
        else:
            raise ValueError('No existe esta linea de artículo.')

    @classmethod
    def get_Item(self, order, product):
        purchase = self.get_ModelObject().objects.get(id=order.id)
        if purchase:
            record = purchase.order_line.get(item=product.id)
            return record
        else:
            raise ValueError('No existe esta linea de artículo.')

    @classmethod
    def search(self, records, _value):
        try:
            _value = int(_value)
        except Exception:
            _value

        if type(_value) is str:
            records = records.filter(warehouse_name__icontains=_value)
        elif type(_value) is int:
            records = records.filter(id=_value)
        return records


class ExpenseBsn(StxBsn):
    model = Expense
    model_resource = ExpenseResource
    model_filter = ExpenseFilter

    @classmethod
    def filter_ByPermission(self, records, user):
        return records.filter(company__in=user.companies.all())

    @classmethod
    def export_XLSX(self, records):
        return ExpenseResource().export(records).xlsx

    @classmethod
    def get_Assigned(self, user):
        return self.get_All().filter(company__in=user.companies.all())

    @classmethod
    def get_ExpensesInDays(self, user, request, days_ago: int = 10):
        string_start_date = request.GET.get('start_date', None)
        string_end_date = request.GET.get('end_date', None)
        if string_end_date:
            end_date = datetime.strptime(string_end_date, '%Y-%m-%d')
        else:
            end_date = datetime.today()

        if string_start_date:
            start_date = datetime.strptime(string_start_date, '%Y-%m-%d')
            outstanding_days = end_date - start_date
            days_ago = outstanding_days.days if outstanding_days.days <= days_ago else days_ago

        records = self.get_Assigned(user)
        if request.GET.get('company', None):
            records = records.filter(company__id=request.GET.get('company'))
        filtered_by_date = records.filter(
            date__lte=end_date,
            date__gte=end_date-timedelta(days=days_ago))
        expenses_total = filtered_by_date.aggregate(Sum('amount'))
        organized_records = []
        
        for days in range(days_ago, -1, -1):
            past_days = end_date - timedelta(days=days)
            past_pay = records.filter(
                date__year=past_days.year,
                date__month=past_days.month,
                date__day=past_days.day
            )
            if past_pay.exists():
                value_day = past_pay.values('date').annotate(
                    Sum('amount'))
                organized_records.append(value_day.get())                    
            else:
                value_day = {"date":past_days, 'amount__sum':Decimal('0.00')}
                organized_records.append(value_day)

        values = {
            'records': organized_records,
            'total': expenses_total['amount__sum']
        }
        return values

    @classmethod
    def create(self, _data, _user):
        with transaction.atomic():
            if _data.get('company'):
                company = _data.get('company').id
            else:
                raise ValueError('Es necesario asignar una compañia.')
            approved = self.validate_Access(_user, 'CREG', company)
            if approved is False:
                raise ValueError('No tiene permiso para crear egresos.')
            record = self.get_ModelObject()()
            record.concept = _data.get('concept')
            record.company = _data.get('company')
            record.date = _data.get('date')
            record.key = _data.get('key')
            record.amount = _data.get('amount')
            record.comments = _data.get('comments')
            record.status = _data.get('status')
            if record.status == 'CO':
                if record.concept == 'PUO':
                    order = PurchaseOrderBsn.get(int(record.key))
                    order.amount_outstanding -= record.amount
                    order.full_clean()
                    order.save()
            record.full_clean()
            record.save()
            return record

    @classmethod
    def get_ExpenseByKey(self, key):
        record = self.get_ModelObject().objects.get(key=key)
        return record

    @classmethod
    def expense_PurchaseOrder(self, _record, _user):
        if _record.paymethod != 'CR':
            data = {
                'company': _record.warehouse.company,
                'concept': 'PUO',
                'key': _record.id,
                'amount': _record.total_amount,
                'comments': 'Pagada al contado.',
                'status': 'CO',
                'date': _record.approved_date
            }
            self.create(data, _user)
