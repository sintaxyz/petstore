# Python's Libraries
# Django's Libraries

# Third-party Libraries
from rest_framework import serializers

# Own's Libraries
from .models import RequisitionOrder
from .models import RequisitionOrderLine
from .models import PurchaseOrder
from .models import PurchaseOrderLine
from .models import Expense

from core.serializers import TaxSerializer


class RequisitionOrderLineSerializer(serializers.ModelSerializer):
    item_name = serializers.SerializerMethodField()

    class Meta:
        model = RequisitionOrderLine
        fields = [
            'id',
            'item',
            'item_name',
            'qty',
            'line',
            'comments',
        ]
        read_only_fields = [
            'id',
            'item_name',
        ]

    def get_item_name(self, obj):
        return obj.item.item.name


class RequisitionOrderSerializer(serializers.ModelSerializer):
    order_line = RequisitionOrderLineSerializer(
        required=True, many=True)
    company_name = serializers.SerializerMethodField()
    warehouse_name = serializers.SerializerMethodField()
    requisited_by_name = serializers.SerializerMethodField()
    status_desc = serializers.SerializerMethodField()
    date_desc = serializers.SerializerMethodField()

    class Meta:
        model = RequisitionOrder
        fields = [
            'id',
            'company_name',
            'warehouse',
            'warehouse_name',
            'requisited_by',
            'requisited_by_name',
            'date',
            'date_desc',
            'qty_items',
            'description',
            'comments',
            'status_desc',
            'order_line',
        ]
        extra_kwargs = {
            'date': {'write_only': True},
        }
        read_only_fields = [
            'id',
            'company_name',
            'warehouse_name',
            'requisited_by_name',
            'status_desc',
            'date_desc',
        ]

    def get_company_name(self, obj):
        return obj.warehouse.company.commercial_name

    def get_warehouse_name(self, obj):
        return obj.warehouse.name

    def get_requisited_by_name(self, obj):
        return obj.requisited_by.name

    def get_status_desc(self, obj):
        return obj.get_status_display()

    def get_date_desc(self, obj):
        return obj.date.strftime("%d/%m/%Y %H:%M")


class RequisitionOrderUpdateSerializer(serializers.ModelSerializer):
    company_name = serializers.SerializerMethodField()
    warehouse_name = serializers.SerializerMethodField()
    requisited_by_name = serializers.SerializerMethodField()
    status_desc = serializers.SerializerMethodField()
    date_desc = serializers.SerializerMethodField()

    class Meta:
        model = RequisitionOrder
        fields = [
            'id',
            'company_name',
            'warehouse',
            'warehouse_name',
            'requisited_by',
            'requisited_by_name',
            'date',
            'date_desc',
            'qty_items',
            'description',
            'comments',
            'status_desc',
        ]
        extra_kwargs = {
            'date': {'write_only': True},
        }
        read_only_fields = [
            'id',
            'company_name',
            'warehouse_name',
            'requisited_by_name',
            'status_desc',
            'date_desc',
        ]

    def get_company_name(self, obj):
        return obj.warehouse.company.commercial_name

    def get_warehouse_name(self, obj):
        return obj.warehouse.name

    def get_requisited_by_name(self, obj):
        return obj.requisited_by.name

    def get_status_desc(self, obj):
        return obj.get_status_display()

    def get_date_desc(self, obj):
        return obj.date.strftime("%d/%m/%Y %H:%M")


class PurchaseOrderLineSerializer(serializers.ModelSerializer):
    item_name = serializers.SerializerMethodField()
    # supplier_name = serializers.SerializerMethodField()
    delivery_date_desc = serializers.SerializerMethodField()

    class Meta:
        model = PurchaseOrderLine
        fields = [
            'id',
            'item',
            'item_name',
            'line',
            'delivery_date',
            'delivery_date_desc',
            'qty',
            'price',
            'tax',
            'subtotal_price',
            'total_tax',
            'total_amount',
            'comments',
        ]
        extra_kwargs = {
            'delivery_date': {'write_only': True},
        }
        read_only_fields = [
            'id',
            'item_name',
            'supplier_name',
            'delivery_date_desc',
            'total_amount',
            'line',
        ]

    def get_item_name(self, obj):
        return obj.item.item.name

    def get_supplier_name(self, obj):
        if obj.supplier:
            return obj.supplier.commercial_name
        else:
            return None

    def get_delivery_date_desc(self, obj):
        if obj.delivery_date:
            return obj.delivery_date.strftime("%Y-%m-%d %H:%M")
        else:
            return None


class PurchaseOrderLineRetrieveSerializer(PurchaseOrderLineSerializer):
    tax = TaxSerializer(required=True)


class PurchaseOrderUpdateDescSerializer(serializers.ModelSerializer):

    class Meta:
        model = PurchaseOrder
        exclude = (
            'id',
            'warehouse',
            'supplier',
            'request_by',
            'approved_by',
            'approved_date',
            'rejected_by',
            'rejection_reason',
            'rejected_date',
            'canceled_by',
            'paymethod',
            'credit_days',
            'cancellation_reason',
            'canceled_date',
            'qty_items',
            'total_amount',
            'tax',
        )


class PurchaseOrderSerializer(serializers.ModelSerializer):
    order_line = PurchaseOrderLineSerializer(required=False, many=True)
    company_name = serializers.SerializerMethodField()
    warehouse_name = serializers.SerializerMethodField()
    supplier_name = serializers.SerializerMethodField()
    request_by_name = serializers.SerializerMethodField()
    approved_by_name = serializers.SerializerMethodField()
    rejected_by_name = serializers.SerializerMethodField()
    canceled_by_name = serializers.SerializerMethodField()
    status_desc = serializers.SerializerMethodField()
    date_desc = serializers.SerializerMethodField()
    approved_date_desc = serializers.SerializerMethodField()
    rejected_date_desc = serializers.SerializerMethodField()
    canceled_date_desc = serializers.SerializerMethodField()
    display_value = serializers.SerializerMethodField()
    tax_abbreviation = serializers.SerializerMethodField()

    class Meta:
        model = PurchaseOrder
        fields = [
            'id',
            'description',
            'company_name',
            'warehouse',
            'warehouse_name',
            'supplier',
            'supplier_name',
            'date',
            'date_desc',
            'request_by',
            'request_by_name',
            'approved_by',
            'approved_by_name',
            'approved_date',
            'approved_date_desc',
            'rejected_by',
            'rejected_by_name',
            'rejection_reason',
            'rejected_date',
            'rejected_date_desc',
            'canceled_by',
            'canceled_by_name',
            'cancellation_reason',
            'canceled_date',
            'canceled_date_desc',
            'status_desc',
            'paymethod',
            'credit_days',
            'qty_items',
            'total_amount',
            'tax',
            'tax_abbreviation',
            'comments',
            'order_line',
            'subtotal_price',
            'total_tax',
            'display_value',
        ]
        extra_kwargs = {
            'status': {'write_only': True},
            'date': {'write_only': True},
            'approved_date': {'write_only': True},
            'rejected_date': {'write_only': True},
            'canceled_date': {'write_only': True},
        }
        read_only_fields = [
            'id',
            'company_name',
            'warehouse_name',
            'supplier_name',
            'request_by_name',
            'approved_by_name',
            'rejected_by_name',
            'canceled_by_name',
            'date_desc',
            'approved_date_desc',
            'rejected_date_desc',
            'canceled_date_desc',
            'status_desc',
            'tax_abbreviation',
            'qty_items',
            'total_amount',
            'subtotal_price',
            'total_tax',
            'display_value',
        ]

    def get_company_name(self, obj):
        return obj.warehouse.company.commercial_name

    def get_display_value(self, obj):
        supplier = obj.supplier.commercial_name
        date = obj.date.strftime("%Y-%d-%m %H:%M")
        return f"{obj.id}: {supplier} ({date})"

    def get_warehouse_name(self, obj):
        if obj.warehouse:
            return obj.warehouse.name
        else:
            return None

    def get_tax_abbreviation(self, obj):
        if obj.tax:
            return obj.tax.abbreviation
        else:
            return None

    def get_supplier_name(self, obj):
        if obj.supplier:
            return obj.supplier.commercial_name
        else:
            return None

    def get_status_desc(self, obj):
        return obj.get_status_display()

    def get_request_by_name(self, obj):
        if obj.request_by:
            return obj.request_by.name
        else:
            return None

    def get_approved_by_name(self, obj):
        if obj.approved_by:
            return obj.approved_by.name
        else:
            return None

    def get_rejected_by_name(self, obj):
        if obj.rejected_by:
            return obj.rejected_by.name
        else:
            return None

    def get_canceled_by_name(self, obj):
        if obj.canceled_by:
            return obj.canceled_by.name
        else:
            return None

    def get_date_desc(self, obj):
        if obj.date:
            return obj.date.strftime("%Y-%d-%m %H:%M")
        else:
            return None

    def get_approved_date_desc(self, obj):
        if obj.approved_date:
            return obj.approved_date.strftime("%d/%m/%Y %H:%M")
        else:
            return None

    def get_rejected_date_desc(self, obj):
        if obj.rejected_date:
            return obj.rejected_date.strftime("%d/%m/%Y %H:%M")
        else:
            return None

    def get_canceled_date_desc(self, obj):
        if obj.canceled_date:
            return obj.canceled_date.strftime("%d/%m/%Y %H:%M")
        else:
            return None


class PurchaseOrderRetrieveSerializer(PurchaseOrderSerializer):
    tax = TaxSerializer(required=True)
    order_line = PurchaseOrderLineRetrieveSerializer(required=True, many=True)


class PurchaseOrderUpdateSerializer(serializers.ModelSerializer):
    company_name = serializers.SerializerMethodField()
    warehouse_name = serializers.SerializerMethodField()
    supplier_name = serializers.SerializerMethodField()
    request_by_name = serializers.SerializerMethodField()
    approved_by_name = serializers.SerializerMethodField()
    rejected_by_name = serializers.SerializerMethodField()
    canceled_by_name = serializers.SerializerMethodField()
    status_desc = serializers.SerializerMethodField()
    date_desc = serializers.SerializerMethodField()
    approved_date_desc = serializers.SerializerMethodField()
    rejected_date_desc = serializers.SerializerMethodField()
    canceled_date_desc = serializers.SerializerMethodField()
    tax = TaxSerializer(required=True)

    class Meta:
        model = PurchaseOrder
        fields = [
            'id',
            'company_name',
            'warehouse',
            'warehouse_name',
            'supplier',
            'supplier_name',
            'date',
            'date_desc',
            'request_by',
            'request_by_name',
            'approved_by',
            'approved_by_name',
            'approved_date',
            'approved_date_desc',
            'rejected_by',
            'rejected_by_name',
            'rejection_reason',
            'rejected_date',
            'rejected_date_desc',
            'canceled_by',
            'canceled_by_name',
            'cancellation_reason',
            'canceled_date',
            'canceled_date_desc',
            'status_desc',
            'paymethod',
            'credit_days',
            'qty_items',
            'total_amount',
            'tax',
            'comments',
        ]
        extra_kwargs = {
            'status': {'write_only': True},
            'date': {'write_only': True},
            'approved_date': {'write_only': True},
            'rejected_date': {'write_only': True},
            'canceled_date': {'write_only': True},
        }
        read_only_fields = [
            'id',
            'company_name',
            'warehouse_name',
            'supplier_name',
            'request_by_name',
            'approved_by_name',
            'rejected_by_name',
            'canceled_by_name',
            'date_desc',
            'approved_date_desc',
            'rejected_date_desc',
            'canceled_date_desc',
            'status_desc',
            'qty_items',
            'total_amount',
        ]

    def get_company_name(self, obj):
        return obj.warehouse.company.commercial_name

    def get_warehouse_name(self, obj):
        return obj.warehouse.name

    def get_supplier_name(self, obj):
        if obj.supplier:
            return obj.supplier.commercial_name
        else:
            return None

    def get_status_desc(self, obj):
        return obj.get_status_display()

    def get_request_by_name(self, obj):
        if obj.request_by:
            return obj.request_by.name
        else:
            return None

    def get_approved_by_name(self, obj):
        if obj.approved_by:
            return obj.approved_by.name
        else:
            return None

    def get_rejected_by_name(self, obj):
        if obj.rejected_by:
            return obj.rejected_by.name
        else:
            return None

    def get_canceled_by_name(self, obj):
        if obj.canceled_by:
            return obj.canceled_by.name
        else:
            return None

    def get_date_desc(self, obj):
        if obj.date:
            return obj.date.strftime("%d/%m/%Y %H:%M")
        else:
            return None

    def get_approved_date_desc(self, obj):
        if obj.approved_date:
            return obj.approved_date.strftime("%d/%m/%Y %H:%M")
        else:
            return None

    def get_rejected_date_desc(self, obj):
        if obj.rejected_date:
            return obj.rejected_date.strftime("%d/%m/%Y %H:%M")
        else:
            return None

    def get_canceled_date_desc(self, obj):
        if obj.canceled_date:
            return obj.canceled_date.strftime("%d/%m/%Y %H:%M")
        else:
            return None


class ExpenseSerializer(serializers.ModelSerializer):
    company_name = serializers.SerializerMethodField()
    concept_desc = serializers.SerializerMethodField()
    status_desc = serializers.SerializerMethodField()

    class Meta:
        model = Expense
        fields = [
            'id',
            'company',
            'company_name',
            'concept',
            'date',
            'key',
            'comments',
            'amount',
            'status',
            'concept_desc',
            'status_desc',
        ]
        extra_kwargs = {
            'concept': {'write_only': True},
            'status': {'write_only': True},
        }
        read_only_fields = [
            'id',
            'concept_desc',
            'status_desc',
        ]

    def get_company_name(self, obj):
        return obj.company.commercial_name

    def get_concept_desc(self, obj):
        return obj.get_concept_display()

    def get_status_desc(self, obj):
        return obj.get_status_display()


class ExpensesInDaysSerializer(serializers.Serializer):
    date = serializers.DateTimeField(format="%m-%d")
    amount__sum = serializers.DecimalField(
        max_digits=12, decimal_places=2)


class ExpensesDashboardSerializer(serializers.Serializer):
    records = ExpensesInDaysSerializer(many=True)
    total = serializers.DecimalField(
        max_digits=10, decimal_places=2)
