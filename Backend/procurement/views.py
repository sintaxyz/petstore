# Python's Libraries

# Django's Libraries
from django.shortcuts import render
from django.shortcuts import redirect
from django.urls import reverse_lazy
from django.contrib import messages
from django.db.models import Sum

# Third-party Libraries

# Own's Libraries
from tools.views import StxListView
from tools.views import StxNewView
from tools.views import StxEditView
from tools.views import StxRetrieveView
from tools.views import AppView
from tools.views import View
from tools.views import StxReactView
from tools.views import StxExportToExcel
from tools.views import ResourceModelImportView
from tools.views import StxEmptyImportView


from tools.utils import Report

from .business import RequisitionOrderBsn
from .business import PurchaseOrderBsn
from .business import ExpenseBsn
from inventory.business import DocReceptionBsn

from .forms import RequisitionOrderForm
from .forms import PurchaseOrderForm
from .forms import ExpenseForm


class RequisitionOrderListView(StxListView):
    template_name = 'procurement/requisition/list/requisition_list.html'
    business_class = RequisitionOrderBsn


class RequisitionOrderNewView(StxReactView):
    template_name = 'procurement/requisition/new/requisition_new.html'


class RequisitionOrderEditView(StxEditView):
    update_record_method = 'update'
    template_name = 'procurement/requisition/edit/requisition_edit.html'
    business_class = RequisitionOrderBsn
    form_class = RequisitionOrderForm
    page_title = 'Editar Requisición'


class RequisitionOrderRetrieveView(StxRetrieveView):
    template_name = "procurement/requisition/retrieve/requisition_retrieve.html"
    business_class = RequisitionOrderBsn


class PurchaseOrderListView(StxListView):
    permissions = ['VEOC']
    template_name = 'procurement/purchaseorder/list/purchaseorder_list.html'
    business_class = PurchaseOrderBsn

    def pre_Return(self):
        records = self.context['queryset']
        if len(records) < 1000:
            totals = records.aggregate(Sum('total_amount'))
            self.append_Context(total_amount=totals['total_amount__sum'])


class PurchaseOrderNewView(StxReactView):
    permissions = ['CROC']
    template_name = 'procurement/purchaseorder/new/purchaseorder_new.html'


class PurchaseOrderEditView(StxEditView):
    permissions = ['EDOC']
    path_related_company = ['warehouse', 'company', 'id']
    update_record_method = 'update'
    template_name = 'procurement/purchaseorder/edit/purchaseorder_edit.html'
    business_class = PurchaseOrderBsn
    form_class = PurchaseOrderForm
    page_title = 'Editar Orden de Compra'


class PurchaseOrderRetrieveView(StxRetrieveView):
    permissions = ['VEOC']
    path_related_company = ['warehouse', 'company', 'id']
    template_name = "procurement/purchaseorder/retrieve/purchaseorder_retrieve.html"
    business_class = PurchaseOrderBsn


class PurchaseOrderCancelView(AppView):
    business_class = PurchaseOrderBsn

    def get(self, request, pk):
        instance = self.business_class.get(pk)
        try:
            if instance.receptions.exists() is False:
                self.business_class.cancel(instance, request.user)
                messages.add_message(
                    request,
                    messages.SUCCESS,
                    'La orden fue cancelada existosamente.'
                )
                return redirect(
                    reverse_lazy('procurement:purchaseorder-list')
                )
            else:
                messages.add_message(
                    request,
                    messages.ERROR,
                    'No puede cancelar, ya ha sido recibida mercancia.'
                )
                return redirect(
                    reverse_lazy('procurement:purchaseorder-list')
                )

        except Exception as e:
            messages.add_message(
                request,
                messages.ERROR,
                e
            )


class PurchaseOrderToPdfView(AppView):
    business_class = PurchaseOrderBsn
    template_name = 'procurement/purchaseorder/report_pdf/purchaseorder_pdf.html'

    def get(self, request, pk, **kwargs):
        instance = self.business_class.get(pk)
        context = {
            'instance': instance
        }
        return Report.render(self.template_name, context)


class PurchaseOrderExportExcel(StxExportToExcel):
    permissions = ['EXOC']
    business_class = PurchaseOrderBsn
    dataset_method = 'export_XLSX'
    name_file = 'Listado de Ordenes de Compra.xlsx'


class ExpenseListView(StxListView):
    permissions = ['VEEG']
    template_name = 'procurement/expense/list/expense_list.html'
    business_class = ExpenseBsn


class ExpenseNewView(StxReactView):
    permissions = ['CREG']
    template_name = 'procurement/expense/new/expense_new.html'


class ExpenseEditView(StxEditView):
    permissions = ['EDEG']
    path_related_company = ['company', 'id']
    update_record_method = 'update'
    template_name = 'procurement/expense/edit/expense_edit.html'
    business_class = ExpenseBsn
    form_class = ExpenseForm
    page_title = 'Editar Egresos'


class ExpenseRetrieveView(StxRetrieveView):
    permissions = ['VEEG']
    path_related_company = ['company', 'id']
    template_name = "procurement/expense/retrieve/expense_retrieve.html"
    business_class = ExpenseBsn


class ExpenseExportExcel(StxExportToExcel):
    permissions = ['EXEG']
    business_class = ExpenseBsn
    dataset_method = 'export_XLSX'
    name_file = 'Listado de Gastos.xlsx'


class ExpenseImportExcel(ResourceModelImportView):
    permissions = ['IMEG']
    template_name = 'procurement/expense/import/expense_import.html'
    business_class = ExpenseBsn
    empty_filename = 'Importar Gastos'


class ExpenseEmptyImportView(StxEmptyImportView):
    permissions = ['IMEG']
    business_class = ExpenseBsn

