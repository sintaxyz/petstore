# Python Libraries
import datetime

# Third Party Libraries
from import_export import resources
from import_export import fields
from import_export.widgets import ForeignKeyWidget
from import_export.widgets import ManyToManyWidget
from import_export.widgets import IntegerWidget
from import_export.widgets import DecimalWidget
from import_export.widgets import DateWidget
from import_export.widgets import Widget

# Own Libraries

from .models import SaleOrder
from .models import SaleOrderItem
from .models import Payment
from security.models import BranchOffice
from security.models import User
from core.models import Item
from core.models import Client
from core.models import Warehouse
from core.models import StockItem
from core.models import Supplier
from core.models import Tax
from tools.resources import BooleanWidget
from tools.resources import ChoicesWidget
from tools.resources import GenericResource


class SaleOrderResource(GenericResource):
    id = fields.Field(
        column_name='ID',
        attribute='id',
        widget=IntegerWidget()
    )
    paymethod = fields.Field(
        column_name='metodo de pago',
        attribute='paymethod',
        widget=ChoicesWidget(choices=SaleOrder.PAYMETHOD_CHOICES)
    )
    credit_days = fields.Field(
        column_name='dias de credito',
        attribute='credit_days',
        widget=ChoicesWidget(choices=SaleOrder.DAYS_CREDIT_CHOICES)
    )
    description = fields.Field(
        column_name='descripción',
        attribute='description',
    )
    total_amount = fields.Field(
        column_name='Total',
        attribute='total_amount',
        widget=DecimalWidget()
    )
    amount_outstanding = fields.Field(
        column_name='Pendiente por pagar',
        attribute='amount_outstanding',
        widget=DecimalWidget()
    )
    warehouse = fields.Field(
        column_name='almacén',
        attribute='warehouse',
        widget=ForeignKeyWidget(Warehouse, 'id')
    )
    branchoffice = fields.Field(
        column_name='sucursal',
        attribute='branchoffice',
        widget=ForeignKeyWidget(BranchOffice, 'id')
    )
    status = fields.Field(
        column_name='estado',
        attribute='status',
        widget=ChoicesWidget(choices=SaleOrder.SALEORDER_STATUS)
    )
    work_id = fields.Field(
        column_name='Obra',
        attribute='work_id',
    )
    tax = fields.Field(
        column_name='impuestos',
        attribute='tax',
        widget=ForeignKeyWidget(Tax, 'factor')
    )
    comments = fields.Field(
        column_name='comentarios',
        attribute='comments',
    )
    client = fields.Field(
        column_name="cliente",
        attribute='client',
        widget=ForeignKeyWidget(Client, 'id')
    )
    qty_items = fields.Field(
        column_name='Cant. Articulos',
        attribute='qty_items',
        widget=DecimalWidget()
    )

    class Meta:
        model = SaleOrder
        skip_unchanged = True
        report_skipped = True
        fields = (
            'id',
            'paymethod',
            'credit_days',
            'description',
            'total_amount',
            'amount_outstanding',
            'warehouse',
            'branchoffice',
            'status',
            'work_id',
            'client',
            'qty_items',
        )
        export_order = (
            'id',
            'paymethod',
            'credit_days',
            'description',
            'total_amount',
            'amount_outstanding',
            'warehouse',
            'branchoffice',
            'status',
            'work_id',
            'client',
            'qty_items',
        )


class SaleOrderItemResource(GenericResource):
    id = fields.Field(
        column_name='Folio',
        attribute='id',
        widget=IntegerWidget()
    )
    order = fields.Field(
        column_name='orden',
        attribute='order',
        widget=ForeignKeyWidget(SaleOrder, 'id')
    )
    line = fields.Field(
        column_name='linea de la orden',
        attribute='line',
    )
    item = fields.Field(
        column_name='artículo',
        attribute='item',
        widget=ForeignKeyWidget(Item, 'id')
    )
    qty = fields.Field(
        column_name='cantidad',
        attribute='qty',
        widget=DecimalWidget()
    )
    price = fields.Field(
        column_name='precio unitario',
        attribute='price',
        widget=DecimalWidget()
    )
    tax = fields.Field(
        column_name='impuestos',
        attribute='tax',
        widget=ForeignKeyWidget(Tax, 'name')
    )
    total_amount = fields.Field(
        column_name='total',
        attribute='total_amount',
        widget=DecimalWidget()
    )
    comments = fields.Field(
        column_name='comentarios',
        attribute='comments',
    )

    class Meta:
        model = SaleOrderItem
        skip_unchanged = True
        report_skipped = True
        fields = (
            'order',
            'line',
            'item',
            'qty',
            'price',
            'tax',
            'total_amount',
        )
        export_order = (
            'order',
            'line',
            'item',
            'qty',
            'price',
            'tax',
            'total_amount',
        )


class PaymentResource(GenericResource):

    id = fields.Field(
        column_name='id',
        attribute='id',
        widget=IntegerWidget()
    )
    saleorder = fields.Field(
        column_name="ID orden de venta",
        attribute='saleorder',
        widget=ForeignKeyWidget(SaleOrder, 'id')
    )
    amount = fields.Field(
        column_name='cantidad',
        attribute='amount',
        widget=DecimalWidget()
    )
    method = fields.Field(
        column_name='metodo de pago',
        attribute='method',
        widget=ChoicesWidget(Payment.METHOD_CHOICES)
    )
    branchoffice = fields.Field(
        column_name='sucursal',
        attribute='branchoffice',
        widget=ForeignKeyWidget(BranchOffice, 'id')
    )
    status = fields.Field(
        column_name='estado',
        attribute='status',
        widget=ChoicesWidget(Payment.STATUS_CHOICES)
    )
    confirmed_date = fields.Field(
        column_name='fecha de confirmada',
        attribute='confirmed_date',
        widget=DateWidget()
    )

    class Meta:
        model = Payment
        skip_unchanged = True
        report_skipped = True
        import_id_fields = ['id', ]
        fields = (
            'id',
            'saleorder',
            'amount',
            'method',
            'branchoffice',
            'status',
            'confirmed_date',
        )
        export_order = (
            'id',
            'saleorder',
            'amount',
            'method',
            'branchoffice',
            'status',
            'confirmed_date',
        )
