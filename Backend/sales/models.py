# Python's Libraries
from decimal import Decimal
from datetime import date
from datetime import datetime
from datetime import time
# Django Libraries
from django.db import models
from django.urls import reverse_lazy
# from django.core.exceptions import ValidationError

# Own Libraries
from tools.models import AppModel
from tools.validators import MinValueValidator
# from tools.utils import FileAdmin

from core.models import Client
from core.models import Warehouse
from core.models import Tax
from core.models import StockItem
from security.models import BranchOffice


class SaleOrder(AppModel):
    PAYMETHOD_CHOICES = (
        ('CA', 'Efectivo'),
        ('TA', 'Tarjeta'),
        ('CR', 'Crédito'),
    )
    DAYS_CREDIT_CHOICES = (
        (30, '30 Días'),
        (60, '60 Días'),
        (90, '90 Días'),
    )
    SALEORDER_STATUS = (
        ('INP', 'EN PROCESO'),
        ('DE', 'ENTREGADA'),
        ('CA', 'CANCELADA'),
    )
    paymethod = models.CharField(
        verbose_name='metodo de pago',
        max_length=3,
        choices=PAYMETHOD_CHOICES,
        default='CA',
        blank=True,
        null=True,
    )
    credit_days = models.SmallIntegerField(
        verbose_name='dias de credito',
        choices=DAYS_CREDIT_CHOICES,
        blank=True,
        null=True,
    )
    description = models.CharField(
        max_length=100,
        verbose_name='descripción',
        blank=True,
        null=True,
    )
    total_amount = models.DecimalField(
        verbose_name='Total',
        max_digits=12,
        decimal_places=2,
        default=0,
        validators=[MinValueValidator(Decimal('0'))],
        null=True,
        blank=False,
    )
    amount_outstanding = models.DecimalField(
        verbose_name='Pendiente por pagar',
        max_digits=12,
        decimal_places=2,
        default=0,
        validators=[MinValueValidator(
            Decimal('0'),
            "El valor de monto pendiente por pagar no puede ser menor a 0")],
        null=True,
        blank=False,
    )
    warehouse = models.ForeignKey(
        Warehouse,
        verbose_name='almacén',
        related_name='in_saleorders',
        on_delete=models.PROTECT,
        blank=False,
        null=True,
    )
    branchoffice = models.ForeignKey(
        BranchOffice,
        verbose_name='sucursal',
        related_name='in_branchoffices',
        on_delete=models.PROTECT,
        blank=False,
        null=True,
    )
    status = models.CharField(
        verbose_name='estado',
        max_length=3,
        choices=SALEORDER_STATUS,
        default='INP',
        blank=False,
    )
    work_id = models.CharField(
        verbose_name='Obra',
        max_length=100,
        blank=True,
        null=True,
    )
    tax = models.ForeignKey(
        Tax,
        verbose_name='impuestos',
        related_name='in_saleorders',
        on_delete=models.PROTECT,
        blank=False,
        null=True
    )
    comments = models.TextField(
        verbose_name='comentarios',
        blank=True,
        null=True,
    )
    client = models.ForeignKey(
        Client,
        verbose_name="cliente",
        related_name='saleorders',
        on_delete=models.PROTECT,
        null=True,
        blank=True,
    )
    qty_items = models.DecimalField(
        verbose_name='Cant. Articulos',
        max_digits=10,
        decimal_places=2,
        default=0,
        validators=[MinValueValidator(Decimal('0'))],
        null=True,
        blank=False,
    )
    invoice = models.CharField(
        max_length=100,
        verbose_name='Factura',
        blank=True,
        null=True,
    )
    xml_invoice_file = models.FileField(
        verbose_name='Factura XML',
        blank=True,
        null=True,
    )
    pdf_invoice_file = models.FileField(
        verbose_name='Factura PDF',
        blank=True,
        null=True,
    )

    @property
    def status_text(self):
        if self.status == 'INP':
            return "En Proceso"
        elif self.status == 'DE':
            return "Entregada"
        elif self.status == 'CA':
            return "Cancelada"

    @property
    def status_color(self):
        if self.status == 'DE':
            return "green"
        elif self.status == 'INP':
            return "gray"
        elif self.status == 'CA':
            return "red"

    @property
    def url_retrieve(self):
        url = reverse_lazy(
            'sales:saleorder-retrieve',
            kwargs={'pk': self.pk}
        )
        return url

    @property
    def url_edit(self):
        url = reverse_lazy(
            'sales:saleorder-edit',
            kwargs={'pk': self.pk}
        )
        return url

    def __str__(self):
        text = '{}: {}'.format(
            self.id,
            self.client,
        )
        return text

    @property
    def subtotal_price(self):
        record_value = 0
        if self.in_saleorderitem.exists() is True:
            record = Decimal(record_value)
            for line in self.in_saleorderitem.all():
                record += Decimal(line.subtotal_price)
            return record
        else:
            return record_value

    @property
    def total_tax(self):
        record_value = 0
        if self.in_saleorderitem.exists() is True:
            record = Decimal(record_value)
            for line in self.in_saleorderitem.all():
                record += Decimal(line.total_tax)
            return record
        else:
            return record_value

    @property
    def tax_value(self):
        if self.tax:
            record = Decimal(self.tax.factor * 100)
            return record

    class Meta:
        verbose_name = 'Orden de Venta'
        verbose_name_plural = 'Ordenes de Venta'


class SaleOrderItem(AppModel):
    order = models.ForeignKey(
        SaleOrder,
        verbose_name='orden',
        related_name='in_saleorderitem',
        on_delete=models.PROTECT,
        blank=False,
        null=True,
    )
    line = models.SlugField(
        verbose_name='linea de la orden',
        max_length=1000,
        null=True,
        blank=True
    )
    item = models.ForeignKey(
        StockItem,
        verbose_name='artículo',
        related_name='has_saleorderitem',
        on_delete=models.PROTECT,
        blank=False,
        null=True,
    )
    qty = models.DecimalField(
        verbose_name='cantidad',
        max_digits=8,
        decimal_places=2,
        default=0,
        validators=[MinValueValidator(Decimal('0'))],
        null=True,
        blank=False
    )
    price = models.DecimalField(
        verbose_name='precio unitario',
        max_digits=12,
        decimal_places=2,
        blank=False,
        null=True,
    )
    tax = models.ForeignKey(
        Tax,
        verbose_name='impuestos',
        related_name='in_saleorderlines',
        on_delete=models.PROTECT,
        blank=True,
        null=True,
    )
    total_amount = models.DecimalField(
        verbose_name='total',
        max_digits=12,
        decimal_places=2,
        blank=False,
        null=True,
    )
    comments = models.TextField(
        verbose_name='comentarios',
        blank=True,
        null=True,
    )

    class Meta:
        unique_together = (('order', 'line'))
        verbose_name = 'Articulo de orden de venta'
        verbose_name_plural = 'Articulos de orden de venta'

    @property
    def subtotal_price(self):
        record = format(self.price * self.qty, '.2f')
        return record

    @property
    def total_tax(self):
        if self.tax:
            record = format(
                Decimal(self.subtotal_price) * (self.tax.factor / 100), '.2f')
            return record
        else:
            record = 0
            return record

    def __str__(self):
        return f'{self.order.warehouse.name} - {self.pk}:{self.line}'

    @property
    def tax_value(self):
        if self.tax:
            record = format(
                Decimal(self.tax.factor * 100), '.2f')
            return record


class Payment(AppModel):
    METHOD_CHOICES = (
        ('CA', 'Efectivo'),
        ('TA', 'Tarjeta'),
        ('CH', 'Cheque'),
    )
    STATUS_CHOICES = (
        ('INP', 'En proceso'),
        ('CO', 'Confirmado'),
        ('CA', 'Cancelado'),
    )
    saleorder = models.ForeignKey(
        SaleOrder,
        verbose_name="orden de venta",
        related_name='payments',
        on_delete=models.PROTECT,
        null=True,
        blank=True,
    )
    amount = models.DecimalField(
        verbose_name='cantidad',
        max_digits=12,
        decimal_places=2,
        default=0,
        validators=[MinValueValidator(Decimal('0'))],
        blank=False,
        null=True,
    )
    method = models.CharField(
        verbose_name='metodo de pago',
        max_length=3,
        choices=METHOD_CHOICES,
        default='CA',
        blank=False,
        null=True,
    )
    branchoffice = models.ForeignKey(
        BranchOffice,
        verbose_name="sucursal",
        related_name='payments_received',
        on_delete=models.PROTECT,
        null=True,
        blank=True,
    )
    status = models.CharField(
        verbose_name='estado',
        max_length=3,
        choices=STATUS_CHOICES,
        default='INP',
        blank=False,
        null=True,
    )
    confirmed_date = models.DateField(
        verbose_name='fecha de confirmada',
        default=date.today,
        blank=True,
        null=True,
    )

    class Meta:
        verbose_name = 'Pago'
        verbose_name_plural = 'Pagos'

    def __str__(self):
        return f'{self.saleorder.id}: ${self.amount}'

    @property
    def status_text(self):
        choices = {
            'INP': 'En proceso',
            'CO': 'Confirmado',
            'CA': 'Cancelado'
        }
        return choices.get(self.status, 'No definido')

    @property
    def status_color(self):
        choices = {
            'INP': 'yellow',
            'CO': 'green',
            'CA': 'red'
        }
        return choices.get(self.status, 'gray')

    @property
    def url_retrieve(self):
        url = reverse_lazy(
            'sales:payment-retrieve',
            kwargs={'pk': self.pk}
        )
        return url

    @property
    def url_edit(self):
        url = reverse_lazy(
            'sales:payment-edit',
            kwargs={'pk': self.pk}
        )
        return url
