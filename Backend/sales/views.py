# Python Libraries
from decimal import Decimal
import facturama
from unipath import Path
import os

# Django Libraries
from django.shortcuts import render
from django.shortcuts import redirect
from django.contrib import messages
from django.views import View
from django.core.exceptions import ImproperlyConfigured
from django.db.models import Sum
from django.http import HttpResponse
from django.urls import reverse_lazy
from django.urls import reverse
from django.core.files import File
from django.conf import settings

# Own Libraries
from tools.views import StxListView
from tools.views import StxEditView
from tools.views import StxRetrieveView
from tools.views import StxView
from tools.views import AppView
from tools.views import StxExportToExcel
from tools.views import StxReactView
from tools.utils import Report
from tools.facturama import InvoiceMultiEmisor
from .business import SaleOrderBsn
from .business import PaymentBsn
from tools.views import ResourceModelImportView
from tools.views import StxEmptyImportView

from .forms import SaleOrderForm
from .forms import SaleOrderInvoiceForm
from .forms import PaymentForm


class PaymentListView(StxListView):
    permissions = ["VEPA"]
    template_name = 'sales/payment/list/payment_list.html'
    business_class = PaymentBsn


class PaymentNewView(StxReactView):
    permissions = ["CRPA"]
    template_name = "sales/payment/new/payment_new.html"


class PaymentEditView(StxEditView):
    permissions = ["EDPA"]
    path_related_company = ['branchoffice', 'company', 'id']
    template_name = "sales/payment/edit/payment_edit.html"
    business_class = PaymentBsn
    form_class = PaymentForm
    update_record_method = "update"
    page_title = 'Editar Pago'


class PaymentRetrieveView(StxRetrieveView):
    permissions = ["VEPA"]
    path_related_company = ['branchoffice', 'company', 'id']
    template_name = "sales/payment/retrieve/payment_retrieve.html"
    business_class = PaymentBsn


class PaymentExportExcel(StxExportToExcel):
    permissions = ['EXPA']
    business_class = PaymentBsn
    dataset_method = 'export_XLSX'
    name_file = 'Listado de Ordenes de Pago.xlsx'


class PaymentImportExcel(ResourceModelImportView):
    permissions = ['IMPA']
    template_name = 'sales/payment/import/payment_import.html'
    business_class = PaymentBsn
    empty_filename = 'Importar pagos'


class PaymentEmptyImportView(StxEmptyImportView):
    permissions = ['IMPA']
    business_class = PaymentBsn


class SaleOrderListView(StxListView):
    permissions = ["VEOV"]
    template_name = 'sales/saleorder/list/saleorder_list.html'
    business_class = SaleOrderBsn

    def pre_Return(self):
        records = self.context['queryset']
        if len(records) < 1000:
            totals = records.aggregate(Sum('total_amount'))
            self.append_Context(total_amount=totals['total_amount__sum'])


class SaleOrderNewView(StxReactView):
    permissions = ['CROV']
    template_name = "sales/saleorder/new/saleorder_new.html"


class SaleOrderEditView(StxEditView):
    permissions = ['EDOV']
    path_related_company = ['warehouse', 'company', 'id']
    update_record_method = "update"
    template_name = "sales/saleorder/edit/saleorder_edit.html"
    business_class = SaleOrderBsn
    form_class = SaleOrderForm
    page_title = 'Editar Orden de Venta'


class SaleOrderRetrieveView(StxRetrieveView):
    permissions = ["VEOV"]
    path_related_company = ['warehouse', 'company', 'id']
    template_name = "sales/saleorder/retrieve/saleorder_retrieve.html"
    business_class = SaleOrderBsn


class SaleOrderToPdfView(AppView):
    business_class = SaleOrderBsn
    template_name = 'sales/saleorder/report_pdf/saleorder_pdf.html'

    def get(self, request, pk, **kwargs):
        instance = self.business_class.get(pk)
        context = {
            'instance': instance
        }
        return Report.render(self.template_name, context)


class SaleOrderSendInvoiceFilesView(StxView):
    business_class = SaleOrderBsn

    def get(self, request, pk):
        try:
            instance = self.business_class.get(pk)
            self.business_class.send_InvoiceMail(instance)
            messages.add_message(
                request,
                messages.SUCCESS,
                "Los archivos en enviaron con exito"
            )
            return redirect(reverse_lazy(
                'sales:saleorder-retrieve',
                kwargs={'pk': instance.pk}
            ))
        except Exception as error:
            messages.add_message(
                request,
                messages.ERROR,
                f"Error al enviar los archivos: {error}"
            )


class SaleOrderInvoiceView(StxView):
    business_class = SaleOrderBsn
    template_name = 'sales/saleorder/invoice/saleorder_invoice.html'
    form = SaleOrderInvoiceForm

    def get(self, request, pk):
        instance = self.business_class.get(pk)
        if instance.invoice:
            raise ValueError('Esta Orden de Venta ya cuenta con Factura')
        context = {
            'instance': instance,
            'form': self.form,
        }
        return render(request, self.template_name, context)

    def post(self, request, pk):

        form = self.form(
            request.POST
        )
        instance = SaleOrderBsn.get(pk)
        if instance.invoice:
            raise ValueError('Esta Orden de Venta ya cuenta con Factura')

        try:
            if form.is_valid():
                data = form.cleaned_data
                SaleOrderBsn.create_Invoice(instance, data, request.user)
                messages.add_message(
                    request,
                    messages.SUCCESS,
                    'Factura Creada Con Éxito'
                )
                return redirect(
                    reverse(
                        'sales:saleorder-retrieve',
                        kwargs={'pk':instance.id}
                    )
                )
            else:
                raise ValueError('Formulario Invalido.')
        except Exception as error:
            messages.error(
                request,
                str(error)
            )
        context = {
            'instance': instance,
            'form': form,
        }
        return render(request, self.template_name, context)


class SaleOrderXMLInvoice(StxView):
    business_class = SaleOrderBsn

    def get(self, request, pk):
        record = self.business_class.get(pk)
        file = record.xml_invoice_file.path
        f = open(file, 'r')
        myfile = File(f)
        response = HttpResponse(myfile, content_type='application/xml')
        response['Content-Disposition'] = f'attachment; filename={record.id}.xml'
        return response


class SaleOrderPDFInvoice(StxView):
    business_class = SaleOrderBsn

    def get(self, request, pk):
        record = self.business_class.get(pk)
        file = record.pdf_invoice_file.path
        f = open(file, 'rb')
        myfile = File(f)
        response = HttpResponse(myfile, content_type='application/pdf')
        response['Content-Disposition'] = f'attachment; filename={record.id}.pdf'
        return response


class SaleOrderInvoiceCancelView(StxView):
    business_class = SaleOrderBsn

    def get(self, request, pk):
        record = self.business_class.get(pk)
        self.business_class.cancel_Invoice(record, request.user)
        messages.success(
            request, "Se canceló la Factura correctamente")
        return redirect(reverse_lazy(
            'sales:saleorder-retrieve',
            kwargs={'pk':record.pk}
        ))


class SaleOrderExportExcel(StxExportToExcel):
    permissions = ['EXOV']
    business_class = SaleOrderBsn
    dataset_method = 'export_XLSX'
    name_file = 'Listado de Ordenes de Venta.xlsx'
