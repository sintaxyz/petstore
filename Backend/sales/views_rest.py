# Django's Libraries
from django.db import transaction

# Third-party Libraries
from rest_framework import viewsets
from rest_framework import status
from rest_framework.response import Response
from rest_framework.permissions import IsAuthenticated

# Own's Libraries
from security.business import CompanyBsn
from .business import SaleOrderBsn
from .business import PaymentBsn

from .serializers import SaleOrderSerializer
from .serializers import SaleOrderRetrieveSerializer
from .serializers import SaleOrderUpdateSerializer
from .serializers import SaleOrderLineSerializer
from .serializers import SaleOrderUpdateDescSerializer
from .serializers import PaymentSerializer
from .serializers import PaymentRetrieveSerializer

from .serializers import PaymentsDashboardSerializer

from .serializers import AccountsReceivableSerializer


class SaleOrderAccountsReceivableAPIView(viewsets.GenericViewSet):
    queryset = ''
    serializer_class = AccountsReceivableSerializer

    def get_queryset(self, user, request):
        records = SaleOrderBsn.get_AccountsReceivable(user, request)
        return records

    def list(self, request, *args, **kwargs):
        queryset = self.get_queryset(request.user, request)
        serializer = self.serializer_class(queryset, many=True)
        return Response(serializer.data)


class SaleOrderListAPIView(viewsets.GenericViewSet):
    serializer_class = SaleOrderSerializer

    def get_QuerySet(self, user):
        records = SaleOrderBsn.get_SaleList(user)
        return records

    def list(self, request, *args, **kwargs):
        queryset = self.filter_queryset(
            self.get_QuerySet(user=request.user)
        )
        serializer = SaleOrderRetrieveSerializer(queryset, many=True)
        return Response(serializer.data)

    def create(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        if serializer.is_valid(raise_exception=False):
            try:
                data_list = serializer.validated_data
                with transaction.atomic():
                    record = SaleOrderBsn.create_Sale(
                        data_list, request.user)
                serializer = SaleOrderSerializer(record)
                return Response(
                    serializer.data,
                    status=status.HTTP_201_CREATED
                )
            except Exception as e:
                return Response(
                    {'detail': str(e)},
                    status=status.HTTP_400_BAD_REQUEST
                )
                print(e)
        else:
            return Response(
                serializer.errors,
                status=status.HTTP_400_BAD_REQUEST
            )


class SaleOrderPendingPayListAPIView(viewsets.GenericViewSet):
    serializer_class = SaleOrderSerializer

    def get_QuerySet(self, user):
        records = SaleOrderBsn.get_PendingPay(user)
        return records

    def list(self, request, *args, **kwargs):
        queryset = self.filter_queryset(
            self.get_QuerySet(user=request.user)
        )
        serializer = SaleOrderRetrieveSerializer(queryset, many=True)
        return Response(serializer.data)


class SaleOrderPendingClientListAPIView(viewsets.GenericViewSet):
    serializer_class = SaleOrderSerializer

    def get_QuerySet(self, user, pk):
        records = SaleOrderBsn.get_PendingClient(user, pk)
        return records

    def list(self, request, pk, *args, **kwargs):
        queryset = self.filter_queryset(
            self.get_QuerySet(user=request.user, pk=pk)
        )
        serializer = self.serializer_class(queryset, many=True)
        return Response(serializer.data)


class SaleOrderRetrieveAPIView(viewsets.GenericViewSet):
    serializer_class = SaleOrderSerializer

    def get_Object(self, pk):
        instance = SaleOrderBsn.get_Sale(pk)
        return instance

    def retrieve(self, request, pk, *args, **kwargs):
        instance = self.get_Object(pk)
        serializer = SaleOrderSerializer(instance)
        return Response(serializer.data)

    def update(self, request, pk, *args, **kwargs):
        instance = self.get_Object(pk)
        serializer = SaleOrderUpdateSerializer(data=request.data)
        if serializer.is_valid(raise_exception=False):
            try:
                data_list = serializer.validated_data
                record = SaleOrderBsn.update_Sale(
                    instance,
                    data_list,
                    request.user
                )
                serializer = SaleOrderUpdateSerializer(record)
                return Response(
                    serializer.data,
                    status=status.HTTP_200_OK
                )
            except Exception as e:
                return Response(
                    {'detail': str(e)},
                    status=status.HTTP_400_BAD_REQUEST
                )
        else:
            return Response(
                serializer.errors,
                status=status.HTTP_400_BAD_REQUEST
            )

    def create(self, request, pk, *args, **kwargs):
        instance = self.get_Object(pk)
        serializer = SaleOrderLineSerializer(data=request.data)
        if serializer.is_valid(raise_exception=False):
            try:
                data_list = serializer.validated_data
                with transaction.atomic():
                    record = SaleOrderBsn.create_SaleLine(
                        instance,
                        data_list,
                        request.user
                    )
                    serializer = SaleOrderLineSerializer(record)
                return Response(
                    serializer.data,
                    status=status.HTTP_200_OK
                )
            except Exception as e:
                return Response(
                    {'detail': str(e)},
                    status=status.HTTP_400_BAD_REQUEST
                )
        else:
            return Response(
                serializer.errors,
                status=status.HTTP_400_BAD_REQUEST
            )


class SaleOrderUpdateDescAPIView(viewsets.GenericViewSet):
    serializer_class = SaleOrderUpdateDescSerializer

    def get_Object(self, pk):
        instance = SaleOrderBsn.get(pk)
        return instance

    def update(self, request, pk):
        instance = self.get_Object(pk)
        serializer = self.serializer_class(data=request.data)
        if serializer.is_valid(raise_exception=False):
            data_list = serializer.validated_data
            try:
                record = SaleOrderBsn.update_Desc(
                    instance, data_list, request.user)
                serializer = SaleOrderSerializer(record)
                return Response(
                    serializer.data,
                    status=status.HTTP_200_OK
                )
            except Exception as e:
                return Response(
                    {'detail': str(e)},
                    status=status.HTTP_400_BAD_REQUEST
                )
        else:
            return Response(
                serializer.errors,
                status=status.HTTP_400_BAD_REQUEST
            )


class SaleOrderFinishAPIView(viewsets.GenericViewSet):
    serializer_class = SaleOrderUpdateDescSerializer

    def get_Object(self, pk):
        instance = SaleOrderBsn.get(pk)
        return instance

    def finish(self, request, pk):
        instance = self.get_Object(pk)
        try:
            record = SaleOrderBsn.finish(instance, request.user)
            serializer = SaleOrderSerializer(record)
            return Response(
                serializer.data,
                status=status.HTTP_200_OK
            )
        except Exception as e:
            return Response(
                {'detail': str(e)},
                status=status.HTTP_400_BAD_REQUEST
            )


class SaleOrderCancelAPIView(viewsets.GenericViewSet):
    serializer_class = SaleOrderUpdateDescSerializer

    def get_Object(self, pk):
        instance = SaleOrderBsn.get(pk)
        return instance

    def cancel(self, request, pk):
        instance = self.get_Object(pk)
        try:
            record = SaleOrderBsn.cancel(instance, request.user)
            serializer = SaleOrderSerializer(record)
            return Response(
                serializer.data,
                status=status.HTTP_200_OK
            )
        except Exception as e:
            return Response(
                {'detail': str(e)},
                status=status.HTTP_400_BAD_REQUEST
            )
        

class SaleOrderLinesUpdateAPIView(viewsets.GenericViewSet):
    serializer_class = SaleOrderLineSerializer

    def get_Object(self, pk, line_pk):
        instance = SaleOrderBsn.get_SaleLine(
            pk, line_pk
            )
        return instance

    def update(self, request, pk, line_pk, *args, **kwargs):
        instance = self.get_Object(pk, line_pk)
        serializer = self.get_serializer(data=request.data)
        if serializer.is_valid(raise_exception=False):
            try:
                data_list = serializer.validated_data
                record = SaleOrderBsn.update_SaleLine(
                    instance,
                    data_list,
                    request.user
                )
                serializer = self.get_serializer(record)
                return Response(
                    serializer.data,
                    status=status.HTTP_200_OK
                )
            except Exception as e:
                return Response(
                    {'detail': str(e)},
                    status=status.HTTP_400_BAD_REQUEST
                )
        else:
            return Response(
                serializer.errors,
                status=status.HTTP_400_BAD_REQUEST
            )


class PaymentListAPIView(viewsets.GenericViewSet):
    serializer_class = PaymentSerializer

    def get_QuerySet(self, user):
        records = PaymentBsn.get_Assigned()
        return records

    def list(self, request, *args, **kwargs):
        queryset = self.filter_queryset(
            self.get_QuerySet(user=request.user)
        )
        serializer = PaymentRetrieveSerializer(queryset, many=True)
        return Response(serializer.data)

    def create(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        if serializer.is_valid(raise_exception=False):
            try:
                data_list = serializer.validated_data
                with transaction.atomic():
                    record = PaymentBsn.create(
                        data_list, request.user)
                serializer = PaymentSerializer(record)
                return Response(
                    serializer.data,
                    status=status.HTTP_201_CREATED
                )
            except Exception as e:
                return Response(
                    {'detail': str(e)},
                    status=status.HTTP_400_BAD_REQUEST
                )
                print(e)
        else:
            return Response(
                serializer.errors,
                status=status.HTTP_400_BAD_REQUEST
            )


class PaymentsInDaysAPIView(viewsets.GenericViewSet):
    queryset = ''
    serializer_class = PaymentsDashboardSerializer

    def get_queryset(self, user, request):
        records = PaymentBsn.get_PaymentsInDays(user, request)
        return records

    def list(self, request, *args, **kwargs):
        queryset = self.get_queryset(request.user, request)
        serializer = self.serializer_class(queryset)
        return Response(serializer.data)
