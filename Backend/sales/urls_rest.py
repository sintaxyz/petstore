# Django's Libraries
from django.urls import path

# Own's Libraries

from .views_rest import SaleOrderListAPIView
from .views_rest import SaleOrderRetrieveAPIView
from .views_rest import SaleOrderLinesUpdateAPIView
from .views_rest import SaleOrderUpdateDescAPIView
from .views_rest import SaleOrderFinishAPIView
from .views_rest import SaleOrderCancelAPIView
from .views_rest import PaymentListAPIView
from .views_rest import SaleOrderPendingPayListAPIView
from .views_rest import SaleOrderPendingClientListAPIView
from .views_rest import SaleOrderAccountsReceivableAPIView
from .views_rest import PaymentsInDaysAPIView
# from .views_rest import OwnSaleOrdersListAPIView
# from .views_rest import SaleOrdersCancelListAPIView

app_name = 'sales-api'

urlpatterns = [

    path(
        'saleorders/',
        SaleOrderListAPIView.as_view({
            'get': 'list',
            'post': 'create',
        }),
        name='saleorder-list'
    ),
    path(
        'saleorders/accountsreceivable/',
        SaleOrderAccountsReceivableAPIView.as_view({
            'get': 'list',
        }),
        name='saleorder-accountsreceivable'
    ),
    path(
        'saleorders/pendingpay/',
        SaleOrderPendingPayListAPIView.as_view({
            'get': 'list',
        }),
        name='saleorder-pendingpay-list'
    ),
    path(
        'saleorders/<int:pk>/pendingclient/',
        SaleOrderPendingClientListAPIView.as_view({
            'get': 'list',
        }),
        name='saleorder-pendingclient-list'
    ),
    path(
        'saleorders/<int:pk>/',
        SaleOrderRetrieveAPIView.as_view({
            'get': 'retrieve',
            'put': 'update',
            'post': 'create',
        }),
        name='saleorder-retrieve'
    ),
    path(
        'saleorders/<int:pk>/desc/',
        SaleOrderUpdateDescAPIView.as_view({
            'post': 'update',
        }),
        name='saleorder-update-desc'
    ),
    path(
        'saleorders/<int:pk>/finish/',
        SaleOrderFinishAPIView.as_view({
            'get': 'finish',
        }),
        name='saleorder-finish'
    ),
    path(
        'saleorders/<int:pk>/cancel/',
        SaleOrderCancelAPIView.as_view({
            'get': 'cancel',
        }),
        name='saleorder-cancel'
    ),
    path(
        'saleorders/<int:pk>/lines/<int:line_pk>/',
        SaleOrderLinesUpdateAPIView.as_view({
            'put': 'update',
        }),
        name='saleorderline-update'
    ),
    path(
        'payments/',
        PaymentListAPIView.as_view({
            'get': 'list',
            'post': 'create',
        }),
        name='payment-list'
    ),
    path(
        'payments/indays/',
        PaymentsInDaysAPIView.as_view({
            'get': 'list',
        }),
        name='payments-indays'
    ),
]
