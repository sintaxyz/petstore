# Python's Libraries
# Django's Libraries
import django_filters

# Third-party Libraries
# Own's Libraries
from .models import SaleOrderItem
from .models import SaleOrder
from .models import Payment

from core.filters import assigned_warehouses
from security.filters import assigned_branchoffices


class SaleOrderItemFilter(django_filters.FilterSet):

    BOOLEAN_CHOICES = (
        ('Si', 'Si'),
        ('No', 'No')
    )

    is_active = django_filters.ChoiceFilter(
        choices=BOOLEAN_CHOICES,
        method='is_ActiveFilter'
    )

    class Meta:
        model = SaleOrderItem
        fields = [
            'item',
            'is_active',
            'total_amount',
            'qty',
        ]

    def is_ActiveFilter(self, queryset, name, value):
        if value == 'Si':
            queryset = queryset.filter(is_active=True)
        elif value == 'No':
            queryset = queryset.filter(is_active=False)
        return queryset


class SaleOrderFilter(django_filters.FilterSet):

    warehouse = django_filters.ModelChoiceFilter(queryset=assigned_warehouses)

    have_amount_outstanding = django_filters.BooleanFilter(
        label="Adeudo Pendiente",
        method='filter_HaveAmountOutstanding'
    )

    client__name = django_filters.CharFilter(
        label="Nombre de Cliente", lookup_expr="icontains")
    client__id = django_filters.CharFilter(
        label="Numero de Cliente")

    class Meta:
        model = SaleOrder
        fields = [
            'client__id',
            'client__name',
            'status',
            'warehouse',
            'work_id',
            'have_amount_outstanding',
        ]

    def filter_HaveAmountOutstanding(self, queryset, name, value):
        if value == True:
            return queryset.filter(amount_outstanding__gt=0)
        else:
            return queryset.filter(amount_outstanding__lte=0)


class PaymentFilter(django_filters.FilterSet):
    
    branchoffice = django_filters.ModelChoiceFilter(queryset=assigned_branchoffices)

    saleorder__client__name = django_filters.CharFilter(
        label="Nombre de Cliente", lookup_expr="icontains")
    saleorder__client__id = django_filters.CharFilter(
        label="Numero de Cliente")

    class Meta:
        model = Payment
        fields = [
            'saleorder__client__id',
            'saleorder__client__name',
            'method',
            'branchoffice',
            'status',
        ]
