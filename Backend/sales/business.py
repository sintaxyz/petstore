# Python's Libraries
from decimal import Decimal
from datetime import datetime
from datetime import timedelta
from datetime import date
# import io

# Django's Libraries
from django.db import transaction
from django.db.models import Sum
from django.conf import settings
# from django.urls import reverse_lazy

# Third-party Libraries
# from xlsxwriter.workbook import Workbook

# Own's Libraries
from .resources import SaleOrderResource
from .resources import PaymentResource

# from tools.utils import Helper
from tools.business import StxBsn
from tools.facturama import InvoiceMultiEmisor

from .models import SaleOrderItem
from .models import SaleOrder
from .models import Payment

from .filters import SaleOrderFilter
from .filters import PaymentFilter

from core.business import StockItemBsn
from support.business import EmailBsn

# # Testing Channels
# from asgiref.sync import async_to_sync
# from sales.serializers import SaleOrderSerializer
# from rest_framework.renderers import JSONRenderer
# import channels
# import json
# # Testing Channels


class SaleOrderBsn(StxBsn):
    model = SaleOrder
    model_filter = SaleOrderFilter
    model_resource = SaleOrderResource

    @classmethod
    def send_InvoiceMail(self, record):
        try:
            client_email = record.client.email
            company = record.branchoffice.company.commercial_name
            files = []
            if record.xml_invoice_file:
                files.append(record.xml_invoice_file)
            if record.pdf_invoice_file:
                files.append(record.pdf_invoice_file)

            email_settings = EmailBsn.get_SettingsByUsername(settings.INVOICE_EMAIL)
            sended_mail = EmailBsn.send(
                settings=email_settings,
                to_emails=[client_email, ],
                subject=f'Factura de {company}.',
                body=f' Factura creada en PetStore. Mas información en www.lead.com.mx',
                files=files,
            )
            return sended_mail
        except Exception as error:
            raise ValueError(str(error))


    @classmethod
    def get_AccountsReceivable(self, user, request):
        records = self.get_Assigned(user)
        if request.GET.get('company', None):
            records = records.filter(branchoffice__company__id=request.GET.get('company'))
        if request.GET.get('branchoffice', None):
            records = records.filter(branchoffice__id=request.GET.get('branchoffice'))
        if request.GET.get('client', None):
            records = records.filter(client__id=request.GET.get('client'))
        with_debt = records.filter(amount_outstanding__gt=0).exclude(credit_days=None)
        organized_records = with_debt.values('credit_days').annotate(
            Sum('amount_outstanding')).order_by('-credit_days')
        return organized_records

    @classmethod
    def filter_ByPermission(self, records, user):
        return records.filter(warehouse__company__in=user.companies.all())

    @classmethod
    def get_Assigned(self, user):
        return self.get_All().filter(warehouse__company__in=user.companies.all())

    @classmethod
    def affect_Stock(self, _record, _user):
        with transaction.atomic():
            for line in _record.in_saleorderitem.all():
                stock = line.item
                stock.qty -= line.qty
                stock.full_clean()
                stock.save()

    @classmethod
    def create_Invoice(self, _record, _data, _user):
        invoice = InvoiceMultiEmisor()
        invoice.set_SaleOrder(_record)
        invoice.check_In(_data)
        invoice.save_DocumentXml()
        invoice.save_DocumentPdf()
        _record.invoice = invoice.document.Id
        _record.save()

    @classmethod
    def cancel_Invoice(self, _record, _user):
        invoice = InvoiceMultiEmisor()
        invoice.cancel_Document(_record.invoice)
        _record.invoice = None
        _record.xml_invoice_file = None
        _record.pdf_invoice_file = None
        _record.save()
        return _record

    @classmethod
    def finish(self, _record, _user):
        with transaction.atomic():
            if _record.warehouse:
                company = _record.warehouse.company.id
            else:
                company = _record.branchoffice.company.id
            approved = self.validate_Access(_user, ['CROV','EDOV'], company)
            if approved is False:
                raise ValueError('No tiene permiso para finalizar orden de venta.')
            self.validate_Editable(_record)
            self.affect_Stock(_record, _user)
            _record.status = 'DE'
            _record.updated_by = _user
            _record.save()
            PaymentBsn.pay_SaleOrder(_record, _user)
            return _record

    @classmethod
    def export_XLSX(self, records):
        return SaleOrderResource().export(records).xlsx

    @classmethod
    def cancel(self, _record, _user):
        with transaction.atomic():
            if _record.warehouse:
                company = _record.warehouse.company.id
            else:
                company = _record.branchoffice.company.id
            approved = self.validate_Access(_user, 'CAOV', company)
            if approved is False:
                raise ValueError('No tiene permiso para cancelar orden de venta.')
            if _record.status == 'INP':
                _record.status = 'CA'
                _record.updated_by = _user
                _record.save()
            else:
                raise ValueError('Esta orden ya ha sido Entregada o Cancelada.')
            return _record

    @classmethod
    def update_Desc(self, _record, _data, _user):
        if _record.warehouse:
            company = _record.warehouse.company.id
        else:
            company = _record.branchoffice.company.id
        approved = self.validate_Access(_user, 'EDOV', company)
        if approved is False:
            raise ValueError('No tiene permiso para vender en este almacen o sucursal.')
        with transaction.atomic():
            self.validate_Editable(_record)
            _record.description = _data.get('description')
            _record.comments = _data.get('comments')
            _record.work_id = _data.get('work_id')
            _record.credit_days = _data.get('credit_days')
            _record.paymethod = _data.get('paymethod')
            _record.updated_by = _user
            self.validate_CreditOptions(_record)
            _record.save()
            return _record

    @classmethod
    def get_SaleList(self, user):
        records = self.get_ModelObject().objects.all()
        return records

    @classmethod
    def validate_CreditOptions(self, _record):
        if _record.paymethod == 'CR' and not _record.credit_days:
            raise ValueError('Debe poner un limite para los días de crédito.')
        if _record.credit_days and _record.paymethod != 'CR':
            raise ValueError('Para asignar un valor a los dias de credito necesita cambiar el metodo de pago.')

    @classmethod
    def create_Sale(self, _data, _user, _files=None):
        with transaction.atomic():
            if _data.get('warehouse'):
                company = _data.get('warehouse').company.id
            elif _data.get('branchoffice'):
                company = _data.get('branchoffice').company.id
            else:
                raise ValueError('Es necesario ingresar Almacen o sucursal.')
            approved = self.validate_Access(_user, company, 'CROV')
            if approved is False:
                raise ValueError('No tiene permiso para vender en este almacen o sucursal.')
            record = self.get_ModelObject()()
            record.comments = _data.get('comments')
            record.warehouse = _data.get('warehouse')
            record.branchoffice = _data.get('branchoffice')
            record.work_id = _data.get('work_id')
            record.credit_days = _data.get('credit_days')
            record.client = _data.get('client')
            record.description = _data.get('description')
            record.tax = _data.get('tax')
            self.validate_CreditOptions(record)
            record.created_by = _user
            record.full_clean()
            record.save()

            self.update_SaleByLine(record, _user, True)
            # self.send_UpdateTowarehouse(record)
            return record

    @classmethod
    def update_Sale(self, _record, _data, _user, _files=None):
        with transaction.atomic():
            _record.warehouse = _data.get('warehouse')
            _record.client = _data.get('client')
            _record.comments = _data.get('comments')
            _record.description = _data.get('description')
            _record.client = _data.get('client')
            _record.tax.set(_data.get('tax'))
            _record.updated_by = _user
            _record.full_clean()
            _record.save()
            return _record

    @classmethod
    def create_SaleLine(self, _record, _data, _user, _files=None):
        with transaction.atomic():
            if _record.warehouse:
                company = _record.warehouse.company.id
            else:
                company = _record.branchoffice.company.id
            approved = self.validate_Access(_user, ['CROV', 'EDOV'], company)
            if approved is False:
                raise ValueError('No tiene permiso para vender en este almacen o sucursal.')
            record = SaleOrderItem()
            item_stock = StockItemBsn.get_ItemStock(
                _data.get('item'), _record.warehouse
            )
            if item_stock.qty < _data.get('qty'):
                error = "La cantidad solicitada es superior a la existencia de {}".format(
                    str(item_stock.qty)
                )
                raise ValueError(error)
            else:
                record.qty = _data.get('qty')
            record.order = _record
            record.item = _data.get('item')
            record.comments = _data.get('comments')
            record.price = record.item.sell_price
            record.tax = _data.get('tax')
            record.total_amount = Decimal(record.subtotal_price) + Decimal(record.total_tax)
            record.full_clean()
            record.save()
            record.line = "OV{}LN{}".format(
                str(record.order.id), str(record.id)
                )
            record.save()
            self.update_SaleByLine(record.order, _user)
            return record

    @classmethod
    def update_SaleLine(self, _record, _data, _user, _files=None):
        with transaction.atomic():
            _record.comments = _data.get('comments')
            if _record.item == _data.get('item'):
                qty_stock = StockItemBsn.get_ItemStock(
                    _record.item, _record.order.warehouse
                    )
                if qty_stock:
                    _record.price = qty_stock.sell_price
                    qty_temp = qty_stock.qty + _record.qty
                    qty_stock.qty = qty_temp - _data.get('qty')
                    qty_stock.updated_by = _user
                    qty_stock.save()
                else:
                    raise NameError('No se encontro el articulo.')
            else:
                qty_stock = StockItemBsn.get_ItemStock(
                    _record.item, _record.order.warehouse
                    )
                if qty_stock:
                    qty_temp = qty_stock.qty + _record.qty
                    qty_stock.qty = qty_temp
                    qty_stock.updated_by = _user
                    qty_stock.save()
                else:
                    raise NameError('No se encontro el articulo.')
                qty_stock = StockItemBsn.get_ItemStock(
                    _data.get('item'), _record.order.warehouse
                    )
                if qty_stock:
                    _record.price = qty_stock.sell_price
                    qty_temp = qty_stock.qty - _data.get('qty')
                    qty_stock.qty = qty_temp
                    qty_stock.updated_by = _user
                    qty_stock.save()
                else:
                    raise NameError('No se encontro el articulo.')
            _record.item = _data.get('item')
            _record.qty = _data.get('qty')
            _record.total_amount = Decimal(format(
                _record.price * _record.qty, '.2f'))
            _record.updated_by = _user
            _record.full_clean()
            _record.save()
            self.update_SaleByLine(_record.order, _user)
            return _record

    @classmethod
    def update_SaleByLine(self, _record, _user, is_created=False):
        self.validate_Editable(_record)
        _record.qty_items = sum(
            [i.qty for i in _record.in_saleorderitem.all()]
        )
        _record.total_amount = sum(
            [Decimal(i.subtotal_price) + Decimal(i.total_tax) for i in _record.in_saleorderitem.all()]
        )
        _record.amount_outstanding = _record.total_amount
        if is_created:
            pass
        else:
            _record.updated_by = _user
        _record.full_clean()
        _record.save()
        return _record

    @classmethod
    def get_Sale(self, pk):
        record = self.get_ModelObject().objects.get(id=pk)
        if record:
            return record
        else:
            raise ValueError('No existe este despacho de articulos.')

    @classmethod
    def get_SaleLine(self, pk, line_pk):
        order = self.get_ModelObject().objects.get(id=pk)
        if order:
            record = order.in_saleorderitem.get(id=line_pk)
            return record
        else:
            raise ValueError('No existe esta linea de artículo.')

    @classmethod
    def get_AllPermissioned(self, **kwargs):
        records = self.get_All()
        return records

    @classmethod
    def get_OwnSaleOrders(self, user):
        records = SaleOrder.objects.filter(
            created_by=user.id).order_by('-created_date')
        return records

    @classmethod
    def get_CancelList(self, **kwargs):
        request = kwargs.get('request', None)
        if request:
            user = request.user
            if user.is_superuser or user.employee_profile:
                records = self.get_All().filter(status='CA')
            else:
                records = [None, ]
        else:
            records = [None, ]
        return records

    @classmethod
    def search(self, records, _value):
        try:
            records = records.filter(id=_value)
        except:
            records = records.filter(client__commercial_name__icontains=_value)
        return records

    @classmethod
    def validate_Editable(self, _record):
        if _record.status != 'INP':
            raise PermissionError('No se puede modificar este registro.')

    @classmethod
    def get_PendingPay(self, _user):
        records = self.get_Assigned(_user)
        pendings = records.filter(amount_outstanding__gt=0, status='DE')
        return pendings

    @classmethod
    def get_PendingClient(self, user, pk):
        records = self.get_Assigned(user)
        records = records.filter(client__id=pk)
        pendings = records.filter(amount_outstanding__gt=0, status='DE')
        return pendings


class PaymentBsn(StxBsn):
    model = Payment
    model_resource = PaymentResource
    model_filter = PaymentFilter
        

    @classmethod
    def filter_ByPermission(self, records, user):
        return records.filter(
            saleorder__warehouse__company__in=user.companies.all())

    @classmethod
    def get_Assigned(self, user):
        return self.get_All().filter(
            saleorder__warehouse__company__in=user.companies.all())

    @classmethod
    def export_XLSX(self, records):
        return PaymentResource().export(records).xlsx

    @classmethod
    def get_PaymentsInDays(self, user, request, days_ago: int = 10):
        string_start_date = request.GET.get('start_date', None)
        string_end_date = request.GET.get('end_date', None)
        if string_end_date:
            end_date = datetime.strptime(string_end_date, '%Y-%m-%d').date()
        else:
            end_date = date.today()

        if string_start_date:
            start_date = datetime.strptime(string_start_date, '%Y-%m-%d').date()
            outstanding_days = end_date - start_date
            days_ago = outstanding_days.days if outstanding_days.days <= days_ago else days_ago
        
        records = self.get_Assigned(user)
        if request.GET.get('company', None):
            records = records.filter(
                branchoffice__company__id=request.GET.get('company'))
        filtered_by_date = records.filter(
            confirmed_date__lte=end_date,
            confirmed_date__gte=end_date-timedelta(days=days_ago))
        payments_total = filtered_by_date.aggregate(Sum('amount'))
        organized_records = []
        
        for days in range(days_ago, -1, -1):
            past_days = end_date - timedelta(days=days)
            past_pay = records.filter(confirmed_date=past_days)
            if past_pay.exists():
                value_day = past_pay.values('confirmed_date').annotate(
                    Sum('amount'))
                organized_records.append(value_day.get())
            else:
                value_day = {"confirmed_date": past_days, 'amount__sum': Decimal('0.00')}
                organized_records.append(value_day)

        values = {
            'records': organized_records,
            'total': payments_total['amount__sum']
        }
        return values

    @classmethod
    def pay_SaleOrder(self, _record, _user):
        if _record.warehouse:
            company = _record.warehouse.company.id
        else:
            company = _record.branchoffice.company.id
        approved = self.validate_Access(_user, ['CRPA', 'EDPA'], company)
        if approved is False:
            raise ValueError('No tiene permiso crear pago en este almacen o sucursal.')
        if _record.paymethod != 'CR':
            data = {
                'saleorder': _record,
                'branchoffice': _record.branchoffice,
                'amount': _record.amount_outstanding,
                'method': _record.paymethod,
            }
            self.create(data, _user)
        else:
            if not _record.client.credit_days:
                raise ValueError('El cliente no cuenta con crédito.')
            else:
                total_debt = _record.client.debt + _record.total_amount
                if _record.client.credit_limit < total_debt:
                    raise ValueError('El cliente excede su limite de credito.')
                else:
                    client = _record.client
                    client.debt += _record.total_amount
                    client.full_clean()
                    client.save()

    @classmethod
    def search(self, records, _value):
        try:
            records = records.filter(id=_value)
        except:
            records = records.filter(saleorder__client__name__icontains=_value)
        return records

    @classmethod
    def get_AllPermissioned(self, **kwargs):
        records = self.get_All()
        return records

    @classmethod
    def create(self, _data, _user):
        with transaction.atomic():
            if _data.get('saleorder').warehouse:
                company = _data.get('saleorder').warehouse.company.id
            else:
                company = _data.get('saleorder').branchoffice.company.id
            approved = self.validate_Access(_user, 'CRPA', company)
            if approved is False:
                raise ValueError('No tiene permiso crear pago en este almacen o sucursal.')
            record = self.get_ModelObject()()
            record.saleorder = _data.get('saleorder')
            record.branchoffice = record.saleorder.branchoffice
            record.amount = _data.get('amount')
            record.method = _data.get('method')
            record.status = _data.get('status', 'CO')
            if record.status == 'CO':
                record.confirmed_date = datetime.today()
                saleorder = record.saleorder
                saleorder.amount_outstanding -= record.amount
                saleorder.full_clean()
                saleorder.save()
                if saleorder.paymethod == 'CR':
                    client = saleorder.client
                    client.debt -= record.amount
                    client.full_clean()
                    client.save()
            record.full_clean()
            record.save()
            return record
