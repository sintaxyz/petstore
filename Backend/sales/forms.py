# Python's Libraries
# Django's Libraries
from django import forms
from django.db.models import Q

# Third-party Libraries
# Own's Libraries
from tools.forms import StxModelForm
from tools.forms import StxModelCreateForm
from tools.forms import StxModelUpdateForm

from .business import SaleOrderBsn
from .business import PaymentBsn

from .models import SaleOrder
from .models import Payment

class SaleOrderInvoiceForm(forms.Form):
    PAY_FORM_CHOICES = (
        ('01', 'Efectivo'),
        ('03', 'Tarjeta'),
        ('99', 'Crédito'),
    )
    PAYMETHOD_CHOICES = (
        ('PPD', 'Pago en Parcialidades Diferidas'),
        ('PUE', 'Pago en Una Exibición'),
    )        
    CFDI_USE_CHOICES = (
        ('D01', 'Honorarios médicos, dentales y gastos hospitalarios.'),
        ('D02', 'Gastos médicos por incapacidad o discapacidad'),
        ('D03', 'Gastos funerales.'),
        ('D04', 'Donativos.'),
        ('D05', 'Intereses reales efectivamente pagados por créditos hipotecarios (casa habitación).'),
        ('D06', 'Aportaciones voluntarias al SAR.'),
        ('D07', 'Primas por seguros de gastos médicos.'),
        ('D08', 'Gastos de transportación escolar obligatoria.'),
        ('D09', 'Depósitos en cuentas para el ahorro, primas que tengan como base planes de pensiones.'),
        ('D10', 'Pagos por servicios educativos (colegiaturas)'),
        ('G01', 'Adquisición de mercancias'),
        ('G02', 'Devoluciones, descuentos o bonificaciones'),
        ('G03', 'Gastos en general'),
        ('I01', 'Construcciones'),
        ('I02', 'Mobilario y equipo de oficina por inversiones'),
        ('I03', 'Equipo de transporte'),
        ('I04', 'Equipo de computo y accesorios'),
        ('I05', 'Dados, troqueles, moldes, matrices y herramental'),
        ('I06', 'Comunicaciones telefónicas'),
        ('I07', 'Comunicaciones satelitales'),
        ('I08', 'Otra maquinaria y equipo'),
        ('P01', 'Por definir')
    )
    payment_form = forms.ChoiceField(choices=PAY_FORM_CHOICES, required=True, label='Forma de Pago')
    payment_method = forms.ChoiceField(choices=PAYMETHOD_CHOICES, required=True, label='Metodo de Pago')
    cfdi_use = forms.ChoiceField(choices=CFDI_USE_CHOICES, required=True, label='Uso de CFDI')

class SaleOrderForm(StxModelCreateForm):
    business_class = SaleOrderBsn

    class Meta:
        model = SaleOrder
        fields = [
            'paymethod',
            'credit_days',
            'description',
            'total_amount',
            'amount_outstanding',
            'warehouse',
            'branchoffice',
            'status',
            'work_id',
            'tax',
            'comments',
            'client',
            'qty_items',
        ]


class PaymentForm(StxModelCreateForm):
    business_class = PaymentBsn

    class Meta:
        model = Payment
        fields = [
            'saleorder',
            'amount',
            'method',
            'branchoffice',
            'status',
            'confirmed_date',
        ]