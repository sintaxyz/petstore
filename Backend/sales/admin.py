from django.contrib import admin

# Register your models here.
from .models import SaleOrder
from .models import SaleOrderItem
from .models import Payment


class SaleOrderItemInLine(admin.StackedInline):
    model = SaleOrderItem
    extra = 1


@admin.register(SaleOrder)
class SaleOrderAdmin(admin.ModelAdmin):
    list_display = (
        'client',
        'warehouse',
    )
    search_fields = (
        'client',
        'warehouse',
    )
    inlines = [SaleOrderItemInLine, ]


@admin.register(Payment)
class PaymentAdmin(admin.ModelAdmin):
    list_display = (
        'saleorder',
        'amount',
        'branchoffice',
    )
    search_fields = (
        'id',
    )
