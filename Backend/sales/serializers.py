# Python's Libraries
# Django's Libraries
# Third-party Libraries
from rest_framework import serializers

# Own's Libraries
from .models import SaleOrder
from .models import SaleOrderItem
from .models import Payment

from core.serializers import TaxSerializer
from core.serializers import ClientSerializer


class AccountsReceivableSerializer(serializers.Serializer):
    credit_days = serializers.IntegerField()
    amount_outstanding__sum = serializers.DecimalField(
        max_digits=10, decimal_places=2)


class SaleOrderUpdateDescSerializer(serializers.ModelSerializer):

    class Meta:
        model = SaleOrder
        exclude = (
            'total_amount',
            'warehouse',
            'tax',
            'client',
            'qty_items',
            'credit_days'
        )


class SaleOrderLineSerializer(serializers.ModelSerializer):
    item_name = serializers.SerializerMethodField()
    tax_desc = serializers.SerializerMethodField()

    class Meta:
        model = SaleOrderItem
        fields = [
            'id',
            'item',
            'item_name',
            'price',
            'qty',
            'total_amount',
            'line',
            'comments',
            'tax',
            'tax_desc',
            'subtotal_price',
            'total_tax',
        ]
        read_only_fields = [
            'id',
            'item_name',
            'price',
            'total_amount',
            'subtotal_price',
            'total_tax',
            'tax_desc'
        ]
        # extra_kwargs = {
        #     'tax': {'write_only': True},
        # }

    def get_item_name(self, obj):
        return obj.item.item.name

    def get_tax_desc(self, obj):
        if obj.tax:
            return f'{obj.tax.abbreviation}: %{obj.tax.factor}'
        else:
            return "Sin impuesto"


class SaleOrderLineRetrieveSerializer(SaleOrderLineSerializer):
    # tax = TaxSerializer(required=False, many=True)
    pass


class SaleOrderSerializer(serializers.ModelSerializer):
    in_saleorderitem = SaleOrderLineSerializer(
        required=False, many=True)
    company_name = serializers.SerializerMethodField()
    warehouse_name = serializers.SerializerMethodField()
    client_name = serializers.SerializerMethodField()
    client_credit_days = serializers.SerializerMethodField()
    branchoffice_name = serializers.SerializerMethodField()
    status_desc = serializers.SerializerMethodField()
    tax_desc = serializers.SerializerMethodField()

    class Meta:
        model = SaleOrder
        fields = [
            'id',
            'company_name',
            'warehouse',
            'branchoffice',
            'branchoffice_name',
            'warehouse_name',
            'client',
            'client_name',
            'credit_days',
            'client_credit_days',
            'qty_items',
            'description',
            'work_id',
            'comments',
            'total_amount',
            'tax',
            'tax_desc',
            'paymethod',
            'status_desc',
            'subtotal_price',
            'total_tax',
            'in_saleorderitem',
        ]
        # extra_kwargs = {
        #     'client': {'write_only': True},
        # }
        read_only_fields = [
            'id',
            'qty_items',
            'company_name',
            'warehouse_name',
            'branchoffice_name',
            'client_name',
            'client_credit_days',
            # 'client',
            'tax_desc',
            'subtotal_price',
            'total_tax',
            'status_desc'
        ]

    def get_company_name(self, obj):
        return obj.warehouse.company.commercial_name

    def get_client_credit_days(self, obj):
        if obj.client:
            if obj.client.credit_days:
                return obj.client.credit_days
        return None

    def get_warehouse_name(self, obj):
        return obj.warehouse.name

    def get_branchoffice_name(self, obj):
        return obj.branchoffice.name

    def get_tax_desc(self, obj):
        if obj.tax:
            return f'{obj.tax.abbreviation}: %{obj.tax.factor}'
        else:
            return "Sin impuesto"

    def get_client_name(self, obj):
        if obj.client:
            return obj.client.commercial_name
        else:
            return None

    def get_status_desc(self, obj):
        return obj.get_status_display()


class SaleOrderRetrieveSerializer(SaleOrderSerializer):
    tax = TaxSerializer(required=False, many=False)
    in_saleorderitem = SaleOrderLineRetrieveSerializer(
        required=False, many=True
        )


class SaleOrderUpdateSerializer(serializers.ModelSerializer):
    company_name = serializers.SerializerMethodField()
    warehouse_name = serializers.SerializerMethodField()
    branchoffice_name = serializers.SerializerMethodField()
    client_name = serializers.SerializerMethodField()
    status_desc = serializers.SerializerMethodField()

    class Meta:
        model = SaleOrder
        fields = [
            'id',
            'company_name',
            'branchoffice',
            'branchoffice_name',
            'warehouse',
            'warehouse_name',
            'client',
            'client_name',
            'qty_items',
            'description',
            'work_id',
            'comments',
            'tax',
            'total_amount',
            'paymethod',
            'status_desc',
        ]
        extra_kwargs = {
            'warehouse': {'write_only': True},
            'client': {'write_only': True},
        }
        read_only_fields = [
            'id',
            'qty_items',
            'company_name',
            'warehouse_name',
            'branchoffice_name',
            'client_name',
            'status_desc'
        ]

    def get_company_name(self, obj):
        return obj.warehouse.company.commercial_name

    def get_warehouse_name(self, obj):
        return obj.warehouse.name

    def get_branchoffice_name(self, obj):
        return obj.branchoffice.name

    def get_client_name(self, obj):
        if obj.client:
            return obj.client.commercial_name
        else:
            return None

    def get_status_desc(self, obj):
        return obj.get_status_display()


class PaymentSerializer(serializers.ModelSerializer):

    client_name = serializers.SerializerMethodField()

    class Meta:
        model=Payment
        fields = [
            'id',
            'saleorder',
            'amount',
            'method',
            'client_name',
            'branchoffice',
        ]
        read_only_fields = [
            'id',
            'client_name',
        ]

    def get_client_name(self, obj):
        return obj.saleorder.client.commercial_name


class PaymentRetrieveSerializer(PaymentSerializer):
    saleorder = SaleOrderRetrieveSerializer(required=True, many=False)


class PaymentsInDaysSerializer(serializers.Serializer):
    confirmed_date = serializers.DateField(format="%m-%d")
    amount__sum = serializers.DecimalField(
        max_digits=10, decimal_places=2)


class PaymentsDashboardSerializer(serializers.Serializer):
    records = PaymentsInDaysSerializer(many=True)
    total = serializers.DecimalField(max_digits=10, decimal_places=2)
