# Django's Libraries
from django.urls import path

# Own's Libraries
from .views import SaleOrderListView
from .views import SaleOrderNewView
from .views import SaleOrderEditView
from .views import SaleOrderRetrieveView
from .views import PaymentListView
from .views import PaymentNewView
from .views import PaymentEditView
from .views import PaymentRetrieveView
from .views import PaymentExportExcel
from .views import PaymentImportExcel
from .views import PaymentEmptyImportView
from .views import SaleOrderToPdfView
from .views import SaleOrderInvoiceView
from .views import SaleOrderExportExcel
from .views import SaleOrderXMLInvoice
from .views import SaleOrderPDFInvoice
from .views import SaleOrderInvoiceCancelView
from .views import SaleOrderSendInvoiceFilesView

app_name = 'sales'


urlpatterns = [
    path(
        'saleorders/',
        SaleOrderListView.as_view(),
        name='saleorder-list'
    ),
    path(
        'saleorders/new/',
        SaleOrderNewView.as_view(),
        name='saleorder-new'
    ),
    path(
        'saleorders/<int:pk>/edit/',
        SaleOrderEditView.as_view(),
        name='saleorder-edit'
    ),
    path(
        'saleorders/<int:pk>/retrieve/',
        SaleOrderRetrieveView.as_view(),
        name='saleorder-retrieve'
    ),
    path(
        'saleorders/<int:pk>/invoice/sendmail/',
        SaleOrderSendInvoiceFilesView.as_view(),
        name='saleorder-invoice-files'
    ),
    path(
        'payments/',
        PaymentListView.as_view(),
        name='payment-list'
    ),
    path(
        'payments/new/',
        PaymentNewView.as_view(),
        name='payment-new'
    ),
    path(
        'payments/<int:pk>/edit/',
        PaymentEditView.as_view(),
        name='payment-edit'
    ),
    path(
        'payments/<int:pk>/retrieve/',
        PaymentRetrieveView.as_view(),
        name='payment-retrieve'
    ),
    path(
        'saleorders/<int:pk>/export/pdf',
        SaleOrderToPdfView.as_view(),
        name='saleorder-export-pdf'
    ),
    path(
        'saleorders/<int:pk>/invoice/',
        SaleOrderInvoiceView.as_view(),
        name='saleorder-invoice'
    ),
    path(
        'saleorders/<int:pk>/invoice/cancel/',
        SaleOrderInvoiceCancelView.as_view(),
        name='saleorder-invoice-cancel'
    ),
    path(
        'saleorders/<int:pk>/invoice/xml/',
        SaleOrderXMLInvoice.as_view(),
        name='saleorder-invoice-xml'
    ),
    path(
        'saleorders/<int:pk>/invoice/pdf/',
        SaleOrderPDFInvoice.as_view(),
        name='saleorder-invoice-pdf'
    ),
    path(
        'saleorders/export/xlsx/',
        SaleOrderExportExcel.as_view(),
        name='saleorders-export-xlsx'
    ),
    path(
        'payments/export/xlsx/',
        PaymentExportExcel.as_view(),
        name='payment-export-xlsx'
    ),
    path(
        'payments/import/',
        PaymentImportExcel.as_view(),
        name='payment-import'
    ),
    path(
        'saleorders/import/empty/',
        PaymentEmptyImportView.as_view(),
        name='payment-import-empty'
    ),
]
