# Python's Libraries

# Django's Libraries

# Third-party Libraries

# Own's Libraries
from tools.views import StxListView
from tools.views import StxRetrieveView
from tools.views import StxNewView
from tools.views import StxEditView
from tools.views import StxExportToExcel
from tools.views import StxRelatedExportToExcel
from tools.views import StxModelRelatedListView
from tools.views import StxModelRelatedNewView
from tools.views import StxModelRelatedEditView
from tools.views import ResourceModelImportView
from tools.views import StxEmptyImportView

from .business import SupplierBsn
from .business import ClientBsn
from .business import ItemBsn
from .business import WarehouseBsn
from .business import UdmBsn
from .business import SatProductBsn
from .business import StockItemBsn

from .forms import SupplierForm
from .forms import SupplierContactForm
from .forms import ClientForm
from .forms import ClientContactForm
from .forms import ItemForm
from .forms import WarehouseForm
from .forms import UdmForm
from .forms import SatProductForm


class SupplierListView(StxListView):
    permissions = ['VEPV']
    template_name = 'core/supplier/list/supplier_list.html'
    business_class = SupplierBsn


class SupplierNewView(StxNewView):
    permissions = ['CRPV']
    template_name = 'core/supplier/new/supplier_new.html'
    business_class = SupplierBsn
    form_class = SupplierForm


class SupplierEditView(StxEditView):
    permissions = ['EDPV']
    path_related_company = ['company', 'id']
    update_record_method = 'update'
    template_name = 'core/supplier/edit/supplier_edit.html'
    business_class = SupplierBsn
    form_class = SupplierForm
    page_title = 'Editar Proveedor'


class SupplierExportExcel(StxExportToExcel):
    permissions = ['EXPV']
    business_class = SupplierBsn
    dataset_method = 'export_XLSX'
    name_file = 'Listado de proveedores.xlsx'


class SupplierImportExcel(ResourceModelImportView):
    permissions = ['IMPV']
    template_name = 'core/supplier/import/supplier_import.html'
    business_class = SupplierBsn
    empty_filename = 'Proveedores'


class SupplierEmptyImportView(StxEmptyImportView):
    permissions = ['IMPV']
    business_class = SupplierBsn


class SupplierContactListView(StxModelRelatedListView):
    permissions = ['VECP']
    template_name = 'core/contactsupplier/list/contactsupplier_list.html'
    related_field = 'belong_to'
    business_class = SupplierBsn
    get_url_new_method = 'get_UrlNewContact'
    page_title = 'Contactos del Proveedor'


class SupplierContactNewView(StxModelRelatedNewView):
    permissions = ['CRCP']
    template_name = 'core/contactsupplier/new/contactsupplier_new.html'
    back_text = "Contactos Proveedor"
    related_field = 'supplier'
    last_record_method = 'get_ContactLastRecord'
    get_url_redirect_method = 'get_UrlOnSaveContact'
    get_url_back_redirect_method = 'get_UrlBackList'
    business_class = SupplierBsn
    form_class = SupplierContactForm
    create_record_method = 'create_Contact'
    page_title = 'Creacion de Contacto'


class SupplierContactEditView(StxModelRelatedEditView):
    permissions = ['EDCP']
    path_related_company = ['supplier', 'company', 'id']
    update_record_method = 'update_Contact'
    template_name = 'core/contactsupplier/edit/contactsupplier_edit.html'
    back_text = "Contactos Proveedor"
    business_class = SupplierBsn
    form_class = SupplierContactForm
    related_field = 'supplier'
    get_record_method = 'get_ContactRecord'
    get_url_redirect_method = 'get_UrlOnSaveContact'


class SupplierContactExportExcel(StxRelatedExportToExcel):
    permissions = ['EXCP']
    business_class = SupplierBsn
    related_field = 'belong_to'
    dataset_method = 'export_Contact_XLSX'
    name_file = 'Listado de contacto de proveedores.xlsx'


class SupplierContactImportExcel(ResourceModelImportView):
    permissions = ['IMCP']
    template_name = 'core/contactsupplier/import/contactsupplier_import.html'
    business_class = SupplierBsn
    empty_filename = 'Contactos Proveedores'
    model_resource_method = 'other_model_resource'
    url_back = 'core:contactsupplier-list'
    text_back = 'Contactos Proveedor'
    page_title = 'Importacion de Contactos Proveedor'


class SupplierContactEmptyImportView(StxEmptyImportView):
    permissions = ['IMCP']
    business_class = SupplierBsn
    empty_filename = 'Contactos Proveedores'
    resource = 'other_model_resource'


class ClientListView(StxListView):
    permissions = ['VECL']
    template_name = 'core/client/list/client_list.html'
    business_class = ClientBsn


class ClientNewView(StxNewView):
    permissions = ['CRCL']
    template_name = 'core/client/new/client_new.html'
    business_class = ClientBsn
    form_class = ClientForm


class ClientEditView(StxEditView):
    permissions = ['EDCL']
    path_related_company = ['company', 'id']
    update_record_method = 'update'
    template_name = 'core/client/edit/client_edit.html'
    business_class = ClientBsn
    form_class = ClientForm
    page_title = 'Editar Cliente'


class ClientExportExcel(StxExportToExcel):
    permissions = ['EXCL']
    business_class = ClientBsn
    dataset_method = 'export_XLSX'
    name_file = 'Listado de clientes.xlsx'


class ClientImportExcel(ResourceModelImportView):
    permissions = ['IMCL']
    template_name = 'core/client/import/client_import.html'
    business_class = ClientBsn
    empty_filename = 'Clientes'


class ClientEmptyImportView(StxEmptyImportView):
    permissions = ['IMCL']
    business_class = ClientBsn


class ClientContactListView(StxModelRelatedListView):
    permissions = ['VECC']
    template_name = 'core/contactclient/list/contactclient_list.html'
    related_field = 'has_contacts'
    business_class = ClientBsn
    get_url_new_method = 'get_UrlNewContact'
    page_title = 'Contactos del Cliente'


class ClientContactNewView(StxModelRelatedNewView):
    permissions = ['CRCC']
    template_name = 'core/contactclient/new/contactclient_new.html'
    back_text = "Contactos Cliente"
    related_field = 'client'
    last_record_method = 'get_ContactLastRecord'
    get_url_redirect_method = 'get_UrlOnSaveContact'
    get_url_back_redirect_method = 'get_UrlBackList'
    business_class = ClientBsn
    form_class = ClientContactForm
    create_record_method = 'create_Contact'
    page_title = 'Creacion de Contacto'


class ClientContactEditView(StxModelRelatedEditView):
    permissions = ['EDCC']
    path_related_company = ['client', 'company', 'id']
    update_record_method = 'update_Contact'
    template_name = 'core/contactclient/edit/contactclient_edit.html'
    back_text = "Contactos Cliente"
    business_class = ClientBsn
    form_class = ClientContactForm
    related_field = 'client'
    get_record_method = 'get_ContactRecord'
    get_url_redirect_method = 'get_UrlOnSaveContact'


class ClientContactExportExcel(StxRelatedExportToExcel):
    permissions = ['EXCC']
    business_class = ClientBsn
    related_field = 'has_contacts'
    dataset_method = 'export_Contact_XLSX'
    name_file = 'Listado de contacto de clientes.xlsx'


class ClientContactImportExcel(ResourceModelImportView):
    permissions = ['IMCC']
    template_name = 'core/contactclient/import/contactclient_import.html'
    business_class = ClientBsn
    empty_filename = 'Contactos Clientes'
    model_resource_method = 'other_model_resource'
    url_back = 'core:contactclient-list'
    text_back = 'Contactos Clientes'
    page_title = 'Importacion de Contactos Clientes'


class ClientContactEmptyImportView(StxEmptyImportView):
    permissions = ['IMCC']
    business_class = ClientBsn
    empty_filename = 'Contactos Clientes'
    resource = 'other_model_resource'


class ItemListView(StxListView):
    permissions = ['VEAR']
    template_name = 'core/item/list/item_list.html'
    business_class = ItemBsn


class ItemNewView(StxNewView):
    permissions = ['CRAR']
    template_name = 'core/item/new/item_new.html'
    business_class = ItemBsn
    form_class = ItemForm


class ItemEditView(StxEditView):
    permissions = ['EDAR']
    path_related_company = ['company', 'id']
    update_record_method = 'update'
    template_name = 'core/item/edit/item_edit.html'
    business_class = ItemBsn
    form_class = ItemForm
    page_title = 'Editar Articulo'


class ItemExportExcel(StxExportToExcel):
    permissions = ['EXAR']
    business_class = ItemBsn
    dataset_method = 'export_XLSX'
    name_file = 'listado de articulos.xls'


class ItemImportExcel(ResourceModelImportView):
    permissions = ['IMPV']
    template_name = 'core/item/import/item_import.html'
    business_class = ItemBsn
    empty_filename = 'Importar Articulos'


class ItemEmptyImportView(StxEmptyImportView):
    permissions = ['IMPV']
    business_class = ItemBsn


class WarehouseListView(StxListView):
    permissions = ['VEAL']
    template_name = 'core/warehouse/list/warehouse_list.html'
    business_class = WarehouseBsn


class WarehouseNewView(StxNewView):
    permissions = ['CRAL']
    template_name = 'core/warehouse/new/warehouse_new.html'
    business_class = WarehouseBsn
    form_class = WarehouseForm


class WarehouseEditView(StxEditView):
    permissions = ['EDAL']
    path_related_company = ['company', 'id']
    update_record_method = 'update'
    template_name = 'core/warehouse/edit/warehouse_edit.html'
    business_class = WarehouseBsn
    form_class = WarehouseForm
    page_title = 'Editar Almacén'


class WarehouseExportExcel(StxExportToExcel):
    permissions = ['EXAL']
    business_class = WarehouseBsn
    dataset_method = 'export_XLSX'
    name_file = 'listado de almacenes.xls'


class WarehouseImportExcel(ResourceModelImportView):
    permissions = ['IMAL']
    template_name = 'core/warehouse/import/warehouse_import.html'
    business_class = WarehouseBsn
    empty_filename = 'Importar Almacén'


class WarehouseEmptyImportView(StxEmptyImportView):
    permissions = ['IMAL']
    business_class = WarehouseBsn


class UdmListView(StxListView):
    template_name = 'core/udm/list/udm_list.html'
    business_class = UdmBsn


class UdmNewView(StxNewView):
    template_name = 'core/udm/new/udm_new.html'
    business_class = UdmBsn
    form_class = UdmForm


class UdmEditView(StxEditView):
    update_record_method = 'update'
    template_name = 'core/udm/edit/udm_edit.html'
    business_class = UdmBsn
    form_class = UdmForm
    page_title = 'Editar unidades de medida'


class SatProductListView(StxListView):
    template_name = 'core/satproduct/list/satproduct_list.html'
    business_class = SatProductBsn


class SatProductNewView(StxNewView):
    template_name = 'core/satproduct/new/satproduct_new.html'
    business_class = SatProductBsn
    form_class = SatProductForm


class SatProductEditView(StxEditView):
    update_record_method = 'update'
    template_name = 'core/satproduct/edit/satproduct_edit.html'
    business_class = SatProductBsn
    form_class = SatProductForm
    page_title = 'Editar Producto SAT'


class StockItemListView(StxListView):
    permissions = ['VEST']
    template_name = 'core/stockitem/list/stockitem_list.html'
    business_class = StockItemBsn


class StockItemExportExcel(StxExportToExcel):
    permissions = ['EXST']
    business_class = StockItemBsn
    dataset_method = 'export_XLSX'
    name_file = 'Listado de Artículos en stock.xlsx'


class StockItemRetrieveView(StxRetrieveView):
    permissions = ['VEST']
    path_related_company = ['warehouse', 'company', 'id']
    template_name = "core/stockitem/retrieve/stockitem_retrieve.html"
    business_class = StockItemBsn


