# Python Libraries
import datetime

# Third Party Libraries
from import_export import resources
from import_export import fields
from import_export.widgets import ForeignKeyWidget
from import_export.widgets import ManyToManyWidget
from import_export.widgets import IntegerWidget
from import_export.widgets import DecimalWidget
from import_export.widgets import DateWidget
from import_export.widgets import Widget
from import_export.widgets import CharWidget

# Own Libraries
from .models import Supplier
from .models import SupplierContact
from .models import Client
from .models import ClientContact
from .models import Item
from .models import Udm
from core.models import Warehouse
from core.models import StockItem
from core.models import Supplier
from .models import SatProduct
from security.models import User
from tools.resources import BooleanWidget
from tools.resources import ChoicesWidget
from tools.resources import GenericResource
from tools.resources import ResourceSettings
from security.models import Company


class SatProductResource(resources.ModelResource):

    class Meta:
        import_id_fields = ['key', ]
        model = SatProduct


class SupplierResource(GenericResource):

    id = fields.Field(
        column_name='ID',
        attribute='id',
        widget=IntegerWidget()
    )
    company = fields.Field(
        column_name='compañía',
        attribute='company',
        widget=ForeignKeyWidget(Company, 'id')
    )
    legal_entity = fields.Field(
        column_name='nombre legal',
        attribute='legal_entity',
    )
    commercial_name = fields.Field(
        column_name='nombre comercial',
        attribute='commercial_name',
    )
    rfc = fields.Field(
        column_name='rfc o clave',
        attribute='rfc',
    )
    is_active = fields.Field(
        column_name='activo',
        attribute='is_active',
        widget=BooleanWidget()
    )
    sector = fields.Field(
        column_name='sector',
        attribute='sector',
        widget=ChoicesWidget(choices=Supplier.SECTOR_CHOICES)
    )
    email = fields.Field(
        column_name='correo',
        attribute='email',
    )
    phone = fields.Field(
        column_name='teléfono',
        attribute='phone',
    )
    website = fields.Field(
        column_name='sitio web',
        attribute='website',
    )
    bank = fields.Field(
        column_name='Banco',
        attribute='bank',
        widget=ChoicesWidget(choices=Supplier.BANK_TYPE)
    )
    account_number = fields.Field(
        column_name='Numero de Cuenta',
        attribute='account_number',
        widget=IntegerWidget()
    )
    CLABE = fields.Field(
        column_name='Clave Interbancaria (CLABE)',
        attribute='CLABE',
        widget=IntegerWidget()
    )    

    class Meta:
        model = Supplier
        skip_unchanged = True
        report_skipped = True
        import_id_fields = ['id', ]
        fields = (
            'id',
            'company',
            'legal_entity',
            'commercial_name',
            'rfc',
            'is_active',
            'sector',
            'email',
            'website',
            'phone',
            'bank',
            'account_number',
            'CLABE',
        )
        export_order = (
            'id',
            'company',
            'legal_entity',
            'commercial_name',
            'rfc',
            'is_active',
            'sector',
            'email',
            'website',
            'phone',
            'bank',
            'account_number',
            'CLABE',
        )


class SupplierContactResource(GenericResource, ResourceSettings):

    id = fields.Field(
        column_name='ID',
        attribute='id',
        widget=IntegerWidget()
    )
    supplier = fields.Field(
        column_name='proveedor',
        attribute='supplier',
        widget=ForeignKeyWidget(Supplier, 'id')
    )
    name = fields.Field(
        column_name='nombre',
        attribute='name',
    )
    is_active = fields.Field(
        column_name='active',
        attribute='is_active',
        widget=BooleanWidget()
    )
    phone = fields.Field(
        column_name='teléfono',
        attribute='phone',
    )
    phone_ext = fields.Field(
        column_name='extensión',
        attribute='phone_ext',
    )
    email = fields.Field(
        column_name='email',
        attribute='email',
    )
    # created_by = fields.Field(
    #     column_name='creado por',
    #     attribute='created_by',
    #     widget=ForeignKeyWidget(User, 'id')
    # )
    # updated_by = fields.Field(
    #     column_name='Modificado por',
    #     attribute='updated_by',
    #     widget=ForeignKeyWidget(User, 'id')
    # )
    # created_date = fields.Field(
    #     column_name='fecha de creación',
    #     attribute='created_date',
    #     widget=DateWidget()
    # )
    # updated_date = fields.Field(
    #     column_name='fecha de modificación',
    #     attribute='updated_date',
    #     widget=DateWidget()
    # )

    class Meta:
        model = SupplierContact
        skip_unchanged = True
        report_skipped = True
        import_id_fields = ['id', ]
        fields = (
            'id',
            'supplier',
            'name',
            'phone',
            'phone_ext',
            'email',
            'is_active',
            # 'created_by',
            # 'created_date',
            # 'updated_by',
            # 'updated_date',
        )
        export_order = (
            'id',
            'is_active',
            'supplier',
            'name',
            'phone',
            'phone_ext',
            'email',
            # 'created_by',
            # 'created_date',
            # 'updated_by',
            # 'updated_date',
        )


class ClientResource(GenericResource):

    id = fields.Field(
        column_name='ID',
        attribute='id',
        widget=IntegerWidget()
    )
    company = fields.Field(
        column_name='Compañia',
        attribute='company',
        widget=ForeignKeyWidget(Company, 'id')
    )
    rfc = fields.Field(
        column_name='rfc o clave',
        attribute='rfc',
    )
    legal_entity = fields.Field(
        column_name='nombre legal',
        attribute='legal_entity',
    )
    commercial_name = fields.Field(
        column_name='nombre comercial',
        attribute='commercial_name',
    )
    is_active = fields.Field(
        column_name='activo',
        attribute='is_active',
        widget=BooleanWidget()
    )
    credit_days = fields.Field(
        column_name='dias de credito',
        attribute='credit_days',
        widget=ChoicesWidget(choices=Client.DAYS_CREDIT_CHOICES)
    )
    credit_limit = fields.Field(
        column_name='limite de cédito',
        attribute='credit_limit',
        widget=DecimalWidget()
    )
    debt = fields.Field(
        column_name='adeudo',
        attribute='debt',
        widget=DecimalWidget()
    )
    nationality = fields.Field(
        column_name='nacionalidad',
        attribute='nationality',
        widget=ChoicesWidget(choices=Client.NATIONALITY_CHOICES)

    )
    sector = fields.Field(
        column_name='sector',
        attribute='sector',
        widget=ChoicesWidget(choices=Client.SECTOR_CHOICES)
    )
    type = fields.Field(
        column_name='tipo',
        attribute='type',
        widget=ChoicesWidget(choices=Client.TYPE_CHOICES)
    )
    email = fields.Field(
        column_name='email',
        attribute='email',
    )
    phone = fields.Field(
        column_name='teléfono',
        attribute='phone',
    )
    website = fields.Field(
        column_name='sitio web',
        attribute='website',
        widget=CharWidget()
    )

    class Meta:
        model = Client
        skip_unchanged = True
        report_skipped = True
        import_id_fields = ['id', ]
        fields = (
            'id',
            'company',
            'rfc',
            'legal_entity',
            'commercial_name',
            'credit_days',
            'credit_limit',
            'debt',
            'nationality',
            'sector',
            'type',
            'email',
            'website',
            'phone',
        )
        export_order = (
            'id',
            'company',
            'rfc',
            'legal_entity',
            'commercial_name',
            'credit_days',
            'credit_limit',
            'debt',
            'nationality',
            'sector',
            'type',
            'email',
            'website',
            'phone',
        )


class ClientContactResource(GenericResource):
    id = fields.Field(
        column_name='ID',
        attribute='id',
        widget=IntegerWidget()
    )
    client = fields.Field(
        column_name='cliente',
        attribute='client',
        widget=ForeignKeyWidget(Client, 'id')
    )
    name = fields.Field(
        column_name='nombre',
        attribute='name',
        widget=CharWidget()
    )
    is_active = fields.Field(
        column_name='active',
        attribute='is_active',
        widget=BooleanWidget()
    )
    phone = fields.Field(
        column_name='teléfono',
        attribute='phone',
    )
    phone_ext = fields.Field(
        column_name='extensión',
        attribute='phone_ext',
        widget=IntegerWidget()
    )
    email = fields.Field(
        column_name='email',
        attribute='email',
    )

    class Meta:
        model = ClientContact
        skip_unchanged = True
        report_skipped = True
        import_id_fields = ['id', ]
        fields = (
            'id',
            'client',
            'name',
            'is_active',
            'phone',
            'phone_ext',
            'email',
        )
        export_order = (
            'id',
            'client',
            'name',
            'is_active',
            'phone',
            'phone_ext',
            'email',
        )


class ItemResource(GenericResource):
    id = fields.Field(
        column_name='ID',
        attribute='id',
        widget=IntegerWidget()
    )
    company = fields.Field(
        column_name='compañía',
        attribute='company',
        widget=ForeignKeyWidget(Company, 'id')
    )
    primary_number = fields.Field(
        column_name='clave',
        attribute='primary_number',
    )
    name = fields.Field(
        column_name='nombre',
        attribute='name',
    )
    description = fields.Field(
        column_name='descripcion',
        attribute='description',
    )
    udm = fields.Field(
        column_name='Unidad de Medida',
        attribute='udm',
        widget=ForeignKeyWidget(Udm, 'name')
    )
    udp = fields.Field(
        column_name='unidad de presentacion',
        attribute='udp',
        widget=DecimalWidget()
    )
    is_active = fields.Field(
        column_name='Activo',
        attribute='is_active',
    )
    upc = fields.Field(
        column_name='codigo de barras',
        attribute='upc',
    )
    sku = fields.Field(
        column_name='sku',
        attribute='sku',
    )
    sat_code = fields.Field(
        column_name='Clave SAT',
        attribute='sat_code',
        widget=ForeignKeyWidget(SatProduct, 'key')
    )
    image = fields.Field(
        column_name="foto",
        attribute='image',
    )

    class Meta:
        model = Item
        skip_unchanged = True
        report_skipped = True
        import_id_fields = ['id', ]
        fields = (
            'id',
            'company',
            'primary_number',
            'name',
            'description',
            'udm',
            'udp',
            'is_active',
            'upc',
            'sku',
            'sat_code',
            'image',
        )
        export_order = (
            'id',
            'company',
            'primary_number',
            'name',
            'description',
            'udm',
            'udp',
            'is_active',
            'upc',
            'sku',
            'sat_code',
            'image',
        )


class WarehouseResource(GenericResource):
    id = fields.Field(
        column_name='ID',
        attribute='id',
        widget=IntegerWidget()
    )
    company = fields.Field(
        column_name='compañía',
        attribute='company',
        widget=ForeignKeyWidget(Company, 'id')
    )
    name = fields.Field(
        column_name='nombre',
        attribute='name',
    )
    street = fields.Field(
        column_name='calle',
        attribute='street',
    )
    outdoor_number = fields.Field(
        column_name='numero Exterior',
        attribute='outdoor_number',
    )
    municipality = fields.Field(
        column_name='Municipio/Alcaldia',
        attribute='municipality',
    )
    description = fields.Field(
        column_name='Descripción',
        attribute='description',
    )
    is_active = fields.Field(
        column_name='Activo',
        attribute='description',
        widget=BooleanWidget()
    )

    class Meta:
        model = Warehouse
        skip_unchanged = True
        report_skipped = True
        import_id_fields = ['id', ]
        fields = (
            'id',
            'company',
            'name',
            'street',
            'outdoor_number',
            'municipality',
            'description',
            'is_active',
        )
        export_order = (
            'id',
            'company',
            'name',
            'street',
            'outdoor_number',
            'municipality',
            'description',
            'is_active',
        )


class StockItemResource(GenericResource):
    warehouse = fields.Field(
        column_name='almacen',
        attribute='warehouse',
        widget=ForeignKeyWidget(Warehouse, 'id')
    )
    item = fields.Field(
        column_name='artículo',
        attribute='item',
        widget=ForeignKeyWidget(Item, 'id')
    )
    qty = fields.Field(
        column_name='cantidad',
        attribute='qty',
        widget=DecimalWidget()
    )
    minimun_qty = fields.Field(
        column_name='minimo en almacen',
        attribute='minimun_qty',
        widget=DecimalWidget()
    )
    price = fields.Field(
        column_name='precio unitario',
        attribute='price',
        widget=DecimalWidget()
    )
    sell_price = fields.Field(
        column_name='precio de venta',
        attribute='sell_price',
        widget=DecimalWidget()

    )

    class Meta:
        model = StockItem
        skip_unchanged = True
        report_skipped = True
        fields = (
            'warehouse',
            'item',
            'qty',
            'minimun_qty',
            'price',
            'sell_price',
        )
        export_order = (
            'warehouse',
            'item',
            'qty',
            'minimun_qty',
            'price',
            'sell_price',
        )

