# Python's Libraries

# Django's Libraries

# Third-party Libraries
from rest_framework import serializers

# Own's Libraries
from .models import Item
from .models import SatProduct
from .models import Warehouse
from .models import StockItem
from .models import Tax
from .models import Supplier
from .models import Client


class MostSelledItems(serializers.Serializer):
    primary_number = serializers.CharField()
    total_amount = serializers.IntegerField()


class TaxSerializer(serializers.ModelSerializer):

    class Meta:
        model = Tax
        fields = (
            'id',
            'abbreviation',
            'description',
            'factor',
        )
        read_only_fields = [
            'id',
        ]


class SupplierSerializer(serializers.ModelSerializer):

    class Meta:
        model = Supplier
        fields = (
            'id',
            'legal_entity',
            'commercial_name',
            'rfc',
            'is_active',
            'sector',
            'email',
            'phone',
            'website',
        )
        read_only_fields = [
            'id',
        ]


class ClientSerializer(serializers.ModelSerializer):

    class Meta:
        model = Client
        fields = (
            'id',
            'legal_entity',
            'commercial_name',
            'rfc',
            'is_active',
            'sector',
            'email',
            'sector',
            'phone',
            'website',
            'nationality',
            'comments',
            'credit_limit',
            'credit_days',
            'debt',
        )
        read_only_fields = [
            'id',
        ]


class SatProductSerializer(serializers.ModelSerializer):

    class Meta:
        model = SatProduct
        fields = (
            'key',
            'description',
        )


class ItemSerializer(serializers.ModelSerializer):
    company_name = serializers.SerializerMethodField()

    class Meta:
        model = Item
        fields = (
            'id',
            'company',
            'company_name',
            'primary_number',
            'name',
            'description',
            'udp',
            'upc',
            'sku',
            'image',
        )

    def get_company_name(self, obj):
        return obj.company.commercial_name


class WarehouseSerializer(serializers.ModelSerializer):
    company_name = serializers.SerializerMethodField()

    class Meta:
        model = Warehouse
        fields = (
            'id',
            'name',
            'company',
            'company_name',
            'is_active',
            'description',
            'street',
            'outdoor_number',
        )
        read_only_fields = [
            'id',
            'company_name',
        ]

    def get_company_name(self, obj):
        return obj.company.commercial_name


class StockItemSerializer(serializers.ModelSerializer):
    company_name = serializers.SerializerMethodField()
    warehouse_name = serializers.SerializerMethodField()
    item_name = serializers.SerializerMethodField()

    class Meta:
        model = StockItem
        fields = [
            'id',
            'company_name',
            'warehouse',
            'warehouse_name',
            'item',
            'item_name',
            'qty',
            'minimun_qty',
            'udm',
            'price',
            'sell_price',
        ]
        read_only_fields = [
            'id',
            'company_name',
            'warehouse_name',
            'item_name',
        ]

    def get_company_name(self, obj):
        return obj.warehouse.company.commercial_name

    def get_warehouse_name(self, obj):
        return obj.warehouse.name

    def get_item_name(self, obj):
        return obj.item.name


class StockItemRetrieveSerializer(serializers.ModelSerializer):
    item = ItemSerializer(
        required=False, many=False)
    company_name = serializers.SerializerMethodField()
    warehouse_name = serializers.SerializerMethodField()

    class Meta:
        model = StockItem
        fields = [
            'id',
            'company_name',
            'warehouse',
            'warehouse_name',
            'qty',
            'udm',
            'price',
            'sell_price',
            'item',

        ]
        read_only_fields = [
            'id',
            'company_name',
            'warehouse_name',
            'price',
            'sell_price',
        ]

    def get_company_name(self, obj):
        return obj.warehouse.company.commercial_name

    def get_warehouse_name(self, obj):
        return obj.warehouse.name
