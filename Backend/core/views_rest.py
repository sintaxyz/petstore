# Python's Libraries

# Django's Libraries
from django.db import transaction

# Third-party Libraries
from rest_framework import viewsets
from rest_framework import status
from rest_framework.response import Response
from rest_framework.permissions import AllowAny

# Own's Libraries
from .serializers import ItemSerializer
from .serializers import WarehouseSerializer
from .serializers import StockItemSerializer
from .serializers import StockItemRetrieveSerializer
from .serializers import TaxSerializer
from .serializers import ClientSerializer
from .serializers import SupplierSerializer
from .serializers import MostSelledItems
from .serializers import SatProductSerializer

from .business import ItemBsn
from .business import WarehouseBsn
from .business import StockItemBsn
from .business import TaxBsn
from .business import SupplierBsn
from .business import ClientBsn
from security.business import CompanyBsn


class SatProductSearchAPI(viewsets.GenericViewSet):
    serializer_class = SatProductSerializer
    permission_classes = [AllowAny]

    def search(self, request, *args, **kwargs):
        try:
            queryset = ItemBsn.search_SatProduct(request.GET.get('key'))
            serializer = self.get_serializer(queryset, many=True)
            return Response(serializer.data)
        except Exception as error:
            raise NameError(str(error))


class ChartMostSelledItemListAPIView(viewsets.ViewSet):
    queryset = ''
    serializer_class = MostSelledItems

    def get_queryset(self, user, request):
        records = ItemBsn.get_MostSelledItems(user, request)
        return records

    def list(self, request, *args, **kwargs):
        queryset = self.get_queryset(request.user, request)
        serializer = self.serializer_class(queryset, many=True)
        return Response(serializer.data)


class ChartMostInStockItemListAPIView(viewsets.ViewSet):
    queryset = ''
    serializer_class = MostSelledItems

    def get_queryset(self, user, request):
        records = ItemBsn.get_MostInStock(user, request)
        return records

    def list(self, request, *args, **kwargs):
        queryset = self.get_queryset(request.user, request)
        serializer = self.serializer_class(queryset, many=True)
        return Response(serializer.data)


class ItemListAPIView(viewsets.GenericViewSet):
    queryset = ''
    serializer_class = ItemSerializer

    def get_QuerySet(self, user):
        records = ItemBsn.get_Assigned(user)
        return records

    def list(self, request, *args, **kwargs):
        queryset = self.filter_queryset(
            self.get_QuerySet(request.user)
        )
        serializer = self.get_serializer(queryset, many=True)
        return Response(serializer.data)


class ItemInWarehouseStockAPIView(viewsets.GenericViewSet):
    queryset = ''
    serializer_class = StockItemSerializer

    def get_QuerySet(self, warehouse_pk, user):
        records = WarehouseBsn.get_ItemStock(warehouse_pk, user)
        return records

    def list(self, request, pk, *args, **kwargs):
        queryset = self.filter_queryset(
            self.get_QuerySet(pk, request.user)
        )
        serializer = self.get_serializer(queryset, many=True)
        return Response(serializer.data)


class ItemInWarehouseStockExistAPIView(viewsets.GenericViewSet):
    queryset = ''
    serializer_class = StockItemSerializer

    def get_QuerySet(self, warehouse_pk, user):
        records = WarehouseBsn.get_ItemStockExist(warehouse_pk, user)
        return records

    def list(self, request, pk, *args, **kwargs):
        queryset = self.filter_queryset(
            self.get_QuerySet(pk, request.user)
        )
        serializer = self.get_serializer(queryset, many=True)
        return Response(serializer.data)


class ItemOutWarehouseStockAPIView(viewsets.GenericViewSet):
    queryset = ''
    serializer_class = ItemSerializer

    def get_QuerySet(self, warehouse_pk, user):
        records = WarehouseBsn.get_ItemOutStock(warehouse_pk, user)
        return records

    def list(self, request, pk, *args, **kwargs):
        queryset = self.filter_queryset(
            self.get_QuerySet(pk, request.user)
        )
        serializer = self.get_serializer(queryset, many=True)
        return Response(serializer.data)


class ItemOutWarehouseStockIsActiveAPIView(viewsets.GenericViewSet):
    queryset = ''
    serializer_class = ItemSerializer

    def get_QuerySet(self, warehouse_pk, user):
        records = WarehouseBsn.get_ItemOutStockActive(warehouse_pk, user)
        return records

    def list(self, request, pk, *args, **kwargs):
        queryset = self.filter_queryset(
            self.get_QuerySet(pk, request.user)
        )
        serializer = self.get_serializer(queryset, many=True)
        return Response(serializer.data)


class WarehouseListAPIView(viewsets.GenericViewSet):
    queryset = ''
    serializer_class = WarehouseSerializer

    def get_QuerySet(self, user):
        records = WarehouseBsn.get_Assigned(user)
        return records

    def list(self, request, *args, **kwargs):
        queryset = self.filter_queryset(
            self.get_QuerySet(user=request.user)
        )
        serializer = self.get_serializer(queryset, many=True)
        return Response(serializer.data)


class WarehouseIsActiveListAPIView(viewsets.GenericViewSet):
    queryset = ''
    serializer_class = WarehouseSerializer

    def get_QuerySet(self, user):
        records = WarehouseBsn.get_IsActive(user)
        return records

    def list(self, request, *args, **kwargs):
        queryset = self.filter_queryset(
            self.get_QuerySet(user=request.user)
        )
        serializer = self.get_serializer(queryset, many=True)
        return Response(serializer.data)


class WarehouseWithStockListAPIView(viewsets.GenericViewSet):
    queryset = ''
    serializer_class = WarehouseSerializer

    def get_QuerySet(self, user):
        records = WarehouseBsn.get_AssignedWithStock(user)
        return records

    def list(self, request, *args, **kwargs):
        queryset = self.filter_queryset(
            self.get_QuerySet(user=request.user)
        )
        serializer = self.get_serializer(queryset, many=True)
        return Response(serializer.data)


class WarehouseRetrieveAPIView(viewsets.GenericViewSet):
    serializer_class = WarehouseSerializer

    def get_Object(self, pk):
        instance = WarehouseBsn.get_Warehouse(pk)
        return instance

    def retrieve(self, request, pk, *args, **kwargs):
        instance = self.get_Object(pk)
        serializer = self.get_serializer(instance)
        return Response(serializer.data)

    def update(self, request, pk, *args, **kwargs):
        instance = self.get_Object(pk)
        serializer = WarehouseSerializer(data=request.data)
        if serializer.is_valid(raise_exception=False):
            try:
                data_list = serializer.validated_data
                record = WarehouseBsn.update(
                    instance,
                    data_list,
                    request.user
                )
                serializer = WarehouseSerializer(record)
                return Response(
                    serializer.data,
                    status=status.HTTP_200_OK
                )
            except Exception as e:
                return Response(
                    {'detail': str(e)},
                    status=status.HTTP_400_BAD_REQUEST
                )
        else:
            return Response(
                serializer.errors,
                status=status.HTTP_400_BAD_REQUEST
            )


class StockItemListAPIView(viewsets.GenericViewSet):
    serializer_class = StockItemSerializer

    def get_QuerySet(self, user):
        records = StockItemBsn.get_StockList(user)
        return records

    def list(self, request, *args, **kwargs):
        queryset = self.filter_queryset(
            self.get_QuerySet(user=request.user)
        )
        serializer = self.get_serializer(queryset, many=True)
        return Response(serializer.data)


class ClientListAPIView(viewsets.GenericViewSet):
    serializer_class = ClientSerializer

    def get_QuerySet(self, user):
        records = ClientBsn.get_Assigned(user)
        return records

    def list(self, request, *args, **kwargs):
        queryset = self.filter_queryset(
            self.get_QuerySet(user=request.user)
        )
        serializer = self.get_serializer(queryset, many=True)
        return Response(serializer.data)


class ClientDebtorListAPIView(viewsets.GenericViewSet):
    serializer_class = ClientSerializer

    def get_QuerySet(self, user):
        records = ClientBsn.get_Debtor(user)
        return records

    def list(self, request, *args, **kwargs):
        queryset = self.filter_queryset(
            self.get_QuerySet(user=request.user)
        )
        serializer = self.get_serializer(queryset, many=True)
        return Response(serializer.data)


class ClientIsActiveListAPIView(viewsets.GenericViewSet):
    serializer_class = ClientSerializer

    def get_QuerySet(self, user):
        records = ClientBsn.get_IsActive(user)
        return records

    def list(self, request, *args, **kwargs):
        queryset = self.filter_queryset(
            self.get_QuerySet(user=request.user)
        )
        serializer = self.get_serializer(queryset, many=True)
        return Response(serializer.data)


class ClientRetrieveAPIView(viewsets.GenericViewSet):
    serializer_class = ClientSerializer

    def get_Object(self, pk):
        instance = ClientBsn.get(pk)
        return instance

    def retrieve(self, request, pk, *args, **kwargs):
        instance = self.get_Object(pk)
        serializer = self.get_serializer(instance)
        return Response(serializer.data)


class ClientChartListAPIView(viewsets.GenericViewSet):
    serializer_class = ClientSerializer

    def get_QuerySet(self, user, request):
        records = ClientBsn.get_ForCharts(user, request)
        print('queryset', records)
        return records

    def list(self, request, *args, **kwargs):
        queryset = self.filter_queryset(
            self.get_QuerySet(request.user, request)
        )
        serializer = self.get_serializer(queryset, many=True)
        return Response(serializer.data)


class WarehouseChartListAPIView(viewsets.GenericViewSet):
    serializer_class = WarehouseSerializer

    def get_QuerySet(self, user, request):
        records = WarehouseBsn.get_ForCharts(user, request)
        print('queryset', records)
        return records

    def list(self, request, *args, **kwargs):
        queryset = self.filter_queryset(
            self.get_QuerySet(request.user, request)
        )
        serializer = self.get_serializer(queryset, many=True)
        return Response(serializer.data)


class StockItemRetrieveAPIView(viewsets.GenericViewSet):
    serializer_class = StockItemRetrieveSerializer

    def get_Object(self, pk):
        instance = StockItemBsn.get_Stock(pk)
        return instance

    def retrieve(self, request, pk, *args, **kwargs):
        instance = self.get_Object(pk)
        serializer = self.get_serializer(instance)
        return Response(serializer.data)


class TaxListAPIView(viewsets.GenericViewSet):
    queryset = ''
    serializer_class = TaxSerializer

    def get_QuerySet(self, user):
        records = TaxBsn.get_TaxList(user)
        return records

    def list(self, request, *args, **kwargs):
        queryset = self.filter_queryset(
            self.get_QuerySet(user=request.user)
        )
        serializer = self.get_serializer(queryset, many=True)
        return Response(serializer.data)


class TaxIsActiveListAPIView(viewsets.GenericViewSet):
    queryset = ''
    serializer_class = TaxSerializer

    def get_QuerySet(self, user):
        records = TaxBsn.get_IsActive(user)
        return records

    def list(self, request, *args, **kwargs):
        queryset = self.filter_queryset(
            self.get_QuerySet(user=request.user)
        )
        serializer = self.get_serializer(queryset, many=True)
        return Response(serializer.data)


class SupplierListAPIView(viewsets.GenericViewSet):
    queryset = ''
    serializer_class = SupplierSerializer

    def get_QuerySet(self, user):
        records = SupplierBsn.get_Assigned(user)
        return records

    def list(self, request, *args, **kwargs):
        queryset = self.filter_queryset(
            self.get_QuerySet(user=request.user)
        )
        serializer = self.get_serializer(queryset, many=True)
        return Response(serializer.data)


class SupplierIsActiveListAPIView(viewsets.GenericViewSet):
    queryset = ''
    serializer_class = SupplierSerializer

    def get_QuerySet(self, user):
        records = SupplierBsn.get_IsActive(user)
        return records

    def list(self, request, *args, **kwargs):
        queryset = self.filter_queryset(
            self.get_QuerySet(user=request.user)
        )
        serializer = self.get_serializer(queryset, many=True)
        return Response(serializer.data)