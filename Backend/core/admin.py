# Python's Libraries

# Django's Libraries
from django.contrib import admin
from import_export import resources
from import_export.admin import ImportExportModelAdmin

# Third-party Libraries

# Own's Libraries
from .models import Supplier
from .models import SupplierContact
from .models import Client
from .models import ClientContact
from .models import Item
from .models import Warehouse
from .models import Udm
from .models import StockItem
from .models import Tax
from .models import SatProduct
from .resources import SatProductResource


@admin.register(SatProduct)
class SatProductAdmin(ImportExportModelAdmin, admin.ModelAdmin):
    resource_class = SatProductResource

    list_display = (
        'key',
        'description',
    )
    search_fields = (
        'key',
    )


class SupplierContactInline(admin.TabularInline):
    model = SupplierContact
    extra = 1


@admin.register(Supplier)
class SupplierAdmin(admin.ModelAdmin):
    list_display = (
        'id',
        'company',
        'commercial_name',
        'legal_entity',
        'is_active',
    )
    search_fields = (
        'id',
        'company',
        'is_active',
    )
    inlines = [
        SupplierContactInline,
    ]


class ClientContactInline(admin.TabularInline):
    model = ClientContact
    extra = 1


@admin.register(Client)
class ClientAdmin(admin.ModelAdmin):
    list_display = (
        'id',
        'commercial_name',
        'legal_entity',
        'nationality',
        'rfc',
        'type',
        'email',
        'phone',
        'website',
        'is_active',
    )
    search_fields = (
        'id',
        'company',
        'legal_entity',
        'rfc',
        'is_active',
    )
    inlines = [
        ClientContactInline,
    ]


@admin.register(Item)
class ItemAdmin(admin.ModelAdmin):
    list_display = (
        'id',
        'company',
        'primary_number',
        'name',
        'description',
        'udp',
        'upc',
        'sku',
        'is_active',
    )
    search_fields = (
        'id',
        'company',
        'name',
        'sku',
    )


@admin.register(Warehouse)
class WarehouseAdmin(admin.ModelAdmin):
    list_display = (
        'id',
        'company',
        'name',
        'description',
        'street',
        'outdoor_number',
        'is_active',
    )
    search_fields = (
        'id',
        'name',
        'company',
        'is_active',
    )


@admin.register(Udm)
class UdmAdmin(admin.ModelAdmin):
    list_display = (
        'id',
        'key',
        'name',
        'type',
    )
    search_fields = (
        'id',
        'company',
        'name',
    )


@admin.register(StockItem)
class StockItemAdmin(admin.ModelAdmin):
    list_display = (
        'id',
        'warehouse',
        'item',
    )
    search_fields = (
        'id',
        'warehouse',
        'item',
    )


@admin.register(Tax)
class TaxAdmin(admin.ModelAdmin):
    list_display = (
        'id',
        'abbreviation',
        'factor',
        'description',
    )
    search_fields = (
        'id',
        'abbreviation',
        'factor',
        'description',
    )
