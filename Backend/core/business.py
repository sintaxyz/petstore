# Python's Libraries
import io

# Django's Libraries
from django.db import transaction
from django.db.models import Count
from django.db.models import Sum
from django.urls import reverse_lazy
from django.db.models import Q

# Third-party Libraries
from xlsxwriter.workbook import Workbook

# Own's Libraries
from tools.business import StxBsn
# from security.business import UserBsn

from .models import Supplier
from .models import SupplierContact
from .models import Client
from .models import ClientContact
from .models import Item
from .models import Warehouse
from .models import Udm
from .models import StockItem
from .models import Tax
from .models import SatProduct
from .resources import ItemResource
from .resources import SupplierResource
from .resources import SupplierContactResource
from .resources import ClientResource
from .resources import ClientContactResource
from .resources import WarehouseResource
from .resources import StockItemResource
from .filters import SupplierFilter
from .filters import ClientFilter
from .filters import ItemFilter
from .filters import WarehouseFilter
from .filters import UdmFilter
from .filters import StockItemFilter
from .filters import TaxFilter
from .filters import SatProductFilter


class SupplierBsn(StxBsn):
    model = Supplier
    model_filter = SupplierFilter
    model_resource = SupplierResource
    other_model_resource = SupplierContactResource
    key_word = 'legal_entity'

    @classmethod
    def get_SupplierList(self, _user):
        records = self.get_All()
        return records

    @classmethod
    def get_Assigned(self, user):
        return self.get_All().filter(company__in=user.companies.all())

    @classmethod
    def export_XLSX(self, records):
        return SupplierResource().export(records).xlsx

    @classmethod
    def export_Contact_XLSX(self, records):
        return SupplierContactResource().export(records).xlsx

    @classmethod
    def get_IsActive(self, user):
        supplier = self.get_Assigned(user)
        records = supplier.filter(is_active=True)
        return records

    @classmethod
    def create(self, _data, _user):
        with transaction.atomic():
            if _data.get('company'):
                company = _data.get('company').id
            else:
                raise ValueError('Es necesarion ingresar una Empresa.')
            approved = self.validate_Access(_user, 'CRPV', company)
            if approved is False:
                raise ValueError('No tiene permiso para crear proveedor de esta copañia.')
            record = self.get_ModelObject()()
            record.company = _data.get('company')
            record.legal_entity = _data.get('legal_entity')
            record.commercial_name = _data.get('commercial_name')
            record.rfc = _data.get('rfc')
            record.sector = _data.get('sector')
            record.email = _data.get('email')
            record.phone = _data.get('phone')
            record.type = _data.get('type')
            record.email = _data.get('email')
            record.phone = _data.get('phone')
            record.website = _data.get('website')
            record.bank = _data.get('bank')
            if _data.get('account_numbre') is not None:
                if len(str(_data.get('account_number')))==16:
                    record.account_number = _data.get('account_number')
                else:
                    raise ValueError('Ingrese 16 digitos.')
            if _data.get('CLABE') is not None:
                if len(str(_data.get('CLABE')))==18:
                    record.CLABE = _data.get('CLABE')
                else:
                    raise ValueError('Ingrese 18 digitos.')
            record.settlement = _data.get('settlement')
            record.municipality = _data.get('municipality')
            record.street = _data.get('street')
            record.outdoor_number = _data.get('outdoor_number')
            record.interior_number = _data.get('interior_number')
            record.between_street_a = _data.get('between_street_a')
            record.between_street_b = _data.get('between_street_b')
            record.zip_code = _data.get('zip_code')
            record.latitude = _data.get('latitude')
            record.longitude = _data.get('longitude')
            record.created_by = _user
            record.full_clean()
            record.save()
            return record

    @classmethod
    def update(self, _record, _data, _user):
        with transaction.atomic():
            if _data.get('company'):
                company = _data.get('company').id
            else:
                raise ValueError('Es necesarion ingresar una Empresa.')
            approved = self.validate_Access(_user, 'EDPV', company)
            if approved is False:
                raise ValueError('No tiene permiso editar proveedor de esta compañia.')
            _record.company = _data.get('company')
            _record.legal_entity = _data.get('legal_entity')
            _record.commercial_name = _data.get('commercial_name')
            _record.rfc = _data.get('rfc')
            _record.sector = _data.get('sector')
            _record.email = _data.get('email')
            _record.phone = _data.get('phone')
            _record.type = _data.get('type')
            _record.email = _data.get('email')
            _record.phone = _data.get('phone')
            _record.website = _data.get('website')
            _record.bank = _data.get('bank')
            if _data.get('account_numbre') is not None:
                if len(str(_data.get('account_number')))==16:
                    _record.account_number = _data.get('account_number')
                else:
                    raise ValueError('Ingrese 16 digitos.')
            if _data.get('CLABE') is not None:
                if len(str(_data.get('CLABE')))==18:
                    _record.CLABE = _data.get('CLABE')
                else:
                    raise ValueError('Ingrese 18 digitos.')
            if _record.is_active != _data.get('is_active'):
                approved = self.validate_Access(_user, 'DEPV', company)
                if approved is False:
                    raise ValueError('No tiene permiso para activar o desactivar el proveedor')
                else:
                    _record.is_active = _data.get('is_active')
            _record.settlement = _data.get('settlement')
            _record.municipality = _data.get('municipality')
            _record.street = _data.get('street')
            _record.outdoor_number = _data.get('outdoor_number')
            _record.interior_number = _data.get('interior_number')
            _record.between_street_a = _data.get('between_street_a')
            _record.between_street_b = _data.get('between_street_b')
            _record.zip_code = _data.get('zip_code')
            _record.latitude = _data.get('latitude')
            _record.longitude = _data.get('longitude')
            _record.update_by = _user
            _record.full_clean()
            _record.save()
            return _record

    @classmethod
    def create_Contact(self, _data, _user):
        with transaction.atomic():
            if _data.get('supplier'):
                company = _data.get('supplier').company.id
            else:
                raise ValueError('Es necesarion ingresar un proveedor.')
            approved = self.validate_Access(_user, 'CRCP', company)
            if approved is False:
                raise ValueError('No tiene permiso crear contacto de proveedor en esta compañia.')
            record = SupplierContact()
            record.supplier = _data.get('supplier')
            record.name = _data.get('name')
            record.phone = _data.get('phone')
            record.phone_ext = _data.get('phone_ext')
            record.email = _data.get('email')
            record.created_by = _user
            record.full_clean()
            record.save()
            return record

    @classmethod
    def update_Contact(self, _record, _data, _user):
        with transaction.atomic():
            if _record.supplier:
                company = _record.supplier.company.id
            else:
                raise ValueError('Es necesarion ingresar un proveedor.')
            approved = self.validate_Access(_user, 'EDCP', company)
            if approved is False:
                raise ValueError('No tiene permiso crear contacto de proveedor en esta compañia.')
            _record.name = _data.get('name')
            _record.phone = _data.get('phone')
            _record.phone_ext = _data.get('phone_ext')
            _record.email = _data.get('email')
            if _record.is_active != _data.get('is_active'):
                approved = self.validate_Access(_user, 'DECP', company)
                if approved is False:
                    raise ValueError('No tiene permiso para activar o desactivar el contacto de proveedor')
                else:
                    _record.is_active = _data.get('is_active')
            _record.update_by = _user
            _record.full_clean()
            _record.save()
            return _record

    @classmethod
    def get_UrlNewContact(self, record):
        url = reverse_lazy(
            'core:contactsupplier-new',
            kwargs={"pk": record.id}
        )
        return url

    @classmethod
    def get_UrlOnSaveContact(self, request, record):
        url = reverse_lazy(
            'core:contactsupplier-list',
            kwargs={"pk": record.id}
        )
        return url

    @classmethod
    def get_UrlBackList(self, pk):
        url = reverse_lazy(
            'core:contactsupplier-list',
            kwargs={"pk": pk}
        )
        return url

    @classmethod
    def get_ContactRecord(self, pk):
        try:
            record = SupplierContact.objects.get(id=pk)
            return record
        except SupplierContact.DoesNotExist:
            return None
        except Exception as error:
            raise NameError(str(error))

    @classmethod
    def get_ContactLastRecord(self, user):
        try:
            record = SupplierContact.objects.filter(
                created_by=user
            ).latest('pk')
            return record
        except SupplierContact.DoesNotExist:
            return None
        except Exception as error:
            raise NameError(str(error))

    @classmethod
    def search(self, records, _value):
        try:
            _value = int(_value)
        except Exception:
            _value

        if type(_value) is str:
            records = records.filter(legal_entity__icontains=_value)
        elif type(_value) is int:
            records = records.filter(id=_value)
        return records


class ClientBsn(StxBsn):
    model = Client
    model_filter = ClientFilter
    model_resource = ClientResource
    other_model_resource = ClientContactResource
    key_word = 'legal_entity'

    @classmethod
    def filter_ByPermission(self, records, user):
        return records.filter(company__in=user.companies.all())

    @classmethod
    def get_Assigned(self, user):
        return self.get_All().filter(company__in=user.companies.all())

    @classmethod
    def get_Debtor(self, user):
        clients = self.get_Assigned(user)
        records = clients.filter(debt__gt=0)
        return records

    @classmethod
    def get_IsActive(self, user):
        clients = self.get_Assigned(user)
        records = clients.filter(is_active=True)
        return records

    @classmethod
    def export_XLSX(self, records):
        return ClientResource().export(records).xlsx

    @classmethod
    def export_Contact_XLSX(self, records):
        return ClientContactResource().export(records).xlsx

    @classmethod
    def get_ForCharts(self, user, request):
        records = self.get_Assigned(user)
        if request.GET.get('company', None):
            records = records.filter(company__id=request.GET.get('company'))
        return records

    @classmethod
    def get_Clients(self, _user):
        records = self.get_All()
        return records

    @classmethod
    def create(self, _data, _user):
        with transaction.atomic():
            if _data.get('company'):
                company = _data.get('company').id
            else:
                raise ValueError('Es necesarion ingresar una Empresa.')
            approved = self.validate_Access(_user, 'CRCL', company)
            if approved is False:
                raise ValueError('No tiene permiso crear cliente en esta compañia.')
            record = self.get_ModelObject()()
            record.rfc = _data.get('rfc')
            record.legal_entity = _data.get('legal_entity')
            record.commercial_name = _data.get('commercial_name')
            record.description = _data.get('description')
            record.comments = _data.get('comments')
            record.nationality = _data.get('nationality')
            record.sector = _data.get('sector')
            record.type = _data.get('type')
            record.company = _data.get('company')
            record.email = _data.get('email')
            record.phone = _data.get('phone')
            record.website = _data.get('website')
            record.credit_days = _data.get('credit_days')
            record.credit_limit = _data.get('credit_limit')
            record.settlement = _data.get('settlement')
            record.municipality = _data.get('municipality')
            record.street = _data.get('street')
            record.outdoor_number = _data.get('outdoor_number')
            record.interior_number = _data.get('interior_number')
            record.between_street_a = _data.get('between_street_a')
            record.between_street_b = _data.get('between_street_b')
            record.zip_code = _data.get('zip_code')
            record.latitude = _data.get('latitude')
            record.longitude = _data.get('longitude')
            record.created_by = _user
            record.full_clean()
            record.save()
            return record

    @classmethod
    def update(self, _record, _data, _user):
        with transaction.atomic():
            if _data.get('company'):
                company = _data.get('company').id
            else:
                raise ValueError('Es necesarion ingresar una Empresa.')
            approved = self.validate_Access(_user, 'EDCL', company)
            if approved is False:
                raise ValueError('No tiene permiso editar cliente en esta compañia.')
            _record.rfc = _data.get('rfc')
            _record.legal_entity = _data.get('legal_entity')
            _record.commercial_name = _data.get('commercial_name')
            _record.description = _data.get('description')
            _record.comments = _data.get('comments')
            _record.nationality = _data.get('nationality')
            _record.sector = _data.get('sector')
            _record.type = _data.get('type')
            _record.company = _data.get('company')
            _record.email = _data.get('email')
            _record.phone = _data.get('phone')
            _record.website = _data.get('website')
            if _record.is_active != _data.get('is_active'):
                approved = self.validate_Access(_user, 'DECL', company)
                if approved is False:
                    raise ValueError('No tiene permiso para activar o desactivar el cliente')
                else:
                    _record.is_active = _data.get('is_active')
            _record.credit_days = _data.get('credit_days')
            _record.credit_limit = _data.get('credit_limit')
            _record.settlement = _data.get('settlement')
            _record.municipality = _data.get('municipality')
            _record.street = _data.get('street')
            _record.outdoor_number = _data.get('outdoor_number')
            _record.interior_number = _data.get('interior_number')
            _record.between_street_a = _data.get('between_street_a')
            _record.between_street_b = _data.get('between_street_b')
            _record.zip_code = _data.get('zip_code')
            _record.latitude = _data.get('latitude')
            _record.longitude = _data.get('longitude')
            _record.update_by = _user
            _record.full_clean()
            _record.save()
            return _record

    @classmethod
    def create_Contact(self, _data, _user):
        with transaction.atomic():
            if _data.get('client'):
                company = _data.get('client').company.id
            else:
                raise ValueError('Es necesarion ingresar un cliente.')
            approved = self.validate_Access(_user, 'CRCC', company)
            if approved is False:
                raise ValueError('No tiene permiso crear contacto de cliente en esta compañia.')
            record = ClientContact()
            record.client = _data.get('client')
            record.name = _data.get('name')
            record.phone = _data.get('phone')
            record.phone_ext = _data.get('phone_ext')
            record.email = _data.get('email')
            record.created_by = _user
            record.full_clean()
            record.save()
            return record

    @classmethod
    def update_Contact(self, _record, _data, _user):
        with transaction.atomic():
            if _record.client:
                company = _record.client.company.id
            else:
                raise ValueError('Es necesarion ingresar un cliente.')
            approved = self.validate_Access(_user, 'EDCP', company)
            if approved is False:
                raise ValueError('No tiene permiso editar contacto de cliente en esta compañia.')
            _record.name = _data.get('name')
            _record.phone = _data.get('phone')
            _record.phone_ext = _data.get('phone_ext')
            _record.email = _data.get('email')
            if _record.is_active != _data.get('is_active'):
                approved = self.validate_Access(_user, 'DECC', company)
                if approved is False:
                    raise ValueError('No tiene permiso para activar o desactivar el contacto del cliente')
                else:
                    _record.is_active = _data.get('is_active')
            _record.update_by = _user
            _record.full_clean()
            _record.save()
            return _record

    @classmethod
    def get_UrlNewContact(self, record):
        url = reverse_lazy(
            'core:contactclient-new',
            kwargs={"pk": record.id}
        )
        return url

    @classmethod
    def get_UrlOnSaveContact(self, request, record):
        url = reverse_lazy(
            'core:contactclient-list',
            kwargs={"pk": record.id}
        )
        return url

    @classmethod
    def get_UrlBackList(self, pk):
        url = reverse_lazy(
            'core:contactclient-list',
            kwargs={"pk": pk}
        )
        return url

    @classmethod
    def get_ContactRecord(self, pk):
        try:
            record = ClientContact.objects.get(id=pk)
            return record
        except ClientContact.DoesNotExist:
            return None
        except Exception as error:
            raise NameError(str(error))

    @classmethod
    def get_ContactLastRecord(self, user):
        try:
            record = ClientContact.objects.filter(
                created_by=user
            ).latest('pk')
            return record
        except ClientContact.DoesNotExist:
            return None
        except Exception as error:
            raise NameError(str(error))

    @classmethod
    def search(self, records, _value):
        try:
            _value = int(_value)
        except Exception:
            _value

        if type(_value) is str:
            records = records.filter(legal_entity__icontains=_value)
        elif type(_value) is int:
            records = records.filter(id=_value)
        return records

    # @classmethod
    # def get_UrlOnSave(self, request, record):
    #     url = reverse_lazy(
    #         'core:contactclient-list',
    #         kwargs={"pk": record.client.id}
    #     )
    #     return url


class ItemBsn(StxBsn):
    model = Item
    model_resource = ItemResource
    model_filter = ItemFilter
    key_word = 'name'

    @classmethod
    def get_MostSelledItems(self, user, request):
        records = self.get_Assigned(user)
        if request.GET.get('company', None):
            records = records.filter(company__id=request.GET.get('company'))
        if request.GET.get('client', None):
            records = records.filter(has_stockitem__has_saleorderitem__order__client__id=request.GET.get('client'))
        if request.GET.get('branchoffice', None):
            records = records.filter(has_stockitem__has_saleorderitem__order__branchoffice__id=request.GET.get('branchoffice'))
        if request.GET.get('paymethod', None):
            records = records.filter(has_stockitem__has_saleorderitem__order__paymethod=request.GET.get('paymethod'))
        selleds = records.annotate(total_amount=Sum('has_stockitem__has_saleorderitem__qty'))
        most_selled = selleds.exclude(total_amount=None).values(
            'primary_number', 'total_amount').order_by('-total_amount')[:9]
        return most_selled

    @classmethod
    def get_MostInStock(self, user, request):
        records = self.get_Assigned(user)
        if request.GET.get('company', None):
            records = records.filter(company__id=request.GET.get('company'))
        if request.GET.get('warehouse', None):
            records = records.filter(has_stockitem__warehouse__id=request.GET.get('warehouse'))
        selleds = records.annotate(total_amount=Sum('has_stockitem__qty'))
        most_selled = selleds.exclude(total_amount=None).values(
            'primary_number', 'total_amount').order_by('-total_amount')[:9]
        return most_selled

    @classmethod
    def get_Assigned(self, user):
        return self.get_All().filter(company__in=user.companies.all())

    @classmethod
    def export_XLSX(self, records):
        return ItemResource().export(records).xlsx

    @classmethod
    def get_IsActive(self, user):
        item = self.get_Assigned(user)
        return item.filter(is_active=True)

    @classmethod
    def create(self, _data, _user, _files=None):
        with transaction.atomic():
            record = self.get_ModelObject()()
            record.company = _data.get('company')
            record.primary_number = _data.get('primary_number')
            record.sat_code = _data.get('sat_code')
            record.name = _data.get('name')
            record.description = _data.get('description')
            record.upc = _data.get('upc')
            record.sku = _data.get('sku')
            record.udm = _data.get('udm')
            record.udp = _data.get('udp')
            if _files:
                record.image = _files.get('image', None)
            record.created_by = _user
            record.full_clean()
            record.save()
            return record

    @classmethod
    def update(self, _record, _data, _user, _files=None):
        with transaction.atomic():
            _record.company = _data.get('company')
            _record.primary_number = _data.get('primary_number')
            _record.sat_code = _data.get('sat_code')
            _record.name = _data.get('name')
            _record.description = _data.get('description')
            _record.upc = _data.get('upc')
            _record.sku = _data.get('sku')
            _record.udm = _data.get('udm')
            _record.udp = _data.get('udp')
            if _files:
                _record.image = _files.get('image', None)
            if _record.is_active != _data.get('is_active'):
                approved = self.validate_Access(_user, 'DEAR', company)
                if approved is False:
                    raise ValueError('No tiene permiso para activar o desactivar el articulo')
                else:
                    _record.is_active = _data.get('is_active')
            _record.updated_by = _user
            _record.full_clean()
            _record.save()
            return _record

    @classmethod
    def search(self, records, _value):
        try:
            _value = int(_value)
        except Exception:
            _value

        if type(_value) is str:
            records = records.filter(name__icontains=_value)
        elif type(_value) is int:
            records = records.filter(id=_value)
        return records

    @classmethod
    def search_SatProduct(self, _value):
        try:
            _value = int(_value)
        except Exception:
            pass

        if type(_value) is str:
            records = SatProduct.objects.filter(
                Q(key__icontains=_value) |
                Q(description__icontains=_value)
            )
        elif type(_value) is int:
            records = SatProduct.objects.filter(id=_value)
        return records

    @classmethod
    def get_AllSatProduct(self):
        return SatProduct.objects.all()

    @classmethod
    def get_SatProduct(self, _description):
        return self.get_AllSatProduct().get(description=_description)


    #@classmethod
    #def export_XLSX(self, **kwargs):
        #records = kwargs.get('records')
        #output = io.BytesIO()

        #workbook = Workbook(output, {'in_memory': True})
        #worksheet = workbook.add_worksheet('Articulos')

        #header_font = workbook.add_format({'bold': True, 'font_size': 11})
        #header_font.set_align('center')
        #header_font.set_align('vcenter')

        #content_font = workbook.add_format({'bold': False, 'font_size': 10})
        #content_font.set_align('center')
        #content_font.set_align('vcenter')

        #description_content_font = workbook.add_format({'bold': False, 'font_size': 10})
        #description_content_font.set_align('vcenter')
        #description_content_font.set_align('left')

        #width_a = worksheet.set_column(0, 0, 20)
        #width_b = worksheet.set_column(1, 1, 20)
        #width_c = worksheet.set_column(2, 2, 20)
        #width_d = worksheet.set_column(3, 3, 24)
        #width_e = worksheet.set_column(4, 4, 14)
        #width_f = worksheet.set_column(5, 5, 30)
        #width_g = worksheet.set_column(6, 6, 8)
        #width_h = worksheet.set_column(7, 7, 8)
        #width_i = worksheet.set_column(8, 8, 4)
        #width_j = worksheet.set_column(9, 9, 14)
        #width_k = worksheet.set_column(10, 10, 16)
        #width_l = worksheet.set_column(11, 11, 6)

        #row = 1

        #worksheet.write(f'A{row}', 'Compañía', header_font)
        #worksheet.write(f'B{row}', 'Nombre', header_font)
        #worksheet.write(f'C{row}', 'SKU', header_font)
        #worksheet.write(f'D{row}', 'Precio máximo al público', header_font)
        #worksheet.write(f'E{row}', 'Precio de venta', header_font)
        #worksheet.write(f'F{row}', 'Descripción', header_font)
        #worksheet.write(f'G{row}', 'Nuevo', header_font)
        #worksheet.write(f'H{row}', 'Agotado', header_font)
        #worksheet.write(f'I{row}', 'IVA', header_font)
        #worksheet.write(f'J{row}', 'Precio unitario', header_font)
        #worksheet.write(f'K{row}', 'Código de barras', header_font)
        #worksheet.write(f'L{row}', 'Activo', header_font)

        #for record in records:
            #row += 1
            #worksheet.write(f'A{row}', record.company.name if record.company else 'No', content_font)
            #worksheet.write(f'B{row}', record.name, description_content_font)
            #worksheet.write(f'C{row}', record.sku if record.sku else 'No', content_font)
            #worksheet.write(f'E{row}', record.sell_price, content_font)
            #worksheet.write(f'F{row}', record.description, description_content_font)
            #worksheet.write(f'G{row}', 'Si' if record.is_new else 'No', content_font)
            #worksheet.write(f'H{row}', 'Si' if record.is_sold_out else 'No', content_font)
            #worksheet.write(f'I{row}',  record.iva if record.iva else 'No', content_font)
            #worksheet.write(f'J{row}', record.price, content_font)
            #worksheet.write(f'K{row}', record.barcode if record.barcode else 'No', content_font)
            #worksheet.write(f'L{row}', 'Si' if record.is_active else 'No', content_font)

        #workbook.close()
        #output.seek(0)
        #file = output.read()
        #output.close()
        #return file

    @classmethod
    def get_ItemList(self, pk):
        records = self.get_ModelObject().objects.filter(
            company_id=pk
        )
        return records

    @classmethod
    def get_ItemStock(self, warehouse_pk):
        records = self.get_ModelObject().objects.filter(
            has_stockitem__warehouse__id=warehouse_pk
        )
        return records


class WarehouseBsn(StxBsn):
    model = Warehouse
    model_resource = WarehouseResource
    model_filter = WarehouseFilter

    @classmethod
    def get_Assigned(self, user):
        return self.get_All().filter(company__in=user.companies.all())

    @classmethod
    def get_IsActive(self, user):
        warehouses = self.get_Assigned(user)
        records = warehouses.filter(is_active=True)
        return records

    @classmethod
    def export_XLSX(self, records):
        return WarehouseResource().export(records).xlsx

    @classmethod
    def get_AssignedWithStock(self, user):
        warehouses = self.get_Assigned(user)
        records = warehouses.filter(
            has_warehouse__isnull=False,
            is_active=True
        ).distinct()
        return records

    @classmethod
    def get_ForCharts(self, user, request):
        records = self.get_Assigned(user)
        if request.GET.get('company', None):
            records = records.filter(company__id=request.GET.get('company'))
        return records

    @classmethod
    def create(self, _data, _user):
        with transaction.atomic():
            if _data.get('company'):
                company = _data.get('company').id
            else:
                raise ValueError('Es necesarion ingresar una empresa.')
            approved = self.validate_Access(_user, 'CRAL', company)
            if approved is False:
                raise ValueError('No tiene permiso crear almacen en esta compañia.')
            record = self.get_ModelObject()()
            record.name = _data.get('name')
            record.company = _data.get('company')
            record.description = _data.get('description')
            record.settlement = _data.get('settlement')
            record.municipality = _data.get('municipality')
            record.street = _data.get('street')
            record.outdoor_number = _data.get('outdoor_number')
            record.interior_number = _data.get('interior_number')
            record.between_street_a = _data.get('between_street_a')
            record.between_street_b = _data.get('between_street_b')
            record.zip_code = _data.get('zip_code')
            record.created_by = _user
            record.full_clean()
            record.save()
            return record

    @classmethod
    def update(self, _record, _data, _user):
        with transaction.atomic():
            if _data.get('company'):
                company = _data.get('company').id
            else:
                raise ValueError('Es necesarion ingresar una empresa.')
            approved = self.validate_Access(_user, 'EDAL', company)
            if approved is False:
                raise ValueError('No tiene permiso editar almacen en esta compañia.')
            _record.name = _data.get('name')
            _record.company = _data.get('company')
            _record.description = _data.get('description')
            if _record.is_active != _data.get('is_active'):
                approved = self.validate_Access(_user, 'DEAL', company)
                if approved is False:
                    raise ValueError('No tiene permiso para activar o desactivar el almacen')
                else:
                    _record.is_active = _data.get('is_active')
            _record.settlement = _data.get('settlement')
            _record.municipality = _data.get('municipality')
            _record.street = _data.get('street')
            _record.outdoor_number = _data.get('outdoor_number')
            _record.interior_number = _data.get('interior_number')
            _record.between_street_a = _data.get('between_street_a')
            _record.between_street_b = _data.get('between_street_b')
            _record.zip_code = _data.get('zip_code')
            _record.updated_by = _user
            _record.full_clean()
            _record.save()
            return _record

    @classmethod
    def get_ItemStock(self, warehouse_pk, user):
        items = self.get_Assigned(user).get(
            id=warehouse_pk).has_warehouse.all()
        records = items.filter(qty__gt=0)
        return records

    @classmethod
    def get_ItemStockExist(self, warehouse_pk, user):
        items = self.get_Assigned(user).get(
            id=warehouse_pk).has_warehouse.all()
        records = items.filter(qty__gt=0)
        return records

    @classmethod
    def get_ItemOutStock(self, warehouse_pk, user):
        records = ItemBsn.get_Assigned(user).exclude(
            has_stockitem__warehouse__id=warehouse_pk)
        return records

    @classmethod
    def get_ItemOutStockActive(self, warehouse_pk, user):
        records = ItemBsn.get_IsActive(user).exclude(
            has_stockitem__warehouse__id=warehouse_pk)
        return records

    @classmethod
    def get_WarehouseList(self, user):
        records = self.get_ModelObject().objects.all()
        return records

    @classmethod
    def get_Warehouse(self, pk):
        record = self.get_ModelObject().objects.get(id=pk)
        if record:
            return record
        else:
            raise ValueError('No existe este almacén.')

   # @classmethod
    #def export_XLSX(self, **kwargs):
        #records = kwargs.get('records')
        #output = io.BytesIO()

        #workbook = Workbook(output, {'in_memory': True})
        #worksheet = workbook.add_worksheet('Almacenes')

        #header_font = workbook.add_format({'bold': True, 'font_size': 11})
        #header_font.set_align('center')
        #header_font.set_align('vcenter')

        #content_font = workbook.add_format({'bold': False, 'font_size': 10})
        #content_font.set_align('center')
        #content_font.set_align('vcenter')

        #description_content_font = workbook.add_format({'bold': False, 'font_size': 10})
        #description_content_font.set_align('vcenter')
        #description_content_font.set_align('left')

        #width_a = worksheet.set_column(0, 0, 20)
        #width_b = worksheet.set_column(1, 1, 20)
        #width_c = worksheet.set_column(2, 2, 20)
        #width_d = worksheet.set_column(3, 3, 14)
        #width_e = worksheet.set_column(4, 4, 14)
        #width_f = worksheet.set_column(5, 5, 20)
        #width_g = worksheet.set_column(6, 6, 20)
        #width_h = worksheet.set_column(7, 7, 16)
        #width_i = worksheet.set_column(8, 8, 14)
        #width_j = worksheet.set_column(9, 9, 14)
        #width_k = worksheet.set_column(10, 10, 6)

        #row = 1

        #worksheet.write(f'A{row}', 'Compañía', header_font)
        #worksheet.write(f'B{row}', 'Nombre', header_font)
        #worksheet.write(f'C{row}', 'Calle', header_font)
        #worksheet.write(f'D{row}', 'Número exterior', header_font)
        #worksheet.write(f'E{row}', 'Número interior', header_font)
        #worksheet.write(f'F{row}', 'Entre calle 1', header_font)
        #worksheet.write(f'G{row}', 'Entre calle 2', header_font)
        #worksheet.write(f'H{row}', 'Código postal', header_font)
        #worksheet.write(f'I{row}', 'Latitud', header_font)
        #worksheet.write(f'J{row}', 'Longitud', header_font)
        #worksheet.write(f'K{row}', 'Activo', header_font)

        #for record in records:
         #   row += 1
         #   worksheet.write(f'A{row}', record.company.name if record.company else 'No', description_content_font)
         #   worksheet.write(f'B{row}', record.name, description_content_font)
         #   worksheet.write(f'C{row}', record.street if record.street else 'No', content_font)
         #   worksheet.write(f'D{row}', record.outdoor_number if record.outdoor_number else 'No', content_font)
         #   worksheet.write(f'E{row}', record.interior_number if record.interior_number else 'No', content_font)
         #   worksheet.write(f'F{row}', record.between_street_a if record.between_street_a else 'No', content_font)
         #   worksheet.write(f'G{row}', record.between_street_b if record.between_street_b else 'No', content_font)
         #   worksheet.write(f'H{row}', record.zip_code if record.zip_code else 'No', content_font)
         #   worksheet.write(f'I{row}',  record.latitude if record.latitude else 'No', content_font)
         #   worksheet.write(f'J{row}', record.longitude if record.longitude else 'No', content_font)
         #   worksheet.write(f'K{row}', 'Si' if record.is_active else 'No', content_font)


    @classmethod
    def search(self, records, _value):
        try:
            _value = int(_value)
        except Exception:
            _value

        if type(_value) is str:
            records = records.filter(name__icontains=_value)
        elif type(_value) is int:
            records = records.filter(id=_value)
        return records


class UdmBsn(StxBsn):
    model = Udm
    model_filter = UdmFilter

    @classmethod
    def filter_ByPermission(self, records, user):
        return records

    @classmethod
    def search(self, records, _value):
        try:
            _value = int(_value)
        except Exception:
            _value

        if type(_value) is str:
            records = records.filter(name__icontains=_value)
        elif type(_value) is int:
            records = records.filter(id=_value)
        return records


class SatProductBsn(StxBsn):
    model = SatProduct
    model_filter = SatProductFilter
    key_word = "key"

    @classmethod
    def filter_ByPermission(self, records, user):
        return records

    @classmethod
    def search(self, records, _value):
        try:
            _value = int(_value)
        except Exception:
            _value

        if type(_value) is str:
            records = records.filter(description__icontains=_value)
        elif type(_value) is int:
            records = records.filter(key=_value)
        return records


class TaxBsn(StxBsn):
    model = Tax
    model_filter = TaxFilter

    @classmethod
    def get_Assigned(self, user):
        return self.get_All().filter(company__in=user.companies.all())

    @classmethod
    def get_TaxList(self, _user):
        records = self.get_All()
        return records

    @classmethod
    def get_IsActive(self, _user):
        records = self.get_All().filter(is_active=True)
        return records

    @classmethod
    def search(self, records, _value):
        try:
            _value = int(_value)
        except Exception:
            _value

        if type(_value) is str:
            records = records.filter(
                Q(abbreviation__icontains=_value)|
                Q(description__icontains=_value)
            )
        elif type(_value) is int:
            records = records.filter(id=_value)
        return records


class StockItemBsn(StxBsn):
    model = StockItem
    model_filter = StockItemFilter

    @classmethod
    def filter_ByPermission(self, records, user):
        return records.filter(warehouse__company__in=user.companies.all())

    @classmethod
    def get_StockList(self, user):
        records = self.get_ModelObject().objects.all()
        return records

    @classmethod
    def export_XLSX(self, records):
        return StockItemResource().export(records).xlsx

    @classmethod
    def get_Stock(self, pk):
        record = self.get_ModelObject().objects.get(id=pk)
        if record:
            return record
        else:
            raise ValueError('No existe este articulo.')

    @classmethod
    def get_ItemStock(self, product, pk):
        try:
            itemstock = self.get_ModelObject().objects.get(id=product.id)
            if itemstock.warehouse == pk:
                record = itemstock
                return record
        except self.get_ModelObject().DoesNotExist:
            return None

    @classmethod
    def search(self, records, _value):
        try:
            _value = int(_value)
        except Exception:
            _value

        if type(_value) is str:
            records = records.filter(warehouse_name__icontains=_value)
        elif type(_value) is int:
            records = records.filter(id=_value)
        return records
