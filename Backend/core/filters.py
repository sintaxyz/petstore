# Python's Libraries

# Django's Libraries
import django_filters
from django.db.models import F


# Third-party Libraries

# Own's Libraries
from .models import Supplier
from .models import Udm
from .models import Client
from .models import Item
from .models import Warehouse
from .models import StockItem
from .models import Tax
from .models import SatProduct


def assigned_warehouses(request):
    if request is None:
        return Warehouse.objects.none()
    else:
        return Warehouse.objects.filter(company__in=request.user.companies.all())


def assigned_items(request):
    if request is None:
        return Item.objects.none()
    else:
        return Item.objects.filter(company__in=request.user.companies.all())


def assigned_suppliers(request):
    if request is None:
        return Item.objects.none()
    else:
        return Supplier.objects.filter(company__in=request.user.companies.all())


class SupplierFilter(django_filters.FilterSet):

    BOOLEAN_CHOICES = (
        ('Si', 'Si'),
        ('No', 'No')
    )

    is_active = django_filters.ChoiceFilter(
        choices=BOOLEAN_CHOICES,
        method='is_ActiveFilter'
    )

    class Meta:
        model = Supplier
        fields = [
            'commercial_name',
            'legal_entity',
            'rfc',
            'email',
            'is_active',
            'bank',
        ]

    def is_ActiveFilter(self, queryset, name, value):
        if value == 'Si':
            queryset = queryset.filter(is_active=True)
        elif value == 'No':
            queryset = queryset.filter(is_active=False)
        return queryset


class ClientFilter(django_filters.FilterSet):

    BOOLEAN_CHOICES = (
        ('Si', 'Si'),
        ('No', 'No')
    )

    is_active = django_filters.ChoiceFilter(
        choices=BOOLEAN_CHOICES,
        method='is_ActiveFilter'
    )

    class Meta:
        model = Client
        fields = [
            'commercial_name',
            'legal_entity',
            'nationality',
            'sector',
            'type',
            'email',
            'is_active',
        ]

    def is_ActiveFilter(self, queryset, name, value):
        if value == 'Si':
            queryset = queryset.filter(is_active=True)
        elif value == 'No':
            queryset = queryset.filter(is_active=False)
        return queryset


class ItemFilter(django_filters.FilterSet):

    BOOLEAN_CHOICES = (
        ('Si', 'Si'),
        ('No', 'No')
    )

    is_active = django_filters.ChoiceFilter(
        choices=BOOLEAN_CHOICES,
        method='is_ActiveFilter'
    )

    class Meta:
        model = Item
        fields = [
            'primary_number',
            'name',
            'is_active',
        ]

    def is_ActiveFilter(self, queryset, name, value):
        if value == 'Si':
            queryset = queryset.filter(is_active=True)
        elif value == 'No':
            queryset = queryset.filter(is_active=False)
        return queryset


class WarehouseFilter(django_filters.FilterSet):

    BOOLEAN_CHOICES = (
        ('Si', 'Si'),
        ('No', 'No')
    )

    is_active = django_filters.ChoiceFilter(
        choices=BOOLEAN_CHOICES,
        method='is_ActiveFilter'
    )

    class Meta:
        model = Warehouse
        fields = [
            'name',
        ]

    def is_ActiveFilter(self, queryset, name, value):
        if value == 'Si':
            queryset = queryset.filter(is_active=True)
        elif value == 'No':
            queryset = queryset.filter(is_active=False)
        return queryset


class SatProductFilter(django_filters.FilterSet):

    class Meta:
        model = SatProduct
        fields = [
            'key',
            'description',
        ]


class UdmFilter(django_filters.FilterSet):

    class Meta:
        model = Udm
        fields = [
            'key',
            'type',
        ]


class StockItemFilter(django_filters.FilterSet):

    MINIMUN_CHOICES = (
        ('EN', 'Suficiente'),
        ('MI', 'Minimo'),
        ('UN', 'Desabastecido'),
    )

    warehouse = django_filters.ModelChoiceFilter(queryset=assigned_warehouses)

    item = django_filters.ModelChoiceFilter(queryset=assigned_items)

    minimun_qty = django_filters.ChoiceFilter(
        label="Abastecimiento",
        choices=MINIMUN_CHOICES,
        method='minimun_ChoiceMethod'
    )

    class Meta:
        model = StockItem
        fields = [
            'item',
            'warehouse',
        ]

    def minimun_ChoiceMethod(self, queryset, name, value):
        if value == "EN":
            return queryset.filter(qty__gt=F("minimun_qty"))
        elif value == "UN":
            return queryset.filter(qty__lte=0)
        elif value == "MI":
            return queryset.filter(qty__gt=0, qty__lte=F("minimun_qty"))
        else:
            return queryset


class TaxFilter(django_filters.FilterSet):

    class Meta:
        model = Tax
        fields = [
            'abbreviation',
            'description',
        ]
