# Python's Libraries
from decimal import Decimal

# Django's Libraries
from django.db import models
from django.urls import reverse_lazy

# Third-party Libraries

# Own's Libraries
from tools.models import AppModel
from tools.models import AddressModel
from tools.validators import MinValueValidator
# from tools.utils import FileAdmin
# from security.models import User
from security.models import Company


class Supplier(AddressModel, AppModel):
    SECTOR_CHOICES = (
        ('AGR', 'Agricultura'),
        ('FOO', 'alimentación'),
        ('COM', 'Comercio'),
        ('CON', 'Construcción'),
        ('EDU', 'Educación'),
        ('FUN', 'Función pública'),
        ('TOU', 'Turismo'),
        ('CHE', 'Química'),
        ('MAN', 'Manufactura'),
        ('MED', 'Medios de comunicación'),
        ('HEA', 'Servicios de Salud'),
        ('MIN', 'Minería'),
        ('OIL', 'Petróleo'),
        ('TEL', 'Telecomunicaciones'),
        ('FIN', 'Servicios financieros'),
        ('PUB', 'Servicios públicos'),
        ('TEX', 'Textiles'),
        ('TRA', 'Transporte'),
    )
    BANK_TYPE = [
        ('BNMX', 'Banamex'),
        ('BSTR', 'Banco Santander (México)'),
        ('HSBC', 'HSBC México'),
        ('SBIL', 'Scotiabank Inverlat'),
        ('BBVA', 'BBVA Bancomer'),
        ('BNTE', 'Banorte'),
        ('BITS', 'Banco Interacciones'),
        ('NIBA', 'Banco Inbursa'),
        ('BMFL', 'Banca Mifel'),
        ('BRMY', 'Banco Regional de Monterrey'),
        ('BIVX', 'Banco Invex'),
        ('BDBJ', 'Banco del Bajio'),
        ('BNSI', 'Bansi'),
        ('BAFE', 'Banca Afirme'),
        ('BOAM', 'Bank of America México'),
        ('BJPM', 'Banco J.P. Morgan'),
        ('BVPM', 'Banco Ve Por Mas'),
        ('BAEM', 'American Express Bank (México)'),
        ('BIVA', 'Investa Bank'),
        ('CIBC', 'CiBanco'),
        ('BOTM', 'Bank of Tokyo-Mitsubishi UFJ (México)'),
        ('BNMO', 'Banco Monex'),
        ('DBMX', 'Deutsche Bank México'),
        ('BNAZ', 'Banco Azteca'),
        ('BCSE', 'Banco Credit Suisse (México)'),
        ('BAMX', 'Banco Autofin México'),
        ('BBMX', 'Barclays Bank México'),
        ('BAFM', 'Banco Ahorro Famsa'),
        ('BITC', 'Intercam Banco'),
        ('BABC', 'ABC Capital'),
        ('BATV', 'Banco Actinver'),
        ('BCPM', 'Banco Compartamos'),
        ('BMTV', 'Banco Multiva'),
        ('BUBS', 'UBS Bank México'),
        ('BCPL', 'Bancoppel'),
        ('BCSU', 'ConsuBanco'),
        ('BWMM', 'Banco Wal-Mart de México'),
        ('BVWG', 'Volkswagen Bank'),
        ('BBSE', 'Banco Base'),
        ('BPGT', 'Banco Pagatodo'),
        ('BFDS', 'Banco Forjadores'),
        ('BAOL', 'Bankaool'),
        ('BIMM', 'Banco Inmobiliario Mexicano'),
        ('BFDD', 'Fundación Dondé Banco'),
        ('BBCA', 'Banco Bancrea'),
    ]
    company = models.ForeignKey(
        Company,
        verbose_name='compañía',
        related_name='has_suppliers',
        on_delete=models.PROTECT,
        blank=False,
        null=True,
    )
    legal_entity = models.CharField(
        verbose_name='nombre legal',
        max_length=150,
        blank=False,
        null=True,
    )
    commercial_name = models.CharField(
        verbose_name='nombre comercial',
        max_length=150,
        blank=True,
        null=True,
    )
    rfc = models.CharField(
        verbose_name='rfc o clave',
        unique=True,
        max_length=13,
        blank=False,
        null=True,
    )
    is_active = models.BooleanField(
        verbose_name='activo',
        blank=False,
        null=False,
        default=True,
    )
    sector = models.CharField(
        verbose_name='sector',
        max_length=3,
        choices=SECTOR_CHOICES,
        blank=False,
        null=True,
    )
    email = models.EmailField(
        verbose_name='correo',
        blank=False,
        null=True,
    )
    phone = models.CharField(
        verbose_name='teléfono',
        max_length=20,
        blank=False,
        null=True,
    )
    website = models.URLField(
        verbose_name='sitio web',
        max_length=150,
        blank=True,
        null=True,
    )
    bank = models.CharField(
        verbose_name='Banco',
        max_length=4,
        choices=BANK_TYPE,
        blank=True,
        null=True,
    )
    account_number = models.BigIntegerField(
        verbose_name='Numero de Cuenta',
        validators=[MinValueValidator(Decimal('0'))],
        blank=True,
        null=True,
    )
    CLABE = models.BigIntegerField(
        verbose_name='Clave Interbancaria (CLABE)',
        validators=[MinValueValidator(Decimal('0'))],
        blank=True,
        null=True,
    )

    class Meta:
        unique_together = (
            ('company', 'rfc'),
            ('company', 'email'),
        )
        verbose_name = 'proveedor'
        verbose_name_plural = 'proveedores'

    def __str__(self):
        return self.legal_entity

    @property
    def status_text(self):
        if self.is_active:
            return "Activo"
        else:
            return "Inactivo"

    @property
    def status_color(self):
        if self.is_active:
            return "green"
        else:
            return "gray"

    @property
    def url_edit(self):
        url = reverse_lazy(
            'core:supplier-edit',
            kwargs={'pk': self.pk}
        )
        return url


class SupplierContact(AppModel):
    supplier = models.ForeignKey(
        Supplier,
        verbose_name='proveedor',
        related_name='belong_to',
        on_delete=models.PROTECT,
        blank=False,
        null=True,
    )
    name = models.CharField(
        verbose_name='nombre',
        max_length=100,
        blank=False,
        null=True,
    )
    is_active = models.BooleanField(
        verbose_name='active',
        blank=False,
        null=False,
        default=True,
    )
    phone = models.CharField(
        verbose_name='teléfono',
        max_length=15,
        blank=False,
        null=True,
    )
    phone_ext = models.CharField(
        verbose_name='extensión',
        max_length=10,
        blank=True,
        null=True,
    )
    email = models.EmailField(
        verbose_name='email',
        blank=False,
        null=True,
    )

    class Meta:
        unique_together = (('supplier', 'name'),)
        verbose_name = 'Contacto Proveedor'
        verbose_name_plural = 'Contactos Proveedor'

    @property
    def status_text(self):
        if self.is_active:
            return "Activo"
        else:
            return "Inactivo"

    @property
    def status_color(self):
        if self.is_active:
            return "green"
        else:
            return "gray"

    @property
    def url_edit(self):
        url = reverse_lazy(
            'core:contactsupplier-edit',
            kwargs={'pk': self.pk}
        )
        return url

    def __str__(self):
        return f'{self.name} - {self.supplier}'


class Client(AddressModel, AppModel):
    SECTOR_CHOICES = (
        ('AGR', 'Agricultura'),
        ('FOO', 'alimentación'),
        ('COM', 'Comercio'),
        ('CON', 'Construcción'),
        ('EDU', 'Educación'),
        ('FUN', 'Función pública'),
        ('TOU', 'Turismo'),
        ('CHE', 'Química'),
        ('MAN', 'Manufactura'),
        ('MED', 'Medios de comunicación'),
        ('HEA', 'Servicios de Salud'),
        ('MIN', 'Minería'),
        ('OIL', 'Petróleo'),
        ('TEL', 'Telecomunicaciones'),
        ('FIN', 'Servicios financieros'),
        ('PUB', 'Servicios públicos'),
        ('TEX', 'Textiles'),
        ('TRA', 'Transporte'),
    )
    NATIONALITY_CHOICES = [
        ('MX', 'Mexicana'),
        ('NE', 'Nacionalidad Extranjera'),
    ]
    DAYS_CREDIT_CHOICES = (
        (30, '30 Días'),
        (60, '60 Días'),
        (90, '90 Días'),
    )
    TYPE_CHOICES = [
        ('PUB', 'Publica'),
        ('PRI', 'Privada'),
    ]
    company = models.ForeignKey(
        Company,
        verbose_name='Compañia',
        related_name='clients',
        on_delete=models.CASCADE,
        blank=False,
        null=True
    )
    rfc = models.CharField(
        verbose_name='rfc o clave',
        max_length=13,
        null=True,
        blank=False,
    )
    legal_entity = models.CharField(
        verbose_name='nombre legal',
        max_length=150,
        unique=True,
        blank=False,
        null=True,
    )
    commercial_name = models.CharField(
        verbose_name='nombre comercial',
        max_length=150,
        blank=True,
        null=True,
    )
    comments = models.TextField(
        verbose_name='descripción',
        blank=True,
        null=True,
    )
    is_active = models.BooleanField(
        verbose_name="active",
        blank=False,
        null=False,
        default=True,
    )
    credit_days = models.SmallIntegerField(
        verbose_name='dias de credito',
        choices=DAYS_CREDIT_CHOICES,
        blank=True,
        null=True,
    )
    credit_limit = models.DecimalField(
        verbose_name='limite de cédito',
        decimal_places=2,
        max_digits=10,
        default=0,
        validators=[MinValueValidator(
            Decimal('0'),
            "El valor de monto pendiente por pagar no puede ser menor a 0")],
        blank=True,
        null=True,
    )
    debt = models.DecimalField(
        verbose_name='adeudo',
        decimal_places=2,
        max_digits=10,
        default=0,
        validators=[MinValueValidator(
            Decimal('0'),
            "El valor de monto pendiente por pagar no puede ser menor a 0")],
        blank=True,
        null=True,
    )
    nationality = models.CharField(
        verbose_name='nacionalidad',
        max_length=2,
        choices=NATIONALITY_CHOICES,
        default='MX',
    )
    sector = models.CharField(
        verbose_name='sector',
        choices=SECTOR_CHOICES,
        max_length=3,
        blank=False,
        null=True,
    )
    type = models.CharField(
        verbose_name='tipo',
        max_length=3,
        choices=TYPE_CHOICES,
        default='PUB',
    )
    email = models.EmailField(
        verbose_name='email',
        blank=False,
        null=True,
    )
    phone = models.CharField(
        verbose_name='teléfono',
        max_length=20,
        blank=False,
        null=True,
    )
    website = models.URLField(
        verbose_name='sitio web',
        blank=True,
        null=True
    )

    class Meta:
        unique_together = (('rfc', 'company'))
        verbose_name = 'cliente'
        verbose_name_plural = 'clientes'

    def __str__(self):
        return self.legal_entity

    @property
    def status_text(self):
        if self.is_active:
            return "Activo"
        else:
            return "Inactivo"

    @property
    def status_color(self):
        if self.is_active:
            return "green"
        else:
            return "gray"

    @property
    def url_edit(self):
        url = reverse_lazy(
            'core:client-edit',
            kwargs={'pk': self.pk}
        )
        return url


class ClientContact(AppModel):
    client = models.ForeignKey(
        Client,
        verbose_name='cliente',
        related_name='has_contacts',
        on_delete=models.PROTECT,
        blank=False,
        null=True,
    )
    name = models.CharField(
        verbose_name='nombre',
        max_length=100,
        blank=False,
        null=True,
    )
    is_active = models.BooleanField(
        verbose_name='active',
        blank=False,
        null=False,
        default=True,
    )
    phone = models.CharField(
        verbose_name='teléfono',
        max_length=15,
        blank=False,
        null=True,
    )
    phone_ext = models.CharField(
        verbose_name='extensión',
        max_length=10,
        blank=True,
        null=True,
    )
    email = models.EmailField(
        verbose_name='email',
        blank=False,
        null=True,
    )

    class Meta:
        unique_together = (('client', 'name'),)
        verbose_name = 'contacto de cliente'
        verbose_name_plural = 'contactos de cliente'

    @property
    def status_text(self):
        if self.is_active:
            return "Activo"
        else:
            return "Inactivo"

    @property
    def status_color(self):
        if self.is_active:
            return "green"
        else:
            return "gray"

    @property
    def url_edit(self):
        url = reverse_lazy(
            'core:contactclient-edit',
            kwargs={'pk': self.pk}
        )
        return url

    def __str__(self):
        return f'{self.name} - {self.client}'


class SatProduct(models.Model):
    key = models.CharField(
        verbose_name='clave',
        primary_key=True,
        max_length=50,
        blank=False,
        null=False,
    )
    description = models.TextField(
        verbose_name='descripcion',
        blank=False,
        null=False,
    )

    class Meta:
        verbose_name = 'Producto SAT'
        verbose_name_plural = 'Productos SAT'

    def __str__(self):
        return f'{self.key}: {self.description}'

    @property
    def url_edit(self):
        url = reverse_lazy(
            'core:satproduct-edit',
            kwargs={'pk': self.pk}
        )
        return url


class Item(AppModel):
    company = models.ForeignKey(
        Company,
        verbose_name='compañía',
        related_name='has_items',
        on_delete=models.PROTECT,
        blank=True,
        null=True,
    )
    sat_code = models.ForeignKey(
        SatProduct,
        verbose_name='Clave SAT',
        related_name='items',
        on_delete=models.PROTECT,
        blank=True,
        null=True,
    )
    primary_number = models.CharField(
        verbose_name='clave',
        max_length=50,
        blank=False,
        null=True,
    )
    name = models.CharField(
        verbose_name='nombre',
        max_length=100,
        blank=False,
        null=True,
    )
    description = models.TextField(
        verbose_name='descripcion',
        blank=False,
        null=True,
    )
    udm = models.ForeignKey(
        'Udm',
        verbose_name='Unidad de Medida',
        related_name='has_udm',
        on_delete=models.PROTECT,
        blank=True,
        null=True,
    )
    udp = models.DecimalField(
        verbose_name='unidad de presentacion',
        max_digits=8,
        decimal_places=2,
        default=0,
        validators=[MinValueValidator(Decimal('0'))],
        null=True,
        blank=False,
    )
    is_active = models.BooleanField(
        verbose_name='Activo',
        blank=False,
        null=False,
        default=True,
    )
    upc = models.CharField(
        verbose_name='codigo de barras',
        max_length=50,
        blank=True,
        null=True,
    )
    sku = models.CharField(
        verbose_name='sku',
        max_length=50,
        blank=True,
        null=True,
    )
    image = models.ImageField(
        verbose_name="foto",
        blank=True,
        null=True,
        upload_to="media/storeItem/image",
    )

    class Meta:
        unique_together = (('company', 'primary_number'),)
        verbose_name = 'artículo'
        verbose_name_plural = 'articulos'

    @property
    def status_text(self):
        if self.is_active:
            return "Activo"
        else:
            return "Inactivo"

    @property
    def status_color(self):
        if self.is_active:
            return "green"
        else:
            return "gray"

    @property
    def url_edit(self):
        url = reverse_lazy(
            'core:item-edit',
            kwargs={'pk': self.pk}
        )
        return url

    def __str__(self):
        return self.name


class ItemImage(AppModel):
    item = models.ForeignKey(
        Item,
        verbose_name='artículo',
        related_name='has_storeitem',
        on_delete=models.PROTECT,
        blank=False,
        null=True,
    )
    image = models.ImageField(
        verbose_name="imagen",
        blank=True,
        null=True,
        upload_to="media/storeItem/image",
    )

    class Meta:
        verbose_name = 'Articulo Imagen'
        verbose_name_plural = 'Articulos Imagenes'

    @property
    def url_edit(self):
        url = reverse_lazy(
            'core:itemimage-edit',
            kwargs={'pk': self.pk}
        )
        return url

    def __str__(self):
        return self.item.name


class Warehouse(AddressModel, AppModel):
    company = models.ForeignKey(
        Company,
        verbose_name='compañía',
        related_name='has_warehouses',
        on_delete=models.PROTECT,
        blank=False,
        null=True,
    )
    name = models.CharField(
        verbose_name='nombre',
        max_length=100,
        blank=False,
        null=True,
    )
    description = models.TextField(
        verbose_name='Descripción',
        blank=True,
        null=True,
    )
    is_active = models.BooleanField(
        verbose_name='Activo',
        blank=False,
        null=False,
        default=True,
    )

    class Meta:
        unique_together = (('company', 'name'),)
        verbose_name = 'almacén'
        verbose_name_plural = 'almacenes'

    def __str__(self):
        return self.name

    @property
    def status_text(self):
        if self.is_active:
            return "Activo"
        else:
            return "Inactivo"

    @property
    def status_color(self):
        if self.is_active:
            return "green"
        else:
            return "gray"

    @property
    def url_edit(self):
        url = reverse_lazy(
            'core:warehouse-edit',
            kwargs={'pk': self.pk}
        )
        return url


class Udm(models.Model):

    key = models.CharField(
        verbose_name='Clave Unidad de Medida',
        max_length=10,
        blank=False,
        null=True,
    )
    name = models.CharField(
        verbose_name='nombre',
        max_length=100,
        blank=False,
        null=True,
    )
    type = models.TextField(
        verbose_name='Tipo',
        blank=True,
        null=True,
    )

    class Meta:
        verbose_name = 'unidad de medida'
        verbose_name_plural = 'unidades de medida'

    def __str__(self):
        return f'{self.key}: {self.name}'

    @property
    def url_edit(self):
        url = reverse_lazy(
            'core:udm-edit',
            kwargs={'pk': self.pk}
        )
        return url


class StockItem(AppModel):
    warehouse = models.ForeignKey(
        Warehouse,
        verbose_name='almacen',
        related_name='has_warehouse',
        on_delete=models.PROTECT,
        blank=False,
        null=True,
    )
    item = models.ForeignKey(
        Item,
        verbose_name='artículo',
        related_name='has_stockitem',
        on_delete=models.PROTECT,
        blank=False,
        null=True,
    )
    qty = models.DecimalField(
        verbose_name='cantidad',
        max_digits=8,
        decimal_places=2,
        default=0,
        validators=[MinValueValidator(Decimal('0'))],
        null=True,
        blank=False,
    )
    minimun_qty = models.DecimalField(
        verbose_name='minimo en almacen',
        max_digits=8,
        decimal_places=2,
        default=10,
        validators=[MinValueValidator(Decimal('0'))],
        null=True,
        blank=False,
    )
    udm = models.ForeignKey(
        Udm,
        verbose_name='udm',
        related_name='in_stock',
        on_delete=models.PROTECT,
        blank=True,
        null=True,
    )
    price = models.DecimalField(
        verbose_name='precio unitario',
        max_digits=12,
        decimal_places=2,
        blank=False,
        null=True,
    )
    sell_price = models.DecimalField(
        verbose_name='precio de venta',
        max_digits=12,
        decimal_places=2,
        blank=False,
        null=True,
    )

    class Meta:
        unique_together = (('warehouse', 'item'),)
        verbose_name = 'Existencia '
        verbose_name_plural = 'Existencias'

    @property
    def url_retrieve(self):
        url = reverse_lazy(
            'core:stockitem-retrieve',
            kwargs={'pk': self.pk}
        )
        return url

    @property
    def status_text(self):
        if self.qty > self.minimun_qty:
            return "Suficiente"
        elif self.qty <= self.minimun_qty and self.qty > 0:
            return "Minimo"
        elif self.qty <= 0:
            return "Desabastecido"

    @property
    def status_color(self):
        if self.qty > self.minimun_qty:
            return "green"
        elif self.qty <= self.minimun_qty and self.qty > 0:
            return "red"
        elif self.qty <= 0:
            return "gray"

    def __str__(self):
        return f'{self.item.name} - {self.warehouse}'


class Tax(AppModel):
    abbreviation = models.CharField(
        verbose_name='abreviación',
        # unique=True,
        max_length=3,
        blank=False,
        null=True,
    )
    factor = models.DecimalField(
        verbose_name='cantidad',
        max_digits=8,
        decimal_places=6,
        default=0,
        validators=[MinValueValidator(Decimal('0'))],
        null=True,
        blank=False
    )
    description = models.CharField(
        verbose_name='nombre',
        max_length=100,
        blank=False,
        null=True,
    )
    comments = models.TextField(
        verbose_name='comentarios',
        blank=True,
        null=True,
    )
    is_active = models.BooleanField(
        verbose_name='activo',
        blank=False,
        null=False,
        default=True,
    )

    class Meta:
        verbose_name = 'Impuesto'
        verbose_name_plural = 'Impuestos'

    def __str__(self):
        return f'{self.abbreviation}'
