# Python's Libraries

# DJango's Libraries
from django.urls import path

# Third-party Libraries

# Own's Libraries
from .views import SupplierListView
from .views import SupplierNewView
from .views import SupplierEditView

from .views import SupplierContactListView
from .views import SupplierContactNewView
from .views import SupplierContactEditView

from .views import ClientListView
from .views import ClientNewView
from .views import ClientEditView

from .views import ClientContactListView
from .views import ClientContactNewView
from .views import ClientContactEditView

from .views import ItemListView
from .views import ItemNewView
from .views import ItemEditView
from .views import ItemExportExcel

from .views import WarehouseListView
from .views import WarehouseNewView
from .views import WarehouseEditView
from .views import WarehouseExportExcel
from .views import WarehouseImportExcel
from .views import WarehouseEmptyImportView
from .views import ClientExportExcel
from .views import SupplierContactExportExcel
from .views import ClientContactExportExcel
from .views import StockItemExportExcel
from .views import ClientImportExcel
from .views import ClientEmptyImportView
from .views import ItemImportExcel
from .views import ItemEmptyImportView
from .views import UdmListView
from .views import UdmNewView
from .views import UdmEditView
from .views import SatProductListView
from .views import SatProductNewView
from .views import SatProductEditView
from .views import SupplierExportExcel
from .views import StockItemListView
from .views import StockItemRetrieveView
from .views import SupplierImportExcel
from .views import SupplierEmptyImportView
from .views import SupplierContactImportExcel
from .views import SupplierContactEmptyImportView
from .views import ClientContactImportExcel
from .views import ClientContactEmptyImportView


app_name = 'core'

urlpatterns = [
    path(
        'suppliers/',
        SupplierListView.as_view(),
        name='supplier-list'
    ),
    path(
        'suppliers/new/',
        SupplierNewView.as_view(),
        name='supplier-new'
    ),
    path(
        'suppliers/<int:pk>/edit/',
        SupplierEditView.as_view(),
        name='supplier-edit'
    ),
    path(
        'suppliers/<int:pk>/contactsuppliers/',
        SupplierContactListView.as_view(),
        name='contactsupplier-list'
    ),
    path(
        'suppliers/<int:pk>/contactsuppliers/new/',
        SupplierContactNewView.as_view(),
        name='contactsupplier-new'
    ),
    path(
        'suppliers/export/xlsx/',
        SupplierExportExcel.as_view(),
        name='supplier-export-xlsx'
    ),
    path(
        'suppliers/<int:pk>/suppliercontact/export/xlsx/',
        SupplierContactExportExcel.as_view(),
        name='suppliercontact-export-xlsx'
    ),
    path(
        'contactsuppliers/<int:pk>/edit/',
        SupplierContactEditView.as_view(),
        name='contactsupplier-edit'
    ),
    path(
        'suppliers/import/',
        SupplierImportExcel.as_view(),
        name='supplier-import'
    ),
    path(
        'suppliers/import/empty/',
        SupplierEmptyImportView.as_view(),
        name='supplier-import-empty'
    ),
    path(
        'suppliers/<int:pk>/contactsuppliers/import/',
        SupplierContactImportExcel.as_view(),
        name='contactsupplier-import'
    ),
    path(
        'contactsuppliers/import/empty/',
        SupplierContactEmptyImportView.as_view(),
        name='contactsupplier-import-empty'
    ),
    path(
        'clients/',
        ClientListView.as_view(),
        name='client-list'
    ),
    path(
        'clients/new/',
        ClientNewView.as_view(),
        name='client-new'
    ),
    path(
        'clients/import/',
        ClientImportExcel.as_view(),
        name='clients-import'
    ),
    path(
        'clients/import/empty/',
        ClientEmptyImportView.as_view(),
        name='clients-import-empty'
    ),
    path(
        'clients/<int:pk>/edit/',
        ClientEditView.as_view(),
        name='client-edit'
    ),
    path(
        'clients/<int:pk>/contactclients/',
        ClientContactListView.as_view(),
        name='contactclient-list'
    ),
    path(
        'clients/<int:pk>/contactclients/new/',
        ClientContactNewView.as_view(),
        name='contactclient-new'
    ),
    path(
        'contactclients/<int:pk>/edit/',
        ClientContactEditView.as_view(),
        name='contactclient-edit'
    ),
    path(
        'clients/<int:pk>/contactclients/import/',
        ClientContactImportExcel.as_view(),
        name='contactclient-import'
    ),
    path(
        'contactclients/import/empty/',
        ClientContactEmptyImportView.as_view(),
        name='contactclient-import-empty'
    ),
    path(
        'client/export/xlsx/',
        ClientExportExcel.as_view(),
        name='client-export-xlsx'
    ),
    path(
        'clients/<int:pk>/clientcontact/export/xlsx/',
        ClientContactExportExcel.as_view(),
        name='contactclient-export-xlsx'
    ),
    path(
        'items/',
        ItemListView.as_view(),
        name='item-list'
    ),
    path(
        'items/new/',
        ItemNewView.as_view(),
        name='item-new'
    ),
    path(
        'items/<int:pk>/edit/',
        ItemEditView.as_view(),
        name='item-edit'
    ),
    path(
        'items/xls',
        ItemExportExcel.as_view(),
        name='item-list-xls'
    ),
    path(
        'items/import/',
        ItemImportExcel.as_view(),
        name='items-import'
    ),
    path(
        'items/import/empty/',
        ItemEmptyImportView.as_view(),
        name='items-import-empty'
    ),
    path(
        'warehouses/',
        WarehouseListView.as_view(),
        name='warehouse-list'
    ),
    path(
        'warehouses/new/',
        WarehouseNewView.as_view(),
        name='warehouse-new'
    ),
    path(
        'warehouses/<int:pk>/edit/',
        WarehouseEditView.as_view(),
        name='warehouse-edit'
    ),
    path(
        'warehouses/xls',
        WarehouseExportExcel.as_view(),
        name='warehouse-list-xls'
    ),
    path(
        'warehouses/import/',
        WarehouseImportExcel.as_view(),
        name='warehouses-import'
    ),
    path(
        'warehouses/import/empty/',
        WarehouseEmptyImportView.as_view(),
        name='warehouses-import-empty'
    ),
    path(
        'udm/',
        UdmListView.as_view(),
        name='udm-list'
    ),
    path(
        'udm/new/',
        UdmNewView.as_view(),
        name='udm-new'
    ),
    path(
        'udm/<int:pk>/edit/',
        UdmEditView.as_view(),
        name='udm-edit'
    ),
    path(
        'satproduct/',
        SatProductListView.as_view(),
        name='satproduct-list'
    ),
    path(
        'satproduct/new/',
        SatProductNewView.as_view(),
        name='satproduct-new'
    ),
    path(
        'satproduct/<int:pk>/edit/',
        SatProductEditView.as_view(),
        name='satproduct-edit'
    ),
    path(
        'stockitems/',
        StockItemListView.as_view(),
        name='stockitem-list',
    ),
    path(
        'stockitems/export/xlsx/',
        StockItemExportExcel.as_view(),
        name='stockitems-export-xlsx'
    ),
    path(
        'stockitems/<int:pk>/retrieve/',
        StockItemRetrieveView.as_view(),
        name='stockitem-retrieve',
    ),
]
