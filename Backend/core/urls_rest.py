# Django's Libraries
from django.urls import path

# Own's Libraries
from .views_rest import ItemListAPIView
from .views_rest import WarehouseListAPIView
from .views_rest import WarehouseRetrieveAPIView
from .views_rest import StockItemListAPIView
from .views_rest import StockItemRetrieveAPIView
from .views_rest import TaxListAPIView
from .views_rest import TaxIsActiveListAPIView
from .views_rest import SupplierListAPIView
from .views_rest import SupplierIsActiveListAPIView
from .views_rest import ItemInWarehouseStockAPIView
from .views_rest import ItemInWarehouseStockExistAPIView
from .views_rest import ItemOutWarehouseStockAPIView
from .views_rest import ItemOutWarehouseStockIsActiveAPIView
from .views_rest import ClientListAPIView
from .views_rest import ClientDebtorListAPIView
from .views_rest import ClientIsActiveListAPIView
from .views_rest import ClientRetrieveAPIView
from .views_rest import ChartMostSelledItemListAPIView
from .views_rest import ChartMostInStockItemListAPIView
from .views_rest import ClientChartListAPIView
from .views_rest import WarehouseChartListAPIView
from .views_rest import WarehouseWithStockListAPIView
from .views_rest import WarehouseIsActiveListAPIView
from .views_rest import SatProductSearchAPI


app_name = 'core-api'


urlpatterns = [
    path(
        'items/',
        ItemListAPIView.as_view({
            'get': 'list',
        }),
        name='item-list',
    ),
    path(
        'items/mostselled/',
        ChartMostSelledItemListAPIView.as_view({
            'get': 'list',
        }),
        name='item-list',
    ),
    path(
        'items/mostinstock/',
        ChartMostInStockItemListAPIView.as_view({
            'get': 'list',
        }),
        name='item-list',
    ),
    path(
        'clients/',
        ClientListAPIView.as_view({
            'get': 'list',
        }),
        name='client-list',
    ),
    path(
        'clients/debtor/',
        ClientDebtorListAPIView.as_view({
            'get': 'list',
        }),
        name='client-debtor-list',
    ),
    path(
        'clients/active/',
        ClientIsActiveListAPIView.as_view({
            'get': 'list',
        }),
        name='client-active-list',
    ),
    path(
        'clients/<int:pk>/',
        ClientRetrieveAPIView.as_view({
            'get': 'retrieve',
        }),
        name='client-retrieve'
    ),
    path(
        'clients/charts/',
        ClientChartListAPIView.as_view({
            'get': 'list',
        }),
        name='client-chart-list',
    ),
    path(
        'warehouses/charts/',
        WarehouseChartListAPIView.as_view({
            'get': 'list',
        }),
        name='warehouse-chart-list',
    ),
    path(
        'warehouses/<int:pk>/items/stock/',
        ItemInWarehouseStockAPIView.as_view({
            'get': 'list',
        }),
        name='warehouse-item-stock',
    ),
    path(
        'warehouses/<int:pk>/items/stockexist/',
        ItemInWarehouseStockExistAPIView.as_view({
            'get': 'list',
        }),
        name='warehouse-item-stockexist',
    ),
    path(
        'warehouses/<int:pk>/items/outstock/',
        ItemOutWarehouseStockAPIView.as_view({
            'get': 'list',
        }),
        name='warehouse-item-outstock',
    ),
    path(
        'warehouses/<int:pk>/items/outstock/active/',
        ItemOutWarehouseStockIsActiveAPIView.as_view({
            'get': 'list',
        }),
        name='warehouse-item-outstock-active',
    ),
    path(
        'warehouses/',
        WarehouseListAPIView.as_view({
            'get': 'list',
        }),
        name='warehouse-list',
    ),
    path(
        'warehouses/active/',
        WarehouseIsActiveListAPIView.as_view({
            'get': 'list',
        }),
        name='warehouseactive-list',
    ),
    path(
        'warehouses/withstock/',
        WarehouseWithStockListAPIView.as_view({
            'get': 'list',
        }),
        name='warehousewithstock-list',
    ),    
    path(
        'suppliers/',
        SupplierListAPIView.as_view({
            'get': 'list',
        }),
        name='supplier-list',
    ),
    path(
        'suppliers/active/',
        SupplierIsActiveListAPIView.as_view({
            'get': 'list',
        }),
        name='supplieractive-list',
    ),
    path(
        'taxes/',
        TaxListAPIView.as_view({
            'get': 'list',
        }),
        name='tax-list',
    ),
    path(
        'taxes/active/',
        TaxIsActiveListAPIView.as_view({
            'get': 'list',
        }),
        name='taxactive-list',
    ),
    path(
        'warehouses/<int:pk>/',
        WarehouseRetrieveAPIView.as_view({
            'get': 'retrieve',
            'put': 'update',
         }),
        name='warehouse-retrieve',
    ),
    path(
        'stockitems/',
        StockItemListAPIView.as_view({
            'get': 'list',
        }),
        name='itemstock-list'
    ),
    path(
        'stockitems/<int:pk>/',
        StockItemRetrieveAPIView.as_view({
            'get': 'retrieve',
        }),
        name='stockitem-retrieve'
    ),
    path(
        'satproducts/search/',
        SatProductSearchAPI.as_view({
            'get': 'search',
        }),
        name='satproduct-search'
    ),
]
