# from django.forms import Form
from django.contrib import messages
from django.forms import CharField
from django.forms import EmailField
from django.forms import Field
from django.forms.widgets import Select

# from tools.forms import StxModelUpdateForm
from django.contrib.sites.shortcuts import get_current_site
from django.core.exceptions import ImproperlyConfigured

# Python's Libraries

# Django's Libraries

# Third-party Libraries

# Own's Libraries
from tools.forms import StxModelCreateForm

from .models import Supplier
from .models import SupplierContact
from .models import Client
from .models import ClientContact
from .models import Item
from .models import Warehouse
from .models import Udm
from .models import StockItem
from .models import SatProduct

from .business import SupplierBsn
from .business import ClientBsn
from .business import ItemBsn
from .business import WarehouseBsn
from .business import UdmBsn
from .business import StockItemBsn
from .business import SatProductBsn

from security.business import CompanyBsn


class SupplierForm(StxModelCreateForm):
    business_class = SupplierBsn

    class Meta:
        model = Supplier
        fields = [
            'company',
            'commercial_name',
            'legal_entity',
            'rfc',
            'sector',
            'email',
            'phone',
            'website',
            'bank',
            'account_number',
            'CLABE',
            'is_active',
            'settlement',
            'municipality',
            'street',
            'outdoor_number',
            'interior_number',
            'between_street_a',
            'between_street_b',
            'zip_code',
            'latitude',
            'longitude',
        ]

    def configurate_Fields(self):
        self.fields['company'].queryset = CompanyBsn.get_Assigned(self.user)


class SupplierContactForm(StxModelCreateForm):
    business_class = SupplierBsn

    class Meta:
        model = SupplierContact
        fields = [
            'name',
            'phone',
            'phone_ext',
            'email',
            'is_active',
        ]


class ClientForm(StxModelCreateForm):
    business_class = ClientBsn

    class Meta:
        model = Client
        fields = [
            'commercial_name',
            'legal_entity',
            'nationality',
            'rfc',
            'sector',
            'type',
            'company',
            'email',
            'phone',
            'website',
            'is_active',
            'credit_days',
            'credit_limit',
            'settlement',
            'municipality',
            'street',
            'outdoor_number',
            'interior_number',
            'between_street_a',
            'between_street_b',
            'zip_code',
            'latitude',
            'longitude',
        ]

    def configurate_Fields(self):
        self.fields['company'].queryset = CompanyBsn.get_Assigned(self.user)


class ClientContactForm(StxModelCreateForm):
    business_class = ClientBsn

    class Meta:
        model = ClientContact
        fields = [
            'name',
            'phone',
            'phone_ext',
            'email',
            'is_active',
        ]


class SatProductForm(StxModelCreateForm):
    business_class = SatProductBsn

    class Meta:
        model = SatProduct
        fields = [
            'key',
            'description',
        ]


class ItemForm(StxModelCreateForm):
    business_class = ItemBsn
    # sat_code_instance = CharField(required=False, label='Producto SAT', disabled=True)
    # sat_product = Field(required=False, widget=Select, label='Asignar Producto SAT')
    
    class Meta:
        model = Item
        fields = [
            'company',
            'primary_number',
            'sat_code',
            # 'sat_code_instance',
            'name',
            'description',
            'is_active',
            'upc',
            'sku',
            'image',
            'udm',
            'udp',
        ]

    def configurate_Fields(self):
        self.fields['company'].queryset = CompanyBsn.get_Assigned(self.user)


class WarehouseForm(StxModelCreateForm):
    business_class = WarehouseBsn

    class Meta:
        model = Warehouse
        fields = [
            'company',
            'name',
            'description',
            'is_active',
            'settlement',
            'municipality',
            'street',
            'outdoor_number',
            'interior_number',
            'between_street_a',
            'between_street_b',
            'zip_code',
            'latitude',
            'longitude',
        ]

    def configurate_Fields(self):
        self.fields['company'].queryset = CompanyBsn.get_Assigned(self.user)


class UdmForm(StxModelCreateForm):
    business_class = UdmBsn

    class Meta:
        model = Udm
        fields = [
            'type',
            'key',
            'name',
        ]


class StockItemForm(StxModelCreateForm):
    business_class = StockItemBsn

    class Meta:
        model = StockItem
        fields = [
            'warehouse',
            'item',
            'qty',
            'udm',
            'price',
            'sell_price',
        ]
