# Django's Libraries
from django.db import transaction
from django.contrib import messages
from django.urls import reverse_lazy
from django.core.exceptions import ImproperlyConfigured

# Own's Libraries
from tools.utils import Helper
from tools.utils import List


class StxBsn(object):
    key_word = 'id'
    model = None
    model_filter = None
    model_icon = None
    model_resource = None
    other_model_resource = None
    model_verbose_name_plural = None

    @classmethod
    def get_ModelVerboseNamePlural(self):
        if self.model_verbose_name_plural:
            return self.model_verbose_name_plural
        else:
            return self.get_ModelObject()._meta.verbose_name_plural.title()

    @classmethod
    def get_KeyField(self, record):
        key_field = getattr(record, self.key_word)
        return key_field

    @classmethod
    def get_ModelObject(self):
        model = self.model
        return model

    @classmethod
    def get_ModelName(self, plural=True):
        model = self.get_ModelObject()
        if plural:
            name = model._meta.verbose_name_plural.title()
        else:
            name = model._meta.verbose_name.title()
        return name

    @classmethod
    def get_ModelIcon(self):
        if self.model_icon:
            return self.model_icon
        else:
            raise ('El icono de modelo no ah sido asignado')

    @classmethod
    def get_All(self):
        records = self.get_ModelObject().objects.all().order_by('-pk')
        return records

    @classmethod
    def get(self, _value):
        record = self.get_ModelObject().objects.get(pk=_value)
        return record

    @classmethod
    def create(self, _data, _user, _files=None):
        try:
            with transaction.atomic():
                record = self.get_ModelObject()()
                for key, value in _data.items():
                    setattr(record, key, value)
                if _files:
                    for key, value in _files.items():
                        setattr(record, key, value)
                record.created_by = _user
                record.full_clean()
                record.save()
                return record
        except Exception as error:
            raise NameError(str(error))

    @classmethod
    def update(self, _instance, _data, _user, _files=None):
        try:
            with transaction.atomic():
                record = _instance
                for key, value in _data.items():
                    setattr(record, key, value)
                if _files:
                    for key, value in _files.items():
                        setattr(record, key, value)
                record.updated_by = _user
                record.full_clean()
                record.save()
                return record
        except Exception as error:
            raise NameError(str(error))

    @classmethod
    def set_SaveMessage(self, request):
        model_name = self.get_ModelObject()._meta.verbose_name
        word_flag = 'from_save'
        if word_flag in request.GET:
            text = "{}: '{}' se guardo exitosamente."
            messages.success(
                request,
                text.format(
                    model_name,
                    request.GET[word_flag].upper()
                )
            )

    @classmethod
    def get_LastRecord(self):
        try:
            record = self.get_ModelObject().objects.latest('pk')
            return record
        except self.get_ModelObject().DoesNotExist:
            return None
        except Exception as error:
            raise NameError(str(error))

    @classmethod
    def get_UrlBack(self):
        app_label = self.get_ModelObject()._meta.app_label
        model_name = self.get_ModelObject()._meta.model_name
        url = reverse_lazy('{}:{}-list'.format(app_label, model_name))
        return url

    @classmethod
    def get_UrlList(self):
        app_label = self.get_ModelObject()._meta.app_label
        model_name = self.get_ModelObject()._meta.model_name
        url = reverse_lazy('{}:{}-list'.format(app_label, model_name))
        return url

    @classmethod
    def get_UrlNew(self):
        app_label = self.get_ModelObject()._meta.app_label
        model_name = self.get_ModelObject()._meta.model_name
        url = reverse_lazy('{}:{}-new'.format(app_label, model_name))
        return url

    @classmethod
    def get_UrlOnSave(self, request, record):
        app_label = self.get_ModelObject()._meta.app_label
        model_name = self.get_ModelObject()._meta.model_name
        url = ""
        if 'savecreate' in request.POST:
            url = Helper.get_UrlWithQueryString(
                reverse_lazy('{}:{}-new'.format(app_label, model_name)),
                from_save=self.get_KeyField(record)
            )

        if 'save' in request.POST:
            url = Helper.get_UrlWithQueryString(
                reverse_lazy('{}:{}-list'.format(app_label, model_name)),
                from_save=self.get_KeyField(record)
            )

        return url

    @classmethod
    def get_Filter(self):
        model_filter = self.model_filter
        return model_filter

    @classmethod
    def filter_ByPermission(self, records, user):
        return records.filter(company__in=user.companies.all())

    @classmethod
    def get_Filtered(self, request, method_records=None):        
        search_value = request.GET.get('search_input', "")
        if method_records:
            records = getattr(self, method_records)(
                request=request
            )()
        else:
            records = self.get_All()

        if search_value:
            filtered_records = self.search(records, search_value)
            filtered_records = self.filter_ByPermission(filtered_records, request.user)
            filters = self.get_Filter()(request.GET, queryset=filtered_records, request=request)
        else:
            records = self.filter_ByPermission(records, request.user)
            filters = self.get_Filter()(request.GET, queryset=records, request=request)
            filtered_records = filters.qs
        return filtered_records, filters, search_value

    @classmethod
    def get_FilteredRelatedField(self, request, pk, related_field, search_method):
        search_value = request.GET.get('search_input', "")

        if search_value:
            filtered_records = self.search_AddressByName(pk, )

    @classmethod
    def get_RelatedFieldList(self, request, pk, related_field):
        instance = self.get_ModelObject().objects.get(pk=pk)
        records = getattr(instance, related_field).all().order_by('-pk')
        return instance, records

    @classmethod
    def empty_Export(self, _model_resource_method=None):
        if _model_resource_method:
            resource_method = getattr(self, _model_resource_method)
            resource = resource_method()
        else:
            resource = self.model_resource()
        dataset = resource.export(
            self.get_ModelObject().objects.none()
        )
        return dataset    

    @classmethod
    def get_ModelResource(self, _model_resource_method=None):
        if _model_resource_method:
            resource = getattr(self, _model_resource_method)
            return resource
        elif self.model_resource:
            return self.model_resource
        else:
            raise ImproperlyConfigured('Las reglas de negocio no cuentan con Resource.')

    @classmethod
    def validate_Access(self, _user, permissions, company=None,):
        if _user.is_superuser is False:
            if company is None:
                has_assignments = _user.has_assignments
            else:
                has_assignments = _user.has_assignments.filter(company__id=company)
            user_permissions = []
            for assignement in has_assignments:
                for permission in assignement.position.permissions.all():
                    user_permissions.append(permission.code)
            approved = False
            for user_permission in user_permissions:
                if user_permission in permissions:
                    approved = True
        else:
            approved = True
        return approved

    # TODO REPLACE THAT
    @classmethod
    def search(self, records, _value):
        # records = records.filter(field=_value)
        # return records
        raise NameError('Redefinir el metodo search en las reglas de negocio')
