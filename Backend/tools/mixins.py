# -*- coding: utf-8 -*-

# Django's Libraries
from django.core.exceptions import PermissionDenied
from django.urls import reverse
from django.shortcuts import redirect
from django.conf import settings
# from security.models import User


class GroupLoginRequiredMixin(object):
    """
    View mixin which verifies that the user has authenticated.
    If group is specified by the parent class,
    then user must be member of that group (as set in django admin)
    """

    def dispatch(self, request, *args, **kwargs):
        if request.user.is_authenticated:
            user = request.user

            # if parent class has group attribute
            if user.is_superuser is False:
                self.validate_Groups(user)
        else:
            return redirect(reverse(settings.LOGIN_URL))

        return super(GroupLoginRequiredMixin, self).dispatch(
            request, *args, **kwargs
        )

    def validate_Groups(self, _user):
        if hasattr(self, 'group'):
            grps = _user.groups.filter(name__in=self.groups)
            if not grps:
                raise PermissionDenied(
                    "No se tienen los permisos para entrar a esta sección")


class StxSecurityMixin(object):
    """
    View mixin which verifies that the user has authenticated.
    If s is specified by the parent class,
    then user must be member of that group (as set in django admin)
    """
    def dispatch(self, request, *args, **kwargs):
        if self.login_required:
            user = request.user
            if user.is_authenticated:
                if user.is_superuser is False:
                    self.validate_Permissions(user)
            else:
                return redirect(reverse(settings.LOGIN_URL))

        return super(StxSecurityMixin, self).dispatch(
            request, *args, **kwargs
        )

    def validate_Permissions(self, _user):
        if hasattr(_user, "has_assignments"):            
            has_assignments = _user.has_assignments
            user_permissions = []
            for assignement in has_assignments.all():
                for permission in assignement.position.permissions.all():
                    user_permissions.append(permission.code)
            if self.permissions:
                approved = False
                for user_permission in user_permissions:
                    if user_permission in self.permissions:                
                        approved = True
                if approved is False:                        
                    raise PermissionDenied(
                        "No se tienen los permisos para entrar a esta sección"
                    )
        else:
            raise PermissionDenied(
                "No se han configurado los permisos"
            )

    def validate_Groups(self, _user):
        if hasattr(self, 'groups'):
            grps = _user.groups.filter(name__in=self.groups)
            if not grps:
                raise PermissionDenied(
                    "No se tienen los permisos para entrar a esta sección")
