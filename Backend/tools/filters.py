
# Third-party Libraries
from django_filters import FilterSet


class StxFilter(FilterSet):

    def filter_BetweenDates(self, queryset, name, value):
        filter_init = "{}__gte".format(name)
        filter_end = "{}__lte".format(name)

        if value.start:
            if value.stop:
                query = queryset.filter(**{
                    filter_init: value.start,
                    filter_end: value.stop
                })
            else:
                query = queryset.filter(**{
                    filter_init: value.start
                })
        elif value.stop:
                query = queryset.filter(**{
                    filter_end: value.stop
                })
        else:
            query = queryset

        return query
