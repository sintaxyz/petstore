
# Django's Libraries
from django.forms import ModelForm
from django.forms import Form
from django.core.exceptions import ImproperlyConfigured


class StxModelForm(ModelForm):

    def __init__(self, *args, **kwargs):
        self.user = kwargs.pop('user')
        super(StxModelForm, self).__init__(*args, **kwargs)
        self.configurate_Fields()

    def configurate_Fields(self):
        return None


class StxForm(Form):

    def __init__(self, *args, **kwargs):
        self.user = kwargs.pop('user')
        super(StxModelForm, self).__init__(*args, **kwargs)
        self.configurate_Fields()

    def configurate_Fields(self):
        return None


class StxModelCreateForm(StxModelForm):
    business_class = None

    def save(self, request):
        if self.business_class is not None:
            data = self.cleaned_data

            if request.FILES:
                record = self.business_class.create(
                    data,
                    self.user,
                    request.FILES
                )
            else:
                record = self.business_class.create(
                    data,
                    self.user
                )
            url = self.business_class.get_UrlOnSave(
                request,
                record
            )
            return url

        else:
            raise ImproperlyConfigured(
                "Favor de especificar una clase de negocio")

    def update(self, request):
        if self.business_class is not None:
            data = self.cleaned_data
            if request.FILES:
                self.business_class.update(
                    self.instance,
                    data,
                    self.user,
                    request.FILES
                )
            else:
                self.business_class.update(
                    self.instance,
                    data,
                    self.user
                )
            url = self.business_class.get_UrlOnSave(
                request,
                self.instance
            )
            return url

        else:
            raise ImproperlyConfigured(
                "Favor de especificar una clase de negocio")


class StxModelUpdateForm(StxModelForm):
    business_clas = None

    def save(self, request):
        if self.business_class is not None:
            data = self.cleaned_data
            self.business_class.update(
                self.instance,
                data,
                self.user
            )
            url = self.business_class.get_UrlOnSave(
                request,
                self.instance
            )
            return url

        else:
            raise ImproperlyConfigured(
                "Favor de especificar una clase de negocio")
