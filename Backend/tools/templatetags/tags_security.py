# Django's Libraries
from django import template
from django.contrib.auth.models import Group

register = template.Library()


@register.inclusion_tag(
    'tags_security/menu_user.html',
    takes_context=True
)
def tag_menu_user(context):
    groups = context['USER']['groups']
    permissions = context['USER']['permissions']
    is_superuser = context['USER']['is_superuser']
    pk = context['USER']['pk']
    context = {
        'groups': groups,
        'permissions': permissions,
        'is_superuser': is_superuser,
        'pk': pk
    }
    return context


@register.inclusion_tag(
    'tags_security/menu_main.html',
    takes_context=True
)
def tag_menu_main(context):
    groups = context['USER']['groups']
    permissions = context['USER']['permissions']
    is_superuser = context['USER']['is_superuser']
    pk = context['USER']['pk']
    context = {
        'groups': groups,
        'permissions': permissions,
        'is_superuser': is_superuser,
        'pk': pk
    }
    return context


@register.filter(name='check_has_group')
def check_has_group(user, group_name):
    """Validate is a user is in a group."""
    group = Group.objects.get(name=group_name)
    return group in user.groups.all()
