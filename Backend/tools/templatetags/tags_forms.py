from django import template

register = template.Library()


@register.inclusion_tag('tags_forms/errors.html', takes_context=False)
def tag_form_errors(_form):
    context = {
        'form': _form,
    }
    return context


@register.inclusion_tag('tags_forms/card_form.html', takes_context=False)
def tag_card_form(_form):
    context = {
        'form': _form,
    }
    return context


@register.inclusion_tag('tags_forms/card_form_controls.html', takes_context=False)
def tag_card_form_controls(_only_save=False):

    only_save = False
    if _only_save:
        only_save = _only_save

    context = {
        'only_save': only_save,
    }
    return context


@register.inclusion_tag('tags_forms/card_image.html', takes_context=False)
def tag_card_image(_form_field, _object):

    context = {
        'form_field': _form_field,
        'object': _object
    }
    return context
