# Django's Libraries
from django import template
from django.urls import reverse_lazy

# Third-party Libraries
from django_filters.fields import DateRangeField

# Own's Libraries
from tools.utils import Value

register = template.Library()


@register.inclusion_tag(
    'tags_lists/list_empty.html',
    takes_context=False
)
def tag_list_empty(text=""):
    return {
        'text': text
    }


@register.inclusion_tag(
    'tags_lists/tabs_list.html',
    takes_context=False
)
def tag_tabs_list(_is_filtered, _url):
    show_search_tab = _is_filtered
    all_class = "active"
    search_class = ""
    if show_search_tab:
        all_class = ""
        search_class = "active"

    context = {
        'show_search_tab': show_search_tab,
        'all_class': all_class,
        'search_class': search_class,
        'url': _url
    }

    return context


@register.inclusion_tag(
    'tags_lists/filters_values.html',
    takes_context=True
)
def tag_filters_values(context):
    filters_str = []
    filters = context.get('filters', None)
    for field in filters.form.fields:
        field_value = context.request.GET.get(field, "")
        if field_value and field_value != 'unknown':
            filters_str.append(
                f"{filters.form.fields.get(field).label}: {context.request.GET.get(field)}"
            )

    context = {
        'filters_str': filters_str
    }
    return context


@register.inclusion_tag(
    'tags_lists/pagination_bar.html',
    takes_context=True
)
def tag_pagination_bar(context):

    start_value = "?page=1"
    previous_value = "?page="
    next_value = "?page="
    last_value = "?page="

    records = context.get('records')
    has_other_pages = records.has_other_pages()
    has_previous = records.has_previous()
    has_next = records.has_next()
    current = "Pag. {} de {}."

    filters = context.get('filters', None)
    filter_fast = context.get('search_value', None)
    filters_values = ""
    is_filtered = False

    if filters:
        for field in filters.form.fields:
            field_obj = filters.form.fields.get(field)
            if isinstance(field_obj, DateRangeField):
                field_min = field + "_min"
                field_max = field + "_max"
                if context.request.GET.get(field_min, ""):
                    filters_values += "&" + field_min + "="
                    filters_values += context.request.GET.get(field_min, "")

                if context.request.GET.get(field_max, ""):
                    filters_values += "&" + field_max + "="
                    filters_values += context.request.GET.get(field_max, "")

            else:
                if context.request.GET.get(field, ""):
                    filters_values += "&" + field + "="
                    filters_values += context.request.GET.get(field, "")

    if filter_fast:
        filters_values += "&search_input=" + filter_fast

    if filters_values:
        is_filtered = True

    if has_other_pages:
        current = current.format(
            records.number,
            records.paginator.num_pages
        )

        if has_previous:
            start_value += filters_values
            previous_value += str(records.previous_page_number()) \
                + filters_values

        if has_next:
            next_value += str(records.next_page_number()) + filters_values
            last_value += str(records.paginator.num_pages) + filters_values

    context = {
        'start_value': start_value,
        'previous_value': previous_value,
        'next_value': next_value,
        'last_value': last_value,
        'records': records,
        'has_other_pages': has_other_pages,
        'has_previous': has_previous,
        'has_next': has_next,
        'current': current,
        'is_filtered': is_filtered
    }
    return context


@register.inclusion_tag(
    'tags_lists/card_header_item.html',
    takes_context=False
)
def tag_card_header_item(_label, _value, _class="", _icon=""):
    value = Value.get_Formated(_value)

    icon = ""
    if _icon:
        icon = "<i class='{}'></i>".format(_icon)

    clase = "mark"
    if _class:
        clase = "mark color-{}".format(_class)

    context = {
        'label': _label,
        'value': value,
        'clase': clase,
        'icon': icon
    }
    return context


@register.inclusion_tag(
    'tags_lists/card_body_item_text.html',
    takes_context=False
)
def tag_card_body_item_text(_label, _value, _url="", _class="",
                            _size="", _icon=""):
    size = ""
    if _size:
        size = f"card-body-item--{_size}"

    icon = ""
    if _icon:
        icon = "<i class='{}'></i>".format(_icon)

    if _value is None:
        _value = 'No definido'

    context = {
        'label': _label,
        'value': _value,
        'url': _url,
        'class': _class,
        'size': size,
        'icon': icon
    }
    return context


@register.inclusion_tag(
    'tags_lists/card_body_item_date.html',
    takes_context=False
)
def tag_card_body_item_date(_label, _value, _url="", _class="",
                            _size="", _icon=""):

    value = Value.get_Formated(_value)

    size = ""
    if _size:
        size = f"card-body-item--{_size}"

    icon = ""
    if _icon:
        icon = "<i class='{}'></i>".format(_icon)

    context = {
        'label': _label,
        'value': value,
        'url': _url,
        'class': _class,
        'size': size,
        'icon': icon
    }
    return context


@register.inclusion_tag(
    'tags_lists/card_body_item_many.html',
    takes_context=False
)
def tag_card_body_item_many(_label, _values, _url="", _class="", _size=""):

    size = ""
    if _size:
        size = f"card-body-item--{_size}"

    context = {
        'label': _label,
        'values': _values,
        'url': _url,
        'class': _class,
        'size': size
    }
    return context


@register.inclusion_tag(
    'tags_lists/card_body_item_currency.html',
    takes_context=False
)
def tag_card_body_item_currency(_label, _value, _simbol="$", _url="", _class="",
                                _size=""):
    size = ""
    if _size:
        size = f"card-body-item--{_size}"

    context = {
        'label': _label,
        'value': _value,
        'simbol': _simbol,
        'url': _url,
        'class': _class,
        'size': size
    }
    return context


@register.inclusion_tag(
    'tags_lists/card_body_item_bool.html',
    takes_context=False
)
def tag_card_body_item_bool(_label, _value, _url="", _class="", _icon=""):
    value = ""
    if _value:
        value = "Si"
    else:
        value = "No"

    icon = ""
    if _icon:
        icon = "<i class='{}'></i>".format(_icon)

    context = {
        'label': _label,
        'value': value,
        'url': _url,
        'class': _class,
        'icon': icon
    }
    return context


@register.inclusion_tag(
    'tags_lists/card_body_item_icon.html',
    takes_context=False
)
def tag_card_body_item_icon(_label, _value, _url="", _class=""):
    value = ""
    url = ""
    if _url:
        value = "fas {}".format(_value)
        url = _url

    context = {
        'label': _label,
        'value': value,
        'url': url,
        'class': _class
    }
    return context


@register.inclusion_tag(
    'tags_lists/card_body_actions.html',
    takes_context=True
)
def tag_card_body_actions(context, _url="", _tag="", _icon="", _path="", _access="", _new_tab=False):
    groups = context['USER']['groups']
    permissions = context['USER']['permissions']
    is_superuser = context['USER']['is_superuser']
    pk = context['USER']['pk']

    url = ""
    if _path:
        domain = "http://{}/{}".format(
            context.request.get_host(),
            _path
        )
        url = domain

    if _url:
        url = _url

    icon = "fa-pen"
    if _icon:
        icon = _icon

    tag = "Editar"
    if _tag:
        tag = _tag

    access = None
    if _access:
        access = _access

    new_tab = None
    if _new_tab is True:
        new_tab = 'target="_blank"'

    context = {
        'url': url,
        'tag': tag,
        'icon': icon,
        'access': access,
        'new_tab': new_tab,
        'groups': groups,
        'permissions': permissions,
        'is_superuser': is_superuser,
        'pk': pk
    }
    return context


@register.inclusion_tag(
    'tags_lists/toolbar_button.html',
    takes_context=True
)
def tag_toolbar_button(context, _url="", _pk="", _access="", _tag="", _icon=""):
    groups = context['USER']['groups']
    permissions = context['USER']['permissions']
    is_superuser = context['USER']['is_superuser']
    pk = context['USER']['pk']

    url = "#"
    if _url:
        if _pk:
            url = reverse_lazy(
                _url,
                kwargs={'pk': _pk}
            )
        else:
            url = reverse_lazy(
                _url,
            )

    tag = 'Exportar'
    if _tag:
        tag = _tag

    icon = "fas fa-arrow-alt-circle-up"
    if _icon:
        icon = _icon

    access = None
    if _access:
        access = _access

    context = {
        'url': url,
        'tag': tag,
        'icon': icon,
        'access': access,
        'groups': groups,
        'permissions': permissions,
        'is_superuser': is_superuser,
        'pk': pk,
    }
    return context


@register.inclusion_tag(
    'tags_lists/action_button.html',
    takes_context=True
)
def tag_action_button(context, _url, _tag="", _access="", _new_tab=False):
    groups = context['USER']['groups']
    permissions = context['USER']['permissions']
    is_superuser = context['USER']['is_superuser']
    pk = context['USER']['pk']

    url = _url

    tag = "Agregar"
    if _tag:
        tag = _tag

    access = None
    if _access:
        access = _access

    new_tab = None
    if _new_tab is True:
        new_tab = 'target="_blank"'

    context = {
        'url': url,
        'tag': tag,
        'access': access,
        'new_tab': new_tab,
        'groups': groups,
        'permissions': permissions,
        'is_superuser': is_superuser,
        'pk': pk
    }
    return context


@register.inclusion_tag(
    'tags_lists/search_bar.html',
    takes_context=False
)
def tag_search_bar(_search_value):
    context = {
        'search_value': _search_value
    }
    return context


@register.inclusion_tag(
    'tags_lists/timeline.html',
    takes_context=False
)
def tag_timeline(_records):
    context = {
        'records': _records
    }
    return context
