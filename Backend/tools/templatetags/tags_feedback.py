# Django's Libraries
from django import template
from django.urls import reverse_lazy

# Own's Libraries
from tools.utils import Value
from datetime import date


register = template.Library()


@register.inclusion_tag(
    'tags_feedback/import_messages.html',
    takes_context=False
)
def tag_import_messages(_import_errors):
    context = {
        'import_errors': _import_errors,
    }
    return context


@register.inclusion_tag(
    'tags_feedback/bar_navigation.html',
    takes_context=False
)
def tag_bar_navigation(url, label, record_nav=False):
    context = {
        'url': url,
        'label': label,
        'record_nav': record_nav
    }
    return context


@register.inclusion_tag(
    'tags_feedback/nav_bar_button.html',
    takes_context=True
)
def tag_nav_bar_button(context, _url="", _pk="", _tag="", _icon="", _access="", _active=False):
    groups = context['USER']['groups']
    permissions = context['USER']['permissions']
    is_superuser = context['USER']['is_superuser']
    pk = context['USER']['pk']

    url = "#"
    if _url:
        if _pk:
            url = reverse_lazy(
                _url,
                kwargs={'pk': _pk}
            )
        else:
            url = reverse_lazy(
                _url,
            )

    tag = None
    if _tag:
        tag = _tag

    icon = None
    if _icon:
        icon = _icon
    
    access = None
    if _access:
        access = _access

    active = None
    if _active is True:
        active = 'active'

    context = {
        'url': url,
        'tag': tag,
        'icon': icon,
        'access': access,
        'active': active,
        'groups': groups,
        'permissions': permissions,
        'is_superuser': is_superuser,
        'pk': pk,
    }
    return context


@register.inclusion_tag(
    'tags_feedback/card_aside_image.html',
    takes_context=False
)
def tag_card_aside_image(_object):
    context = {
        'object': _object
    }
    return context


@register.inclusion_tag(
    'tags_feedback/card_aside_map.html',
    takes_context=False
)
def tag_aside_map(_instance, _header=None):
    if _instance.latitude and _instance.longitude:
        location = True
    if not _header:
        _header = 'Localizacion'
    context = {
        'location': location,
        'header': _header,
    }
    return context


@register.inclusion_tag(
    'tags_feedback/messages.html',
    takes_context=False
)
def tag_messages(_messages):
    context = {
        'messages': _messages,
    }
    return context


@register.inclusion_tag(
    'tags_feedback/card_aside_audit.html',
    takes_context=False
)
def tag_card_aside_audit(_record, id_field=None):
    has_history = False
    if hasattr(_record, "url_history"):
        has_history = True

    record = None

    if not id_field:
        if _record.id:
            record = _record
    else:
        if hasattr(_record, id_field):
            record = _record

    context = {
        'record': record,
        'has_history': has_history
    }
    return context


@register.inclusion_tag(
    'tags_feedback/card_aside_body_empty.html',
    takes_context=False
)
def tag_card_aside_body_empty(_value):
    context = {
        'value': _value
    }
    return context


@register.inclusion_tag(
    'tags_feedback/card_aside_body_item_currency.html',
    takes_context=False
)
def tag_card_aside_body_item_currency(_label, _value, _url="", _class="",
                                      _icon="", _sign=""):
    icon = ""
    if _icon:
        icon = "<i class='{}'></i>".format(_icon)

    context = {
        'label': _label,
        'value': _value,
        'url': _url,
        'class': _class,
        'icon': icon,
        'simbol': _sign
    }
    return context


@register.inclusion_tag(
    'tags_feedback/card_aside_action_whatsapp.html',
    takes_context=False
)
def tag_card_aside_action_whatsapp(request, id):
    scheme = request.scheme
    host = request.get_host()
    url = '{}://{}/goods/estates/{}/detail/'.format(
        scheme, host, id
    )

    context = {
        'url_share': url,
    }
    return context


@register.inclusion_tag(
    'tags_feedback/card_aside_action_download.html',
    takes_context=False
)
def tag_card_aside_download():
    return {}


@register.inclusion_tag(
    'tags_feedback/card_aside_body_item_text.html',
    takes_context=False
)
def tag_card_aside_body_item_text(_label, _value, _url="", _class="",
                                  _icon=""):
    icon = ""
    if _icon:
        icon = "<i class='{}'></i>".format(_icon)

    context = {
        'label': _label,
        'value': _value,
        'url': _url,
        'class': _class,
        'icon': icon
    }
    return context


@register.inclusion_tag(
    'tags_feedback/card_aside_body_item_date.html',
    takes_context=False
)
def tag_card_aside_body_item_date(_label, _value, _url="", _class=""):
    value = Value.get_Formated(_value)

    context = {
        'label': _label,
        'value': value,
        'url': _url,
        'class': _class
    }
    return context


@register.inclusion_tag(
    'tags_feedback/card_aside_action_button.html',
    takes_context=True
)
def tag_card_aside_action_button(context, _url="", _pk="", _tag="", _icon="", _access="", _button="", _new_tab=False):
    groups = context['USER']['groups']
    permissions = context['USER']['permissions']
    is_superuser = context['USER']['is_superuser']
    pk = context['USER']['pk']

    url = "#"
    if _url:
        if _pk:
            url = reverse_lazy(
                _url,
                kwargs={'pk': _pk}
            )
        else:
            url = reverse_lazy(
                _url,
            )

    tag = None
    if _tag:
        tag = _tag

    icon = None
    if _icon:
        icon = _icon
    
    access = None
    if _access:
        access = _access

    button = None
    if _button:
        button = _button

    new_tab = None
    if _new_tab is True:
        new_tab = 'target="_blank"'

    context = {
        'url': url,
        'tag': tag,
        'icon': icon,
        'access': access,
        'button': button,
        'new_tab': new_tab,
        'groups': groups,
        'permissions': permissions,
        'is_superuser': is_superuser,
        'pk': pk,
    }
    return context


@register.inclusion_tag(
    'tags_feedback/card_info_body_item_currency.html',
    takes_context=False
)
def tag_card_info_body_item_currency(_label, _value, _simbol="$", _url="",
                                     _class="", _size=""):
    size = ""
    if _size:
        size = "item--{}".format(
            _size
        )

    context = {
        'label': _label,
        'value': _value,
        'simbol': _simbol,
        'url': _url,
        'class': _class,
        'size': size
    }
    return context


@register.inclusion_tag(
    'tags_feedback/card_info_body_item_date.html',
    takes_context=False
)
def tag_card_info_body_item_date(_label, _value, _url="",
                                 _class="", _size=""):
    size = ""
    if _size:
        size = "item--{}".format(
            _size
        )

    context = {
        'label': _label,
        'value': _value,
        'url': _url,
        'class': _class,
        'size': size
    }
    return context


@register.inclusion_tag(
    'tags_feedback/card_info_body_item_text.html',
    takes_context=False
)
def tag_card_info_body_item_text(_label, _value, _url="",
                                 _class="", _size="", _icon=""):
    icon = ""
    if _icon:
        icon = "<i class='{}'></i>".format(_icon)

    size = ""
    if _size:
        size = "item--{}".format(
            _size
        )

    if _value is None:
        _value = 'No definido'

    context = {
        'label': _label,
        'value': str(_value),
        'url': _url,
        'class': _class,
        'size': size,
        'icon': icon
    }
    return context


@register.inclusion_tag(
    'tags_feedback/card_info_body_item_bool.html',
    takes_context=False
)
def tag_card_info_body_item_bool(_label, _value, _url="",
                                 _class="", _size="", _icon=""):
    icon = ""
    if _icon:
        icon = "<i class='{}'></i>".format(_icon)

    size = ""
    if _size:
        size = "item--{}".format(
            _size
        )

    value = ""
    if _value:
        value = "Si"
    else:
        value = "No"

    context = {
        'label': _label,
        'value': value,
        'url': _url,
        'class': _class,
        'size': size,
        'icon': icon
    }
    return context


@register.inclusion_tag(
    'tags_feedback/card_info_body_item_age.html',
    takes_context=False
)
def tag_card_info_body_item_age(_label, _record, _url="",
                                _class="", _size="", _icon=""):
    icon = ""
    if _icon:
        icon = "<i class='{}'></i>".format(_icon)

    size = ""
    if _size:
        size = "item--{}".format(
            _size
        )
    expedition = _record.expedition_date
    born = _record.client.birthdate

    age = expedition.year - born.year
    context = {
        'label': _label,
        'record': _record,
        'url': _url,
        'class': _class,
        'size': size,
        'icon': icon,
        'age': age
    }
    return context
