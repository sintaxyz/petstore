# Django's Libraries
from django import template

register = template.Library()


@register.filter
def get_WidgetName(field):
    clase = field.field.widget.__class__.__name__
    return clase


@register.filter
def get_FilterClass(filter):
    clase = filter.__class__.__name__
    return clase


@register.inclusion_tag('tags_fields/field_text.html', takes_context=False)
def tag_field_text(_form_field, _size=""):
    size = ""
    if _size:
        size = "field--{}".format(_size)

    context = {
        'form_field': _form_field,
        'size': size
    }
    return context


@register.inclusion_tag('tags_fields/field_qty.html', takes_context=False)
def tag_field_qty(_form_field, _size=""):
    size = ""
    if _size:
        size = "field--{}".format(_size)

    context = {
        'form_field': _form_field,
        'size': size
    }
    return context


@register.inclusion_tag('tags_fields/field_select.html', takes_context=False)
def tag_field_select(_form_field, _size="", _has_button=False):
    size = ""
    if _size:
        size = "field--{}".format(_size)

    context = {
        'form_field': _form_field,
        'has_button': _has_button,
        'size': size
    }
    return context


@register.inclusion_tag('tags_fields/field_checkbox.html', takes_context=False)
def tag_field_checkbox(_form_field, _size=""):
    size = ""
    if _size:
        size = "field--{}".format(_size)

    context = {
        'form_field': _form_field,
        'size': size
    }
    return context


@register.inclusion_tag('tags_fields/field_file.html', takes_context=False)
def tag_field_file(_form_field, _size=""):
    size = ""
    if _size:
        size = "field--{}".format(_size)

    context = {
        'form_field': _form_field,
        'size': size
    }
    return context


@register.inclusion_tag('tags_fields/field_textarea.html', takes_context=False)
def tag_field_textarea(_form_field, _size=""):
    size = ""
    if _size:
        size = "field--{}".format(_size)

    context = {
        'form_field': _form_field,
        'size': size
    }
    return context
