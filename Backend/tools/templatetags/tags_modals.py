# Django's Libraries
from django import template

register = template.Library()


@register.inclusion_tag(
    'tags_modals/modal_filters.html',
    takes_context=False
)
def tag_modal_filters(_form, _url):
    context = {
        'form': _form,
        'url': _url
    }
    return context
