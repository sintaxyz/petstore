import pusher


class PusherProvider(object):
    app_id = '798901'
    key = '17193be520ed37ae22a1'
    secret = '7aa359f16cdc60f6923c'
    cluster = 'us2'
    ssl = True

    @classmethod
    def send(self, channel, event, message):
        client = pusher.Pusher(
            app_id=self.app_id,
            key=self.key,
            secret=self.secret,
            cluster=self.cluster,
            ssl=self.ssl
        )
        client.trigger(
            channel,
            event,
            message
        )
