# Python's Libraries
from math import sin, cos, sqrt, atan2, radians
import os
import urllib
import pytz
from io import BytesIO
from datetime import datetime
from datetime import date
from datetime import timedelta
from math import floor
from math import ceil

# Django's Libraries
from django.db.models import Q
from django.apps import apps
from django.core.paginator import Paginator
from django.core.paginator import EmptyPage
from django.core.paginator import PageNotAnInteger
from django.http import HttpResponse
from django.template.loader import get_template

# Third-party Libraries
import xhtml2pdf.pisa as pisa
# from dateutil.relativedelta import relativedelta


class Value(object):

    @classmethod
    def get_Formated(self, _value):
        value = ""
        if _value:
            if isinstance(_value, datetime):
                value = _value.strftime("%d/%m/%y %H:%M")
            elif isinstance(_value, date):
                value = _value.strftime("%d/%m/%y")
            else:
                value = _value

        return value


class Geolocation(object):

    @classmethod
    def get_DistanceBetweemPoints(self, lat1, lon1, lat2, lon2):
        """
        Calculate distance between two points.
        """
        # approximate radius of earth in km
        R = 6373.0

        dlon = lon2 - lon1
        dlat = lat2 - lat1

        a = sin(dlat / 2)**2 + cos(lat1) * cos(lat2) * sin(dlon / 2)**2
        c = 2 * atan2(sqrt(a), sqrt(1 - a))

        distance = R * c
        print("Result:", distance)


class List(object):

    @classmethod
    def get_Pagination(self, _queryset, _page, _num_pages=10):
        paginator = Paginator(_queryset, _num_pages)
        try:
            records = paginator.page(_page)
        except PageNotAnInteger:
            records = paginator.page(1)
        except EmptyPage:
            records = paginator.page(paginator.num_pages)
        return records

    @classmethod
    def filter_ByPermission(self, _queryset, _user):
        return _queryset


class Helper(object):

    @classmethod
    def get_UrlWithQueryString(self, _path, **kwargs):
        return _path + '?' + urllib.parse.urlencode(kwargs)

    @classmethod
    def set_TimeZoneMexico(self, _datetime):
        mexico_tz = pytz.timezone('America/Mexico_City')
        tranformed_datetime = mexico_tz.localize(_datetime)
        return tranformed_datetime


class Chronos(object):

    @classmethod
    def get_DateStart(self, value):
        date = value.replace(hour=0, minute=0, second=0)
        return date

    @classmethod
    def get_DateEnd(self, value):
        date = value.replace(hour=23, minute=59, second=59)
        return date

    @classmethod
    def get_ListDates_FromDates(self, init_date, end_date, until_endmonth=False):
        """Regresa una lista con las fechas que existen entre:
        - init_date
        - end_date
        Las fechas ya deben ser proporcionadas como objeto date."""

        date_list = []
        delta = end_date - init_date
        month_index = 1
        month = init_date.month
        for i in range(delta.days + 1):
            next_date = init_date + timedelta(i)

            if until_endmonth:
                if next_date.month == end_date.month \
                        and next_date.year == end_date.year:
                    break

            if month != next_date.month:
                month = next_date.month
                month_index += 1
            date_list.append({
                'index_month': month_index,
                'date': next_date
            })
        return date_list

    @classmethod
    def get_ListDates_FromDateToMonth(self, date, months):
        """Devuelve una lista con las fechas que existen entre
        una fecha dada y un numero de meses."""
        init_date = date
        end_date = init_date + relativedelta(months=months)
        date_list = self.get_ListDates_FromDates(
            init_date, end_date, until_endmonth=True)
        return date_list

    @classmethod
    def get_DateRangeHours(self, date):
        """Dada una fecha, devuelve una fecha con 00:00:00 y
        otra con 23:59:59"""
        init = date.replace(hour=0, minute=0, second=0)
        end = date.replace(hour=23, minute=59, second=59)
        return init, end

    @classmethod
    def get_DatesRangeHours(self, date_init, date_end):
        """Dada dos fechas, devuelve una fecha con 00:00:00 y
        otra con 23:59:59"""
        init = date_init.replace(hour=0, minute=0, second=0)
        end = date_end.replace(hour=23, minute=59, second=59)
        return init, end


class Calendar(object):

    @classmethod
    def get_WeekNumber(self, start_date, _date):
        if isinstance(start_date, datetime):
            start_date = start_date.date()
        if isinstance(_date, datetime):
            _date = _date.date()
        week_number = ceil((_date-start_date).days/7)
        return week_number

    @classmethod
    def get_RemainingWeeks(self, end_date, date):
        remaining_weeks = floor((end_date-date).days/7)
        return remaining_weeks


class Record(object):

    @classmethod
    def fill(self, _record, _data):
        for key, value in _data.items():
            if hasattr(_record, key):
                setattr(_record, key, value)
            else:
                text = "El {} atribute no existen el " \
                       "el objeto {}"
                text = text.format(
                    key,
                    type(_record)
                )
                raise ValueError(text)


class Render(object):

    @staticmethod
    def render(path: str, context: dict):
        template = get_template(path)
        html = template.render(context)
        response = BytesIO()
        pdf = pisa.pisaDocument(BytesIO(html.encode("UTF-8")), response)
        if not pdf.err:
            return HttpResponse(
                response.getvalue(), content_type='application/pdf')
        else:
            return HttpResponse("Error Rendering PDF", status=400)


class ModelManager(object):

    @classmethod
    def search_Model(self, app, model):
        MyModel = apps.get_model(app, model, True)
        return MyModel


class FileAdmin:
    @classmethod
    def path_Photograpy(self, instance, filename):
        path = os.path.join(
            instance._meta.model_name,
            str(instance.id),
            'photograpy',
            filename,
        )
        return path

    @classmethod
    def path_SignedReceipt(self, instance, filename):
        path = os.path.join(
            instance._meta.model_name,
            instance.id,
            'signed_receipt',
            filename,
        )
        return path

    @classmethod
    def path_Prescription(self, instance, filename):
        path = os.path.join(
            instance._meta.model_name,
            instance.id,
            'prescription',
            filename,
        )
        return path

    @classmethod
    def path_ImagesFolder(self, instance, filename):
        path = os.path.join(
            instance._meta.model_name,
            str(instance.estate.id),
            'images',
            filename
        )
        return path

    @classmethod
    def path_FileFolder(self, instance, filename):
        path = os.path.join(
            instance._meta.model_name,
            str(instance.estate.id),
            'files',
            filename
        )
        return path


class Report(object):

    @staticmethod
    def render(path: str, context: dict):
        template = get_template(path)
        html = template.render(context)
        response = BytesIO()
        pdf = pisa.pisaDocument(BytesIO(html.encode("UTF-8")), response)
        if not pdf.err:
            return HttpResponse(
                response.getvalue(), content_type='application/pdf')
        else:
            return HttpResponse("Error Rendering PDF", status=400)


class DbAdmin(object):

    @classmethod
    def get_BackupName(self):
        file_name = "{}_bk.sql".format(
            datetime.now().strftime('%d_%m_%y-%H_%M')
        )
        return file_name

    @classmethod
    def get_CommandBackup(self, _db_name, _user, _password, _path, _file_name):
        command = "mysqldump --single-transaction -u {0} -p{1} -c {2} " \
            "--ignore-table={2}.django_admin_log " \
            "--ignore-table={2}.django_content_type " \
            "--ignore-table={2}.django_migrations " \
            "--ignore-table={2}.django_session -r " \
            "{3}/{4}".format(
                _user,
                _password,
                _db_name,
                _path,
                _file_name
            )

        return command

        # command = 'mysqldump -u %s -p %s goodslydb '\
        #     '--ignore-table=goodslydb.django_admin_log ' \
        #     '--ignore-table=goodslydb.django_content_type '\
        #     '--ignore-table=goodslydb.django_migrations '\
        #     '--ignore-table=goodslydb.django_session -r '\
        #     '%s/%s' % (user, password, path, file_name)