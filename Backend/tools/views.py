# Python Libreries
from tablib import Dataset

# Django's Libraries
from django.views.generic import View
from django.contrib import messages
from django.conf import settings
from django.core.exceptions import PermissionDenied
from django.urls import reverse_lazy
from django.shortcuts import render
from django.shortcuts import redirect
from django.core.exceptions import ImproperlyConfigured
from django.http import HttpResponse
# from django.shortcuts import render_to_response
# from django.template import RequestContext

# Own's Libraries
from .utils import List
from .mixins import StxSecurityMixin


class AppView(View):
    page_title = ""
    page_name = ""
    business_class = None
    get_model_name_method = "get_ModelName"
    message_method = "set_SaveMessage"
    get_url_back_method = "get_UrlBack"
    back_text = ""
    back_url = None
    get_url_list_method = "get_UrlList"
    url_list = None
    form_class = None
    last_record_method = "get_LastRecord"
    get_keyfield_method = "get_KeyField"
    get_record_method = "get"
    company = None

    def validate_AppName(self, _origin):
        if settings.APP_NAME is None:
            message = "Favor de configurar el nombre de la APP"
            raise ImproperlyConfigured(
                f"{_origin} ({message})"
            )

    def validate_Bsn(self, _origin):
        if self.business_class is None:
            message = "Favor de especificar una clase de negocio"
            raise ImproperlyConfigured(
                f"{_origin} ({message})"
            )

    def validate_BsnMethod(self, _origin, _method):
        if not _method:
            message = f"No se especifico: {_method}"
            raise ImproperlyConfigured(
                f"{_origin} ({message})"
            )
        if _method not in dir(self.business_class):
            message = f"No existe {_method} in {self.business_class}"
            raise ImproperlyConfigured(
                f"{_origin} ({message})"
            )

    def validate_Form(self):
        origin = "AppView.validate_Form"
        if self.form_class is None:
            message = "Favor de especificar una clase de formulario"
            raise ImproperlyConfigured(
                f"{origin} ({message})"
            )

    def validate_FormMethod(self, _method):
        origin = "AppView.validate_FormMethod"
        if not _method:
            message = f"No se especifico: {_method}"
            raise ImproperlyConfigured(
                f"{origin} ({message})"
            )
        if _method not in dir(self.form_class):
            message = f"No existe {_method} in {self.form_class}"
            raise ImproperlyConfigured(
                f"{origin} ({message})"
            )

    def get_LastRecord(self):
        origin = "AppView.get_LastRecord"
        last_record = None

        if self.last_record_method:
            self.validate_BsnMethod(origin, self.last_record_method)
            last_record = getattr(
                self.business_class,
                self.last_record_method
            )()

        return last_record

    def get_Record(self, pk):
        origin = "AppView.get_Record"
        record = None

        if self.get_record_method:
            self.validate_BsnMethod(
                origin,
                self.get_record_method
            )
            record = getattr(
                self.business_class,
                self.get_record_method
            )(pk)
        else:
            message = "Favor de especificar metodo para obtener record"
            raise ImproperlyConfigured(
                f"{origin} ({message})"
            )

        return record

    def set_PageName(self):
        origin = "AppView.set_PageName"
        self.validate_AppName(origin)
        if self.page_title:
            value = "{} - {}".format(
                self.page_title,
                settings.APP_NAME
            )
            self.page_name = value
        else:
            self.page_name = settings.APP_NAME

    def set_Context(self, **kwargs):
        self.context = {}
        if kwargs:
            self.context = kwargs

        self.set_PageName()
        self.context['page_name'] = self.page_name
        self.context['page_title'] = self.page_title
        self.context['back_text'] = self.back_text
        self.context['back_url'] = self.back_url
        return self.context

    def get_Context(self, **kwargs):
        return self.set_Context(**kwargs)

    def set_TextBack(self):
        origin = "AppView.set_TextBack"
        if not self.back_text:
            self.validate_BsnMethod(
                origin,
                self.get_model_name_method
            )
            self.back_text = getattr(
                self.business_class,
                self.get_model_name_method
            )()

    def set_UrlBack(self):

        origin = "AppView.set_UrlBack"
        if self.back_url is None:
            self.validate_BsnMethod(
                origin,
                self.get_url_back_method
            )
            self.back_url = getattr(
                self.business_class,
                self.get_url_back_method
            )()

    def set_PageTitle(self, _text, _record=None):
        origin = "AppView.set_PageTitle"
        if not self.page_title:
            self.validate_BsnMethod(
                origin,
                self.get_model_name_method
            )
            self.page_title = "{} {}".format(
                _text,
                getattr(
                    self.business_class,
                    self.get_model_name_method
                )()
            )

        if _record:
            if self.get_keyfield_method:
                self.validate_BsnMethod(
                    origin,
                    self.get_keyfield_method
                )
                word = getattr(
                    self.business_class,
                    self.get_keyfield_method
                )(_record)
                self.page_title = f'{self.page_title}:{word}'

    def set_Message(self, _request):
        origin = "AppView.set_Message"
        self.validate_BsnMethod(
            origin,
            self.message_method
        )
        getattr(self.business_class, self.message_method)(_request)

    def set_UrlList(self):
        origin = "AppView.set_UrlList"
        if self.url_list is None:
            self.validate_BsnMethod(
                origin,
                self.get_url_list_method
            )
            self.url_list = getattr(
                self.business_class,
                self.get_url_list_method
            )()

    def get(self, _request):
        context = self.get_Context()
        return render(_request, self.template_name, context)

    def get_Company(self, record):
        base = record
        for path in self.path_related_company:
            new_base = base
            base = getattr(new_base, path)
            company = base
        return company

    def get_Companies(self, record):
        base = record
        for path in self.path_many_related_company:
            new_base = base
            base = getattr(new_base, path)
            company = base
        return company


class AppListView(AppView):
    get_url_new_method = "get_UrlNew"
    post_filter_method = None
    url_new = None
    method_records = None
    filtered_method = "get_Filtered"
    paginated_method = None
    optionals = {}

    def set_UrlNew(self):
        origin = "AppListView.set_UrlNew"
        if self.url_new is None:
            self.validate_BsnMethod(
                origin,
                self.get_url_new_method
            )
            self.url_new = getattr(
                self.business_class,
                self.get_url_new_method
            )()

    def set_Filters(self, _request):
        origin = "AppListView.set_Filters"
        records = None
        filters = None
        svalues = None

        if self.filtered_method:
            self.validate_BsnMethod(
                origin,
                self.filtered_method
            )
            records, filters, svalues = getattr(
                self.business_class,
                self.filtered_method
            )(_request, self.method_records)
        return records, filters, svalues

    def has_Filters(requestself, _request):
        is_filtered = False
        for key in _request.GET.keys():
            if _request.GET.get(key) and key != "from_save":
                is_filtered = True

        return is_filtered

    def get_PaginatedRecords(self, _request, _records):
        page = _request.GET.get('page', 1)
        paginated_records = List.get_Pagination(_records, page)
        return paginated_records

    def set_Pagination(self, _request, _records):
        origin = "AppListView.set_UrlList"
        if self.paginated_method:
            self.validate_BsnMethod(
                origin,
                self.paginated_method
            )
            records = getattr(self.business_class, self.paginated_method)(
                _request, _records
            )
        else:
            records = self.get_PaginatedRecords(_request, _records)

        return records

    def append_Context(self, **kwargs):
        self.context.update(kwargs)

    def pre_Return(self):
        pass

    def get(self, request):
        origin = "AppListView.get"
        self.validate_Bsn(origin)
        self.set_PageTitle("Listado de")
        self.set_TextBack()
        self.set_UrlBack()
        self.set_UrlList()
        self.set_UrlNew()
        self.set_Message(request)

        records, filters, svalues = self.set_Filters(
            request
        )

        records = records.distinct()

        is_filtered = self.has_Filters(request)

        paginated_records = self.set_Pagination(request, records)
        context = self.set_Context(
            queryset=records,
            records=paginated_records,
            filters=filters,
            search_value=svalues,
            is_filtered=is_filtered,
            url_list=self.url_list,
            url_new=self.url_new,
            optionals=self.optionals,
        )
        self.pre_Return()
        return render(request, self.template_name, context)


class ModelRelatedListView(AppView):
    business_class = None
    filtered_method = "get_RelatedFieldList"
    related_field = None
    paginated_method = None
    search_method = None
    get_url_list_method = "get_UrlList"
    get_url_new_method = "get_UrlNew"
    # message_method = "set_SaveMessage"
    optionals = {}
    url_list = None
    url_new = None

    def get_PaginatedRecords(self, request, records):
        page = request.GET.get('page', 1)
        paginated_records = List.get_Pagination(records, page)
        return paginated_records

    def get(self, request, pk):
        if not self.page_title:
            self.page_title = 'Actualizar {}'.format(
                self.business_class.get_ModelVerboseName())

        if self.business_class is not None:
            # records = None
            # filters = None
            # svalues = None
            business = self.business_class
            related_field = self.related_field

            # if self.message_method in dir(business):
            #     getattr(business, self.message_method)(request)
            instance, records = getattr(
                business,
                self.filtered_method
            )(request, pk, related_field)

            if self.paginated_method:
                records = getattr(business, self.paginated_method)(
                    request, records
                )
            else:
                records = self.get_PaginatedRecords(request, records)

            self.set_TextBack()

            if self.url_list is None:
                if self.get_url_list_method:
                    self.url_list = getattr(
                        business,
                        self.get_url_list_method
                    )()
                else:
                    raise ImproperlyConfigured(
                        "Favor de especificar una URL para listado"
                    )

            if self.url_new is None:
                if self.get_url_new_method:
                    self.url_new = getattr(
                        business,
                        self.get_url_new_method
                    )(instance)
                else:
                    raise ImproperlyConfigured(
                        "Favor de especificar una URL para Nuevo"
                    )
            context = self.set_Context(
                records=records,
                instance=instance,
                back_name=self.back_text,
                url_list=self.url_list,
                url_new=self.url_new,
                optionals=self.optionals,
            )
            return render(request, self.template_name, context)

        else:
            raise ImproperlyConfigured(
                "Favor de especificar una clase de negocio")


class AppNewView(AppView):
    create_record_method = None

    def get(self, request):
        origin = "AppNewView.get"
        self.validate_Bsn(origin)
        self.validate_Form()
        self.set_TextBack()
        self.set_UrlBack()
        self.set_UrlList()
        self.set_PageTitle("Crear")
        self.set_Message(request)
        form = self.form_class(user=request.user)
        last_record = self.get_LastRecord()

        context = self.set_Context(
            last_record=last_record,
            form=form,
            url_list=self.url_list
        )
        return render(request, self.template_name, context)

    def post(self, request):
        origin = "AppNewView.post"
        self.validate_Bsn(origin)
        self.validate_Form()
        self.set_TextBack()
        self.set_UrlBack()
        self.set_UrlList()
        self.set_PageTitle("Crear")
        last_record = self.get_LastRecord()
        form = self.form_class(
            request.POST,
            user=request.user
        )

        if form.is_valid():
            try:
                if self.create_record_method:
                    self.validate_FormMethod(
                        self.create_record_method
                    )
                    url = getattr(
                        form,
                        self.create_record_method
                    )(request)
                else:
                    url = form.save(request)

                return redirect(url)
            except Exception as error:
                messages.error(
                    request,
                    str(error)
                )

        context = self.set_Context(
            last_record=last_record,
            form=form,
            url_list=self.url_list
        )
        return render(request, self.template_name, context)


class ModelRelatedNewView(AppView):
    get_url_redirect_method = None
    get_url_back_redirect_method = None
    create_record_method = None
    related_field = None
    last_record_method = None

    def get(self, request, **kwargs):
        back_list = getattr(
            self.business_class, self.get_url_back_redirect_method
        )(kwargs.get('pk'))
        self.set_TextBack()
        last_record = getattr(
            self.business_class, self.last_record_method
        )(request.user)
        form = self.form_class(user=request.user)
        if kwargs.get('pk'):
            instance = self.business_class.get(kwargs.get('pk'))

        context = {
            'last_record': last_record,
            'instance': instance,
            'form': form,
            'page_title': self.page_title,
            'url_list': back_list,
            'back_text': self.back_text,
        }

        return render(request, self.template_name, context)

    def post(self, request, **kwargs):
        last_record = getattr(
            self.business_class, self.last_record_method
        )(request.user)
        form = self.form_class(request.POST, user=request.user)
        if kwargs.get('pk', None):
            instance = self.business_class.get(kwargs.get('pk'))

            if form.is_valid():
                data = form.cleaned_data
                try:
                    data.update({self.related_field: instance})
                    if self.create_record_method:
                        record = getattr(
                            self.business_class, self.create_record_method
                        )(data, request.user)
                        if self.url_list is None:
                            self.url_list = getattr(
                                self.business_class,
                                self.get_url_redirect_method
                            )(request, instance)
                            messages.add_message(
                                request,
                                messages.SUCCESS,
                                "Creacion exitosa."
                            )

                        return redirect(self.url_list)
                except Exception as error:
                    messages.add_message(
                        request,
                        messages.ERROR,
                        str(error)
                    )

            context = {
                'last_record': last_record,
                'instance': instance,
                'form': form,
                'page_title': self.page_title
            }

            return render(request, self.template_name, context)
        else:
            raise ValueError('No se recibio un valor pk en los parametros.')


class AppEditView(AppView):
    update_record_method = None
    path_related_company = None
    path_many_related_company = None
    validate_access = "validate_Access"

    def get(self, request, pk):
        origin = "AppEditView.get"
        self.validate_Bsn(origin)
        self.validate_Form()
        self.set_TextBack()
        self.set_UrlBack()
        self.set_UrlList()
        self.set_Message(request)
        record = self.get_Record(pk)
        if self.path_related_company:
            self.company = self.get_Company(record)
            access = getattr(
                self.business_class, self.validate_access
            )(request.user, self.permissions, self.company)
        elif self.path_many_related_company:
            companies = self.get_Companies(record)
            access = False
            for company in companies.all():
                user_access = getattr(
                    self.business_class, self.validate_access
                )(request.user, self.permissions, company.id)
                if user_access is True:
                    access = True
        else:
            access = True

        if access is False:
            raise PermissionDenied(
                "No se tienen los permisos para entrar a esta sección"
            )
        self.set_PageTitle("Actualizar", record)
        form = self.form_class(
            user=request.user,
            instance=record
        )
        context = self.set_Context(
            url_list=self.url_list,
            form=form
        )
        return render(request, self.template_name, context)

    def post(self, request, pk):
        origin = "AppEditView.post"
        self.validate_Bsn(origin)
        self.validate_Form()
        self.set_TextBack()
        self.set_UrlBack()
        self.set_UrlList()
        record = self.get_Record(pk)
        self.set_PageTitle("Actualizar", record)
        form = self.form_class(
            request.POST,
            user=request.user,
            instance=record
        )
        if form.is_valid():
            try:
                if self.update_record_method:
                    self.validate_FormMethod(
                        self.update_record_method
                    )
                    url = getattr(
                        form,
                        self.update_record_method
                    )(request)
                else:
                    url = form.save(request)

                if url:
                    return redirect(url)

            except Exception as error:
                messages.error(
                    request,
                    str(error)
                )

        context = self.set_Context(
            url_list=self.url_list,
            form=form
        )
        return render(request, self.template_name, context)


class ModelRelatedEditView(AppView):
    get_record_method = None
    update_record_method = None
    get_url_back_redirect_method = None
    related_field = None
    form_class = None
    path_related_company = None
    path_many_related_company = None
    validate_access = "validate_Access"

    def get(self, request, pk):
        record = getattr(self.business_class, self.get_record_method)(pk)
        if self.path_related_company:
            self.company = self.get_Company(record)
            access = getattr(
                self.business_class, self.validate_access
            )(request.user, self.permissions, self.company)
        elif self.path_many_related_company:
            companies = self.get_Companies(record)
            access = False
            for company in companies.all():
                user_access = getattr(
                    self.business_class, self.validate_access
                )(request.user, self.permissions, company.id)
                if user_access is True:
                    access = True
        else:
            access = True
        if access is False:
            raise PermissionDenied(
                "No se tienen los permisos para entrar a esta sección"
            )
        self.set_TextBack()
        self.set_UrlBack()
        self.set_Message(request)
        self.page_title = ('Actualizar ' + record._meta.verbose_name)
        form = self.form_class(
            user=request.user,
            instance=record
        )
        related_model = getattr(record, self.related_field)
        url_list = getattr(
            self.business_class, self.get_url_redirect_method
        )(request, related_model)
        context = self.set_Context(
            url_list=url_list,
            back_name=self.back_text,
            form=form
        )
        return render(request, self.template_name, context)

    def post(self, request, pk):
        record = getattr(self.business_class, self.get_record_method)(pk)
        self.set_TextBack()
        self.set_UrlBack()
        self.set_Message(request)
        self.page_title = ('Actualizar ' + record._meta.verbose_name)
        form = self.form_class(
            request.POST,
            user=request.user,
            instance=record
        )
        related_model = getattr(record, self.related_field)
        url_list = getattr(
            self.business_class, self.get_url_redirect_method
        )(request, related_model)
        if form.is_valid():
            data = form.cleaned_data
            try:
                if self.update_record_method:
                    getattr(
                        self.business_class,
                        self.update_record_method
                    )(record, data, request.user)
                if url_list:
                    return redirect(url_list)

            except Exception as error:
                messages.error(
                    request,
                    str(error)
                )

        context = self.set_Context(
            url_list=url_list,
            back_name=self.back_text,
            form=form
        )
        return render(request, self.template_name, context)


class AppRetrieveView(AppView):
    path_related_company = None
    path_many_related_company = None
    validate_access = "validate_Access"

    def get(self, request, pk):
        origin = "AppRetrieveView.get"
        self.validate_Bsn(origin)
        self.set_TextBack()
        self.set_UrlBack()
        self.set_UrlList()
        self.set_Message(request)
        record = self.get_Record(pk)
        if self.path_related_company:
            self.company = self.get_Company(record)
            access = getattr(
                self.business_class, self.validate_access
            )(request.user, self.permissions, self.company)
        elif self.path_many_related_company:
            companies = self.get_Companies(record)
            access = False
            for company in companies.all():
                user_access = getattr(
                    self.business_class, self.validate_access
                )(request.user, self.permissions, company.id)
                if user_access is True:
                    access = True
        else:
            access = True
        if access is False:
            raise PermissionDenied(
                "No se tienen los permisos para entrar a esta sección"
            )

        self.set_PageTitle("Detalle de", record)
        context = self.set_Context(
            record=record,
            url_list=self.url_list
        )
        return render(request, self.template_name, context)


class AppReactView(AppView):

    def get(self, request):

        context = self.set_Context()
        return render(request, self.template_name, context)


class AppReactRelatedNewView(View):
    template_name = None
    business_class = None
    get_record_method = None

    def get(self, request, pk):
        if self.business_class is not None:
            record = getattr(self.business_class, self.get_record_method)(pk)
            context = {'record': record}
        else:
            context = {}
        return render(request, self.template_name, context)


class StxView(StxSecurityMixin, AppView):
    permissions = []
    login_required = True


class StxRetrieveView(StxSecurityMixin, AppRetrieveView):
    permissions = []
    login_required = True


class StxNewView(StxSecurityMixin, AppNewView):
    permissions = []
    login_required = True


class StxListView(StxSecurityMixin, AppListView):
    permissions = []
    login_required = True


class StxEditView(StxSecurityMixin, AppEditView):
    permissions = []
    login_required = True


class StxReactView(StxSecurityMixin, AppReactView):
    permissions = []
    login_required = True


class StxReactRelatedNewView(StxSecurityMixin, AppReactRelatedNewView):
    permissions = []
    login_required = True


class StxModelRelatedListView(StxSecurityMixin, ModelRelatedListView):
    permissions = []
    login_required = True


class StxModelRelatedNewView(StxSecurityMixin, ModelRelatedNewView):
    permissions = []
    login_required = True


class StxModelRelatedEditView(StxSecurityMixin, ModelRelatedEditView):
    permissions = []
    login_required = True


class StxExportToExcel(StxView):
    business_class = None
    filtered_method = "get_Filtered"
    method_records = None
    dataset_method = "get_DataSet"
    name_file = "file.xls"

    def get(self, request):
        if self.business_class is not None:
            business = self.business_class

            records, filters, svalues = getattr(
                business,
                self.filtered_method
            )(request, self.method_records)

            dataset = getattr(business, self.dataset_method)(records=records)

            response = HttpResponse(
                dataset,
                content_type='application/vnd.openxmlformats'
                             '-officedocument.spreadsheetml.sheet'
            )
            disposition = 'attachment; filename="{}"'.format(self.name_file)
            response['Content-Disposition'] = disposition
            return response
        else:
            raise ImproperlyConfigured(
                "Favor de especificar una clase de negocio")


class StxRelatedExportToExcel(StxView):
    business_class = None
    filtered_method = "get_RelatedFieldList"
    method_records = None
    dataset_method = "get_DataSet"
    name_file = "file.xls"
    related_field = None

    def get(self, request, pk):
        if self.business_class is not None:
            business = self.business_class

            instance, records = getattr(
                business,
                self.filtered_method
            )(request, pk, self.related_field)

            dataset = getattr(business, self.dataset_method)(records=records)

            response = HttpResponse(
                dataset,
                content_type='application/vnd.openxmlformats'
                             '-officedocument.spreadsheetml.sheet'
            )
            disposition = 'attachment; filename="{}"'.format(self.name_file)
            response['Content-Disposition'] = disposition
            return response
        else:
            raise ImproperlyConfigured(
                "Favor de especificar una clase de negocio")


class ResourceModelImportView(StxView):
    resource = 'get_ModelResource'
    model_resource_method = None
    url_back = None
    text_back = None

    def set_PageName(self):
        if not self.page_title:
            plural = self.business_class.get_ModelVerboseNamePlural()
            self.page_title = f'Importacion de {plural}'

    def get(self, request, pk=None):
        self.set_PageName()
        if self.text_back:
            self.back_text = self.text_back
        else:
            self.set_TextBack()
        if self.url_back:
            if pk:
                self.back_url = reverse_lazy(
                    self.url_back, kwargs={'pk': pk}
                )
            else:
                self.back_url = reverse_lazy(
                    self.url_back
                )
        else:
            self.set_UrlBack()
        context = {
            'url_list': self.back_url,
            'back_text': self.back_text,
            'page_title': self.page_title
        }
        return render(request, self.template_name, context)

    def post(self, request, pk=None):
        if self.model_resource_method:
            resource = getattr(self.business_class, self.resource)(
                self.model_resource_method
            )
        else:
            resource = getattr(self.business_class, self.resource)()
        dataset = Dataset()
        errors = None

        if 'load' in request.POST:
            if 'excel_file' in request.FILES:
                file = request.FILES['excel_file']
                data = file.read()
                dataset = dataset.load(data, 'xlsx')
                result = resource().import_data(dataset=dataset, dry_run=False)
                if not result.has_errors():
                    messages.add_message(
                        request,
                        messages.SUCCESS,
                        "Se cargo con exito el archivo"
                    )
                else:
                    errors = result.row_errors()
            else:
                messages.add_message(
                    request,
                    messages.ERROR,
                    "Favor de especificar un archivo a cargar"
                )

        self.set_PageName()
        if self.text_back:
            self.back_text = self.text_back
        else:
            self.set_TextBack()
        if self.url_back:
            if pk:
                self.back_url = reverse_lazy(
                    self.url_back, kwargs={'pk': pk}
                )
            else:
                self.back_url = reverse_lazy(
                    self.url_back
                )
        else:
            self.set_UrlBack()
        context = {
            'url_list': self.back_url,
            'back_text': self.back_text,
            'page_title': self.page_title,
            'import_errors': errors
        }
        return render(request, self.template_name, context)


class StxEmptyImportView(StxView):
    empty_filename = None
    empty_resource_method = 'empty_Export'
    resource = None

    def set_Filename(self):
        if not self.empty_filename:
            self.empty_filename = self.business_class.get_ModelVerboseNamePlural()

    def get(self, request):
        self.set_Filename()
        dataset = getattr(
            self.business_class,
            self.empty_resource_method)(
                self.resource
        )
        response = HttpResponse(
            dataset.xlsx,
            content_type='application/vnd.openxmlformats'
                            '-officedocument.spreadsheetml.sheet'
        )
        response['Content-Disposition'] = \
            f'attachment; filename="{self.empty_filename}.xlsx"'
        return response


def Stx404view(request, exception):
    gotologin = True

    if request.user.is_authenticated:
        gotologin = False

    context = {
        "message": "Este sitio no existe o no está disponible.",
        "gotologin": gotologin
    }
    return render(request, 'home/errors/404/404.html', context)


def Stx500view(request):
    gotologin = True

    if request.user.is_authenticated:
        gotologin = False

    context = {
        "message": "Este sitio no existe o no está disponible.",
        "gotologin": gotologin
    }
    return render(request, 'home/errors/404/404.html', context)
