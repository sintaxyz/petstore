#Python Libraries
from datetime import date

# Third-party Libraries
from import_export import resources
from import_export import fields
from import_export.widgets import Widget


class BooleanWidget(Widget):
    """
    Widget for converting boolean fields.
    """
    TRUE_VALUES = ["Si", 1, "True", "1"]
    FALSE_VALUES = ["No", 0, "False", "0"]

    def render(self, value, obj=None):
        if value is None:
            return ""
        return self.TRUE_VALUES[0] if value else self.FALSE_VALUES[0]

    def clean(self, value, row=None, *args, **kwargs):
        if value == "":
            return None
        return True if value in self.TRUE_VALUES else False


class ChoicesWidget(Widget):
    """
    Widget that uses choice display values in place of datebase values
    """
    def __init__(self, choices, *args, **kwargs):
        """
        Creates a self.choices dict with a key, display value, and value,
        db value, e.g. {'Chocolate': 'CHOC'}
        """
        self.choices = dict(choices)
        self.revert_choices = dict((v, k) for k, v in self.choices.items())

    def clean(self, value, row=None, *args, **kwargs):
        """Returns the db value given the display value"""
        return self.revert_choices.get(value, value) if value else None

    def render(self, value, obj=None):
        """Returns the display value given the db value"""
        return self.choices.get(value, '')


class GenericResource(resources.ModelResource):

    @classmethod
    def field_from_django_field(self, field_name, django_field, readonly):
        FieldWidget = self.widget_from_django_field(django_field)
        widget_kwargs = self.widget_kwargs_for_field(field_name)
        field = fields.Field(
            attribute=field_name,
            column_name=django_field.verbose_name,
            widget=FieldWidget(**widget_kwargs),
            readonly=readonly)
        return field


class ResourceSettings(object):

    @classmethod
    def set_createdUpdatedUser(self, instance, new, user):
        if user:
            if new:
                instance.created_by = user
                instance.created_date = date.today()
            else:
                instance.updated_by = user
                instance.updated_date = date.today()
        else:
            if new:
                instance.created_by = root
                instance.created_date = date.today()
            else:
                instance.updated_by = root
                instance.updated_date = date.today()

        instance.status = 'RE'

        return instance
