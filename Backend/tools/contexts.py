# Django's Libraries
from django.conf import settings
# from django.urls import reverse


def app_information(request):
    data = {
        'name': settings.APP_NAME,
        'partner_name': settings.APP_PARTNER_NAME,
        'year_release': settings.APP_YEAR_RELEASE,
        'version': settings.APP_VERSION,
    }
    return {'APP': data}


def user_information(request):
    pk = ""
    email = ""
    # username = ""
    short_name = ""
    name = ""
    is_superuser = None
    is_staff = None
    url_start = ""
    groups = []
    permissions = []

    if request.user.is_anonymous is False:
        pk = request.user.pk
        email = request.user.email
        short_name = request.user.short_name
        name = request.user.name
        is_superuser = request.user.is_superuser
        is_staff = request.user.is_staff
        url_start = request.user.url_start
        groups = request.user.groups.values_list("name", flat=True)
        for assignement in request.user.has_assignments.all():
            for permission in assignement.position.permissions.all():
                permissions.append(permission.code)

    data = {
        'pk': pk,
        'email': email,
        # 'username': username,
        'short_name': short_name,
        'name': name,
        'is_superuser': is_superuser,
        'is_staff': is_staff,
        'url_start': url_start,
        'groups': groups,
        'permissions': permissions
    }
    return {'USER': data}
