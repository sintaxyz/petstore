# Django's Libraries
from django.db import models


class AddressModel(models.Model):

    # TODO add location Field
    # TODO add minicipio Field
    # TODO add estate Field
    latitude = models.DecimalField(
        verbose_name="Latitud",
        null=True,
        blank=True,
        max_digits=10,
        decimal_places=8
    )
    longitude = models.DecimalField(
        verbose_name="Longitud",
        null=True,
        blank=True,
        max_digits=11,
        decimal_places=8
    )
    street = models.CharField(
        verbose_name='calle',
        max_length=140,
        blank=True,
        null=True,
    )
    outdoor_number = models.CharField(
        verbose_name='numero Exterior',
        max_length=144,
        null=True,
        blank=True,
    )
    municipality = models.CharField(
        verbose_name='Municipio/Alcaldia',
        max_length=144,
        null=True,
        blank=True,
    )
    settlement = models.CharField(
        verbose_name='colonia',
        max_length=144,
        null=True,
        blank=True,
    )
    interior_number = models.CharField(
        verbose_name='numero Interior',
        max_length=144,
        null=True,
        blank=True,
    )
    between_street_a = models.CharField(
        verbose_name='entre calle 1',
        max_length=140,
        blank=True,
        null=True,
    )
    between_street_b = models.CharField(
        verbose_name='entre calle 2',
        max_length=140,
        blank=True,
        null=True,
    )
    zip_code = models.IntegerField(
        verbose_name='codigo Postal',
        blank=True,
        null=True,
    )

    class Meta:
        abstract = True


class AppModel(models.Model):
    created_date = models.DateTimeField(
        verbose_name="fecha registro",
        auto_now=False,
        auto_now_add=True,
        null=True,
        blank=True
    )
    updated_date = models.DateTimeField(
        verbose_name="fecha actualización",
        auto_now=True,
        auto_now_add=False,
        null=True,
        blank=True
    )
    created_by = models.ForeignKey(
        'security.User',
        verbose_name="Creado por",
        related_name="%(class)s_created",
        on_delete=models.PROTECT,
        blank=True,
        null=True
    )
    updated_by = models.ForeignKey(
        'security.User',
        verbose_name="Modificado por",
        related_name="%(class)s_updated",
        on_delete=models.PROTECT,
        blank=True,
        null=True
    )

    class Meta:
        abstract = True
