# Django's Libraries
from django_filters.rest_framework import DjangoFilterBackend
from rest_framework.filters import OrderingFilter
from django.core.exceptions import ImproperlyConfigured

# Third-party Libraries
from rest_framework import viewsets
# from rest_framework import status
from rest_framework.response import Response
from rest_framework.permissions import IsAuthenticated
# from rest_framework.permissions import AllowAny


class StxListAPIView(viewsets.GenericViewSet):
    business_class = None
    queryset_class = None

    def get_queryset(self):
        queryset = self.url_list = getattr(
            self.business_class,
            self.queryset_class
        )()
        return queryset

    def list(self, request, *args, **kwargs):
        if self.business_class is not None:
            if self.queryset_class is not None:
                queryset = self.filter_queryset(self.get_queryset())
                serializer = self.get_serializer(queryset, many=True)
                return Response(serializer.data)
            else:
                raise ImproperlyConfigured(
                    "Favor de especificar una clase de negocio")

        else:
            raise ImproperlyConfigured(
                "Favor de especificar una clase de negocio")


class StxAPIList(viewsets.GenericViewSet):
    permission_classes = (IsAuthenticated,)
    serializer_class = None
    filter_backends = (OrderingFilter, DjangoFilterBackend,)
    ordering_fields = '__all__'
    ordering = ('id',)
    filter_class = None
    business_class = None
    business_method = None

    def get_queryset(self):
        if self.business_class is not None:
            if self.business_method:
                queryset = getattr(
                    self.business_class,
                    self.business_method
                )()
            else:
                queryset = getattr(
                    self.business_class,
                    'get_All'
                )()
            return queryset
        else:
            raise ImproperlyConfigured(
                "Favor de especificar una clase de negocio")

    def list(self, request, *args, **kwargs):
        queryset = self.filter_queryset(self.get_queryset())
        serializer = self.get_serializer(queryset, many=True)
        return Response(serializer.data)


class StxAPIRetrieve(viewsets.GenericViewSet):
    permission_classes = (IsAuthenticated,)
    serializer_class = None
    business_class = None
    business_method = None

    def get_object(self, param):
        if self.business_class is not None:
            if self.business_method:
                instance = getattr(
                    self.business_class,
                    self.business_method
                )(param)
            else:
                instance = getattr(
                    self.business_class,
                    'get'
                )(param)
            return instance
        else:
            raise ImproperlyConfigured(
                "Favor de especificar una clase de negocio")

    def retrieve(self, request, param, *args, **kwargs):
        instance = self.get_object(param)
        if self.serializer_class:
            serializer = self.get_serializer(instance)
        return Response(serializer.data)
