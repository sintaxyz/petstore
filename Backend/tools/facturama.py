import facturama
import os
from django.core.exceptions import ImproperlyConfigured
from django.core.files.storage import default_storage
from unipath import Path
from django.core.files.base import ContentFile
from django.core.files import File
from decimal import Decimal
from django.conf import settings



class InvoiceMultiEmisor(object):
    files_path =  Path(__file__).ancestor(3)
    service = None
    sale_order = None
    document = None
    default_filename_attr = 'id'

    def __init__(self):
        
        self.service = facturama
        self.service._credentials = (
            settings.FACTURAMA_USERNAME,
            settings.FACTURAMA_PASSWORD
        )

        if settings.FACTURAMA_SANDBOX:
            self.service.api_lite = True
            self.service.sandbox = True
        else:
            self.service.api_lite = False
            self.service.sandbox = False

    def validate_CompanyBillingSettings(self, sale_order):
        pass

    def validate_SaleOrderStatus(self, sale_order):
        pass

    def validate_SaleOrderFields(self, sale_order):
        pass

    def set_SaleOrder(self, sale_order):
        self.validate_CompanyBillingSettings(sale_order)
        self.validate_SaleOrderStatus(sale_order)
        self.validate_SaleOrderFields(sale_order)
        self.sale_order = sale_order
        return self.sale_order
        
    def get_CfdiObject(self, data:dict):
        instance = self.sale_order
        if not instance.branchoffice.company.billing_settings:
            raise ValueError('No se cuenta con la información de Facturacion.')
        expedition_zip_code = instance.branchoffice.zip_code
        payment_condition = f'CREDITO A {instance.credit_days} DIAS' if instance.paymethod == 'CR' else 'AL CONTADO'

        issuer_name = instance.branchoffice.company.legal_entity
        issuer_rfc = instance.branchoffice.company.billing_settings.rfc
        fiscal_regimen = instance.branchoffice.company.billing_settings.fiscal_regimen
        receiver_name = instance.branchoffice.company.legal_entity
        receiver_rfc = instance.branchoffice.company.billing_settings.rfc

        items = []
        for line in instance.in_saleorderitem.all():
            tax_factor = format(line.tax.factor / 100, '.2f')
            tax_total = format((line.tax.factor / 100) * Decimal(line.subtotal_price), '.2f')
            item = {
                "ProductCode": line.item.item.sat_code.key,
                "IdentificationNumber": line.item.item.primary_number,
                "Description": line.item.item.name,
                "Unit": str(line.item.item.udp),
                "UnitCode": line.item.item.udm.key,
                "UnitPrice": f'{line.item.sell_price}',
                "Quantity": f'{line.qty}',
                "Subtotal": f'{line.subtotal_price}',
                # TODO Change to M2M
                "Taxes": [
                    {
                        "Total": tax_total,
                        "Name": line.tax.abbreviation,
                        "Base": f'{line.subtotal_price}',
                        "Rate": tax_factor,
                        "IsRetention": "false"
                    }
                ],
                "Total": str(line.total_amount)
            }
            items.append(item)

        cfdi = {
            "Folio": "105",
            "Serie": "R",
            "ExpeditionPlace": expedition_zip_code,
            "PaymentConditions": payment_condition,
            "CfdiType": "I",
            "PaymentForm": data.get("payment_form"),
            "PaymentMethod": data.get("payment_method"),
            "Issuer": {
                "FiscalRegime": fiscal_regimen,
                "Name": issuer_name,
                "Rfc": issuer_rfc,
            },
            "Receiver": {
                "Rfc": receiver_rfc,
                "Name": receiver_name,
                "CfdiUse": data.get('cfdi_use')
            },
            "Items": items,
            "Total": str(instance.total_amount)
        }
        return cfdi

    def check_In(self, data:dict):
        if not self.sale_order:
            raise ImproperlyConfigured('Es necesario cargar una Orden de Venta.')
        cfdi = self.get_CfdiObject(data)
        self.document = self.service.CfdiMultiEmisor.create(cfdi)
        return self.document

    def get_DefaultFilename(self, extension:str):
        filename = getattr(self.sale_order, self.default_filename_attr)
        full_filename = str(filename) + f'.{extension}'
        return full_filename

    def get_DocumentXml(self, filename:str=None):
        if not filename:
            filename = self.get_DefaultFilename()
        xml = self.document.saveAsXML(
            self.document.Id,
            filename)
        return xml

    def get_DocumentPdf(self, filename:str=None):
        if not filename:
            filename = self.get_DefaultFilename()
        xml = self.document.saveAsPdf(
            self.document.Id,
            filename)
        return xml

    def save_DocumentXml(self, filename:str=None, remove:bool=True):
        if not filename:
            filename = self.get_DefaultFilename('xml')
        else:
            filename = filename + '.xml'
        file = self.get_DocumentXml(filename)
        relative_path = os.path.join(
            self.files_path,
            filename
        )
        self.sale_order.xml_invoice_file.save(filename, File(open(relative_path)))
        if remove:
            os.remove(relative_path)

    def save_DocumentPdf(self, filename:str=None, remove:bool=True):
        if not filename:
            filename = self.get_DefaultFilename('pdf')
        else:
            filename = filename + '.pdf'
        file = self.get_DocumentPdf(filename)
        relative_path = os.path.join(
            self.files_path,
            filename
        )
        self.sale_order.pdf_invoice_file.save(filename, File(open(relative_path, 'rb')))
        if remove:
            os.remove(relative_path)

    def cancel_Document(self, id:str):
        self.service.CfdiMultiEmisor.delete(id)

    def update_Csds(self, data:dict, remove:bool=True):
        tmp_key = os.path.join(settings.MEDIA_ROOT, "tmp", data.get('key').name)
        path_key = default_storage.save(tmp_key, ContentFile(data.get('key').read()))
        tmp_cer = os.path.join(settings.MEDIA_ROOT, "tmp", data.get('cer').name)
        path_cer = default_storage.save(tmp_cer, ContentFile(data.get('cer').read()))
        self.service.csdsMultiEmisor.update(
            data.get('rfc'),
            path_key,
            path_cer,
            data.get('password')
        )
        if remove:
            os.remove(tmp_key)
            os.remove(tmp_cer)

    def create_Csds(self, data:dict, remove:bool=True):
        tmp_key = os.path.join(settings.MEDIA_ROOT, "tmp", data.get('key').name)
        path_key = default_storage.save(tmp_key, ContentFile(data.get('key').read()))
        tmp_cer = os.path.join(settings.MEDIA_ROOT, "tmp", data.get('cer').name)
        path_cer = default_storage.save(tmp_cer, ContentFile(data.get('cer').read()))
        self.service.csdsMultiEmisor.upload(
            data.get('rfc'),
            path_key,
            path_cer,
            data.get('password')
        )
        if remove:
            os.remove(tmp_key)
            os.remove(tmp_cer)

    def delete_Csds(self, rfc:str):
        self.service.csdsMultiEmisor.delete(rfc)