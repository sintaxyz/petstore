# Python's Libraries

# DJango's Libraries
from django.urls import path

# Third-party Libraries

# Own's Libraries
from .views import TermsView

app_name = 'ecommerce'

url_patterns = [
    path(
        'terms/',
        TermsView.as_view(),
        name="terms"
    ),
]
