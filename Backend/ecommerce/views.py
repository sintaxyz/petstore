# Python's Libraries

# Django's Libraries
from django.shortcuts import render

# Third-party Libraries

# Own's Libraries
from tools.views import AppView


class TermsView(AppView):
    template_name = "ecommerce/terms/ecommerce_terms.html"
    page_title = "Terminos y Condiciones"
