# Python's Libraries

# Django's Libraries
from django.shortcuts import render
# from django.shortcuts import redirect
from django.urls import reverse_lazy
# from django.views.generic import View
from django.contrib import messages

# Own's Libraries
from tools.views import StxNewView
from tools.views import StxListView
from tools.views import StxEditView

# from tools.utils import Helper
from .filters import BackupFilter

from .forms import EmailSettingForm
from .forms import AlertForm
from .forms import PostForm
from .forms import TicketForm

from .business import BackupBusiness
from .business import EmailBsn
from .business import AlertBusiness
from .business import PostBsn
from .business import TicketBsn

# from .task import backup

from tools.utils import List
from tools.views import StxView


class BackupListView(StxView):
    template_name = 'support/backup/list/backup_list.html'
    page_title = 'Listado de Backups'

    def get(self, request):
        """
        Metodo Get de BackupListView
        """

        if 'from_save' in request.GET:
            messages.success(
                request,
                "La Backup '{}' se guardo exitosamente.".format(
                    request.GET['from_save'].upper()
                )
            )

        url_list = reverse_lazy('support:backup-list')

        records = BackupBusiness.get_All()
        search_value = request.GET.get('search_input', "")
        filters = BackupFilter(request.GET, queryset=records)
        if search_value:
            filtered_records = BackupBusiness.search(search_value)
        else:
            filtered_records = filters.qs

        page = request.GET.get('page', 1)
        paginated_records = List.get_Pagination(filtered_records, page)

        back_url = BackupBusiness.get_UrlBack()
        back_text = "Configuración"

        context = {
            'records': paginated_records,
            'filters': filters,
            'search_value': search_value,
            'url_list': url_list,
            'page_title': self.page_title,
            'back_url': back_url,
            'back_text': back_text
        }
        return render(request, self.template_name, context)


# class BackupRunView(StxView):
#     template_name = 'support/backup/run/template.html'
#     page_title = 'Listado de Backups'

#     def get(self, request):
#         backup.delay()
#         context = {}
#         return render(request, self.template_name, context)


class EmailSettingsView(StxView):
    template_name = "support/email/settings/email_settings.html"
    page_title = "Configuración de Correo Electronico"

    def get(self, request):
        instance = EmailBsn.get_Settings()

        self.back_url = EmailBsn.get_UrlBack()
        self.back_text = "Configuración"

        if instance is None:
            messages.error(
                request,
                "Aún no se cuenta con configuración inicial."
            )
        form = EmailSettingForm(
            instance=instance,
            user=request.user
        )
        context = self.set_Context(
            instance=instance,
            form=form
        )
        return render(request, self.template_name, context)

    def post(self, request):
        instance = EmailBsn.get_Settings()

        form = EmailSettingForm(
            request.POST,
            instance=instance,
            user=request.user
        )

        self.back_url = EmailBsn.get_UrlBack()
        self.back_text = "Configuración"

        if form.is_valid():
            try:
                form.save(request)
            except Exception as error:
                messages.error(
                    request,
                    str(error)
                )

        context = self.set_Context(
            instance=instance,
            form=form
        )
        return render(request, self.template_name, context)


class AlertView(StxView):
    template_name = "support/alert/settings/alert_settings.html"
    page_title = "Configuración de Alertas"

    def get(self, request):
        instance = None
        try:
            instance = AlertBusiness.get_Settings()
        except Exception as error:
            messages.error(
                request,
                str(error)
            )
        form = AlertForm(instance=instance)
        context = self.get_Context(instance=instance, form=form)
        return render(request, self.template_name, context)

    def post(self, request):
        instance = AlertBusiness.get_Settings()
        form = AlertForm(request.POST, instance=instance)
        if form.is_valid():
            data = form.cleaned_data
            AlertBusiness.update_Settings(
                instance,
                data
            )
            messages.success(
                request,
                "La configuración se guardo correctamente."
            )
        context = self.get_Context(instance=instance, form=form)
        return render(request, self.template_name, context)


class AlertStatusView(StxView):
    template_name = "support/alert/status/alert_status.html"
    page_title = "Configuración de Alertas"


class PostListView(StxListView):
    template_name = 'support/post/list/post_list.html'
    business_class = PostBsn


class PostNewView(StxNewView):
    template_name = "support/post/new/post_new.html"
    business_class = PostBsn
    form_class = PostForm


class PostEditView(StxEditView):
    update_record_method = 'update'
    template_name = "support/post/edit/post_edit.html"
    business_class = PostBsn
    form_class = PostForm


class TicketListView(StxListView):
    template_name = 'support/ticket/list/ticket_list.html'
    business_class = TicketBsn


class TicketNewView(StxNewView):
    template_name = "support/ticket/new/ticket_new.html"
    business_class = TicketBsn
    form_class = TicketForm


class TicketEditView(StxEditView):
    update_record_method = 'update'
    template_name = "support/ticket/edit/ticket_edit.html"
    business_class = TicketBsn
    form_class = TicketForm
