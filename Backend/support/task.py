# Python's Libraries
# import os

# Third-party Libraries
from celery import task
from celery.utils.log import get_task_logger
# from celery import shared_task
# from celery.task.schedules import crontab
# from celery.decorators import periodic_task

# Own's Libraries
from .business import BackupBusiness


logger = get_task_logger(__name__)


@task(
    bind=True,
    default_retry_delay=60,
    max_retries=5,
    name="backup"
)
def backup(self):
    logger.info("Iniciando Backup........")
    BackupBusiness.make_Backup()


@task(
    bind=True,
    default_retry_delay=60,
    max_retries=1,
    name="test_ByHour"
)
def test_ByHour(self):
    logger.info("Testing ........")
    print("aun funcionando")
# @shared_task
# def test_programado():
#     logger.info("Esta madre es la mamada")
#     print("prueba...ok")


# @periodic_task(
#     run_every=(
#         crontab(hour='20', minute='50')
#         # crontab(minute='*/1')
#     ),
#     name="la_programada",
#     ignore_result=True
# )
# def la_programada():
#     logger.info("La mamada programada")
#     print("prueba ...programada...ok")
