# Django's Libraries
from django.urls import path

# Own's Libraries
from .views import BackupListView
# from .views import BackupRunView
from .views import EmailSettingsView
from .views import AlertView
from .views import AlertStatusView
from .views import PostListView
from .views import PostNewView
from .views import PostEditView
from .views import TicketListView
from .views import TicketNewView
from .views import TicketEditView


app_name = 'support'

urlpatterns = [
    path(
        "backups/",
        BackupListView.as_view(),
        name="backup-list"
    ),
    path(
        'email/settings/',
        EmailSettingsView.as_view(),
        name="email-settings"
    ),    
    path(
        "alerts/settings/",
        AlertView.as_view(),
        name="alert-settings"
    ),
    path(
        "alerts/settings/status/",
        AlertStatusView.as_view(),
        name="alertstatus-settings"
    ),
    # path(
    #     "backups/run/",
    #     BackupRunView.as_view(),
    #     name="backup_run"
    # ),
    path(
        'posts/',
        PostListView.as_view(),
        name='post-list'
    ),
    path(
        'posts/new/',
        PostNewView.as_view(),
        name='post-new'
    ),
    path(
        'posts/<int:pk>/edit/',
        PostEditView.as_view(),
        name='post-edit'
    ),
    path(
        'tickets/',
        TicketListView.as_view(),
        name='ticket-list'
    ),
    path(
        'tickets/new/',
        TicketNewView.as_view(),
        name='ticket-new'
    ),
    path(
        'tickets/<int:pk>/edit/',
        TicketEditView.as_view(),
        name='ticket-edit'
    ),
]
