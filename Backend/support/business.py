# Python's Libraries
import os
import logging
# Django's Libraries
from django.db import transaction
from django.conf import settings
from django.core.mail import EmailMessage
from django.core.mail import EmailMultiAlternatives
from django.core.mail.backends.smtp import EmailBackend
from django.template import loader
from django.urls import reverse_lazy

# Own's Librarie
from .models import Backup
from .models import Notification
from .models import EmailSetting
from .models import Alert
from .models import Post
from .models import Ticket

from tools.utils import Record
from tools.utils import DbAdmin
from tools.business import StxBsn
from stxtools.filesystem import Folder

from .filters import PostFilter
from .filters import TicketFilter
from .filters import EmailFilter
# from stxtools.filesystem import File
logger = logging.getLogger('error_logger')


class BackupBusiness:

    @classmethod
    def get_UrlBack(self):
        url = reverse_lazy('home:settings')
        return url

    @classmethod
    def get_All(self):
        """
        Obtener todas los Backups.
        """
        try:
            records = Backup.objects.all().order_by('-pk')
        except Backup.DoesNotExist:
            records = None
        return records

    @classmethod
    def search(self, _value):
        try:
            records = Backup.objects.filter(
                created_by__username=_value
            )
        except Backup.DoesNotExist:
            records = None
        return records

    @classmethod
    def make_Backup(self):
        path = os.path.join(
            settings.BASE_DIR,
            'media',
            settings.BACKUP_FOLDER
        )

        folder = Folder(path)
        folder.create(_create_struct=True)

        db_name = settings.DATABASES['default']['NAME']
        user = settings.DATABASES['default']['USER']
        password = settings.DATABASES['default']['PASSWORD']
        file_name = DbAdmin.get_BackupName()
        command = DbAdmin.get_CommandBackup(
            db_name,
            user,
            password,
            path,
            file_name
        )
        try:
            os.popen(command)
            Backup.objects.create(
                name=file_name,
                path=os.path.join('media', settings.BACKUP_FOLDER, file_name)
            )
            print("Respaldo Creado")

        except Exception as inst:
            print(str(inst.args))


class EmailBsn(StxBsn):
    model = EmailSetting
    model_filter = EmailFilter

    @classmethod
    def get_UrlBack(self):
        url = reverse_lazy('home:settings')
        return url

    @classmethod
    def get_Settings(self):
        try:
            email_settings = EmailSetting.objects.get(
                is_active=True
            )
            return email_settings
        except EmailSetting.DoesNotExist:
            # raise NameError("Aun no se tiene una configuración Inicial")
            return None

    @classmethod
    def get_SettingsByUsername(self, _username):
        try:
            email_settings = EmailSetting.objects.get(
                company=None,
                username=_username,
                is_active=True
            )
            return email_settings
        except EmailSetting.DoesNotExist:
            # raise NameError("Aun no se tiene una configuración Inicial")
            return None

    @classmethod
    def create_Settings(self, _data, _user):
        record = EmailSetting.objects.create(
            company=_data.get('company'),
            host=_data.get('host'),
            port=_data.get('port'),
            username=_data.get('username'),
            password=_data.get('password'),
            use_tls=_data.get('use_tls'),
            fail_silently=_data.get('fail_silently'),
            is_active=True,
            created_by=_user
        )
        return record

    @classmethod
    def update_Settings(self, _record, _data, _user):
        _record.host = _data.get('host')
        _record.port = _data.get('port')
        _record.username = _data.get('username')
        _record.password = _data.get('password')
        _record.use_tls = _data.get('use_tls')
        _record.fail_silently = _data.get('fail_silently')
        _record.updated_by = _user
        _record.save()

    @classmethod
    def send(self, settings, to_emails, subject, body, files=None):

        if settings:
            try:
                backend = EmailBackend(
                    host=settings.host,
                    port=settings.port,
                    username=settings.username,
                    password=settings.password,
                    use_tls=settings.use_tls,
                    use_ssl=settings.use_ssl,
                    fail_silently=False
                )
                email = EmailMessage(
                    subject,
                    body,
                    settings.username,
                    to_emails,
                    connection=backend,
                )
                if files:
                    for f in files:
                        email.attach_file(f.path)
                email.send()
                return True

            except Exception as error:
                value = "Emailbsn.send: {}".format(
                    error
                )
                logger.error(value)
                return False

        else:
            value = "Emailbsn.send: {}".format(
                "No se encontro configuración de correo"
            )
            logger.error(value)
            return False

    @classmethod
    def send_Template(self,
                      to_emails,
                      data,
                      subject_template,
                      body_template):

        settings = self.get_Settings()

        if settings:
            backend = EmailBackend(
                host=settings.host,
                port=settings.port,
                username=settings.username,
                password=settings.password,
                use_tls=settings.use_tls,
                fail_silently=settings.fail_silently
            )

            subject = loader.render_to_string(subject_template, data)
            subject = ''.join(subject.splitlines())
            body = loader.render_to_string(body_template, data)

            email_message = EmailMultiAlternatives(
                subject,
                body,
                settings.username,
                to=to_emails,
                connection=backend
            )
            email_message.attach_alternative(body, 'text/html')
            email_message.send()
            return True

        else:
            return False


class AlertBusiness(object):

    @classmethod
    def get_Settings(self):
        try:
            email_settings = Alert.objects.get(
                is_active=True
            )
            return email_settings
        except Alert.DoesNotExist:
            raise NameError("Aun no se tiene una configuración Inicial")

    @classmethod
    def update_Settings(self, _record, _data):
        Record.fill(_record, _data)
        _record.save()


class NotificationBsn(object):

    @classmethod
    def create(self, _record, _description, _url, _execution_date, _task_id):
        app_name = _record._meta.object_name
        model_name = _record._meta.app_label
        record = Notification.objects.create(
            app_name=app_name,
            model_name=model_name,
            object_id=_record.id,
            description=_description,
            url=_url,
            execution_date=_execution_date,
            task_id=_task_id
        )
        return record

    @classmethod
    def get_All(self):
        queryset = Notification.objects.all()
        return queryset


class PostBsn(StxBsn):
    model = Post
    model_filter = PostFilter

    @classmethod
    def get_UrlBack(self):
        url = reverse_lazy('home:settings')
        return url


class TicketBsn(StxBsn):
    model = Ticket
    model_filter = TicketFilter
    key_word = 'user'

    @classmethod
    def get_UrlBack(self):
        url = reverse_lazy('home:settings')
        return url

    @classmethod
    def get_UrlOnSave(self, request, record):
        url = reverse_lazy(
            'support:ticket-list'
        )
        return url

    @classmethod
    def create(self, _data, _user):
        with transaction.atomic():
            record = self.get_ModelObject()()
            record.assigned_to = _data.get('assigned_to')
            record.type = _data.get('type')
            record.description = _data.get('description')
            record.status = _data.get('status')
            record.is_active = _data.get('is_active')
            record.user = _user
            record.created_by = _user
            record.full_clean()
            record.save()
            return record

    @classmethod
    def search(self, records, _value):
        try:
            _value = int(_value)
        except Exception:
            _value

        if type(_value) is str:
            records = records.filter(user__name__icontains=_value)
        elif type(_value) is int:
            records = records.filter(id=_value)
        return records
