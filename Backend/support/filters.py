# Django's Libraries
import django_filters

# Own's Libraries
from .models import Backup
from .models import Post
from .models import Ticket
from .models import EmailSetting


class BackupFilter(django_filters.FilterSet):
    # def __init__(self, *args, **kwargs):
    #     super(BackupFilter, self).__init__(*args, **kwargs)
    #     # self.form.fields['created_date'].widget.attrs['placeholder'] = 'AAAA-MM-DD'

    # created_date = django_filters.DateFilter(lookup_expr='icontains', label='Fecha de Creacion:')

    class Meta:
        model = Backup
        fields = [
            'created_date',
        ]


class PostFilter(django_filters.FilterSet):

    class Meta:
        model = Post
        fields = [
            'type',
        ]


class TicketFilter(django_filters.FilterSet):
    user__email = django_filters.CharFilter(
        label='email',
        lookup_expr='icontains',
    )

    class Meta:
        model = Ticket
        fields = [
            'user__email',
            'type',
            'status',
            'is_active',
        ]


class EmailFilter(django_filters.FilterSet):

    BOOLEAN_CHOICES = (
        ('Si', 'Si'),
        ('No', 'No')
    )

    is_active = django_filters.ChoiceFilter(
        choices=BOOLEAN_CHOICES,
        method='is_ActiveFilter'
    )

    class Meta:
        model = EmailSetting
        fields = [
            'company',
            'is_active',
        ]

    def is_ActiveFilter(self, queryset, name, value):
        if value == 'Si':
            queryset = queryset.filter(is_active=True)
        elif value == 'No':
            queryset = queryset.filter(is_active=False)
        return queryset
