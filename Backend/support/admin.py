from django.contrib import admin

# Own's Libraries
from .models import Backup
from .models import EmailSetting
from .models import Alert
from .models import Post
from .models import Ticket

@admin.register(Backup)
class BackupAdmin(admin.ModelAdmin):
    list_display = (
        'id',
        'path',
        'created_date',
        'created_by',
    )


@admin.register(EmailSetting)
class EmailSettingAdmin(admin.ModelAdmin):
    list_display = (
        'host',
        'port',
        'username',
        'password',
        'use_tls',
        'fail_silently',
        'is_active',
    )


@admin.register(Alert)
class AlertAdmin(admin.ModelAdmin):
    list_display = (
        'days_without_dates',
        'days_without_notes',
        'days_before_date',
        'is_active',
        'created_by',
        'updated_by',
    )


@admin.register(Post)
class PostAdmin(admin.ModelAdmin):
    list_display = (
        'type',
        'short_content',
        'is_active',
    )


@admin.register(Ticket)
class TicketAdmin(admin.ModelAdmin):
    list_display = (
        'user',
        'assigned_to',
        'type',
        'description',
        'status',
        'is_active',
    )
