# Third-party Libraries
from rest_framework import serializers

# Own's Libraries
from .models import Notification


class NotificationSerializer(serializers.ModelSerializer):

    class Meta:
        model = Notification
        fields = ('__all__')
