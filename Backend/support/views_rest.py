# Third-party Libraries
# from rest_framework import viewsets
# from rest_framework import mixins
# from rest_framework import views
# from rest_framework import status
# from rest_framework.response import Response
# from django_filters.rest_framework import DjangoFilterBackend

# Own's Libraries
# from .models import AlertStatus
from .business import NotificationBsn
from .serializers import NotificationSerializer
from tools.views_rest import StxAPIList


class NotificationAPIListView(StxAPIList):
    serializer_class = NotificationSerializer
    # filter_class = NotificationSearchFilter
    business_class = NotificationBsn


# class AlertStatussAPIListView(viewsets.GenericViewSet,
#                               mixins.ListModelMixin,
#                               mixins.CreateModelMixin):
#     queryset = AlertStatus.objects.all().order_by("created_date")
#     serializer_class = AlertStatusSerializer
#     filter_backends = (DjangoFilterBackend, )
#     filter_class = AlertStatusFilter

#     def create(self, request, *args, **kwargs):
#         serializer = self.get_serializer(data=request.data)
#         if serializer.is_valid(raise_exception=False):
#             data_list = serializer.validated_data
#             record = AlertBusiness.add(data_list, request.user)
#             serializer = self.get_serializer(record)
#             return Response(
#                 serializer.data,
#                 status=status.HTTP_201_CREATED
#             )
#         else:
#             return Response(
#                 serializer.errors,
#                 status=status.HTTP_400_BAD_REQUEST
#             )


# class AlertStatussAPIDetailView(viewsets.GenericViewSet,
#                                 mixins.RetrieveModelMixin,
#                                 mixins.UpdateModelMixin):
#     queryset = AlertStatus.objects.all()
#     serializer_class = AlertStatusFilter
