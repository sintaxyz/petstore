# Django's Libraries
from django.urls import path

# Own's Libraries
# from .views_rest import AlertStatusAPIListView
# from .views_rest import AlertStatusAPIDetailView
from .views_rest import NotificationAPIListView

app_name = 'support'


urlpatterns = [
    # path(
    #     'alerts/status/',
    #     AlertStatusAPIListView.as_view({
    #         'get': 'list',
    #         'post': 'create',
    #     }),
    #     name="api-alertstatus-list"
    # ),
    # path(
    #     'alerts/status/<int:pk>/',
    #     AlertStatusAPIDetailView.as_view({
    #         'get': 'retrieve',
    #         'put': 'update',
    #         'patch': 'partial_update',
    #     }),
    #     name="api-alertstatus-detail"
    # ),
    path(
        'notifications/',
        NotificationAPIListView.as_view({
            'get': 'list'
        }),
        name="api-notifications-list"
    )
]
