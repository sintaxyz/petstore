# Django's Libraries
from django.forms import ModelForm
from django.contrib import messages
from tools.forms import StxModelCreateForm

# Own's Libraries
from .models import EmailSetting
from .models import Alert
from .models import Post
from .models import Ticket

from security.models import Company

from .business import EmailBsn
from .business import PostBsn
from .business import TicketBsn


class EmailSettingForm(ModelForm):
    class Meta:
        model = EmailSetting
        fields = [
            'host',
            'port',
            'username',
            'password',
            'use_tls',
            'fail_silently',
            # 'status',
        ]

    def __init__(self, *args, **kwargs):
        self.user = kwargs.pop('user')
        super(EmailSettingForm, self).__init__(*args, **kwargs)    


class EmailCompanyForm(StxModelCreateForm):
    class Meta:
        model = EmailSetting
        fields = [
            'company',
            'host',
            'port',
            'username',
            'password',
            'use_tls',            
        ]


class AlertForm(ModelForm):
    class Meta:
        model = Alert
        fields = [
            'days_without_dates',
            'days_without_notes',
            'days_before_date',
        ]


class PostForm(StxModelCreateForm):
    business_class = PostBsn

    class Meta:
        model = Post
        fields = [
            'type',
            'content',
            'is_active',
        ]


class TicketForm(StxModelCreateForm):
    business_class = TicketBsn

    class Meta:
        model = Ticket
        fields = [
            'type',
            'assigned_to',
            'description',
            'status',
            'is_active',
        ]
