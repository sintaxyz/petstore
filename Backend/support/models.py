
# Django's Libraries
from django.db import models
from django.db import transaction
from django.urls import reverse_lazy
from django.template.defaultfilters import truncatechars

# Own's Libraries
from security.models import User
from tools.validators import MinValueValidator
from tools.models import AppModel
from security.models import Company
# from tools.utils import FileAdmin


class Backup(models.Model):
    name = models.CharField(
        verbose_name="nombre",
        max_length=144,
    )
    path = models.TextField(
        verbose_name="ruta",
        max_length=500,
        help_text='Ruta de donde se encuentra el archivo.',
        null=True,
        blank=True,
    )
    created_by = models.ForeignKey(
        User,
        verbose_name="creado por",
        related_name='uploaded_files',
        on_delete=models.PROTECT,
        null=True,
        blank=True,
    )
    created_date = models.DateTimeField(
        verbose_name="fecha registro",
        auto_now=False,
        auto_now_add=True,
    )

    class Meta:
        verbose_name = 'respaldo'
        verbose_name_plural = 'respaldos'

    @property
    def link(self):
        # current_site = Site.objects.get_current()
        # path = current_site.domain +
        print(self.path)
        return self.path


class EmailSetting(models.Model):
    company = models.OneToOneField(
        Company,
        verbose_name='compañía',
        related_name='email_settings',
        on_delete=models.PROTECT,
        blank=True,
        null=True,
    )
    host = models.CharField(
        'host',
        max_length=150,
        null=True,
        blank=True,
    )
    port = models.IntegerField(
        'puerto',
        null=True,
        blank=True,
        validators=[MinValueValidator(1)]
    )
    username = models.CharField(
        'usuario',
        max_length=150,
        null=True,
        blank=True,
    )
    password = models.CharField(
        'contraseña',
        max_length=150,
        null=True,
        blank=True,
    )
    use_tls = models.BooleanField(
        'TLS',
        default=False,
    )
    use_ssl = models.BooleanField(
        'SSL',
        default=False,
    )
    fail_silently = models.BooleanField(
        'errores en silencio',
        default=True,
    )
    is_active = models.BooleanField(
        'activo',
        default=True,
        unique=True
    )
    created_date = models.DateTimeField(
        "Fecha registro",
        auto_now=False,
        auto_now_add=True,
    )
    updated_date = models.DateTimeField(
        "Fecha actualización",
        auto_now=True,
        auto_now_add=False,
    )
    created_by = models.ForeignKey(
        User,
        verbose_name="Creado por",
        related_name='email_created',
        on_delete=models.PROTECT,
        blank=True,
        null=True,
    )
    updated_by = models.ForeignKey(
        User,
        verbose_name="Modificado por",
        related_name='email_updated',
        on_delete=models.PROTECT,
        blank=True,
        null=True
    )

    class Meta:
        verbose_name = 'configuracion de correo'
        verbose_name_plural = 'configuraciones de correo'    


class Alert(AppModel):
    days_without_dates = models.IntegerField(
        'Dias sin Citas',
        null=True,
        blank=True,
        validators=[MinValueValidator(1)],
        help_text="Al llegar a este limite de dias se emitira la alerta."
    )
    days_without_notes = models.IntegerField(
        'Dias sin Notas',
        null=True,
        blank=True,
        validators=[MinValueValidator(1)],
        help_text="Al llegar a este limite de dias se emitira la alerta."
    )
    days_before_date = models.IntegerField(
        'Dias antes de la Cita',
        null=True,
        blank=True,
        validators=[MinValueValidator(1)],
        help_text="No. de dias antes de la Cita para emitir la alerta."
    )
    is_active = models.BooleanField(
        'activo',
        default=False,
        unique=True
    )

    class Meta:
        verbose_name = 'alerta'
        verbose_name_plural = 'alertas'


class Notification(AppModel):

    app_name = models.CharField(
        verbose_name="nombre de aplicacion",
        max_length=144,
    )
    model_name = models.CharField(
        verbose_name="nombre de modelo",
        max_length=144,
    )
    object_id = models.PositiveIntegerField(
        verbose_name='id de modelo'
    )
    description = models.CharField(
        verbose_name="descripcion",
        max_length=144,
    )
    url = models.CharField(
        verbose_name="url",
        max_length=144,
    )
    is_viewed = models.BooleanField(
        default=False
    )
    is_revoked = models.BooleanField(
        default=False
    )
    execution_date = models.DateTimeField(
        verbose_name='Fecha de ejecucion',
        null=True,
        blank=True
    )
    task_id = models.CharField(
        verbose_name="nombre de modelo",
        max_length=144,
    )
    created_by = models.ForeignKey(
        User,
        verbose_name="Creado por",
        related_name='notification_created',
        on_delete=models.PROTECT,
        blank=True,
        null=True,
    )
    updated_by = models.ForeignKey(
        User,
        verbose_name="Modificado por",
        related_name='notification_updated',
        on_delete=models.PROTECT,
        blank=True,
        null=True
    )

    class Meta:
        verbose_name = 'Notificacion'
        verbose_name_plural = 'Notificaciones'


class Post(AppModel):
    TYPE_CHOICES = (
        ('AL', 'Aviso Legal'),
        ('PP', 'Política de Privacidad'),
        ('PC', 'Política de Cookies'),
        ('CC', 'Condiciones de Contratación'),
    )

    type = models.CharField(
        verbose_name="tipo",
        max_length=2,
        choices=TYPE_CHOICES,
        blank=False,
        null=True,
    )
    content = models.TextField(
        verbose_name="contenido",
        null=True,
        blank=True,
    )
    is_active = models.BooleanField(
        verbose_name='activo',
        default=False,
    )

    class Meta:
        verbose_name = 'Documento Legal'
        verbose_name_plural = 'Documentos Legales'

    @property
    def status_text(self):
        if self.is_active:
            return "Activo"
        else:
            return "Inactivo"

    @property
    def status_color(self):
        if self.is_active:
            return "green"
        else:
            return "gray"

    def save(self, *args, **kwargs):
        if self.is_active:
            with transaction.atomic():
                Post.objects.filter(
                    is_active=True, type=self.type
                ).update(is_active=False)
                return super(Post, self).save(*args, **kwargs)
        else:
            return super(Post, self).save(*args, **kwargs)

    @property
    def short_content(self):
        return truncatechars(self.content, 100)

    @property
    def url_edit(self):
        url = reverse_lazy(
            'support:post-edit',
            kwargs={'pk': self.pk}
        )
        return url

    def __str__(self):
        return self.type


class Ticket(AppModel):
    TYPE_CHOICES = [
        ('PCS', 'Problemas con el servicio'),
        ('DYQ', 'Dudas'),
    ]
    STATUS_CHOICES = [
        ('SNR', 'Sin revisar'),
        ('PND', 'Pendiente'),
        ('SLC', 'Solucionado'),
    ]
    user = models.ForeignKey(
        User,
        verbose_name='usuario',
        related_name='has_tickets',
        on_delete=models.PROTECT,
        blank=False,
        null=True,
    )
    assigned_to = models.ForeignKey(
        User,
        verbose_name='asignado a',
        related_name='assigned_tickets',
        on_delete=models.PROTECT,
        blank=False,
        null=True,
    )
    type = models.CharField(
        verbose_name='tipo',
        max_length=3,
        choices=TYPE_CHOICES,
        default='PCS',
    )
    description = models.TextField(
        verbose_name='descripción',
        blank=True,
        null=True,
    )
    status = models.CharField(
        verbose_name='estado',
        max_length=3,
        choices=STATUS_CHOICES,
        default='SNR',
    )
    is_active = models.BooleanField(
        verbose_name='Activo',
        blank=False,
        null=False,
        default=True,
    )

    class Meta:
        verbose_name = 'Ticket'
        verbose_name_plural = 'Tickets'

    @property
    def status_text(self):
        if self.is_active:
            return "Activo"
        else:
            return "Inactivo"

    @property
    def status_color(self):
        if self.is_active:
            return "green"
        else:
            return "gray"

    @property
    def url_edit(self):
        url = reverse_lazy(
            'support:ticket-edit',
            kwargs={'pk': self.pk}
        )
        return url

    def __str__(self):
        return str(self.user.name)
