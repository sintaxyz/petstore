# Python's Libraries
from datetime import datetime

# Django's Libraries
from django.contrib.auth import authenticate
from django.contrib.auth import login
from django.urls import reverse_lazy

# Own's Libraries
from security.business import UserBsn


class HomeBsn(object):

    @classmethod
    def get_UrlStart(self, user):
        url = reverse_lazy('home:dashboard')
        return url

    @classmethod
    def login(self, request, email, password):
        user = authenticate(
            request,
            email=email,
            password=password
        )
        if user is not None:
            login(request, user)
            return user

        else:
            raise NameError(
                "Cuenta de usuario o contraseña no valida"
            )

    @classmethod
    def change_Password(self, _record, _data, activation=False):

        if "password1" in _data:
            _record.set_password(_data.get("password1"))

        if "new_password1" in _data:
            _record.set_password(_data.get("new_password1"))

        if "reset_password" in _data:
            _record.reset_password = _data.get("reset_password")
        else:
            _record.reset_password = False

        if activation:
            _record.is_active = True

        _record.save()

    @classmethod
    def activate(self, _record):
        _record.date_activated = datetime.now()
        _record.save()
        employee = _record.employee_profile
        employee.is_active = True
        employee.save()
