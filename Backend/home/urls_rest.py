# Django's Libraries
from django.urls import path
from django.views.generic import TemplateView

# Third-party Libraries
from rest_framework.schemas import get_schema_view

app_name = 'home-api'

urlpatterns = [
    path(
        'openapi/',
        get_schema_view(
            title="CRM",
            description="API's of modules"
            # version="1.0.0"
        ),
        name='openapi-schema'
    ),
    path(
        'docs/',
        TemplateView.as_view(
            template_name='home/redoc/redoc.html',
            extra_context={'schema_url': 'home-api:openapi-schema'}
        ),
        name='redoc'
    ),
]
