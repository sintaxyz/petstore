# Django's Libraries
from django.shortcuts import render
from django.shortcuts import redirect
from django.conf import settings
from django.urls import reverse
from django.urls import reverse_lazy
from django.contrib import messages
from django.contrib.auth.views import PasswordResetConfirmView
from django.contrib.auth.views import PasswordResetCompleteView

# Own's Libraries
from security.business import UserBsn

from .forms import LoginForm
from .forms import StartForm
from .forms import ProfilePasswordForm
from .forms import PasswordForgotForm
from .forms import PasswordResetForm
from .forms import ActivationConfirmForm

from tools.views import AppView
from tools.views import StxView
from tools.views import StxRetrieveView
from tools.views import StxEditView


class LoginView(AppView):
    template_name = "home/login/home_login.html"
    page_title = "Login"

    def get(self, request):
        form = LoginForm()
        url = form.check_Auth(request)
        if url:
            return redirect(url)
        context = self.set_Context(form=form)
        return render(request, self.template_name, context)

    def post(self, request):
        form = LoginForm(request.POST)
        if form.is_valid():
            try:
                url = form.login(request)
                return redirect(url)
            except Exception as error:
                messages.error(
                    request,
                    str(error)
                )

        context = self.set_Context(form=form)
        return render(request, self.template_name, context)


class DashboardView(StxView):
    template_name = "home/dashboard/home_dashboard.html"
    page_title = "Bienvenido"


class TermsView(StxView):
    template_name = "home/terms/terms.html"
    page_title = "Terminos y Condiciones"


class QuestionsView(StxView):
    template_name = "home/questions/questions.html"
    page_title = "Preguntas Frecuentes"


class PrivacityView(StxView):
    template_name = "home/privacity/privacity.html"
    page_title = "Aviso de Privacidad"
    

class StartView(StxEditView):
    template_name = "home/start/home_start.html"
    page_title = "Configurar tu cuenta"
    business_class = UserBsn
    form_class = StartForm


class SettingsView(StxView):
    template_name = "home/settings/home_settings.html"
    page_title = "Configuraciones"


class ProfileDetailView(StxRetrieveView):
    template_name = "home/profile/detail/profile_detail.html"
    page_title = "Perfil del usuario"
    business_class = UserBsn


class ProfilePasswordView(StxEditView):
    template_name = "home/profile/password/profile_password.html"
    page_title = "Perfil del usuario"
    business_class = UserBsn
    form_class = ProfilePasswordForm


class PasswordForgotView(AppView):
    template_name = "home/password/forgot/password_forgot.html"
    page_title = "{} - Restablecer Contraseña".format(
        settings.APP_NAME
    )

    def get(self, request):
        form = PasswordForgotForm()
        context = self.set_Context(form=form)
        return render(request, self.template_name, context)

    def post(self, request):
        form = PasswordForgotForm(request.POST)
        if form.is_valid():
            try:
                form.save(
                    use_https=request.is_secure(),
                    request=request
                )
                return redirect(reverse(
                    'home:password-request'
                ))
            except Exception as error:
                messages.error(
                    request,
                    str(error)
                )

        context = {
            'form': form
        }
        return render(request, self.template_name, context)


class PasswordRequestView(AppView):
    template_name = "home/password/request/password_request.html"

    def get(self, request):
        return render(request, self.template_name, {})


class PasswordResetView(PasswordResetConfirmView):
    form_class = PasswordResetForm
    template_name = 'home/password/reset/password_reset.html'
    success_url = reverse_lazy('home:password-done')


class PasswordDoneView(PasswordResetCompleteView):
    template_name = 'home/password/done/password_done.html'


class ActivationConfirmView(PasswordResetConfirmView):
    form_class = ActivationConfirmForm
    template_name = "home/activation/confirm/activation_confirm.html"
    success_url = reverse_lazy('home:activation-done')


class ActivationDoneView(PasswordResetCompleteView):
    template_name = "home/activation/done/activation_done.html"

