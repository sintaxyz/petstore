# Django's Libraries
from django.forms import Form
from django.forms import TextInput
from django.forms import CharField
from django.forms import EmailInput
from django.forms import PasswordInput

from django.contrib.auth import update_session_auth_hash
from django.contrib.auth.tokens import default_token_generator
from django.contrib.auth.forms import AdminPasswordChangeForm
from django.contrib.auth.forms import PasswordResetForm
from django.contrib.sites.shortcuts import get_current_site

from django.utils.http import urlsafe_base64_encode
from django.utils.safestring import mark_safe
from django.utils.encoding import force_bytes

from django.shortcuts import redirect
from django.core.exceptions import ValidationError
from django.conf import settings

# Own's Libraries
from tools.utils import Helper

from .business import HomeBsn
from support.business import EmailBsn


class LoginForm(Form):
    error_css_class = 'login-content-field--error'

    email = CharField(
        widget=EmailInput(attrs={
            'class': 'login-content-field',
            'placeholder': 'Correo Electronico'
        })
    )
    password = CharField(
        widget=PasswordInput(attrs={
            'class': 'login-content-field',
            'placeholder': 'Contraseña'
        })
    )

    def check_Auth(self, request):
        if request.user.is_authenticated:
            url = HomeBsn.get_UrlStart(
                request.user
            )
            return url

        return None

    def login(self, request):
        data = self.cleaned_data
        user = HomeBsn.login(
            request,
            data.get('email'),
            data.get('password')
        )
        url = HomeBsn.get_UrlStart(user)
        return url


class StartForm(AdminPasswordChangeForm):
    password1 = CharField(
        label='Nueva contraseña',
        help_text=mark_safe(
            '<ul><li>La contraseña no puede ser similar a'
            'su otra información personal.</li>'
            '<li>La contraseña debe contener al menos 8 caracteres.</li>'
            '<li>La contraseña no puede ser una contraseña común.</li>'
            '<li>La contraseña no puede ser enteramente numérica.</li></ul>'),
        widget=PasswordInput()
    )
    password2 = CharField(
        label='Confirmar contraseña',
        help_text="Para verificar, introduzca la misma "
                  "contraseña que introdujo antes.",
        widget=PasswordInput()
    )

    def __init__(self, *args, **kwargs):
        self.user = kwargs.pop('user')
        self.instance = kwargs.pop('instance')
        super(StartForm, self).__init__(self.user, *args, **kwargs)

    def save(self, request):
        data = self.cleaned_data
        HomeBsn.change_Password(
            self.user,
            data
        )
        update_session_auth_hash(request, self.user)
        url = Helper.get_UrlWithQueryString(
            self.user.url_profile,
            from_save="change_password"
        )
        return url


class ProfilePasswordForm(AdminPasswordChangeForm):
    password1 = CharField(
        label='Nueva contraseña',
        help_text=mark_safe(
            '<ul><li>La contraseña no puede ser similar a'
            'su otra información personal.</li>'
            '<li>La contraseña debe contener al menos 8 caracteres.</li>'
            '<li>La contraseña no puede ser una contraseña común.</li>'
            '<li>La contraseña no puede ser enteramente numérica.</li></ul>'),
        widget=PasswordInput()
    )
    password2 = CharField(
        label='Confirmar contraseña',
        help_text="Para verificar, introduzca la misma "
                  "contraseña que introdujo antes.",
        widget=PasswordInput()
    )

    def __init__(self, *args, **kwargs):
        self.user = kwargs.pop('user')
        self.instance = kwargs.pop('instance')
        super(ProfilePasswordForm, self).__init__(self.user, *args, **kwargs)

    def save(self, request):
        data = self.cleaned_data
        HomeBsn.change_Password(
            self.user,
            data
        )
        update_session_auth_hash(request, self.user)
        url = Helper.get_UrlWithQueryString(
            self.user.url_profile,
            from_save="change_password"
        )
        return url


class PasswordForgotForm(PasswordResetForm):

    email = CharField(
        label='',
        widget=EmailInput()
    )

    def clean_email(self):
        email = self.cleaned_data["email"]

        try:
            active_user = HomeBsn.get_ByMail(email)

        except Exception:
            raise ValidationError(
                "No se encontro un Usuario con el correo {}".format(
                    email
                )
            )

        if active_user.has_usable_password() is False:
            raise ValidationError(
                "El Usuario {} "
                "no tiene una contraseña valida".format(email)
            )
        return active_user

    def save(self, domain_override=None,
             subject_template='home/password/email/password_email_subject.txt',
             body_template='home/password/email/password_email_body.html',
             use_https=False, token_generator=default_token_generator,
             from_email=None, request=None, html_email_template_name=None,
             extra_email_context=None):

        user = self.cleaned_data["email"]

        email = user.email
        if not domain_override:
            current_site = get_current_site(request)
            site_name = current_site.name
            domain = current_site.domain
        else:
            site_name = domain = domain_override

        uid = urlsafe_base64_encode(force_bytes(user.pk))

        data = {
            'app': settings.APP_NAME,
            'email': email,
            'domain': domain,
            'site_name': site_name,
            'uid': uid,
            'user': user,
            'token': token_generator.make_token(user),
            'protocol': 'https' if use_https else 'http',
        }
        if extra_email_context is not None:
            data.update(extra_email_context)

        to_emails = [email]

        response = EmailBsn.send_Template(
            to_emails,
            data,
            subject_template,
            body_template
        )

        if response is False:
            raise NameError("Ocurrio un problema al enviar el correo, "
                            "favor de informar al administrador")

        return user


class PasswordResetForm(AdminPasswordChangeForm):
    password1 = CharField(
        label='Nueva contraseña',
        widget=PasswordInput(
            attrs={'class': 'form-control'}
        )
    )
    password2 = CharField(
        label='Confirmar contraseña',
        widget=PasswordInput(
            attrs={'class': 'form-control'}
        )
    )


class ActivationConfirmForm(AdminPasswordChangeForm):

    password1 = CharField(
        label='Nueva contraseña',
        help_text=mark_safe(
            '<ul><li>La contraseña no puede ser similar a '
            'su otra información personal.</li>'
            '<li>La contraseña debe contener al menos 8 caracteres.</li>'
            '<li>La contraseña no puede ser una contraseña común.</li>'
            '<li>La contraseña no puede ser enteramente numérica.</li></ul>'),
        widget=PasswordInput()
    )
    password2 = CharField(
        label='Confirmar contraseña',
        help_text="Para verificar, introduzca la misma "
                  "contraseña que introdujo antes.",
        widget=PasswordInput()
    )

    def save(self):
        data = self.cleaned_data
        HomeBsn.change_Password(
            self.user,
            data,
            True
        )
        HomeBsn.activate(self.user)
        return self.user
