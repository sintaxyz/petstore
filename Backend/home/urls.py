# Django's Libraries
from django.urls import path
from django.urls import re_path
from django.contrib.auth.views import LogoutView

# Own's Libraries
from .views import ActivationConfirmView
from .views import ActivationDoneView
from .views import DashboardView
from .views import LoginView
from .views import StartView
from .views import ProfileDetailView
from .views import ProfilePasswordView
from .views import PasswordForgotView
from .views import PasswordRequestView
from .views import PasswordResetView
from .views import PasswordDoneView
from .views import ProfileDetailView
from .views import ProfilePasswordView
from .views import SettingsView
from .views import TermsView
from .views import QuestionsView
from .views import PrivacityView

app_name = 'home'

urlpatterns = [
    path(
        '',
        LoginView.as_view(),
        name="login"
    ),
    path(
        'terms/',
        TermsView.as_view(),
        name="terms"
    ),
    path(
        'questions/',
        QuestionsView.as_view(),
        name="questions"
    ),
    path(
        'privacity/',
        PrivacityView.as_view(),
        name="privacity"),
    path(
        'dashboard/',
        DashboardView.as_view(),
        name="dashboard"
    ),
    path(
        'logout/',
        LogoutView.as_view(),
        name="logout"
    ),
    path(
        'start/',
        StartView.as_view(),
        name="start"
    ),
    path(
        'settings/',
        SettingsView.as_view(),
        name="settings"
    ),
    path(
        'profile/<int:pk>/detail/',
        ProfileDetailView.as_view(),
        name="profile-detail"
    ),
    path(
        'profile/<int:pk>/password/',
        ProfilePasswordView.as_view(),
        name="profile-password"
    ),
    path(
        'password/forgot/',
        PasswordForgotView.as_view(),
        name="password-forgot"
    ),
    path(
        'password/request/',
        PasswordRequestView.as_view(),
        name="password-request"
    ),
    re_path(
        r'password/reset/(?P<uidb64>[0-9A-Za-z_\-]+)/(?P<token>[0-9A-Za-z]{1,13}-[0-9A-Za-z]{1,20})/$',
        PasswordResetView.as_view(),
        name='password-reset'
    ),
    path(
        'password/done/',
        PasswordDoneView.as_view(),
        name='password-done'
    ),
    re_path(
        r'activation/(?P<uidb64>[0-9A-Za-z_\-]+)/(?P<token>[0-9A-Za-z]{1,13}-[0-9A-Za-z]{1,20})/$',
        ActivationConfirmView.as_view(),
        name='activation-confirm'
    ),
    path(
        'activation/done/',
        ActivationDoneView.as_view(),
        name='activation-done'
    )
]
