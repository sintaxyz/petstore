Django==2.2.8
django-cors-headers
django-debug-toolbar
django-filter
Pillow
python-dateutil
Unipath
django-simple-history
django-widget-tweaks
djangorestframework
psycopg2-binary
djangorestframework-simplejwt
XlsxWriter
xhtml2pdf
stx-tools
uritemplate.py
pyyaml
django-jet

# pip install -e git://github.com/Facturama/facturama-python-sdk.git@master#egg=facturama
